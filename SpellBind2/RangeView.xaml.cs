﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using ArzooSoftware.SpellBinder.spellbinder;
using System.ComponentModel;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <summary>
    /// Interaction logic for DurationView.xaml
    /// </summary>
    public partial class RangeView : Window, IPropertyView
    {
        public RangeView()
        {
            InitializeComponent();
            int mageLevel = Properties.Settings.Default.MageLevel;
            int mageIntelligence = Properties.Settings.Default.MageIntelligence;
            _casterViewModel = new BookViewModel(new Mage(mageLevel, mageIntelligence), spellLstVw);
            _propertyView = new PropertyView(spellLstVw, _casterViewModel, this);
        }

        /// <summary>
        /// Holds caster info for BookView
        /// </summary>
        BookViewModel _casterViewModel;

        PropertyView _propertyView;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Create list of spells will all possible durations
            List<Cast> spell_list = _propertyView.GetSpellList().ToList();
            spellLstVw.ItemsSource = spell_list;

            // Set group levels
            CollectionView cv = (CollectionView)CollectionViewSource.GetDefaultView(spellLstVw.ItemsSource);
            PropertyGroupDescription pgd = new PropertyGroupDescription("RangePropertyValue");
            pgd.Converter = new GroupRangeConverter();
            cv.GroupDescriptions.Add(pgd);

            pgd = new PropertyGroupDescription("Category");
            pgd.Converter = new GroupCategoryConverter();
            cv.GroupDescriptions.Add(pgd);

            SortDescription sort_description = new SortDescription("RangePropertyValue", ListSortDirection.Descending);
            cv.SortDescriptions.Add(sort_description);

            sort_description = new SortDescription("Category", ListSortDirection.Ascending);
            cv.SortDescriptions.Add(sort_description);

            sort_description = new SortDescription("Cost", ListSortDirection.Ascending);
            cv.SortDescriptions.Add(sort_description);

            cv.Filter = null;

        }

        /// <summary>
        /// Display double-clicked spell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void spellLstVw_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _propertyView.spellLstVw_MouseDoubleClick(sender, e);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (spellLstVw != null)
            {
                _propertyView.ComboBox_SelectionChanged(sender, e);
            }

            return;
        }

        #region IPropertyView Members

        public bool AtMaxValue(Cast cast)
        {
            return cast.RangePropertyValue.CompareTo(new NumericValue(1, "mile")) >= 0;
        }

        public IModifier GetPropertyModifier(ISpell spell)
        {
            IModifier ret = null;
            if (spell.RangeProperty.Value is NumericValue)
                ret = new RangeModifier(spell.RangeProperty.Value);
            return ret;
        }

        #endregion
    }

    /// <summary>
    /// Converter class to Group based on duration
    /// </summary>

    public class GroupRangeConverter : IValueConverter
    {

        public object Convert(object value, System.Type targetType,
                              object parameter,
                              System.Globalization.CultureInfo culture)
        {
            string ret = "Described";
            if (!(value is DescribedValue))
            {
                ret = "Misc";
                if (value is NumericValue)
                    ret = value.ToString();
            }

            return ret;
        }

        public object ConvertBack(object value, System.Type targetType,
                                  object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }

    /// <summary>
    /// Converter class to display Duration Group label
    /// </summary>

    public class GroupLabelRangeConverter : IValueConverter
    {

        public object Convert(object value, System.Type targetType,
                              object parameter,
                              System.Globalization.CultureInfo culture)
        {
            string ret = "???";
            if (value is string)
            {
                ret = value as string;
                if (ret == "Described")
                    ret = "Lasting";                
            }

            return ret;
        }

        public object ConvertBack(object value, System.Type targetType,
                                  object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }

    ///// <summary>
    ///// Converter class to group based on spell category
    ///// </summary>

    //public class GroupCategoryConverter : IValueConverter
    //{

    //    public object Convert(object value, System.Type targetType,
    //                          object parameter,
    //                          System.Globalization.CultureInfo culture)
    //    {
    //        string ret = value.ToString();

    //        return ret;
    //    }

    //    public object ConvertBack(object value, System.Type targetType,
    //                              object parameter,
    //                              System.Globalization.CultureInfo culture)
    //    {
    //        throw new System.NotImplementedException();
    //    }
    //}

    ///// <summary>
    ///// Converter class to display spell category label
    ///// </summary>

    //public class GroupLabelCategoryConverter : IValueConverter
    //{

    //    public object Convert(object value, System.Type targetType,
    //                          object parameter,
    //                          System.Globalization.CultureInfo culture)
    //    {
    //        string ret = value as string;

    //        return ret;
    //    }

    //    public object ConvertBack(object value, System.Type targetType,
    //                              object parameter,
    //                              System.Globalization.CultureInfo culture)
    //    {
    //        throw new System.NotImplementedException();
    //    }
    //}

    /// <summary>
    /// Converter class to display spell category label
    /// </summary>

    public class ExpandedRangeConverter : IValueConverter
    {

        public object Convert(object value, System.Type targetType,
                              object parameter,
                              System.Globalization.CultureInfo culture)
        {
            bool ret = false;
            string str_val = value as string;
            if (str_val != null)
            {
                if (str_val == "Described")
                    ret = true;
                else if (str_val == "1 day")
                    ret = true;
                else if (str_val == "12 hours")
                    ret = true;
            }

            return ret;
        }

        public object ConvertBack(object value, System.Type targetType,
                                  object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}
