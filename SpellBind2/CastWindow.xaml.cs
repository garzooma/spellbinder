﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ArzooSoftware.SpellBinder.spellbinder;
using System.Text.RegularExpressions;
using System.Xml;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <summary>
    /// Interaction logic for CastWindow.xaml
    /// </summary>
    public partial class CastWindow : Window
    {
        private Cast _spellCast;
        private Mage _mage;
        private CastViewModel _viewModel;

        public CastWindow(ISpell spell, Mage mage)
        {
            InitializeComponent();

            _spellCast = new Cast(spell, mage);
            _mage = mage;
            _viewModel = new CastViewModel(_spellCast);
        }

        public CastWindow(Cast cast)
        {
            InitializeComponent();

            _spellCast = cast;
            _mage = cast.Mage;
            _viewModel = new CastViewModel(_spellCast);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CastWindow window = sender as CastWindow;
            window.Title = _spellCast.Spell.Name;

            Binding lvl_bind = new Binding();
            lvl_bind.Source = _viewModel;
            lvl_bind.Path = new PropertyPath("Level");
            lvl_bind.Mode = BindingMode.OneWay;

            lvlTxtBx.SetBinding(TextBox.TextProperty, lvl_bind);

            Binding cost_bind = new Binding();
            cost_bind.Source = _viewModel;
            cost_bind.Path = new PropertyPath("SpellCost");
            cost_bind.Mode = BindingMode.OneWay;

            costTxtBx.SetBinding(TextBox.TextProperty, cost_bind);

            // See if we need Modifiers Description tab
            if (String.IsNullOrEmpty(_spellCast.Spell.ModifiersDescription))
            {
                tabControl1.Items.Remove(modDescrTabItem);
            }
            else
            {
                Binding bind = new Binding();
                bind.Source = _spellCast;
                bind.Path = new PropertyPath("Spell.ModifiersDescription");
                bind.Mode = BindingMode.OneWay;
                modDescrTxtBx.SetBinding(TextBox.TextProperty, bind);
            }

            // See if we need Morphic tab
            if (!_spellCast.Spell.IsMorphic)
            {
                tabControl1.Items.Remove(morphTabItem);
            }
            else
            {
                // Put in dynamic controls
                AddMorphicControls(morphGrid);
            }

            // See if we need Damage tab
            //VMBinding vm_bind;
            if (_spellCast.Damage.Value == 0)
            {
                tabControl1.Items.Remove(damageTabItem);
            }
            else
            {
                // Damage
                //this.dmgIUpDown.ValueChanged += _viewModel.NumUpDn_ValueChanged;
                //vm_bind = new VMBinding(_spellCast, _spellCast.Spell.DamageProperty);
                //vm_bind.BoundProperties.Add("DamageNumber");   // name of property for duration text box
                //vm_bind.BoundProperties.Add("DamageExpectation");   // name of property for duration text box
                //vm_bind.BoundProperties.Add("DamageMin");   // name of property for duration text box
                //vm_bind.BoundProperties.Add("DamageMax");   // name of property for duration text box
                //dmgIUpDown.Tag = vm_bind;
            }
 
            // See if we need Special Properties tab
            if (_spellCast.Spell.ExtraPropertiesList.Count == 0)
            {
                tabControl1.Items.Remove(specTabItem);
            }

            CastViewGrid.DataContext = _viewModel;

            // Standard properties
            //this.izoeupdn.ValueChanged += _viewModel.NumUpDn_ValueChanged;
            //vm_bind = new VMBinding(_spellCast, _spellCast.Spell.ZOEProperty);
            //vm_bind.BoundProperties.Add("SpellZOE");   // name of property for duration text box
            //izoeupdn.Tag = vm_bind;

            //iDurUpDown.ValueChanged += _viewModel.NumUpDn_ValueChanged;
            //vm_bind = new VMBinding(_spellCast, _spellCast.Spell.DurationProperty);
            //vm_bind.BoundProperties.Add("SpellDuration");   // name of property for duration text box
            //iDurUpDown.Tag = vm_bind;

            //this.irngupdn.ValueChanged += _viewModel.NumUpDn_ValueChanged;
            //vm_bind = new VMBinding(_spellCast, _spellCast.Spell.RangeProperty);
            //vm_bind.BoundProperties.Add("SpellRange");   // name of property for duration text box
            //irngupdn.Tag = vm_bind;

            //this.savIUpDn.ValueChanged += _viewModel.NumUpDn_ValueChanged;
            //vm_bind = new VMBinding(_spellCast, _spellCast.Spell.SaveProperty);
            //vm_bind.BoundProperties.Add("SpellDifficultyClass");   // name of property for duration text box
            //savIUpDn.Tag = vm_bind;

            // Standard modifiers
            //if (_viewModel.TakesAffectsOthersProperty && false)
            //{
            //    this.affectOthersChkBx.Click += _viewModel.CheckBox_ValueChanged;
            //    spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AffectsOthers");
            //    vm_bind = new VMBinding(_spellCast, spell_prop);
            //    affectOthersChkBx.Tag = vm_bind;
            //}
            //if (_viewModel.TakesAtRangeProperty && false)
            //{
            //    this.atRangeChkBx.Click += _viewModel.CheckBox_ValueChanged;
            //    spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AtRange");
            //    vm_bind = new VMBinding(_spellCast, spell_prop);
            //    vm_bind.BoundProperties.Add("SpellRange");  // range property changes
            //    atRangeChkBx.Tag = vm_bind;
            //}

            //castOnRunChkBx.Click += _viewModel.CheckBox_ValueChanged;
            //spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "CastOnTheRun");
            //vm_bind = new VMBinding(_spellCast, spell_prop);
            //castOnRunChkBx.Tag = vm_bind;

            //this.concealChkBx.Click += _viewModel.CheckBox_ValueChanged;
            //spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "Concealment");
            //vm_bind = new VMBinding(_spellCast, spell_prop);
            //concealChkBx.Tag = vm_bind;

            //this.powerWordChkBx.Click += _viewModel.CheckBox_ValueChanged;
            //spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "PowerWord");
            //vm_bind = new VMBinding(_spellCast, spell_prop);
            //powerWordChkBx.Tag = vm_bind;

            //this.redGestCmbBx.SelectionChanged += _viewModel.ComboBox_ValueChanged;
            //spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "ReducedGestures");
            //vm_bind = new VMBinding(_spellCast, spell_prop);
            //redGestCmbBx.Tag = vm_bind;

            //this.redInCantCmbBx.SelectionChanged += _viewModel.ComboBox_ValueChanged;
            //spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "ReducedIncantation");
            //vm_bind = new VMBinding(_spellCast, spell_prop);
            //redInCantCmbBx.Tag = vm_bind;

            // Special properties for this spell
            // this.specPropLstVw.ItemsSource = _spellCast.Spell.ExtraPropertiesList;
            specPropLstVw.ItemsSource = _viewModel.ExtraProperties;
            //specPropLstVw.DataContext = _viewModel.ExtraProperties;

            // Morphic widgets
            if (_spellCast.Spell.IsMorphic)
            {
                //bind = new Binding();
                //bind.Source = _viewModel;
                //bind.Path = new PropertyPath("MorphicBaseLevel");
                // bind.Mode = BindingMode.OneWay;
                //descrTxtBx.SetBinding(TextBox.TextProperty, bind);
                //morphicLvlCmbBx.SetBinding(ComboBox.SelectedItemProperty, bind);
                //morphicLvlCmbBx.SelectionChanged += new SelectionChangedEventHandler(_viewModel.UpdateComboBox);
                //morphicAppCmbBx.SelectionChanged += new SelectionChangedEventHandler(_viewModel.UpdateComboBox);
            }

            _viewModel.MessageEvent += _viewModel_MessageEvent;

            return;
        }

        /// <summary>
        /// Put in special controls for morphic tab
        /// </summary>
        /// <param name="morphGrid"></param>
        private void AddMorphicControls(Grid morphGrid)
        {
            // First check on base level
            int row_count = 0;
            Thickness label_thickness = new Thickness(5, 3, 5, 7);
            Thickness combo_thickness = new Thickness(5, 3, 5, 1);
            if (_viewModel.BaseLevelList.Count > 0)
            {
                RowDefinition row_def = new RowDefinition();
                row_def.Height = GridLength.Auto;
                morphGrid.RowDefinitions.Add(row_def);
                System.Windows.Controls.Label newLbl = new Label();
                newLbl.Content = "Level";

                newLbl.Margin = label_thickness;
                Grid.SetRow(newLbl, row_count);
                Grid.SetColumn(newLbl, row_count);
                morphGrid.Children.Add(newLbl);

                System.Windows.Controls.ComboBox ctl = new ComboBox();
                ctl.Margin = combo_thickness;
                ctl.VerticalAlignment = VerticalAlignment.Top;
                ctl.ItemsSource = _viewModel.BaseLevelList;
                ctl.DisplayMemberPath = ".";
                ctl.SelectedValuePath = ".";
                Binding binding = new Binding();
                binding.Source = _viewModel;
                binding.Path = new PropertyPath("MorphicBaseLevel");
                ctl.SetBinding(ComboBox.SelectedValueProperty, binding);
                ctl.SelectionChanged += new SelectionChangedEventHandler(_viewModel.UpdateComboBox);
                Grid.SetRow(ctl, row_count);
                Grid.SetColumn(ctl, 1);
                morphGrid.Children.Add(ctl);

                row_count++;
            }

            // Spell Application selection
            int property_count = 0;
            foreach (SpellProperty morphic_prop in _viewModel.MorphicPropertyList)
            {
                property_count++;   // number of properties so far
                RowDefinition row_def = new RowDefinition();
                row_def.Height = GridLength.Auto;
                morphGrid.RowDefinitions.Add(row_def);
                System.Windows.Controls.Label label = new Label();
                label.Content = morphic_prop.Name;

                label.Margin = label_thickness;
                Grid.SetRow(label, row_count);
                Grid.SetColumn(label, 0);
                morphGrid.Children.Add(label);

                ComboBox ctl = new ComboBox();

                ctl.Margin = combo_thickness;
                ctl.Height = 23;
                ctl.VerticalAlignment = VerticalAlignment.Top;
                ctl.ItemsSource = morphic_prop.ModifierList;
                ctl.DisplayMemberPath = "IncrementValue.DisplayValue";
                ctl.SelectedValuePath = "IncrementValue.DisplayValue";
                ctl.SelectedValue = morphic_prop.Value.DisplayValue;
                ctl.Tag = morphic_prop;
                ctl.SelectionChanged += _viewModel.MorphicComboBox_SelectionChanged;

                Grid.SetRow(ctl, row_count);
                Grid.SetColumn(ctl, 1);
                morphGrid.Children.Add(ctl);
                row_count++;
            }
        }

        private void _viewModel_MessageEvent(object sender, CastViewModel.MessageEventArgs e)
        {
            MessageBox.Show(e.Message);
        }

        private void modDescrTxtBx_TextChanged(object sender, EventArgs e)
        {
            string text = modDescrTxtBx.Text;
            Match pageMatch = Regex.Match(text, @"\(page d+\)");
            if (pageMatch.Success)
            {
            }
        }
    }

    public class ToolTipConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length > 0 && values[0] != null)
            {
                ISpell spell = values[0] as ISpell;

            }

            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion
    }


    public class HyperlinkConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TextBlock text_block = value[0] as TextBlock;
            if (text_block == null)
                return Binding.DoNothing;
            string in_text = value[1] as string;
            if (in_text == null)
                return Binding.DoNothing;

            SpellDescription spell_descr = new SpellDescription(in_text);
            IList<SpellDescription.Reference> ref_list = spell_descr.FindReferences();
            int text_start = 0; // keep track of text to output
            string remainder = in_text;
            foreach (SpellDescription.Reference spell_ref in ref_list)
            {
                string pre_text = in_text.Substring(text_start, spell_ref.Start - text_start);
                text_block.Inlines.Add(new Run(pre_text));
                if (SpellBook.Book.Contains(spell_ref.SpellName))
                {
                    Hyperlink hyperLink = new Hyperlink()
                    {
                        NavigateUri = new Uri("http://somesite.com"),
                        Tag = spell_ref.SpellName
                    };
                    hyperLink.RequestNavigate += new System.Windows.Navigation.RequestNavigateEventHandler(hyperLink_RequestNavigate);
                    hyperLink.Inlines.Add(in_text.Substring(spell_ref.Start, spell_ref.End - spell_ref.Start));
                    text_block.Inlines.Add(hyperLink);
                }
                else
                {
                    text_block.Inlines.Add(new Run(in_text.Substring(spell_ref.Start, spell_ref.End - spell_ref.Start)));
                }
                remainder = in_text.Substring(spell_ref.End);
                text_start = spell_ref.End;
            }

            // split up bullet lists
            string[] list_lines = remainder.Split("*".ToCharArray());
            bool first_line = true;
            foreach (string line in list_lines)
            {
                string insert_line = line;
                if (!first_line)
                {
                    text_block.Inlines.Add(new LineBreak());
                    insert_line = "*" + line;
                }
                text_block.Inlines.Add(new Run(insert_line));
                first_line = false;
            }
            return Binding.DoNothing;
        }

        void hyperLink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Hyperlink hyper_link = sender as Hyperlink;
            string spell_name = hyper_link.Tag.ToString();
            TextBlock description_textblock = hyper_link.Parent as TextBlock;
            CastViewModel view_model = description_textblock.DataContext as CastViewModel;

            ISpell spell = SpellBook.Book[spell_name];

            SpellWindow link_win = new SpellWindow();
            link_win.DataContext = spell;
            link_win.Show();

            return;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HyperlinkConverterX : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TextBlock text_block = value[0] as TextBlock;
            string in_text = value[1] as string;
            Match page_match = Regex.Match(in_text, @"\((page \d+)\)");
            if (page_match.Success)
            {
                int match_point = page_match.Groups[0].Index;
                string pre_text = in_text.Substring(0, match_point);
                // Check for spell name
                string[] word_list = pre_text.Split(" ".ToCharArray()).Where(w => !String.IsNullOrEmpty(w.Trim())).ToArray();
                string spell_word = word_list[word_list.Length - 1];
                if (spell_word.Length < 2 || !Char.IsUpper(spell_word[0]) || Char.IsUpper(spell_word[1]))
                {
                    text_block.Inlines.Add(new Run(in_text));
                    return null;
                }

                // Add Runs
                text_block.Inlines.Add(new Run(pre_text));
                string match_string = page_match.Groups[0].Captures[0].Value;
                Run run = new Run(match_string) { FontStyle = FontStyles.Italic };
                text_block.Inlines.Add(run);
                text_block.Inlines.Add(in_text.Substring(match_point + match_string.Length));
            }
            else
            {
                text_block.Inlines.Add(new Run(in_text));
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
