﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using ArzooSoftware.SpellBinder.spellbinder;
using System.Windows.Controls;
using System.Windows.Input;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.SpellBind2.BookViewModel</name>
    /// <summary>
    /// Description for BookViewModel
    /// </summary>
    /// <version>1.0 - Initial Release - 2/21/2013 10:28:19 AM</version>
    /// <author>Greg Arzoomanian</author>    
    class BookViewModel : INotifyPropertyChanged
    {
        public Mage Mage { get { return new Mage(MageLevel, MageIntelligence); } }

        private ListView _spellLstVw;

        internal BookViewModel(Mage caster, ListView spellLstVw)
        {
            PropertyChanged += NullEventHandler;  // remove need to check for no handler
            _spellLstVw = spellLstVw;
            MageLevel = caster.Level;
            MageIntelligence = caster.Intelligence;
        }

        #region Properties
        private int _mageLevel;
        public int MageLevel
        {
            get
            {
                return _mageLevel;
            }
            set
            {
                _mageLevel = value;
                PropertyChanged(this, new PropertyChangedEventArgs("MageLevel"));
                Properties.Settings.Default.MageLevel = value;
                Properties.Settings.Default.Save();

                Mouse.OverrideCursor = Cursors.Wait;
                _spellLstVw.Items.Refresh();    // refresh to change enabled spells
                Mouse.OverrideCursor = null;
            }
        }

        private int _mageInt;
        public int MageIntelligence
        {
            get
            {
                return _mageInt;
            }
            set
            {
                _mageInt = value;
                PropertyChanged(this, new PropertyChangedEventArgs("MageIntelligence"));
                Properties.Settings.Default.MageIntelligence = value;
                Properties.Settings.Default.Save();
            }
        }

        public decimal MageBonus { get { return Mage.AbilityBonus; } }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region event handling

        /// <summary>
        /// Null event to call for event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>To avoid checking for null event</remarks>
        private void NullEventHandler(object sender, EventArgs e)
        {
            ;
        }
        #endregion
    }
}
