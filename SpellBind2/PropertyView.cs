﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Xml;
using ArzooSoftware.SpellBinder.spellbinder;
using System.ComponentModel;
using System.Windows.Data;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <copyright>Arzoo Software, 2014</copyright>
    /// <name>ArzooSoftware.SpellBinder.SpellBind2.PropertyView</name>
    /// <summary>
    /// Common class for views that display property-based (duration, range, etc.)
    /// </summary>
    /// <version>1.0 - Initial Release - 2/9/2014 9:39:15 AM</version>
    /// <author>Greg Arzoomanian</author>    
    class PropertyView
    {
        private ListView _spellLstVw;    // mimic SpellLstView from XAML
        private IPropertyView _propertyView;
        public PropertyView(ListView listView, BookViewModel castViewModel, IPropertyView propertyView)
        {
            _spellLstVw = listView;
            _casterViewModel = castViewModel;
            _propertyView = propertyView;
        }

        /// <summary>
        /// Holds caster info for BookView
        /// </summary>
        BookViewModel _casterViewModel;

        /// <summary>
        /// Return list of cast spells with different modifiers
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<Cast> GetSpellList()
        {
            Mage mage = _casterViewModel.Mage;
            foreach (ISpell base_spell in SpellBook.Book.List)  // loop on spells in book
            {
                if (CostCalc.GetMaxLevel(mage.Level) >= base_spell.Level)
                {
                    IModifier mod = _propertyView.GetPropertyModifier(base_spell);
                    if (mod != null)  // must be extendable
                    {
                        // List<Cast> add_list = new List<Cast>(); // temp list 
                        Cast cast = new Cast(base_spell, mage);
                        yield return cast;
                        int cost = cast.Cost;   // save cost

                        // Add modified version of spell if less than max
                        while (cast.Level < cast.MaxLevel
                            && !_propertyView.AtMaxValue(cast))
                        {
                            Cast mod_cast = new Cast(cast);  // build another copy
                            try 
                            {
                                mod_cast.AddModifier(mod);
                            }
                            catch (Cast.BadStateException excp)
                            {
                                break;
                            }

                            if (mod_cast.Cost > cast.Cost)
                            {
                                yield return mod_cast;
                                cast = mod_cast;    // for next time
                            }
                            else // cost didn't increment
                            {
                                cast.AddModifier(mod);  // modify existing cast
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Display double-clicked spell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void spellLstVw_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //Get the ItemsControl and then get the item, and 
            //check there is an actual item, as if we are using 
            //a ListView we may have clicked the
            //headers which are not items
            // as per http://www.codeproject.com/Articles/42111/Selector-DoubleClick-Behaviour-calling-ViewModel-I
            ItemsControl listView = sender as ItemsControl;
            DependencyObject originalSender =
                e.OriginalSource as DependencyObject;
            if (listView == null || originalSender == null) return;

            DependencyObject container =
                ItemsControl.ContainerFromElement
                (sender as ItemsControl,
                e.OriginalSource as DependencyObject);

            if (container == null ||
                container == DependencyProperty.UnsetValue) return;

            // found a container, now find the item.
            object activatedItem =
                listView.ItemContainerGenerator.
                    ItemFromContainer(container);

            if (activatedItem == null)
            {
                return;
            }

            Cast cast = _spellLstVw.SelectedItem as Cast;
            if (cast != null)  // scroll clicks being mis-interpreted
            {
                // See that spell is castable by mage
                if (CostCalc.GetMaxLevel(_casterViewModel.Mage.Level) < cast.Level)
                {
                    MessageBox.Show(String.Format("Mage of level {0} can't throw spell of level {1}", _casterViewModel.Mage.Level, cast.Level));
                }
                else
                {
                    CastWindow cast_win = new CastWindow(cast);
                    cast_win.Show();
                }
            }
        }

        internal void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo_box = sender as ComboBox;
            CollectionView cv = (CollectionView)CollectionViewSource.GetDefaultView(_spellLstVw.ItemsSource);
            ComboBoxItem item = combo_box.SelectedItem as ComboBoxItem;
            if (item.Content.Equals("All"))
                cv.Filter = null;
            else
            {
                cv.Filter = i =>
                {
                    Cast c = i as Cast;
                    return c.Category.Equals(item.Content);
                };
            }
            cv.Refresh();

            return;
        }
    }

    /// <summary>
    /// Property specific methods from property view
    /// </summary>
    interface IPropertyView
    {
        bool AtMaxValue(Cast cast);
        IModifier GetPropertyModifier(ISpell spell);
    }
}
