<book>
  <spell name='Attune' level='morphic'>
    <description>This spell attunes the mage to an object creating a magic bond between the mage and the object. The target of the  spell may be a literal object, or a place or a person. If the object is a person, that person must either drop their saving  throw or fail to save twice for the attunement to work.  

Once attuned to the target, the mage enjoys a connection which enhances the operation of some spells:  
* Locate (page 59) works better with attuned objects.  
* Message (page 64) works at much greater range with attuned persons.  
* ESP (page 66) works at greater range and effectiveness with attuned persons.  
* Clairsentience (page 69) works at greater range and effectiveness with attuned persons.  
* Telepathy (page 74) may be forced on attuned targets and used at greater range than normal.  
* Summon (page 78) only works with attuned objects.  
* Teleport (page 78) to distant locations is only safe with attuned places or objects.  

Aside from these specific spell effects, any spell cast on an attuned target allows the target only half its normal saving  throw bonus.  

Attuning to a literal object or a place is a Level 1 base spell.  
Attuning to a person is a Level 2 base spell.  

It takes an hour to attune to an object. A mage can be attuned to a maximum of 7 objects without penalty. Attuning to  more objects makes the spell more difficult: attempting to attune an 8th object requires a +1  2 modifier, the 9th attunement  requires +1, etc. Note that these modifiers are not required if the mage drops one of his existing attunements in the  process of casting the new one, which is the standard action for a mage already holding 7 attunements and requires no  extra time or casting cost.  

Attuning to an object which another mage has already attuned breaks the other mage's bond to the object, but it requires  a Level Contest with the previously attuned mage, who gets a 2 level bonus. Players should keep track of the objects their  mages are attuned to on their character sheet. It is assumed that every mage is attuned to their home unless otherwise  specified.  </description>
	  <BaseLevels>
		  <Level name='object/place' level='1'/>
		  <Level name='person' level='2'/>
	  </BaseLevels>
	  <ZOE value='1' units='object' type=''/>
    <Range value='touch' units=''/>
    <Duration value='permanent' units=''/>
    <SavingThrow value='Will' effect='negates'/>
  </spell>
  <spell name='Counterspell' level='morphic' category='Counterspells'>
    <BaseLevels>
      <Level name='base' level='0'/>
    </BaseLevels>
    <description>This spell can be cast only to negate a known spell in the process of being cast. The caster must recognize the  target's spell as it is being declared during Declarations phase: this requires that the caster already have Analyze Spell  (page 68) running, or else make a Spellcraft check against 15 + (base level of spell being cast). Counterspell may be  declared after other spellcasting has been declared, even if the caster is abandoning a spell preparation of his own in order  to Counterspell, an exception to the usual rule that declarations are simultaneous.  The base level at which Counterspell is cast must equal the base level of the spell being countered (modifiers need not  match). Another special property of Counterspell is that it is resolved, in the spell phase sequence, as though it cost 0  points to cast: thus it will always go off before the spell it is countering is completed. The target (the caster of the spell  being countered) and the caster of the Counterspell engage in a contest of levels, with the Counterspell having a +3 level  bonus (net +9 to the d20 roll). If the counterspell wins, the targeted spell fails, although the caster still pays full spell  points.  Clerical prayers cannot be Counterspelled. Spells cast as Power Words cannot be counterspelled unless the Counterspell  is cast as a Power Word also; Analyze Spell will give warning at the beginning of the Powers phase when a caster is  about to speak a Power Word, or a second Spellcraft check against 18 + (base level of spell being cast) will alert the  Counterspell caster that a Power Word is needed. If the Counterspell caster cannot or chooses not to match the original  caster's Power Word, the Counterspell may simply be dropped at no cost, as with any spell.  </description>
    <ZOE value='1' units='spell' type=''/>
    <Range value='LOS' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='level' effect='contest'/>
    <Properties>
      <Property name='Target Spell' value='1'>
        <Modifier type='TargetSpell' value='1' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Enhance Ability' level='morphic' category='Enhancement'>
    <description>This spell allows a mage to temporarily boost any living being's abilities. The base cost is 1/2 spell level per  +1 enhancement bonus up to a modified ability score of 20, +1 full level per point of additional enhancement bonus.  (So to give a 16-Strength Hero a Strength 18 would cost 1 spell level, 20 would cost 2, 21 would cost 3, 22 would  cost 4 levels, etc.) Increases to Con provide extra hit points, which are real, not temporary. Increases to Int or Cha do  not provide additional spell points, nor do increases to Wis provide additional prayer points. </description>
    <modifiers_description>Extra ZOE +0.5 allows one extra person to be affected by the same casting, per increment; multiple bene?ciaries can be touched until the charge is exhausted. This replaces the usual progression of Extra ZOE</modifiers_description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='Fortitude' effect='negates'/>
    <Properties>
      <Property name='Increase to 20' value='0'>
        <Modifier type='IncreaseTo20' value='1' cost='.5'></Modifier>
      </Property>
      <Property name='Increase over 20' value='0'>
        <Modifier type='IncreaseOver20' value='1' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Hallucination' level='morphic' category='Combat'>
    <description>This spell projects hallucinations directly into the mind. The base level depends on the nature of the hallucination and the nature of the target.  
Type Of Target: The base level is 0 for a spell targeting a single being, or 1 for ZOE of 20' radius sphere. In the latter  case, all the beings initially in the ZOE are targeted, and are affected for the duration (or until they save against the  Hallucination). Entering or leaving the ZOE after the spell has been cast does not have any effect. In both cases, the  scope of the hallucinations is unrestricted (e.g. it may extend well beyond the ZOE).  
Type Of Hallucination:  
* A minor change, such as introducing an unimportant object or sensation (perhaps the smell of warm bread): +0.  
* A major change, such as introducing imaginary opponents, changing the apparent positions of combatants, relocating  terrain features, changing the direction of a sensation such as gravity: +1.  
* A complete change, such as making friends look (and act and sound) like enemies, enclosing an entire area in a phony  building, or overriding a target's senses so they are unaware of damage they are taking: +2  
* Hallucination can do non-lethal damage to those who fail to save: +1.  
* Hallucination can do lethal damage to those who fail to save: +2.  
* Hallucination has non-damage-based incapacitating powers: +2.  
* Hallucination involves complex or preprogrammed activity: +1.  
* Caster retains control and can adjust the hallucination: +0, but duration changes to 'Concentration'.  

Unlike illusions, hallucinations have no real existence. Constructs and other beings immune to mental effects cannot  perceive them. Hallucinated light does not illuminate the real surroundings, but shows what the caster decided the victim  should perceive.  

Any being gets a Will save upon first exposure to a hallucination. Those who save faintly perceive the hallucination and  can tell it is unreal. Generally they may immediately notify their comrades.  

A character who believes he is the victim of a hallucination can take a full-round action to get another saving throw. This  must be declared in Declaration Phase and the save attempt occurs during Powers Phase (so it could never be done on  the round the hallucination was cast).  

A Hallucination, unlike an illusion, can harm those who believe in it if empowered to do so. Without those modifiers,  attacks and other effects will cause pain but no actual harm to the victim. A damaging Hallucination may do at most  1d6 damage per total level of the spell, per victim, per round. For instance, a level 3 hallucination of a fireball -- major  change, lethal damage -- would cause at most 3d6 damage per target. However, it need not follow the reduced damage  spread of an actual Fireball spell. With two levels of Hard To Save, it could cause 4d6 per target. In the Powers phase  following any round in which a victim takes damage, whether directly from the hallucination or from effects concealed  by the hallucination (such as a hallucination of a solid floor and cozy legs while you actually stand in a pool of acid), he  gets a saving throw to free himself from the hallucination.  

If the Hallucination is only empowered for non-lethal damage, then the damage inflicted will be non-lethal even if the  hallucination is of something (molten lava, sword blows) that normally causes lethal damage. If a player character is  the victim of a nonlethal-damage hallucination, the GM has the right to announce damage while secretly tracking and  recording the fact that it is nonlethal.  

The victim actually suffers any damage taken while under the influence of a Hallucination; it does not disappear when he  saves or when the duration ends. A character killed by lethal damage from a hallucination actually dies; however, there  is never visible damage. An Aru might conclude he was 'frightened to death.'  

The entry for 'non-damage-based incapacitating powers' is for hallucinations of creatures with powers such as a Medusa's  petrifying gaze, a Basilisk's deathgaze, or a ghoul's paralysis. The victims of such abilities may make aWill save against  the ability even if they believe the hallucination; the DC of this save cannot be higher than the save DC of the spell even  if the monster's power would normally have a higher DC. If the power has no saving throw, the victim gets a Will save  against the spell DC. In addition, they do not actually die, turn to stone, etc., but merely believe they have suffered this  fate for the duration of the spell. (Of course, there is little functional difference between believing you are paralyzed, and  actually being paralyzed.)  </description>
	  <BaseLevels>
		  <Level name='single being' level='0'/>
		  <Level name="20' sphere" level='1'/>
	  </BaseLevels>
    <ZOE value='see description' units='' type='described'/>
    <Range value='120' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Type' value='minor'>
        <Modifier type='Type' value='minor' cost='0'></Modifier>
        <Modifier type='Type' value='major' cost='1'></Modifier>
        <Modifier type='Type' value='complete' cost='2'></Modifier>
      </Property>
      <Property name='Damage' value='none'>
        <Modifier type='Damage' value='none' cost='0'></Modifier>
        <Modifier type='Damage' value='non-lethal' cost='1'></Modifier>
        <Modifier type='Damage' value='lethal' cost='2'></Modifier>
      </Property>
      <Property name='Incapacitating' value='no'>
        <Modifier type='Incapacitating' value='no' cost='0'></Modifier>
        <Modifier type='Incapacitating' value='yes' cost='2'></Modifier>
      </Property>
      <Property name='Complex Activity' value='no'>
        <Modifier type='Complex' value='no' cost='0'></Modifier>
        <Modifier type='Complex' value='yes' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Illusion' level='morphic' category='Appearance'>
    <description>This is a very powerful tool. It can be used to create illusions by warping air to reflect light or sound. Illusions are not  artifacts of mind control. An illusion will never cause damage. The instant that an illusion would have caused damage,  the spell will be broken. All modes except programmed (see below) automatically have the Concealment modifier cast  upon them at no extra cost.  It has two modes, visual and aural. It has two modifiers, programmed and interactive. Use of each mode allows the caster  to construct an illusion using that sense.  Visual: This is of course the most common. It may be used to create or hide a door, disguise a person, or create a false  image of something threatening, or any other purpose imaginable.  Aural: Most illusions will be far more convincing when used with sound. Many animals will not be fooled by any  illusion without sound; in fact, some animals will not even detect an illusion without sound.  The Visual and Aural modes can be bought any number of times each, proportional to the complexity of the illusion  desired. A single tone or blank wall would be one level, a voice or body two, a specific voice or body three or four, a  symphony or army five, etc.  The programmed modifier allows the mage to set a specific set of circumstances that would trigger the illusion. The  illusion then will perform some prearranged show. It will only work once, unless a Permanence is cast upon it. The site  where the illusion is to take place will detect as magic unless it is concealed.  The interactive modifier will allow the caster, if concentrating, to shape the illusion's responses and actions. Note that  unless the illusion is interactive, the entire script of the illusion must be chosen at the time of casting. Hence, shadow  fighters must be interactive, and thus require concentration.  There is no such thing as �disbelieving an illusion�. Illusions are really there: illusory walls do block sight, loud noises  will obscure other sounds. Creatures can ignore them just as they can ignore anything else. Remember that illusions will  never cause damage. In addition, most illusions are dispelled by touch of flesh; all illusions can be dispelled by Dispel  Magic (page 69).  Light and Darkness: Aside from some cantrips and highly specialized spells such as Ventriloquism and Magic Mouth,  Illusions are the mage's main power over light and darkness, noise and silence. Illusory light sources create real light  which can illuminate areas beyond the spell's ZOE. Illusory shadows genuinely do block vision. (However, if they are  to affect people inside them, they must be cast to survive the touch of flesh, unlike light sources which can simply be  placed out of the way. Unlike Darkness prayers, they do not block Darkvision.) Similar concerns apply to sounds and  silence: real sounds can be heard at a distance, while an anechoic zone will only deafen people inside it and must be built  to withstand their presence. Note that neither light nor sound can ever be made bright enough to be harmful, nor can an  illusion's glow have the properties that make sunlight deadly to some Undead.  Cost: 1  2 level per level of visual or aural mode. +2 spell levels for programmed. +1 for interactive . +1 for the illusion  to not be dispelled by touch of flesh. +1 for a �traveling� illusion with a movable ZOE. The ZOE is centered on some  object or being and moves with it. Of course, it must either be �touchproof� or else the actual illusions must be restricted  to a part of the ZOE that doesn't come into contact with anyone. In addition, an illusion cast on an unwilling person or  an object carried by an unwilling person allows a Will save to negate the spell. Extra ZOE doubles the ZOE for +1. The  progression is geometric  </description>
    <ZOE value='20 foot sphere' units='' type='described'/>
    <Range value='120' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Visual' value='0'>
        <Modifier type='Visual' value='1' cost='.5'></Modifier>
      </Property>
      <Property name='Aural' value='0'>
        <Modifier type='Aural' value='1' cost='.5'></Modifier>
      </Property>
      <Property name='Programmed' value='no' max='yes'>
        <Modifier type='Programmed' value='no' cost='0'></Modifier>
        <Modifier type='Programmed' value='yes' cost='2'></Modifier>
      </Property>
      <Property name='Interactive' value='no' max='yes'>
        <Modifier type='Interactive' value='no' cost='0'></Modifier>
        <Modifier type='Interactive' value='yes' cost='1'></Modifier>
      </Property>
      <Property name='Dispellable' value='yes' max='no'>
        <Modifier type='Dispellable' value='yes' cost='0'></Modifier>
        <Modifier type='Dispellable' value='no' cost='1'></Modifier>
      </Property>
      <Property name='Traveling' value='no' max='yes'>
        <Modifier type='Traveling' value='no' cost='0'></Modifier>
        <Modifier type='Traveling' value='yes' cost='1'></Modifier>
      </Property>
      <Property name='ZOE Multiplier' value='1'>
        <Modifier type='ExtraZOE' value='1' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Locate' level='morphic' category='Senses'>
    <description>This spell is used to find the direction and distance to a specified target, if it is within range. The base spell will give  the direction to the target, or the nearest target if it is not unique. The distance to the detected target may be known for  +1 spell level. The possible targets are:  Attuned object: Base level 1 and base range 1 mile. The object may be a person, place or literal object.  Person: Base level 2. A specific sentient creature is targeted. The creature must be named in a unique fashion.  Specific Object: Base level 2. A specific object is targeted. The object must be described enough to distinguish it from  all items that are not completely identical, or named in the case of named artifacts.  Any Object of a Specific Type: Base level 1. A specific type of object (again, this can include a category of person or  creature, or a place meeting certain specifications) is targeted. The type must be described in terms of its current physical  state, i.e. described so that an ordinary person who could see, touch, hear, and smell the object where it currently is could  answer the question, �Is this the object?�, based solely on the description given, without resort to unusual senses, skill,  or expertise. The description may not include past or future locations or conditions. The description may not include  properties such as ownership, purpose, good, or evil. The description may include references to other co-located objects,  i.e. �a sword in a red sheath lying on an altar�. The type can be as broad or narrow as the caster wants. The caster could  locate a collection of books, i.e. a library. The caster could locate a book with a specific word in its title. The caster can  not select a type of object that they are unfamiliar with. For instance, if the caster heard of a left-handed smoke shifter  and tried to locate one, the locate would fail. The caster can not locate a type of object that requires information they do  not have. For instance, �the objects that were taken from this room� could not be located unless the caster knew what  they were. For the same reason, a caster could not locate something like �a clue that we have overlooked�.  The range is 480' extendible along the following progression at the cost of +1  2 per step:  480', 1  4 mile, 1  2 mile, 1 mile, 2 miles, 4 miles, doubling.  Extra Effect +1: Range is reduced to 60' but all targets in range are located. Range may be doubled for +1  2 .  Affects Others will give the knowledge given by the spell to another. The detection range is then computed from the  recipient. Concealment will hide the fact that a person has a Locate spell running.  </description>
    <modifiers_description>The range is 480’ extendible along the following progression at the cost of +1 2 per step: 480’, 1 4 mile, 1 2 mile, 1 mile, 2 miles, 4 miles, doubling. Extra Effect +1: Range is reduced to 60’ but all targets in range are located. Range may be doubled for +1 2. Affects Others will give the knowledge given by the spell to another. The detection range is then computed from the recipient. Concealment will hide the fact that a person has a Locate spell running.</modifiers_description>
    <BaseLevels>
		  <Level name='Attuned object' level='1'>
			  <Properties>
			    <Property name='Range' value='1' units='miles'/>
			  </Properties>
		  </Level>
		  <Level name='Person' level='2'/>
		  <Level name='Specific object' level='2'/>
		  <Level name='Any object' level='1'/>
	  </BaseLevels>
    <ZOE value='self' units='' type='described'/>
    <Range value='480' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='none'/>
	<Properties>
	  <Property name='Get Distance' value='no' max='yes'>
        <Modifier type='DetermineRange' value='yes' cost='1'></Modifier>
	  </Property>
	  <Property name='All Targets' value='no' max='yes'>
        <Modifier type='ExtraEffect' value='yes' cost='1'></Modifier>
	  </Property>
	</Properties>
  </spell>
  <spell name='Monster Summoning' level='morphic' category='Conjuring'>
    <description>This spell will summon a single being, which will appear next to the caster in the powers phase of the  round after the spell is cast. The creature will be inclined to serve the caster, under conditions similar to those specified  for the 3rd level spell Suggestion (page 71).  The creature is real and has been transported here from elsewhere in the general region. At the end of the spell's duration,  it will return, alive or dead, whence it came, along with everything it brought with it. The spell's duration begins when  the monster appears, so the monster will return after 13 rounds.  A base spell of level N allows the summoning of one monster of level N, 1d3 of level N-1, or 1d6 of level N-2. The  caster may choose the monster from the Monster Summoning Table (see page 84) or from other tables as supplied by  the GM. Attempts to summon unique individuals or of monsters into inappropriate terrain (as a non-flying creature in  midair) generally has no effect. Attempting to summon a monster that is appropriate but doesn't happen to exist in the  vicinity will result in the arrival of a plausible substitute at the GM's discretion. </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='none' units=''/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='none'/>
	  <Properties>
	    <Property name='Level' value='0'>
          <Modifier type='Level' value='1' cost='1'></Modifier>
	    </Property>
	  </Properties>
  </spell>
  <spell name='Patterning' level='morphic' category='Environment'>
    <description>This strengthens or weakens, but does not transmute, nonliving substances and objects. The base spell level  depends on how strong the material is, as defined by its Hardness (see p.42). The base spell level is 1/3 of the Hardness  of the target, rounding up to the nearest half-level. The final spell level, including base plus modifiers, must be at least  1. Patterning cast on a composite object (e.g. a hafted weapon) can be cast at a level too low to affect the strongest  materials, in which case it will affect only the weaker materials. If a substance has multiple Hardness ratings against  different attack types (e.g. rope), use the lowest.  Patterning has several applications which may raise the base level of the spell. All applications have duration �momentary�:  their effects are real and permanent and cannot be dispelled. Note, however, that Strengthened and Grand Patterned  materials do detect as magical: the magic is innate to the substance and cannot be removed except by destroying it or  undoing the Patterning.  Bind: makes something a seamless whole, strengthening any weak points or flaws. Fastening a tool to its handle, or a  weapon blade into its haft, need only affect the weaker material. Bind can splice ropes. When Binding a complex  object, such as a lock, parts that are intended to move retain their freedom of movement. Bind makes an object as  strong as it can be without introducing preternatural properties to the materials. It incidentally repairs all scuffs,  scratches, and other minor cosmetic flaws. It cannot repair major holes or gaps. Level +0.  Weaken: simply weakens the material or object. Its Hardness, hit points, and strength (load capacity) can be lowered  to half their normal values. Weaken undoes Strengthen; when weakening a Strengthened material the material  properties can be lowered to half their natural (pre-Strengthening) values. Level +0.  Seal: has all the effects of Bind, and in addition will make things watertight or even airtight. It can also be reversed  (Unseal) to make a normally watertight or airtight substance leaky. Level +1  2 .  Mend: has all the effects of Bind, plus Seal/Unseal if desired, plus it can repair major damage. Holes up to half the size  of the ZOE can be repaired; an object significantly smaller than the ZOE can be reassembled from a few scattered  fragments. Mend cannot build brand-new objects from raw materials; the thing you are trying to mend must have  previously existed in the form you are trying to restore. +1 level.  Break: is the obvious opposite of Mend. An object that more or less fills the ZOE can be broken in half or have a hole  half its size punched through it. Smaller objects can be broken into more pieces proportionately. +1 level.  Disintegrate: is just a more extreme form of Break. The object is reduced to a fine powder from which it cannot be  reconstituted by Mending. +2 levels.  Strengthen: allows you to increase the Hardness, hit points, and strength/loadbearing of a material. Simply cast the  spell at the base level for the Hardness you want to achieve instead of the current Hardness of the material. When  strengthening flexible material like rope or fabric, you choose whether the increase in Hardness merely makes it  more resistant to damage or actually makes it rigid and hard. Load strength and hit points can be increased by  a factor of either the proportion by which you are increasing Hardness, or the base level of the Patterning spell,  whichever is less.  Grand: enchants a material with magical permanence and unity. Hardness per se is not altered, but it acquires DR50/-  against all forms of physical or magical attack, including energy attacks such as fire, lightning, acid, etc. Its  strength or load capacity increases tenfold. Note that Grand Patterned clothing does not extend its invulnerability  to the wearer. Once an object has been Grand Patterned, to affect it further with Patterning requires the caster to  apply the Grand modifier and win a Level Contest with the original caster. All magic items (save scrolls, potions,  and naturally magical materials) have been Grand Patterned as part of their construction.+4 levels.  The ZOE for any form of Patterning is one object (or portion of a larger object) that may weigh up to 200 lbs and fit into  a cube not more than 10 feet on a side. This can be doubled for +1 (geometric progression)  </description>
	  <BaseLevels>
		  <Level name='Paper, fabric, rope' level='1'/>
		  <Level name='Glass' level='1'/>
		  <Level name='Ice' level='1'/>
		  <Level name='Leather' level='1'/>
		  <Level name='Wood' level='2'/>
		  <Level name='Stone' level='3'/>
		  <Level name='Iron' level='4'/>
		  <Level name='Mithril' level='5'/>
	  </BaseLevels>
    <ZOE value='see description' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='none'/>
	  <Properties>
	    <Property name='Applications' value='Bind/Weaken'>
          <Modifier type='Application' value='Bind/Weaken' cost='0'></Modifier>
          <Modifier type='Application' value='Seal' cost='.5'></Modifier>
          <Modifier type='Application' value='Mend/Break' cost='1'></Modifier>
          <Modifier type='Application' value='Disintegrate' cost='2'></Modifier>
          <Modifier type='Application' value='Grand' cost='4'></Modifier>
	    </Property>
	  </Properties>
  </spell>
  <spell name='Pyromancy' level='morphic' category='Environment'>
    <description>Use of this spell allows the mage to control, start, or stop fires. Applications can be combined in a single casting  if so indicated. All applications have duration &quot;momentary&quot;, except Control which has duration &quot;concentration&quot;.  Applications  Ignite: Flammable material in the ZOE will ignite. Level 1 will ignite anything that will catch from a brief touch of a  torch flame. Level 2 will ignite even the most recalcitrant materials that are actually flammable. Can be combined  with Intensify.  Intensify: N levels of Intensify will multiply the heat/light/damage (normal fires typically do 1d6/round) by N+1. Fuel consumption rate increases proportionately. Can be combined with Ignite. Consecutive castings of Intensify add  to the multiplier, they don't multiply it.  Quench: Flames go out through the ZOE. Level 1, or equal to the number of d6/round this fire inflicts, whichever is  higher.  Bank: As Quench, but instead of putting the fire out you may reduce it to any desired intensity of burning, including  glowing embers or a slow smolder.  Smother: May be included as part of Quench or Bank at no additional cost. The fire produces increased smoke of any  desired thickness up to &quot;opaque&quot; and &quot;unbreatheable&quot;. The smoke will dissipate normally, although it will be  renewed if the fire is still burning.  Fireworks: As Quench but the fire shoots out sparks and flares to 3x the radius of the base ZOE, which can ignite  secondary fires if they meet highly flammable material.  Control: You may control the movements of the flames, and direct the spread of the fire as you see fit, even against the  wind. Control can be combined with Intensify, Bank, or Smother, in which case you may turn the secondary effect  on and off at will. If you spread the flames beyond the original ZOE you may shift the ZOE to determine which  part of the conflagration you control, but you cannot expand it.  Special Modifier: Magical (+2). This is necessary to control or manipulate magical fires (e.g. Wall of Fire spells, flaming  swords). If applied to igniting or manipulating a normal fire it will make the fire magical.  The base ZOE of pyromancy is a sphere of 20' diameter (10' radius), or any one object, stack of objects (e.g. pyre), or  active fire that fits into the ZOE. It can be cast on just part of a larger fire if desired. Extra ZOE extends the radius by 10' per +0.5 level.  </description>
    <modifiers_description>
      Special Modifier: Magical (+2). This is necessary to control or manipulate magical fires (e.g. Wall of Fire spells, flaming swords). If applied to igniting or manipulating a normal fire it will make the fire magical.
      The base ZOE of pyromancy is a sphere of 20’ diameter (10’ radius), or any one object, stack of objects (e.g. pyre), or active fire that fits into the ZOE. It can be cast on just part of a larger fire if desired. Extra ZOE extends the radius by 10’ per +0.5 level.
    </modifiers_description>
    <BaseLevels>
		  <Level name='Ignite' level='1'/>
		  <Level name='Ignite Recalcitrant' level='2'/>
		  <Level name='Intensify' level='1'/>
		  <Level name='Quench' level='1'/>
		  <Level name='Bank' level='1'/>
		  <Level name='Smother' level='1'/>
		  <Level name='Fireworks' level='1'/>
		  <Level name='Control' level='1'/>
	  </BaseLevels>
	  <ZOE value='one object or 10’r sphere' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='see description' units=''/>
    <SavingThrow value='none'/>
	  <Properties>
	    <Property name='Magical' value='no' max='yes'>
          <Modifier type='Magical' value='yes' cost='2'></Modifier>
	    </Property>
	  </Properties>
  </spell>
  <spell name='Shaping' level='morphic'>
    <description>This spell allows the mage to transform himself, others, or objects into various other substances or creatures. Its  most common uses are to turn oneself or another into a creature, and to transform substances into other substances, such  as flesh to stone, stone to mud, or water to wine.  The base level is determined by the nature of the object or creature being shaped. Legal target types are self, other living  creature, and any non-magical object or substance. Non-monster plants and organic matter may be considered to be  objects. An object of up to 1000 pounds and 25 cubic feet may be affected; this can be doubled for an additional +1.  The level is then modified by the degree of change between the target and the product. A change of substance will  increase the level by one, for example stone to gold. Note that living creature to living creature usually does not involve  a change of substance. Also, the caster may choose to change the size of the target, as defined under Size Change  (page 73), for an additional +1 for each casting of the modifier.  The caster decides, when casting the spell, which of two modes to employ.  Willing Mode allows the target to end the spell at will, even in the same phase that it was cast. (He senses the transformation  beginning, before it has progressed far enough to have a game effect.) Duration is 40 minutes, unless ended  sooner. If the target is �self�, the mode is always willing.  Unwilling Mode allows a physical saving throw to avoid the effect and has lasting duration, which cannot be ended  prematurely without using Dispel Magic (page 69).  Finally, the level is modified further by the number of special abilities given to the target. Without these modifiers, a  man shaped into a dragon would walk at human speed, not be able to fly, fight as an unarmed human, and have no breath  weapon. These ability modifiers MUST be bought semisequentially. There are four ability modifiers, each detailed  below: Movement, Combat, Senses, and Magical. Movement must be bought BEFORE any others, and Magical, if  desired, must be bought AFTER all others.  Buying movement will give the target the natural movement abilities of the shape assumed, for example flight, tunneling,  swimming, etc. It will not give magical abilities like teleportation. The Combat modifier gives non-magical combat  abilities of the form assumed, for example claw-claw-bite, spikes, etc. The senses modifier gives the target the senses  appropriate to the form assumed, i.e. an eagle's eyes, bloodhound's nose, etc.  The Magical Abilities modifier allows the caster to use all abilities of the assumed form. These include poison, teleportation,  breath weapon, phase shift, level drain, etc. Note that spell ability beyond the caster's level can never be gained,  and that no spell casting is possible unless the form has humaniform hands and vocal abilities.  Any of these modifiers may be used in a Shaping cast on a non-living object. For example, creating a sword requires the  casting of the Combat Abilities modifier. In general, if something can inflict damage, it must have the Combat modifier.  Unlike with living targets, Combat may be bought without first buying Movement, and Magical may be bought without  first buying Senses. Magic items can not be affected or created by this spell.  Target Base Level Changes Abilities  Self 3 Change of Substance +1 Movement +1  Object 4 Size Change +1 Combat / Senses +1  Living, willing 4 Combat + Senses +1  Living, unwilling 5 Magical +1  </description>
	  <BaseLevels>
		  <Level name='Self' level='3'/>
		  <Level name='Object' level='4'/>
		  <Level name='Living, willing' level='4'/>
		  <Level name='Living, unwilling' level='5'/>
	  </BaseLevels>
    <ZOE value='1' units='target' type=''/>
    <Range value='none' units=''/>
    <Duration value='see description' units=''/>
    <SavingThrow value='Fortitude' effect='negates'/>
	  <Properties>
	    <Property name='Change Substance' value='no' max='yes'>
          <Modifier type='Substance' value='yes' cost='1'></Modifier>
	    </Property>
      <Property name='Change Size' value='0' max='yes'>
        <Modifier type='Size' value='1' cost='1'></Modifier>
      </Property>
      <Property name='Abilities' value='none' max='Magical'>
        <Modifier type='Ability' value='none' cost='0'></Modifier>
        <Modifier type='Ability' value='Movement' cost='1'></Modifier>
        <Modifier type='Ability' value='Combat/Senses' cost='2'></Modifier>
        <Modifier type='Ability' value='Combat+Senses' cost='3'></Modifier>
        <Modifier type='Ability' value='Magical' cost='4'></Modifier>
	    </Property>
	  </Properties>
  </spell>
  <spell name='Skylore' level='morphic' category='Environment'>
    <description>This spell gives the mage the ability to control the weather. He may bring or banish rain, lightning, clouds, storms  of various intensity, and raise or lower the temperature. This spell will never cause normal damage except to creatures that would be harmed by normal weather, e.g. water damage from rain. If the mage is attempting to control magical or sentient storms or weather controlled by another mage, he must win a Level Contest.  
    It has five spheres of control: temperature, wind, rain / snow, clouds, and lightning. When casting the spell, the mage  may cast one or any combination of spheres, positively or negatively, and may stack multiple castings of a sphere. The only exception to this is the casting of rain or lightning which only requires clouds.  When decreasing weather effects, each level of a sphere will decrease the effect by one unit. When increasing all effects except temperature, the caster must build the effect as if there were no existing weather. For example, faced with a Force-4 wind, the caster may spend one level to decrease it to Force-2. To increase the wind to Force-6, he must cast  three levels of wind.  

    Wind Sphere: Each half-level of this sphere increases or decreases the wind velocity by one increment on the Beaufort Scale (generally about 6 knots). The minimum casting cost is one level. If the caster is increasing or equaling the speed  of the wind, he may also determine the direction. All missile rolls suffer a -5% penalty per 12 knots of wind.  

    Temperature Sphere: Each casting of this sphere will raise or lower the existing air temperature by 10 degrees F. Temperatures above 100 degrees or below 0 degrees generally have deleterious effects on humans.  

    Rain Sphere: This will appear as snow if the temperature is below freezing.  
    * 1 level: light snow or rain. Visibility is 200 feet.  
    * 2 levels: medium rain or snow. Visibility is 100 feet.  
    * 3 levels: hard rain or snow. Visibility is 50 feet.  
    * 4 levels: driving rain or blizzard. Visibility is 15 feet.  
    * 5 levels: torrential rain or whiteout. Visibility is 5 feet.  

    Further levels are possible. All melee and missile rolls suffer a basic -10% penalty per level. After a number of rounds, movement will be decreased, especially if traveling on loose soil. This sphere can only be used in conjunction with clouds.  

    Cloud Sphere: Basic use of this sphere allows the mage to summon either clouds in the sky or fog on earth. For fog, visibility is the same as rain. If clouds, the amount of light will be cut in half for each level and the sight of the sun or stars will be blocked. Melee and missile attacks suffer a penalty of 5% for each level. This sphere must be cast if lightning or rain is to be cast.  

    Lightning Sphere: Basic use of this sphere creates lightning and thunder in the ZOE. Although there are no melee or missile modifiers, this will act as a considerable modifier against morale checks for animals and primitives. This can only be cast if clouds are present. The number of lightning flashes a minute is the square of the number of levels of the sphere.  

    Each level of each sphere will increase the cost of the spell by one spell level.  Traveling, which moves the center of the  ZOE with the caster, increases the level by +1.  

    The Extra ZOE modifier will double the radius of the ZOE for +1 level.  </description>
    <ZOE value='120' units='foot' type='sphere'/>
    <Range value='none' units=''/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='none'/>
	  <Properties>
	    <Property name='Level' value='1'>
          <Modifier type='Level' value='1' cost='1'></Modifier>
	    </Property>
	  </Properties>
  </spell>
  <spell name='Veil' level='morphic' category='Counterspells'>
    <description>This spell veils the target from various magical means of detection. Once veiled, a target can only be affected by spells  that it is veiled against, if the opposing caster wins a Level Contest against the adjusted level of the mage that cast the  Veil. If the target has been Attuned (page 56) by either the mage who cast the Veil, or the mage attempting to detect it,  that mage receives a 3 level bonus for purposes of the Level Contest. Veil takes 10 rounds to cast and is automatically  Concealed, so the spell only radiates magic only if two Level Contests are lost.  Target Base Level Protection Cost  Self 1 Detect Magic, ESP, Infravision, Range Finder, See Invisible +0  Other 2 Above + Locate, Clairsentience, Trace Summoning +1  Object 2 Above + Summon, Teleport +2  House 3 Attune +2  Castle 4 Clerical Detects +1  Space 3  Magic Item 3  Targets are defined as follows:  Self is the caster plus the caster's non-magical possessions.  Other is another living animal or plant.  Object is one object or group of related objects weighing up to 1000 lbs. and of volume up to 1 cubic yard.  House is any house sized structure of up to 3000 square feet of internal space.  Castle is any structure larger than a house.  Space is an area of up to 3000 square feet.  Magic Item is any magic item  Related objects must be very similar, like a group of coins, or objects in a container, such as a pack full of gear.  Veiled structures and spaces do not protect occupants inside. They are just veiled from these spells themselves. So if a  house was made Invisible and then veiled against See Invisible, it could not be detected by a see invisible but its invisible  occupants could be unless they were also veiled against See Invisible.  Protection is defined as follows:  Detect Magic through Trace Sending: the target is Veiled from these spells.  Summon: A mage attempting to Summon the target must win a Level Contest first.  Teleport: A mage attempting to Teleport to an attuned object must win a Level Contest first.  Attune: the target is veiled against a mage attempting to attune to it. The mage attempting to Attune to such an object  must first win a Level Contest against the Veil then overcome any defenses the target has as described in the Attune spell.  Clerical Detects: the target is veiled against all generic detect type prayers.  </description>
    <modifiers_description>Protect a being’s magic possessions as well as mundane items +1. Increase difficulty of penetrating the Veil by 1 level +0.5.</modifiers_description>
    <BaseLevels>
		  <Level name='Self' level='1'/>
		  <Level name='Other' level='2'/>
		  <Level name='Object' level='2'/>
		  <Level name='House' level='3'/>
		  <Level name='Castle' level='4'/>
		  <Level name='Space' level='3'/>
		  <Level name='Magic Item' level='3'/>
	  </BaseLevels>
    <ZOE value='1' units='being' type='/'/>
    <Range value='touch' units=''/>
    <Duration value='1' units='day'/>
    <SavingThrow value='none'/>
	  <Properties>
	    <Property name='Protection' value='Detect Magic' max='Summon' category='Counterspells'>
        <Modifier type='Protection' value='Detect Magic' cost='0'></Modifier>
        <Modifier type='Protection' value='Locate' cost='1'></Modifier>
        <Modifier type='Protection' value='Summon' cost='2'></Modifier>
	    </Property>
	    <Property name='Protect Attune' value='no' max='yes'>
        <Modifier type='ProtectAttune' value='no' cost='0'></Modifier>
        <Modifier type='ProtectAttune' value='yes' cost='2'></Modifier>
	    </Property>
	    <Property name='Protect Clerical' value='no' max='yes'>
        <Modifier type='ProtectClerical' value='no' cost='0'></Modifier>
        <Modifier type='ProtectClerical' value='yes' cost='1'></Modifier>
	    </Property>
	  </Properties>
  </spell>
  <spell name='Confuse' level='1' category='Combat'>
    <description>A confused creature will not be able to coordinate his actions with anyone else. (In the case of player characters, the players may not consult, and must submit orders in writing.) In addition there is a 1/3 chance each round that the creature will not be able to decide what to do that round, and thus will do absolutely nothing at all. Those creatures controlled by some outside source will not be affected, unless the controlling force also fails to save or fails to make other relevant control check. Only those of 4 HD or more will get saving throws. Those of 2 HD or less are affected immediately; others get a delay of d6 minus the level of the caster rounds.</description>
    <ZOE value='1' units='being' />
    <Range value='60' units='feet' />
    <Duration value='12' units='rounds' />
    <SavingThrow value='Will' effect='negates' />
    <Properties>
      <Property name='Duration' die='10' number = '0'>
        <Modifier type='ExtraEffect' die='10' number = '1' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Detect Magic' level='1' category='Senses'>
    <description>Detects magic in effect within range in LOS, be it spell or item (since an item is just an extended spell) or prayer. It does not detect Concealed Magic, nor reveal hidden or invisible objects. The caster sees a faint blue glow around anything magical that he could otherwise see. The spell offers no analysis, not even whether something is a spell or a prayer or a magic item, nor does it tell whether or not several of these sources are stacked on each other.</description>
    <ZOE value='self' type='described'/>
    <Range value='as sight' />
    <Duration value='10' units='minutes' />
    <SavingThrow value='none' />
  </spell>
  <spell name='Detect North' level='1' category='Senses'>
    <description>It lets the caster know which direction is true geographical North.</description>
    <ZOE value='self' type='described'/>
    <Range value='none' />
    <Duration value='90' units='minutes' />
    <SavingThrow value='none' />
    <Properties>
      <Property name='AffectsOthers' value='0'>
        <Modifier type='AffectsOthers' value='true' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Displace Image' level='1' category='Enhancement'>
    <description>Displaces the subject’s image against all forms of vision including Darksight. The subject gains the combat benefits of Partial Concealment from this (attacks are -3 to hit). Stealth and other effects of partial concealment are not provided, since the subject’s image is still in plain sight, just in the wrong place. Modifiers: Extra Effect (additional -1 modifier) +1. The defensive benefit can’t be made bigger than -5; this spell cannot provide Total Concealment.
    </description>
    <ZOE value='1' units='target'/>
    <Range value='touch' />
    <Duration value='10' units='minutes' />
    <SavingThrow value='Will negates' />
    <Properties>
      <Property name='Additional -1' value='-3' max='-5'>
        <Modifier type='ExtraEffect' value='-1' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Enhance Hearing' level='1' category='Senses'>
    <description>Adds +5 to Awareness for hearing rolls only.</description>
    <ZOE value='1' units='target'/>
    <Range value='10' units='feet'/>
    <Duration value='3' units='hours' />
    <SavingThrow value='none' />
  </spell>
  <spell name='Expeditious Retreat' level='1' category='Movement'>
    <description>Recipient’s running rate is doubled, with concomitant increase in jumping distance for running jumps. This is an enhancement bonus to the character’s running, equal to her normal running rate. It does not change the length of the combat step. No other movement type is affected. Although the name was coined by 1st level mages, there is no actual requirement that the extra movement be used to retreat. Modifiers: Affects Others is only a +1/2 modifier for this spell.</description>
    <ZOE value='1' units='target'/>
    <Range value='touch'/>
    <Duration value='10' units='minutes' />
    <SavingThrow value='Will negates' />
    <Properties>
      <Property name='AffectsOthers' value='false'>
        <Modifier type='AffectsOthers' value='true' cost='.5'></Modifier>
      </Property>
      <Property name='Increase multiplier' value='2'>
        <Modifier type='ExtraEffect' value='1' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Hold Portal' level='1' category='Environment'>
    <description>Holds closed a door, chest, panel, etc., which must be completely closed at the time of casting. A strongly anti-magical creature (e.g. Balrog) may shatter it. Dispel Magic (page 69) gets rid of it automatically, as does a Knock (page 67), which will open the door. Forcing the door open by brute strength requires a strength contest against the effective strength of the door’s construction. This is usually 25 for interior doors and 30 or more for gates but the GM may assign higher or lower values based on the condition of the door. Forcing the door destroys it.
    </description>
    <ZOE value='1' units='portal'/>
    <Range value='10' units='feet'/>
    <Duration value='1' units='hour' />
    <SavingThrow value='none' />
  </spell>
  <spell name='Lance of (Element)' level='1' category='Combat'>
		<description>Exists in three different elemental versions: Fire, Ice, and Lightning (elemental fire, water, and air respectively). The Earth element equivalent is the Magic Missile spell (see below). Each Lance creates the given element and flings it at a single target. The element is nonmagical once created, so there is no saving throw but the appropriate form of Resistance grants complete immunity. (Ice Lance is stopped by Resist Cold). The Lance spell is a ranged touch attack which ignores cover penalties, including the 'cover' provided by a friendly character in melee: therefore it can never hit an unintended target, though it can simply miss. The base damage is 2d6.</description> 
		<ZOE value='1' units='being' /> 
		<Range value='60' units='feet' /> 
		<Duration value='momentary' units='' /> 
		<SavingThrow value='none' effect='' /> 
		<Damage die='6' number='2'/>
		<Modifiers> 
			<Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
		</Modifiers>
    <Properties>
      <Property name='Increased Accuracy' value='0'>
        <Modifier type='IncreasedAccuracy' value='2' cost='.5'></Modifier>
      </Property>
    </Properties>
	</spell>
  <spell name='Mage Armor' level='1' category='Enhancement'>
    <description>Hardens the air around caster’s clothing into a resistant shell that follows the caster’s movements, granting an armor bonus of +6. Note that since this is an armor bonus, it does not stack with any armor worn but does stack with the Shield spell. Likewise, as an armor bonus it is cancelled by touch attacks. The “armor” is completely invisible and non-encumbering, and does not penalize the caster in any way.
    </description>
    <ZOE value='self' type='described'/>
    <Range value='none' />
    <Duration value='10' units='minutes' />
    <SavingThrow value='none' />
    <Properties>
      <Property name='Additional AC' value='6'>
        <Modifier type='ExtraEffect' value='1' cost='0.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Magic Missile' level='1' category='Combat'>
    <description>Magical missile(s) emanate from the caster’s fingers. Each missile hits and does damage exactly as if the caster had fired a +1 heavy crossbow bolt (Damage d10+1, Range Increment 100’, point-blank damage d10+2 within 30’). The Extra Range modifier increases the range increment, including point-blank range (which doubles from the base 30’ with each application). The base spell gives one missile, extra missiles are added as a modifier. Multiple missiles may be aimed at separate targets as long as all are within a 60 degree arc. Roll for each missile separately to see if it hits. A Shield (page 65) spell provides total defense.</description>
    <ZOE value='60' units='degrees' type='spread'/>
    <Range value='100' units='feet' />
    <Duration value='momentary' units='' />
    <SavingThrow value='none' effect='' />
    <Damage die='10' number='1' die_plus='1'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='10' number = '1' cost='1' />
    </Modifiers>
    <Properties>
      <Property name='Extra Damage' die='10' number = '0' die_plus='1'>
        <Modifier type='ExtraPlus' die='10' number = '0' die_plus='1' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Message' level='1' category='Communication'>
    <description>The Mage sends a telepathic message of up to 25 words per round to any recipient in range. There is no saving throw unless the recipient is trying to avoid the message. If the recipient is trying to avoid the message, Will negates. The message cannot be overheard, and background noise and Silence have no effect on it, although they may prevent the spell itself. If the recipient is attuned by the mage their save is halved, and the base distance is 1/2 mile instead of 480’
    </description>
    <ZOE value='1' units='person'/>
    <Range value='480' units='feet'/>
    <Duration value='1' units='round' />
    <SavingThrow value='none or Will negates' />
  </spell>
  <spell name='Range Finder' level='1' category='Enhancement'>
    <description>The recipient will know the precise range (but not velocity) of all objects which he can see. This gives +2 to hit on any ranged to-hit roll. This cancels a Range Loser, regardless of any disparity in bonus or duration. Modifiers: Extra Effect (+1 more bonus) 1/2 , Find Velocity (in addition to giving exact knowledge of objects’ direction and speed of movement, this grants an extra +2 to hit) +1.
    </description>
    <ZOE value='1' units='target'/>
    <Range value='touch' units=''/>
    <Duration value='10' units='minutes' />
    <SavingThrow value='Will negates' />
    <Properties>
      <Property name='Increased Accuracy' value='2'>
        <Modifier type='ExtraEffect' value='1' cost='.5'></Modifier>
      </Property>
      <Property name='Find Velocity' value='0' max='2'>
        <Modifier type='ExtraEffect' value='2' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Range Loser' level='1' category='Combat'>
    <description>
      If the single victim fails to save, he will be unable to accurately gauge distances. He will not be aware of this fact. The victim takes a penalty of 􀀀2 to all ranged attacks that make to-hit rolls. This cancels and is canceled by Range Finder (page 65) . Modifiers: Extra Effect (􀀀2 more penalty).
    </description>
    <ZOE value='1' units='target'/>
    <Range value='180' units='feet'/>
    <Duration value='90' units='minutes' />
    <SavingThrow value='Will negates' />
    <Properties>
      <Property name='Increased Accuracy' value='-2'>
        <Modifier type='ExtraEffect' value='-2' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Read / Write Languages' level='1' category='Senses'>
    <description>
      It gives the caster the ability to both read and write one specified language. He can do so as would an ordinary native. Optionally, the GM may require Extra Effect to read especially obscure languages.
    </description>
    <ZOE value='self' type='described'/>
    <Range value='as reading' units=''/>
    <Duration value='40' units='minutes' />
    <SavingThrow value='none' />
  </spell>
  <spell name='Read Magic' level='1' category='Senses'>
    <description>
      This spell is used to read magical writings. Magical writing appears to the unaided eye as meaningless constantly shifting and changing blue script. This spell enables the caster to read this magical script. It is not necessary to use this spell to cast a spell off a scroll.
    </description>
    <ZOE value='caster' type='described'/>
    <Range value='as sight' units=''/>
    <Duration value='90' units='minutes' />
    <SavingThrow value='none' />
  </spell>
  <spell name='Shield' level='1' category='Enhancement'>
    <description>
      The recipient becomes completely immune to Magic Missile (page 64), counts as having an interposed shield (half damage) against Ice Storm (page 73). Against attacks that must roll to hit, the spell grants a +4 shield bonus.
    </description>
    <ZOE value='1' units='target'/>
    <Range value='touch' />
    <Duration value='10' units='minutes' />
    <SavingThrow value='Will negates' />
    <Properties>
      <Property name='Additional Bonus' value='4'>
        <Modifier type='ExtraEffect' value='1' cost='0.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Sleep' level='1' category='Combat'>
    <description>A Sleep spell can affect a maximum of 12 HD of creatures within the ZOE. The spell will affect the lowest-level targets first, continuing until the strength of the spell is used up or no more creatures can be affected. Larger creatures use up spell strength disproportionately: beings of 1 or 2 HD count at face value, but a 3HD creature counts as 4, 4HD counts as 8, 5HD counts as 16, etc. Undead or other non-living entities can not be slept regardless of level, nor can living organisms that do not naturally sleep. Creatures that fail their save cannot be awakened by non-magical means for 10 rounds. If they are not disturbed they will sleep for 2 hours.
    </description>
    <ZOE value='60' units='foot' type='cone'/>
    <Range value='Always zero' units='' />
    <Duration value='see description' units='' />
    <SavingThrow value='Will' effect='negates' />
    <Properties>
      <Property name='Duration' value='10' units = 'rounds'>
        <Modifier type='ExtraDuration' value='5' units = 'rounds' cost='.5'></Modifier>
      </Property>
      <Property name='Extra Effect' value='12' units = 'HD'>
        <Modifier type='ExtraEffect' value='3' units = 'HD' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Swim' level='1' category='Movement'>
    <description>This spell allows the recipient to swim at 60’ per round. The recipient may stay afloat or stay under water as he desires. It does not give the ability to breathe underwater.
    </description>
    <ZOE value='1' units='target'/>
    <Range value='touch' units=''/>
    <Duration value='40' units='minutes' />
    <SavingThrow value='Will negates' />
    <Properties>
      <Property name='Extra Speed' value='60' units = 'feet per round'>
        <Modifier type='ExtraSpeed' value='30' units = 'feet per round' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Telescopic Vision' level='1' category='Senses'>
    <description>While the spell lasts, recipient may switch at will between normal vision and up to x6 magnification. This is only useful to scrutinize a particular location at any given moment: trying to "scan" with the magnified view produces only a headache-inducing blur.</description>
    <ZOE value='1' units='target' />
    <Range value='touch' units='' />
    <Duration value='3' units='hours' />
    <SavingThrow value='Will' effect='negates' />
    <Properties>
      <Property name='Extra Effect' value='6' units = 'x'>
        <Modifier type='ExtraSpeed' value='3' units = 'x' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Trip' level='1' category='Combat'>
    <description>
      This spell knocks the victim prone if he fails his save. Prone combatants are at a disadvantage. It usually takes one round to get up. This spell has a built-in saving throw penalty.
    </description>
    <ZOE value='1' units='biped' />
    <Range value='120' units='feet' />
    <Duration value='momentary' />
    <SavingThrow value='Reflex at -3' effect='negates' />
  </spell>
  <spell name='Ventriloquism' level='1' category='Communication'>
    <description>The mage may make the sound of his voice come from somewhere else up to the spell range distant. He may also use it to imitate the voices of others. The difference will not be detected if he has heard the voice before. This function of the spell may be used in conjunction with Magic Mouth (page 67), Long Talk (page 67), or Disguise (page 69).
    </description>
    <ZOE value='self' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='40' units='minutes' />
    <SavingThrow value='none' />
  </spell>
  <spell name='Air Blast' level='2' category='Combat'>
    <description>It produces a damaging blast of air, which does 2d6 points of damage to those in the area, unless they save. It does not affect non-corporeal creatures or air-based creatures. It has half effect on creatures currently flying, but knocks them back 30’ if they are smaller than man-sized. If they hit an obstacle in this distance, they take full damage.
    </description>
    <ZOE value='60' units='foot' type='cone'/>
    <Range value='always zero' units='' />
    <Duration value='momentary' />
    <SavingThrow value='Reflect' effect='negates' />
    <Damage die='6' number='2'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
    </Modifiers>
  </spell>
  <spell name='Concentrate' level='2' category='Enhancement'>
    <description>Caster gains +2 to his concentration check. </description>
    <ZOE value='caster' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Plus To Concentration Check' value='2' units = ''>
        <Modifier type='ExtraEffect' value='1' units = '' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Darkvision' level='2' category='Senses'>
    <description>Recipient gains 60' Darkvision. If recipient already has Darkvision, its range is extended by 60'. This applies  even if the extant Darkvision is a magical effect rather than innate. </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='12' units='hours'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Distance' value='60' units = 'feet'>
        <Modifier type='ExtraEffect' value='60' units = 'feet' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='ESP' level='2' category='Senses'>
    <description>ESP allows the caster to know what another being is currently consciously thinking. If the victim saves, then he will  know some spell has been thrown at him. It may be targeted on a single visible or attuned individual, or at the ZOE.  If ESP is targeted at a visible person the saving throw is 20% worse for the victim.  If ESP is targeted at a person attuned by the mage their saving throw is halved and the base range is 480'.  If ESP is targeted at the ZOE, the ZOE does not need to be in sight. A single victim is chosen at random from among  any possible victims.  The range limit applies only at casting time; the spell continues if the target wanders out of range. This is a Detect-type  spell.  </description>
    <ZOE value='60' units='foot' type='sphere'/>
    <Range value='120' units='feet'/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='AffectsOthers' value='false'>
        <Modifier type='AffectsOthers' value='true' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Faerie Fire' level='2' category='Environment'>
    <description>It surrounds all objects or creatures within the ZOE with a pale blue glow. It will outline invisible objects or creatures. Creatures and objects continue to glow if they leave the ZOE.  </description>
    <ZOE value='30' units='foot' type='cube'/>
    <Range value='60' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Faux Magic' level='2' category='Environment'>
    <description>This spell does nothing, but it detects as another spell. Only spells that the mage could cast can be faked. This  ruse will affect Detect Magic (page 63), Observe Magic (page 67), and Analyze Spell (page 68). A Level Contest is  needed to see the truth. If the Faux Magic is concealed, then the fake spell will appear concealed, and its concealment  must be overcome before the fact that it is fake can be tested. The fake spell cannot be concealed unless Faux Magic  is concealed. Targeting a Dispel Magic (page 69) against the spell the Faux Magic appears to be, rather than the Faux  Magic, will always result in failure.  </description>
    <ZOE value='Spell’s ZOE' type='described'/>
    <Range value='touch' units=''/>
    <Duration value='1' units='day'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Fire Bomb' level='2' category='Combat'>
    <description>It does fire damage of 2d6 to all in the ZOE who fail to save. </description>
    <ZOE value='10' units='foot' type='burst'/>
    <Range value='60' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='negates'/>
    <Damage die='6' number='2' />
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number='1' cost='.5' />
    </Modifiers>
  </spell>
  <spell name='Flame Weapon' level='2' category='Enhancement'>
    <description>Allows the caster to cause any edged weapon to flame along its edge. This grants an enhancement bonus to  hit and damage in combat: +1 against most targets, +2 against Trolls, +3 against Undead and plant creatures. No bonus  is gained against beings with any level of Fire Resistance. Note that magical weapons also have enhancement bonuses,  so the two do not stack if this is cast on a magical weapon. The flame can be turned on and off by the wielder as a free  action for as long as the spell lasts, if for example the wielder wishes to sheathe the weapon. If cast on a weapon held  or carried by another, it automatically fails unless the weapon's wielder is willing for it to take effect. Modifers: Extra  Effect (additional +1 vs all targets) +1  2 . For this spell, the Concealment modifier makes the weapon look like a magical  weapon instead of a spell.  </description>
    <ZOE value='1' units='weapon' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='none/willing'/>
  </spell>
  <spell name='Invisibility' level='2' category='Appearance'>
    <description>It makes something not visible, including to those using Darkvision. The spell will be broken the instant that the  recipient: completes casting a spell, actively uses a magical device, opens a door, becomes immersed in water, engages  in melee, attempts to grapple, or fires a missile. The recipient may always break the spell if he chooses. If a being is  made invisible, objects he is carrying at the time become invisible. A group of related objects (as a pile of coins) may  be treated as one object, but the object, being, or objects must fit in the ZOE. An illusion, or an object concealed by an  illusion, cannot be made invisible. </description>
    <modifiers_description>Extra Effect (Improved) +1: Improved Invisibility is not broken by any  of the above conditions, although note that many of them will give the invisible character’s location away. The invisible  character can fight while invisible. Combat does strain the spell: any round in which the recipient attacks counts as 10  minutes towards the spell’s duration. The invisible character can still dismiss her invisibility at will.
    </modifiers_description>
    <ZOE value='One target that fits into a 10’ cube' units='' type='described'/>
    <Range value='10' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='willing only' effect=''/>
    <Properties>
      <Property name='AffectsOthers' value='false'>
        <Modifier type='AffectsOthers' value='true' cost='1'></Modifier>
      </Property>
      <Property name='Improved Invisibility' value='no' units = '' max='yes'>
        <Modifier type='Improved' value='yes' units = '' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Knock' level='2' category='Environment'>
    <description>It opens a magically held door without breaking the spell. Normally, no Level Contest is required. It will open  mechanically locked objects but will not affect barred doors or portcullises.  </description>
    <ZOE value='1' units='portal,' type='chest,'/>
    <Range value='10' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Levitation' level='2' category='Movement'>
    <description>It allows the recipient to levitate himself, vertically only. He may lift up to 50 pounds besides his own weight.  Maximum Height: 100'. Maximum Vertical Speed: 60' per round. If cast on a falling creature, it will coast to a halt over  one round during which it will descend a total of 60' or as far as it had already fallen, whichever is less. </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Weight' value='50'>
        <Modifier type='Extra Weight' value='200' cost='1' />
      </Property>
      <Property name='Extra Ceiling' value='100' units='feet'>
        <Modifier type='Extra Ceiling' value='100' units='feet' cost='.5' />
      </Property>
      <Property name='Extra Speed' value='60' units='feet per round'>
        <Modifier type='Extra Speed' value='60' units='feet per round' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Long Talk' level='2' category='Communication'>
    <description>The caster may send a verbal message of up to 25 words in length per round. A magical mouth will appear and  speak the message at the place specified by the caster, who must specify exact distance and direction from his present  location.  </description>
    <ZOE value='special' units='' type='described'/>
    <Range value='5' units='miles'/>
    <Duration value='1' units='round'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Magic Hand' level='2'>
    <description>It creates a small humanoid hand that can hold up to 300 lbs. of stress. (So for example, one could cast a rope  to it, and then scale the rope.) The hand is unable to grasp an unwilling living object. The location of the hand remains  fixed, although it can open and close, and rotate around a fixed point. Controlling the Hand requires the same degree of  attention as controlling one's natural hands; spell concentration is not required. The caster can control the hand from any  range once it has been created, but of course will not know what is going on at the hand's location if he gets too far away.  </description>
    <ZOE value='special' units='' type='described'/>
    <Range value='10' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Weight' value='300' units='lbs'>
        <Modifier type='Extra Effect' value='150' units='lbs' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Magic Mouth' level='2' category='Communication'>
    <description>The spell is cast on an object. At a later time the object will deliver the specified message once. A mouth  will appear on the object or the spell will use the mouth of the object if it has one, and it will speak the message in  the caster's voice. The message may be up to 25 words long per round. The speaking time is 1 round, extendable with  Extra Duration. The conditions under which the Magic Mouth will speak are limited to those that the caster, if physically  present, could answer using non-magical means, plus any spells (detects, locates, etc.) that are placed upon the Magic  Mouth. This may be done by the caster of the Magic Mouth as if casting upon himself, or by other mages or clerics or  from items as if casting on a willing character that had dropped his saving throws. A spell of Lasting Duration will be  effective until the mouth speaks, others will cease to be effective when their duration is up, possibly making triggering  of the Magic Mouth impossible.  </description>
    <ZOE value='special' units='' type='described'/>
    <Range value='10' units='feet'/>
    <Duration value='until it speaks' units=''/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Mini-Flash' level='2' category='Combat'>
    <description>May be thrown at one creature. A small bright flash will go off in front of all its eyes. Unless a Reflex save is  made, it is blind for 2d4 rounds. If its eyes are closed or covered, give +4 on the saving throw. This spell is useless vs.  creatures that do not use eyes, such as Undead or bats. </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='60' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='negates'/>
    <Properties>
      <Property name='Extra Duration' die='4' number = '2'>
        <Modifier type='Extra Effect' die='4' number = '1' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Mirror Image' level='2' category='Enhancement'>
    <description>The mage creates d4 images of himself, randomly distributed around him within the ZOE, which are indistinguishable  from him and appear to do exactly what he does. Any attack (melee, missile, or single target damage spell)  upon an image will dispel it, whether the attack would have been successful or not. </description>
    <ZOE value='10' units='foot' type='circle'/>
    <Range value='none' units=''/>
    <Duration value='6' units='rounds'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Images' value = '0' units = 'images'>
        <Modifier type='Extra Effect' value='1' units = 'images' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Observe Magic' level='2' category='Counterspells'>
    <description>Per Detect Magic (page 63), but it forces a Level Contest to detect Concealed Magic and reveals whether  enchantment comes from spell, prayer, enchanted item, or is an innate ability of some creature. It does not reveal the  alignment of a prayer.  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Pain' level='2' category='Combat'>
    <description>This spell inflicts wracking pains, causing 3d6 points of damage unless the victim saves vs. Will.</description>
    <ZOE value='1' units='target' />
    <Range value='120' units='feet' />
    <Duration value='momentary' />
    <SavingThrow value='Will' effect='negates' />
    <Damage die='6' number='3'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
    </Modifiers>
  </spell>
  <spell name='Prot / Enchanted Monster' level='2' category='Enhancement'>
    <description>Gives the recipient +4 to AC and saving throws against Enchanted Monsters, (Elementals, Golems,  Invisible Stalkers, Aerial Servants, Djinns, Efreets, Homunculi, Salamanders, Summoned Demons, Angels, Simulacra,  and undead.) This is an “unnamed” bonus that adds to all other kinds of bonuses. </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
  </spell>
  <spell name='Quickmarch' level='2' category='Movement'>
    <description>The recipient of this spell has all natural movement rates doubled. This is an enhancement bonus to each  movement mode, equal to its base value. Quickmarch does not change your combat step distance. It does not interact  with any magically endowed movement ability, e.g. Fly spell. </description>
    <ZOE value='1' units='recipient' type=''/>
    <Range value='30' units='feet'/>
    <Duration value='3' units='hours'/>
    <SavingThrow value='Reflex' effect='negates'/>
  </spell>
  <spell name='See Invisible' level='2' category='Senses'>
    <description>Recipient can see any invisible objects or beings within her normal LOS. Telescopic vision applies normally:  this is an enhancement to the recipient's vision, not a detection spell that reports visually.  </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
  </spell>
  <spell name='Weakness' level='2' category='Combat'>
    <description>The victim must save or lose 2d4 strength. A person whose Str is reduced to 0 cannot move. Str cannot be reduced  below 0. </description>
    <ZOE value='1' units='person' type=''/>
    <Range value='120' units='feet'/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='Fortitude' effect='negates'/>
  </spell>
  <spell name='Web' level='2' category='Combat'>
    <description>This may be cast as a wall between two (or more) anchors giving vertical support, or as a horizontal circle which settles  over its targets and then onto the ground. As a wall, it is up to 20' tall by up to 100' wide by 6” thick: as a circle, it  covers a circle of ground 30' in radius and coats everything therein in a 6” thick layer. Characters in danger of being  caught by the web as it forms may make a Reflex save to avoid the strands: thereafter a vertical wall will still entrap  anyone who blunders into it, but the horizontal circle will be coated with dirt after 1 round and can no longer catch new  victims. Entrapped characters roll d20 + Str damage bonus each round, accumulating their total: when the total passes  10' the thickness in inches, they are free. A character who had a bladed weapon already in hand can add the weapon  damage to this accumulating roll each round. The web strands are highly flammable: a torch, or a flaming weapon,  can slash someone free in a single round. A trapped character cannot use missile weapons, nor use melee weapons  except to cut the web. Trapped casters are considered to be bound; this does not impede clerics (unless they have also  been gagged), but mages and guardians must use the Reduced Gestures modifier (at the “No Gestures” level) to cast  spells. Trapped characters can use natural abilities not requiring movement. Attacks against trapped characters are at +2:  trapped characters lose their Dex bonuses and all dodge bonuses. </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='Reflex' effect='negates'/>
    <Properties>
      <Property name='Extra Thickness' value = '6' units = 'inches'>
        <Modifier type='Extra Effect' value='6' units = 'inches' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Wizard Lock' level='2' category='Environment'>
    <description>Wizard Lock holds closed a door, chest, drawer, etc., which must be completely closed at the time of casting.  A strong anti-magical creature (e.g. a Balrog) may shatter it. A Knock spell will automatically open it unless it is also  physically barred. A mage three levels higher than the caster, or the caster himself, will not be affected by the spell.  Forcing the door open by brute strength requires a strength contest against a difficulty representing the strength of the  door's construction. This difficulty is usually 25 for dungeon and castle doors but the GM may assign higher or lower  values based on the condition of the door. Forcing the door destroys it. </description>
    <ZOE value='1' units='portal' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='4' units='days'/>
    <Properties>
      <Property name='Hard to Knock' value = 'no' units = ''>
        <Modifier type='Hard to Knock' value='yes' units = '' cost='2' />
      </Property>
      <Property name='Extra People' value = '0' units = 'people'>
        <Modifier type='Extra People' value='1' units = 'person' cost='0.5' />
      </Property>
    </Properties>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Analyze Spell' level='3' category='Counterspells'>
    <description>Analyze Spell reveals the presence of all spells, prayers, and items per Observe Magic (page 67). A Level  Contest is required to analyze a spell cast with Concealed Magic. It completely analyzes all detected mage spells, telling  base spell and modifiers (but not caster's level, nor whether the modifiers come from the caster or from an item). Also, in  Breath Phase, the caster learns what mage base spells or morphic spells are being prepared in that round, and will know  in which phase (if any) the spell is being cast. He will get this information before he announces his action for that phase.  If two Analyze Spells are in effect, the higher-level caster will know what the lower-level caster is doing.  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Blindsight' level='3' category='Senses'>
    <description>Recipient gains a Blindsight ability based on acute hearing and sensitivity to vibration and air currents. The base range is 60'. If the character has enhanced hearing, either naturally or through a spell/prayer, the base range is 90'. If  the caster is deafened or inside the ZOE of Silence, range is halved. If the caster is wearing medium or heavier armor,  range is halved. (If both conditions apply, Blindsight is effectively negated – range is zero.) </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Sight Range' value='60' units='feet'>
        <Modifier type='Extra Effect' value='30' units='feet' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Clairsentience' level='3' category='Senses'>
    <description>This spell enables the caster to tap into one sense, usually sight or hearing, of another being, thus sensing  what that being is sensing. He will sense with the abilities of that being, but will not gain any control over what is being  sensed. It may be targeted on a single visible or attuned individual, or at the ZOE.  If it is targeted at a visible person the saving throw is normal for the target.  If it is targeted at a person attuned by the mage their saving throw is halved and the base range is 1  2 mile.  If it is targeted at the ZOE, the ZOE does not need to be in sight. A single victim is chosen at random from among any  possible targets.  The ZOE will center on the target thereafter, and the caster may switch to any other target within the ZOE. Of course,  the new target gets a saving throw, which will end the spell if successful. The caster's own sense will be almost nil while  employing the spell, but he may turn it off and then turn it on again. Clairsentience may be cast simultaneously with ESP  for the sum of the spell point cost, without taking any additional time. </description>
    <ZOE value='30' units='foot' type='sphere'/>
    <Range value='240' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Sense' value='1' units=''>
        <Modifier type='Extra Effect' value='1' units='' cost='1.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Cool Object' level='3' category='Environment'>
    <description>It takes 10 rounds to cast and then begins cooling one solid object to about 30 degrees Fahrenheit in 20 rounds. The  maximum weight is 300 pounds. It may also be used to freeze water; it will produce a maximum of 3000 cubic feet of  ice. If someone is in contact with a metal object being cooled, like metal armor, give them damage as in heat object, as  well as a 1/6 chance per round that it will stick to his skin, doing d8 hit points when it is removed. This spell does not  grant the wearer a save when cast on an object being worn. </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='120' units='feet'/>
    <Duration value='12' units='hours'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Mass' value='300' units='lbs'>
        <Modifier type='Extra Effect' value='150' units='lbs' cost='0.5' />
      </Property>
      <Property name='Extra Water' value='3000' units='cubic feet'>
        <Modifier type='Extra Effect' value='1500' units='3000' cost='0.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Disguise' level='3' category='Appearance'>
    <description>The caster may change the recipient's appearance so that he looks like someone else. (The being imitated must be a  member of a humanoid species of similar size.) There is no saving throw against being fooled. In order to fool a member  of the species concerned that the recipient is a particular member of that species, the caster must be a member of that  species himself, and the caster must have had a chance to carefully study the model. This spell only affects visual details.  Ventriloquism (page 65) may be used to imitate voices.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='1' units='day'/>
    <SavingThrow value='none/willing'/>
  </spell>
  <spell name='Dispel Magic' level='3' category='Counterspells'>
    <description>This spell permanently breaks magical spells and prayers. It cannot be used on items. A mage is always  successful against enchantments that he cast himself; this ability can never be transferred with the Affects Others modifier.  Otherwise determine success by a level contest. Attempts against prayers take a ..3 penalty to the caster's level in this  contest (and see page 87 for details on dispelling a Mass or Touch prayer). If the mage fails to dispel a permanent  enchantment, he does not get a second chance until he makes his next level. The spell will exorcise possessions by  demons, Magic Jar, etc., but it takes a ..3 level penalty.  </description>
    <ZOE value='10' units='foot' type='sphere'/>
    <Range value='60' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='level' effect='contest'/>
  </spell>
  <spell name='Explosive Runes' level='3' category='Environment'>
    <description>The mage inscribes a scroll or book with protective runes to keep it from being read by other than a specified  list of people. The maximum number of people on this list is the Int score of the caster. If the reader is not one of the  persons named when the spell was cast, then the runes will explode. This destroys the book or scroll and does 6d6  damage to the reader and anyone else within reading distance of the Runes. The Runes may be detected by a Mage only  if they declare they are searching for explosive runes and they win a Level Contest against the caster. Explosive Runes  are automatically concealed against detection magic at no extra cost. They can only be triggered by a deliberate effort  to read the protected text: waving a Rune-written sheet in front of somebody's face is not a viable combat tactic. Runes  can be incorporated on an inscription written or carved into a wall, door, chest, or other object more durable than a scroll  or book; in this case the object bearing the runes is not destroyed. Whoever triggered the explosion by reading the runes  does not get a saving throw: others within the burst radius get a Reflex save. </description>
    <ZOE value='1' units='inscription/10’' type='burst'/>
    <Range value='must handle' units='' type='described'/>
    <Duration value='until triggered' units='' type='described'/>
    <SavingThrow value='none/Reflex' effect='negates'/>
    <Damage die='6' number='6'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
    </Modifiers>
  </spell>
  <spell name='Fireball' level='3' category='Combat'>
    <description>This creates an explosion of magical fire. The explosion loses power as it expands. The inner layer is a 10' spread  from the origin point: all in this area take 5d6. The outer layer is a 20' spread from the origin (that is, all distances  between 10' and 20' along the “spread path”) and does half the damage of the inner layer. In either layer a Fortitude save  cancels the damage. The magical fire does not ignite loose combustibles, but will destroy any scrolls on a person killed  by it. It will do damage to inanimate objects if it exceeds their Hardness rating. </description>
    <ZOE value='20' units='foot' type='sphere'/>
    <Range value='120' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Fortitude' effect='negates'/>
    <Damage die='6' number='5'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '3' cost='1' />
    </Modifiers>
  </spell>
  <spell name='Fly' level='3' category='Movement'>
    <description>Recipient can fly through the air. Maximum speed is 180' / round, of which 60' can be vertical. The recipient can carry  besides his own weight, 50 pounds of encumbrance. Flying requires the same concentration as walking. Hovering allows  casting. Combat steps are allowed. Note that, regardless of height, visibility is generally limited to 20 miles or so because  of natural haze, etc. Ceiling: 1000' above ground level. </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Speed' value='180' units='feet per round'>
        <Modifier type='Extra Speed' value='120' units='feet per round' cost='.5' />
      </Property>
      <Property name='Extra Weight' value='50'>
        <Modifier type='Extra Weight' value='200' cost='1' />
      </Property>
      <Property name='Extra Ceiling' value='1000' units='feet'>
        <Modifier type='Extra Ceiling' value='1000' units='feet' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Haste' level='3' category='Enhancement'>
    <description>The recipient of the spell acts twice in each round. The extra actions may be taken in the same or different phases, and  actions may be delayed as normal (for example, a Hasted Hero who is out of reach of all foes may delay his Combat  strike until movement so as to run into battle and swing, all in the same round). Haste does not allow mages to cast  spells faster, nor clerics to pray faster, although both casting and praying can be combined with other actions. Mages  and guardians can take an extra action that would fall between the declaration and the completion of spellcasting only by  delaying that action until after spell phase.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='30' units='feet'/>
    <Duration value='5' units='rounds'/>
    <SavingThrow value='Reflex' effect='negates'/>
  </spell>
  <spell name='Heat Object' level='3' category='Environment'>
    <description>It takes 10 rounds to cast. It then begins heating one solid non-living object to about 200 degrees Fahrenheit in 20  rounds. The maximum weight is 300 pounds, extendable with the Extra ZOE modifier. Cast on metal armor will give the  recipient wearing the armor (T / 5)..d4 (rounded to the nearest non-negative number) hit points of damage per round. T  is the number of rounds since the heating began if less than 20, and 20 otherwise. This spell does not grant the wearer a  saving throw to negate a spell cast on an object being worn.  </description>
    <ZOE value='1' units='object*' type=''/>
    <Range value='120' units='feet'/>
    <Duration value='12' units='hours'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Implosion' level='3' category='Combat'>
    <description>The caster attempts to crush one victim, for 5d4 damage. This will not work against non-corporeal beings or  beings without solid bodies such as Giant Slugs, Water Elementals, Ochre Jellies, etc. This can damage inanimate  objects, constructs, corporeal undead, etc. </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='120' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Fortitude' effect='half'/>
    <Damage die='4' number='5'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='4' number = '3' cost='1' />
    </Modifiers>
  </spell>
  <spell name='Invade Dreams' level='3'>
    <description>The caster can send a message to a sleeping person. The sleeper will have a vivid, lucid dream of the caster  appearing and speaking the message, and will remember the dream with full accuracy upon awakening. Caster and  sleeper need not have a common language: the dreaming mind will translate the caster's intended meaning. The caster  has no control over the sleeper's dream environment and cannot, in particular, make the dream threatening or frightening  (although the content of the message can be). The caster does not perceive the sleeper's dreamscape or reactions and  does not get any return information from the sleeper. While there is no saving throw for the spell itself, a sleeper who  does not wish to listen to the caster any more can terminate the spell at any time with a successful Will save. Time spent  in an invaded dream still counts normally as restful sleep for all purposes. If external phenomena awaken the sleeper  during the invaded dream, any remaining part of the message is lost. The sleeper may choose to awaken at the dream's  end. The caster may send a message up to 10 minutes long, although most useful messages are shorter.  The spell may be cast on a visible sleeping target, or on a location where someone is known to be sleeping, with 240'  range (LOS not required when casting at a location). It may be cast on a person known to the caster with a range of up to  10 miles. It may be cast on a person to whom the caster is attuned at any range. If the target is not asleep at the time of  casting, the spell automatically fails (but still costs full points).  </description>
    <ZOE value='1' units='sleeping' type='target'/>
    <Range value='see description' units=''/>
    <Duration value='max 1 hour' units=''/>
    <SavingThrow value='Will special' effect=''/>
  </spell>
  <spell name='Invisibility Sphere' level='3' category='Appearance'>
    <description>An expanded version of Invisibility (page 66). The spell affects a 10' emanation centered on the caster.  All objects and beings within the ZOE at the time of casting (including the caster) become invisible; other beings that  enter the ZOE later do not. The whole spell is broken if the caster does anything that would normally turn him visible (per  Invisibility). The spell ends if the caster dies. Even if the spell remains up, others become visible if they do something  that would normally turn them visible, or if they are no longer within 10' of the caster. (If you move outside of the 10'  r moving back inside does not help). The group of invisible beings cannot see each other unless they have some means  of seeing invisible. </description>
    <ZOE value='10' units='foot' type='emanation'/>
    <Range value='none' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Improved Invisibility' value='no' units = '' max='yes'>
        <Modifier type='Improved' value='yes' units = '' cost='1'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Lightning Bolt' level='3' category='Combat'>
    <description>A line of electricity stretches from the caster's hand to the limit of the spell's effect, doing 5d6 electrical  damage to all it touches. Targets get a Reflex save to dodge. Lightning bolts do not bounce or ricoshet and always  proceed in a straight line from the caster. </description>
    <ZOE value='line 120’ x 1’' units='' type='described'/>
    <Range value='always' units='zero'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='negates'/>
    <Damage die='6' number='5'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '3' cost='1' />
    </Modifiers>
  </spell>
  <spell name='Prot / Normal Missiles' level='3' category='Enhancement'>
    <description>The recipient gains DR 10/+1 against missiles only. Missiles of +1 or better bonus negate the protection.  Extra Effect (+5 DR amount) +1  2 ; Extra Effect (increase required bonus to penetrate DR by +1) +1  2 .  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra DR' value='10' units = ''>
        <Modifier type='Extra Effect' value='5' units = '' cost='0.5'></Modifier>
      </Property>
      <Property name='Extra Bonus' value='1' units = ''>
        <Modifier type='Extra Effect' value='1' units = '' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Rope Trick' level='3' category='Movement'>
    <description>The spell enables the caster to throw a rope (of length 6' to 24') in the air and have it stand upright. Any who  climb the rope to the top will vanish into a tiny “pocket universe”. This pocket universe is only big enough to comfortably  hold 4 people. It has breathable air but no natural light. The rope may be pulled up into the pocket universe. When the  spell ends anything in the pocket universe finds itself back in the normal plane at the appropriate height above ground.  </description>
    <ZOE value='special' units='' type='described'/>
    <Range value='24' units='feet'/>
    <Duration value='3' units='hours'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='More People' value='4'>
        <Modifier type='Extra Effect' value='2' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Second Sight' level='3' category='Senses'>
    <description>Allows the recipient to see perfectly normally without the use of his eyes, or the need for any light. Unlike  Darkvision, full color vision is preserved and there is no range limit. This spell does work in a clerical darkness.  </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
  </spell>
  <spell name='Slow' level='3' category='Combat'>
    <description>Slow prevents the affected creature from acting in consecutive rounds. A successful Haste will negate a Slow.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='120' units='feet'/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='Reflex' effect='negates'/>
  </spell>
  <spell name='Slow Motion' level='3' category='Movement'>
    <description>All beings in the ZOE who fail their saving throws have their movement rate halved. Slow (page 71) supercedes  Slow Motion while they are both in effect. A successful Swiftness (page 133), Quickmarch (page 68), or Expeditious  Retreat (page 64) negates a Slow Motion.  </description>
    <ZOE value='30' units='foot' type='sphere'/>
    <Range value='120' units='feet'/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='Reflex' effect='negates'/>
  </spell>
  <spell name='Snowball' level='3' category='Combat'>
    <description>Does 5d4 cold damage throughout the ZOE. This spell will destroy any potions on a person killed by it. </description>
    <ZOE value='20' units='foot' type='sphere'/>
    <Range value='120' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Fortitude' effect='negates'/>
    <Damage die='4' number='5'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='4' number = '3' cost='1' />
    </Modifiers>
  </spell>
  <spell name='Suggestion' level='3'>
    <description>If the single target fails to save (vs. Will), then the caster may make one suggestion to him. The caster applies  his Charisma bonus as an extra modifier to the save DC. (That is, for a mage the final save DC will be 10 + casting level  + Int mod + Cha mod, while for a guardian it will be 10 + casting level + 2x Cha mod). The suggestion must be short  and simple. Its phrasing is incorporated into the spell incantation and the target will be aware of it even if she makes her  saving thow. The victim who fails the Will ST will then follow the suggestion, provided it is something that he might  conceivably have chosen to do herself. A victim who fails to save will have no memory of the spell's having been cast  on him and will believe her actions were her own idea. </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='120' units='feet'/>
    <Duration value='2' units='weeks'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Lasting Duration' value='no' max='yes'>
        <Modifier type='Lasting Duration' value='yes' cost='2' />
      </Property>
    </Properties>
  </spell>
  <spell name='Water Breathing' level='3' category='Movement'>
    <description>It allows the recipient to breathe under water, as if he were in air. It does not allow free underwater  movement but does allow speech.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
  </spell>
  <spell name='Blinding Flash' level='4' category='Combat'>
    <description>The caster may create a flash of light. All within the ZOE must save or be temporarily blinded. Those who  are facing away from the center or who have their eyes closed, get +5 to save. </description>
    <ZOE value='20' units='foot' type='burst'/>
    <Range value='60' units='feet'/>
    <Duration value='2d6' units='rounds'/>
    <SavingThrow value='Reflex' effect='negates'/>
    <Properties>
      <Property name='Extra Duration' die='6' number='2' >
        <Modifier type='Extra Duration' die='6' number='1' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Chain Lightning' level='4' category='Combat'>
    <description>Identical to Lightning Bolt (page 71) except that the base damage is 6d6, the line of lightning may swerve  through any desired angle after every 10' segment, and targets still take half damage even if they save. Although this  flexible ZOE can bend around to cross the same spot multiple times, any one target is only in the ZOE once, and only  affected once, no matter how many times the line crosses it. </description>
    <ZOE value='120' units='foot' type='line'/>
    <Range value='always zero' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='half'/>
    <Damage die='6' number='6'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '3' cost='1' />
    </Modifiers>
  </spell>
  <spell name='Cold Cone' level='4' category='Combat'>
    <description>Does 6d4 points of magical cold damage to all within the ZOE who fail to save. A save results in half damage.  </description>
    <ZOE value='60' units='foot' type='cone'/>
    <Range value='always zero' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='half'/>
    <Damage die='4' number='6'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='4' number = '3' cost='1' />
    </Modifiers>
  </spell>
  <spell name='Control Self' level='4' category='Enhancement'>
    <description>This spell protects the caster from mental attacks, giving complete immunity to Fear, Confusion, and to any  paralyzing or immobilizing attack that has a Will save. The caster takes only half damage from crushing attacks. Finally  the caster can make his body do amazing feats, such as hold his breath for extended periods of time, stop his heartbeat,  hold objects with an iron grip, seal his ears, etc. (He can't perform actions physically impossible for his body).  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Dimension Door' level='4' category='Movement'>
    <description>The mage creates a pair of opaque vertical doors 6' wide and 8' tall. The mage must designate one of the  doors as the origin and the other as the destination. Creatures or objects that enter the origin door immediately come out  the destination door. If part of the destination door is blocked, that part of the origin door is also blocked. Once part of  the creature or object is through the rest will be drawn through as well. Objects that can not fit through the unblocked  portions of the doors will bounce back out of the origin door. One of the doors must be within 10' of the mage at the  time of casting and not touching a being. The other may be up to the spell range away. The location of the doors must be  specified in reference to the position of the mage. For the base spell, the origin door has one “entrance” side: the “back”  side of the origin door has no effect on movement (although it is still opaque). The destination door likewise has one  “exit” side from which all travelers emerge, though creatures at the destination location may walk through the destination  door in either direction without being affected by the spell. Spells cast in the appropriate direction may trace their range  and/or ZOE through the Door's transposition. </description>
    <ZOE value='special' units='' type='described'/>
    <Range value='240' units='feet'/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Transparent' value='no' max='yes'>
        <Modifier type='Transparent' value='yes' cost='.5' />
      </Property>
      <Property name='Two-Sided' value='no' max='yes'>
        <Modifier type='Two-Sided' value='yes' cost='.5' />
      </Property>
      <Property name='Two-Way' value='no' max='yes'>
        <Modifier type='Two-Way' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Fear' level='4' category='Combat'>
    <description>All those failing to save will attempt to flee or to get as far away as possible from the caster. They are afraid until they  make a successful saving throw. Each will get additional chances to save at intervals of 60 / HD rounds, HD being hit  dice or level. Victims will use magical means of fleeing (e.g. teleport) if they are available and preferable. There is a  60% chance, minus 10% per level of the victim, that a victim will drop whatever is in his hands when hit by the Fear.  This will not apply to items that can speed the victim from the scene. Also it will not apply to Magical Items that may  not normally be dropped such as cursed items or intelligent weapons that make their control rolls. A creature controlled  by an outside source (e.g. a magical sword) is immune. </description>
    <ZOE value='60' units='foot' type='cone'/>
    <Range value='always zero' units=''/>
    <Duration value='until saves' units=''/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Rounds' value='60' units='/HD rounds'>
        <Modifier type='Extra Effect' value='30' units='/HD rounds' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Growth Plants' level='4' category='Environment'>
    <description>May only be cast outdoors. It causes normal brush or woods to become thickly overgrown, making the area  virtually impassable. This takes only one round to happen. It may also be used to aid the growth of crops. In that mode  the plants will grow twice their ordinary rate during the duration of the spell. </description>
    <ZOE value='up to 80’ x 80’' units='' type='described'/>
    <Range value='120' units='feet'/>
    <Duration value='1' units='week'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Lasting Duration' value='no' max='yes'>
        <Modifier type='Lasting Duration' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Hallucinatory Terrain' level='4' category='Environment'>
    <description>May be cast only outdoors, creating an illusion that affects a large area. Terrain features can either  be hidden or created within the ZOE. When any intelligent creature contacts the area affected, the spell will be broken,  unless he is specifically trying not to do so. Creatures below Int 2 will be totally unaffected by the spell.  </description>
    <ZOE value='2000' units='foot' type='square'/>
    <Range value='120' units='feet'/>
    <Duration value='1' units='day'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Ice Storm' level='4' category='Combat'>
    <description>May only be cast outdoors. It creates a storm of large hailstones. It does 3d6+6 hit points of damage to those  within. There is no saving throw, due to the large number of hailstones, but Prot / Normal Missiles (page 71) will give  complete protection. Resistance to Crushing protects against Ice Storm damage, but Resistance to Cold has no effect. It  does not affect completely non-corporeal beings, (e.g. Spectres). Any character who has a ready shield as of the Spell  Phase is presumed to block the hail and takes only half damage, as do those with a Shield (page 65) spell. A Fireball  (page 70) would melt the hailstones where the two spells overlap, resulting in no damage from the hailstones. </description>
    <ZOE value='60' units='foot' type='cube'/>
    <Range value='120' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='none'/>
    <Properties>
      <Damage die='6' number='3' plus='6'/>
      <Modifiers>
        <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
      </Modifiers>
    </Properties>
  </spell>
  <spell name='Levitation Sphere' level='4' category='Movement'>
    <description>As the second level spell, except that the caster levitates, and all within the ZOE levitate with him. Those  who do not wish to levitate receive a saving throw. The Affects Others modifier will make another the controller of the  spell. </description>
    <ZOE value='10' units='foot radius' type='emanation'/>
    <Range value='none' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Weight' value='50'>
        <Modifier type='Extra Weight' value='200' cost='1' />
      </Property>
      <Property name='Extra Ceiling' value='100' units='feet'>
        <Modifier type='Extra Ceiling' value='150' units='feet' cost='.5' />
      </Property>
      <Property name='Extra Speed' value='60' units='feet per round'>
        <Modifier type='Extra Speed' value='60' units='feet per round' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Magic Bridge' level='4' category='Movement'>
    <description>Allows the caster to produce a temporary bridge, similar to a fine netting, so it may also be climbed. It may  not be detached by ordinary means, but Dispel Magic (page 69) has its usual chance of working. The bridge will remain  until the end of the spell duration, or until the caster dismisses it. The bridge dimensions must not exceed the ZOE. The  bridge can support 1200 pounds. </description>
    <ZOE value='120' units='foot' type='10’'/>
    <Range value='10' units='feet'/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Weight' value='1200' units='pounds'>
        <Modifier type='Extra Weight' value='600' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Magical Trap' level='4' category='Environment'>
    <description>This spell may be set with one spell from the caster. The chosen spell and the Magical Trap spell are cast  simultaneously, for the sum of the spell points, taking 10 rounds. The spell must be one the caster can normally throw.  The caster must state the conditions under which the trap is to be sprung, which are limited to those that the caster, if  physically present, could answer using non-magical means, plus any spells (detects, locates, etc.) that are cast upon the  Magical Trap. This may be done by the caster of the Magical Trap as if casting upon himself, or by other mages or clerics  or from items as if casting on a willing character that had dropped his saving throws. Such spells are only effective until  their duration expires, possibly making triggering of the Trap impossible thereafter. The spell can only be cast on an  inanimate object that is fixed either to the ground or to a large object such as a ship. Concealment need only be bought  on the Magical Trap spell to hide the spell before triggering.  </description>
    <ZOE value='30' units='foot' type='range'/>
    <Range value='10' units='feet'/>
    <Duration value='1' units='week'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Massmorph' level='4' category='Appearance'>
    <description>May only be thrown outdoors, concealing up to 100 persons (i.e. two-legged, generally mammalian living  beings, Medium size or smaller.) They will appear as a woods or orchard. The concealed figures may be moved through  without being detected as anything other than trees, and it will not affect the spell. A Detect Magic (page 63) will  detect the spell. The caster must concentrate in order to maintain the spell. Anyone taking any action that would break  Invisibility (page 66) will no longer be concealed by this spell. Unwilling or moving recipients are not affected by this  spell. </description>
    <ZOE value='120' units='foot' type='circle'/>
    <Range value='always zero' units=''/>
    <Duration value='6' units='hours'/>
    <SavingThrow value='none/willing'/>
    <Properties>
      <Property name='Extra Effect' value='100' units='people'>
        <Modifier type='Extra Effect' value='50' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Prot / Normal Weapons' level='4' category='Enhancement'>
    <description>The recipient gains DR 10/+1 against all attacks (as the +1 indicates, weapons of +1 or better  enchantment, and all magical and energy effects, ignore the damage reduction.) </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra DR' value='10' units = ''>
        <Modifier type='Extra Effect' value='5' units = '' cost='0.5'></Modifier>
      </Property>
      <Property name='Extra Bonus' value='1' units = ''>
        <Modifier type='Extra Effect' value='1' units = '' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Size Change' level='4' category='Appearance'>
    <description>The recipient may freely vary his size anywhere from 4 times his normal size to 1  4 his normal size while the spell  lasts. Everything that he is carrying or wearing changes size with him, although enlarged weapons do normal damage.  There is corresponding change in his mass and movement rate, and a related one in his effective strength. Effective  strength is the strength usable in combat or against doors; however, the caster is always strong enough to move.  Every doubling or halving of the caster's size changes the caster's size category by one step. This changes both the  character's Strength and the character's size modifier. Recall that the size modifier affects both attack rolls and AC.  The character's Str score cannot be reduced below 1. Movement rate changes proportionally to height, but cannot be  reduced to less than 1/4 of normal total. Note that the character's Str modifier for combat will be changed by an amount  equal to half the Str score change. Characters who are innately Small in size already start on the '1  2 column rather than  '1 and will be modified by size shifts accordingly. </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Multiplier' value='0'>
        <Modifier type='Extra Effect' value='2' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Telepathy' level='4' category='Communication'>
    <description>Telepathy allows full two way communication. It may be targeted on a single visible or attuned individual, or at  the ZOE.  If cast on a single visible individual, there is no saving throw but the spell takes effect only if the target is willing to  accept it.  If cast on an attuned invidividual, LOS is not needed and the base range is 1 mile, but the target gets a Will save to avoid  the spell.  If cast at the ZOE, LOS is not needed. A random individual from among all those in the ZOE is selected as target, and  gets a Will save to negate the effect. The caster may attempt to influence the target by telepathic pressure once the link  is established. Treat this as a +4 to the caster's Charisma (+2 increase in Cha bonus) for any relevant skill, intimidation,  etc. (Intimidation attempts are a Contest of Charisma with character level added as an expertise modifier.)  </description>
    <ZOE value='60' units='foot' type='sphere'/>
    <Range value='240' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='see' effect='description'/>
  </spell>
  <spell name='Temporary Bag of Holding' level='4'>
    <description>If cast on an ordinary sack, it will act as a Bag of Holding for the duration of the spell. The bag  will hold 1000 pounds as if they were only 30. Objects of up to 10' by 5' by 3' may be stuffed into the bag, but they  seem as if they weigh only 30 pounds encumbrance. Anything inside the bag when the spell wears off is lost. One may  not put one Bag of Holding inside another. </description>
    <ZOE value='1' units='bag' type=''/>
    <Range value='contact' units=''/>
    <Duration value='12' units='hours'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Weight/Size' value='1000'>
        <Modifier type='Extra Effect' value='500' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Trace Summoning' level='4'>
    <description>Allows the caster to find the direction to the caster of a summoning spell when confronted with the  summoned monster. The summoned creature must still be in the control of the other caster. The range of the spell is the  maximum distance between the caster and the summoned creature. If the summoning was cast with the Concealment,  then a successful Level Contest is needed to trace the spell. Affects Others makes someone else know the direction, but  the initial casting range is still calculated from the caster. </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='120’*' units='feet'/>
    <Duration value='4' units='days'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Learn Distance' value='no' max='yes'>
        <Modifier type='Learn Distance' value='yes' cost='2' />
      </Property>
    </Properties>
  </spell>
  <spell name='Trace Warning' level='4' category='Senses'>
    <description>This spell allows the caster to learn the direction and distance to the caster of a spell that has set off aWarning  spell. Hence, it can be used to Trace a Detect, a Locate, a Sending, or even another Trace. Following the Warning, the  caster has 10 rounds to cast the Trace Warning. He then learns the direction to the original caster. For +2 levels, he also  learns the distance to the caster. For +4 levels, he sees the true form of the caster (illusions and polymorphs are pierced)  as well as the distance. The duration of the spell is 4 days. If the Trace is cast against a spell that was transferred to  another with the Affects Others modifier, then the caster of the Trace has a choice of whether to trace the actual caster  or the recipient of the spell. In this case, repeated castings are possible. </description>
    <ZOE value='1' units='warning' type=''/>
    <Range value='unlimited' units=''/>
    <Duration value='4' units='days'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Initial Duration' value='10'>
        <Modifier type='Extra Initial Duration' value='10' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Wall of Electricity' level='4' category='Environment'>
    <description>It creates a wall of magical electricity. Resistance to Lightning includes generic Electricity. The shape of  the wall is either a 20' r hemisphere, or a plane up to 60' long, and 20' high. The wall is completely transparent. Thus  it will not be visible, but those who stand very near it or approach it very cautiously will feel something (e.g. the hair  on the back or their hand stands up.) It remains where it is cast for the duration, unless dispelled. It gives damage to all  those who attempt to pass through it that are not immune to lightning. (e.g. Will O'Wisp, Electric Eel, Blue Dragon, etc.)  Anyone taking damage from the wall must make a Fortitude or Reflex saving throw (whichever has the higher bonus)  to pass through the wall, failure resulting in them bouncing out in the direction they entered from. Base damage is 2d6:  creatures who are standing in water or are otherwise well grounded take double damage. Note that this damage is taken  regardless of the saving throw. Creatures in the wall when it is created get a Reflex saving throw to dodge and avoid  damage. </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='see' effect='description'/>
    <Damage die='6' number='2'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
    </Modifiers>
    <Properties>
      <Property name='Bending' value='no' max='yes'>
        <Modifier type='Bending' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Wall of Fire' level='4'>
    <description>Creates a wall of magical fire. Mechanics are identical to Wall of Electricity (page 74) except as follows:  * The relevant Resistance is, of course, Fire.  * Being grounded is irrelevant: it is Undead and Cold-based creatures who take double damage.  * The wall is opaque; beings on each side have Total Concealment from beings on the other side.  * Cold/ice based spells cannot be cast through the wall.  </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='see' effect='description'/>
    <Damage die='6' number='2'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
    </Modifiers>
    <Properties>
      <Property name='Bending' value='no' max='yes'>
        <Modifier type='Bending' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Wall of Force' level='4' category='Environment'>
    <description>Creates an invisible wall of pure force. May be either a 20' hemisphere or a plane up to 60' wide and 20' high.  At the caster's option the “hemisphere” mode can also be a full sphere, extending down into ground and blocking tunnels  or tunnelers. It has no thickness. While the Wall provides no concealment it blocks all missile fire and all spells that  create physical effects, and movement.  * Missiles will bounce off regardless of size or power. Opponents attempting to melee each other through the wall treat  one another as having DR20/-.  * Spells or prayers that work through material manifestations (solid objects, lightning, fire, etc.) are either blocked by the  Wall or have their ZOE cut off by the Wall. Any spell or prayer that does not depend on such effects (e.g. light/darkness  spells, spells with Will saves, etc.) is completely unaffected by the Wall. Gases diffuse through the wall but forceful  movement is blocked: Air Blast will be stopped, but a Cloudkill kept in contact will seep through in d6 rounds.  * Moving through the Wall requires a Strength check (d20+full ability score) of 40. A creature can take 10, but cannot  take 20. Since the Wall is a force effect, incorporeal and ethereal creatures are still blocked; however, movement that  does not pass through the intervening space (e.g. Dimension Door, Teleport, natural “blink” abilities) is not affected.  </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='see' effect='description'/>
    <Properties>
      <Property name='Tougher' value='40'>
        <Modifier type='Tougher' value='10' cost='.5' />
      </Property>
      <Property name='Bending' value='no' max='yes'>
        <Modifier type='Bending' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Wall of Ice' level='4' category='Environment'>
    <description>Creates a wall of supernaturally cold ice. The wall may either be a hemisphere of 20' radius, or a plane up to 60'  long and 20' high. The wall is 1' thick and has hardness 0 and 36 hit points per 5' square of facing, just as would normal  ice (see Breaking Things, p.42). The ice is opaque. However, any being touching the ice takes 2d6 damage (Resistance  to Cold protects normally, fire-based creatures take double damage), with no saving throw. (Creatures occupying the  space where the wall forms get a Reflex save to dodge it as it crystallizes; they may choose which side they end up on if  they succeed, and are placed randomly to one side or the other, after taking damage, if they fail.) Passing through a hole  in the wall, unless there is at least an extra 5' of clearance, requires a Reflex save to avoid brushing against the ice and  taking damage. Fire magic cannot be cast through the wall even if it could normally be cast through a solid obstacle. The  wall remains where it is cast for the duration, unless dispelled. </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='see' effect='description'/>
    <Damage die='6' number='2'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
    </Modifiers>
    <Properties>
      <Property name='Bending' value='no' max='yes'>
        <Modifier type='Bending' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Wizard Eye' level='4' category='Senses'>
    <description>Creates a remotely controlled invisible visual sensor in the form and size of an average demihuman eyeball. The  eye initially appears within 5' of the caster, but can then travel in any direction within the range of the spell as long as the  spell lasts. Solid barriers block its passage, but it can pass through a hole or space as small as 1 inch in diameter. The eye  can't enter another plane of existence, even through a gate or similar magical portal. The eye moves up to 180'/round.  The eye sees exactly as you would see if you were there; thus, if you have cast See Invisible (page 68) on yourself,  the eye can see invisible objects. You must concentrate to move an eye or see through it. If you do not concentrate, or  move out of range of the eye, the eye is inert until you are again within range and concentrate. </description>
    <ZOE value='special' units='' type='described'/>
    <Range value='1' units='mile'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Speed' value='180'>
        <Modifier type='Extra Speed' value='180' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Charm Monster' level='5' category='Combat'>
    <description>This spell will charm targets that fail a will saving throw. The charmed victim then obeys the mage's  commands, but will not do anything blatantly self-destructive, nor anything strongly contrary to its nature. Commands  are not telepathic and the spell does not give any language ability, although hand signals may work in some cases. The  mage must continue to concentrate on the spell, and in addition, the victim will get an additional saving throw every 6 /  T hours, where T is its level. Mindless creatures are immune to this spell.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='60' units='feet'/>
    <Duration value='concentration' units=''/>
    <SavingThrow value='Will' effect='negates'/>
  </spell>
  <spell name='Cloudkill' level='5' category='Environment'>
    <description>It may only be cast outdoors, creating a moving poisonous cloud of vapor. Its movement is 20' / round in the  direction of the wind, or directly away from the caster if there is no wind. Unfortunately, due to the vapor's ability to  seep through skin, holding one's breath is no defense, although getting under water will work. Each round a creature  is in the Cloudkill it is dosed with a strong HP poison (d6 surge, 2/rnd continuing, 10 rnd duration, DC as spell). A  creature that stays in contact for multiple rounds will suffer multiple doses. However, once a creatures makes its initial  save against any dose, it cannot be further poisoned by that Cloudkill, though it will continue to take the ongoing damage  of any doses already suffered. The cloud is heavier than air, and thus it will follow the contours of the ground. It will be  dispelled by unusually strong winds or by trees. </description>
    <ZOE value='40' units='foot' type='H'/>
    <Range value='60' units='feet'/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='Fortitude' effect='negates'/>
    <Damage die='6' number='2'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='1.5' />
    </Modifiers>
    <Properties>
      <Property name='Extra Speed' value='20'>
        <Modifier type='Extra Speed' value='20' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Cone of Weakness' level='5' category='Combat'>
    <description>All within the cone take 2d4 points of temporary ability damage. (Fortitude save to half). Although  Strength is customary as implied by the name, any physical ability (i.e. Strength, Dexterity, or Constitution) can be  targeted. Strength or Dexterity can be reduced to 0 by this spell, leaving the target helpless; Constitution cannot be  reduced below 3 (the spell cannot be used to kill through Constitution loss).  </description>
    <ZOE value='60' units='foot' type='cone'/>
    <Range value='always zero' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Fortitude' effect='half'/>
  </spell>
  <spell name='Conjure Elemental' level='5' category='Conjuring'>
    <description>This spell conjures a 16 HD elemental. There are four kinds of Elemental: Fire, Earth, Water and Air.  In order to call forth an elemental one needs a considerable quantity of the corresponding element. The caster must be  within 10 feet of the element. The elemental springs forth from the element. A mage may not call forth more than one  elemental of the same type during any 24 hour period. If at any time an elemental occupies a point within 400 feet of  where another elemental of the same type is or was during the previous 24 hours, the elemental will return whence it  came. The mage must maintain undivided attention on the elemental in order to maintain control of it. Once broken,  control may not be re-established, and the elemental will move directly to attack the one who summoned it. Any who try  to bar its path are also attacked. An uncontrolled elemental will return whence it came after a period of time 10 times  that during which it was controlled. A controlled elemental will return whence it came at the command of the one who  summoned it. </description>
    <ZOE value='480' units='foot' type='control'/>
    <Range value='10' units='feet'/>
    <Duration value='concentration' units=''/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Effect' value='16' units='HD' max='24'>
        <Modifier type='Extra Effect' value='4' units='HD' cost='1' />
      </Property>
      <Property name='Extra Safety' value='0' units='HP'>
        <Modifier type='Extra Safety' value='10' units='HP' cost='1.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Contact Higher Plane' level='5' category='Communication'>
    <description>Spell points used to cast this spell are expended for one week. It allows the mage to seek knowledge  from creatures inhabiting higher planes of existence. One question will be answered, only yes or no. If the creature does  not know the answer to a question, it will answer randomly, though the answers will be consistent from casting to casting.  The base chance for knowing the answer to a question is 90%, but this should be modified downwards for difficulty and  obscurity. </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='1' units='minute'/>
    <SavingThrow value='veracity roll' effect=''/>
    <Properties>
      <Property name='Extra Effect' value='1' units='question'>
        <Modifier type='Extra Effect' value='1' units='question' cost='1.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Feeblemind' level='5' category='Combat'>
    <description>The target's Intelligence is reduced to 6 through permanent ability damage. This cannot be cured by rest or  ordinary Cure spells (although Cure Feeblemind will do nicely). Feebleminded Mages lose all casting ability. Guardians  retain their Charisma-based spellcasting powers but must make an Int check (d20+Int) against DC20 to remember any  particular spell when they try to cast it. Other classes will not be so crippled although they may provide much inadvertant  amusement to their companions until cured.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='120' units='feet'/>
    <Duration value='permanent' units=''/>
    <SavingThrow value='mental -15%' effect=''/>
  </spell>
  <spell name='Growth Animals' level='5'>
    <description>It causes up to 8 ordinary animals (e.g. cats, dogs, wolves, horses, lions, etc.) to grow up to four times their  normal size. They will have their combat abilities (damage, HD) increased by a factor of two. Animals trained to accept  the spell get no saving throw. It does not give the caster any control. The animals will revert to normal after the spell  duration. This spell will not be effective on humans in animal form. </description>
    <ZOE value='90' units='foot' type='cone'/>
    <Range value='always zero' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='Fortitude' effect='negates'/>
    <Properties>
      <Property name='Extra Size' value='0'>
        <Modifier type='Extra Effect' value='1' cost='1' />
      </Property>
      <Property name='Extra Animals' value='8'>
        <Modifier type='Extra Animals' value='4' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Hold Monster' level='5' category='Combat'>
    <description>This spell will affect up to 3 Monsters in the ZOE, immobilizing them for as long as the Mage continues to  concentrate. Should either the caster or a held target take damage, the spell will be broken for that target. </description>
    <ZOE value='60' units='foot' type='sphere'/>
    <Range value='60' units='feet'/>
    <Duration value='concentration' units=''/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Monsters' value='3'>
        <Modifier type='Extra Effect' value='1' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Immolate' level='5' category='Enhancement'>
    <description>The recipient may cause her body to burst into flames at will. Turning the flames on or off is a free action in Powers  phase. Creatures who come into contact with the recipient take 3d6 points of fire damage. Cold-based creatures take  double damage. The flame aura extends beyond the character's body, so direct contact is not necessary: the character  may dodge a blow and yet inflict immolation damage on the attacker's arm. An opponent is assumed to contact the flame  aura and take damage if any of the following conditions apply:  * Opponent grapples or is grappled by the immolator.  * Opponent hits AC10 or better with natural weapons, or with any melee weapon 3' or less in length.  * Immolator successfully hits opponent with a touch attack.  * Immolator successfully hits opponent with a melee weapon 3' or less in length.  While immolated, the recipient has Resistance to fire damage, with a per-round rating equal to the maximum immolation  damage. Note that the immolation itself does not damage the character even if this Resistance is cancelled: two immolators  running equally powerful spells will do no damage to each other. </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='none'/>
    <Damage die='6' number='3'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='0.5' />
    </Modifiers>
  </spell>
  <spell name='Mind Blast' level='5' category='Combat'>
    <description>If the target fails its save, it is knocked unconscious and cannot be awakened until the spell expires or is dispelled.  The target must have a mind.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='60' units='feet'/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='Will' effect='negates'/>
  </spell>
  <spell name='Mind Link' level='5' category='Communication'>
    <description>This spell allows the caster to make mental contact with another being, which must be sentient. The caster must  have LOS to the other being or must know his position due to a Locate (with Distance) or Scrying spell / item. The  link is automatically established. Either side can attempt to break the spell, but if the other party is unwilling, a Level  Contest results. Full two-way communication is allowed. In addition, any Mental attack spells can be cast through the  link, without range restrictions. These include: Suggestion, Magic Jar possession, Mind Blast, Hold / Charm Monster,  Fear, Clairsentience, ESP, Pain etc. The spells affect only the linked mind, even if they are multi-target spells.  Spells such as Range Loser, Control Self, Concentrate, or Mind Blank are not allowed. This spell cannot take Affects Others.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='unlimited' units=''/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Mind Shield' level='5' category='Enhancement'>
    <description>This spell fortifies the recipient's mind against mental attacks or possession attempts. It gives +6 on saves and  +3 to level in Level Contests when defending against these attacks. If cast on a recipient of lower level than the caster,  the level of defense is the caster's level +3 rather than recipient's level +3. </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='touch' units=''/>
    <Duration value='3' units='hours'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Plus' value='6'>
        <Modifier type='Extra Effect' value='2' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Misdirection' level='5' category='Combat'>
    <description>A profound dizziness strikes the victims. They are unable to tell direction. This halves movement rates and  causes -4 to melee combat and -8 to missile combat and spell targeting. Spells that do not normally need a targeting  roll have a flat 40% chance of being launched in a random direction. </description>
    <ZOE value='30' units='foot' type='sphere'/>
    <Range value='60' units='feet'/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Negative' value='-4'>
        <Modifier type='Extra Effect' value='-2' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Pass Wall' level='5' category='Environment'>
    <description>It opens a hole in non-magical, solid wood, stone or earth. It will not work through metal. The hole is 6' wide, 8'  high, and 10' deep. At the end of the spell duration, the hole closes from the center first, so there is a chance to jump out  either side. </description>
    <ZOE value='8’H x 6’W x 10’D' units='' type='described'/>
    <Range value='10' units='feet'/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Cross-Section' value='6' units='feet'>
        <Modifier type='Extra Cross-Section' value='3' cost='.5' />
      </Property>
      <Property name='Extra Length' value='10' units='feet'>
        <Modifier type='Extra Length' value='10' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Phase In' level='5' category='Combat'>
    <description>This spell is useful against beings in another plane (e.g. Normal, Ethereal, Astral, etc.) It will temporarily bring one  such being into the plane occupied by the caster. It would enable one to attack a Phase Spider, will make non-corporeal  undead and Shadows solid, and therefore subject to attack by ordinary weapons, etc. </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='60' units='foot radius'/>
    <Duration value='20' units='rounds'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra creature' value='1'>
        <Modifier type='Extra Effect' value='1' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Stone Walking' level='5' category='Movement'>
    <description>It allows the recipient to slowly move through solid stone or earth, but not metal. Movement is at one 5' step  per round. The stone will “melt” in front of the recipient, and reform immediately behind him. He will be able to breathe  while he is in the stone. </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='6' units='hours'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Speed' value='5'>
        <Modifier type='Extra Effect' value='5' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Summon' level='5' category='Conjuring'>
    <description>This spell is used to summon attuned objects to the mage. When this spell is cast, the attuned object is teleported  directly to the mage's reach. The object may be a literal object, which gets no saving throw, or a person, who gets  a mental saving throw only if they wish to resist. Although the mage can be attuned to a place, the place can not be  summoned. Attempting to summon a place is equivalent to Teleport (page 78). The mage need not know the location of  the object to summon it. If the object is out of range, the spell points are wasted. Damp Teleport (page 81) automatically  blocks a summon. If another mage is holding the target object and resists the summon, a Level Contest ensues, with a  +2 level bonus to either mage that is attuned to the object. The base level of Summoning is limited to object of up to 250  lbs. The range is 480' extendible along the following progression at the cost of +1  2 per step:  480', 1  4 mile, 1  2 mile, 1 mile, 2 miles, 4 miles, doubling. </description>
    <ZOE value='1' units='attuned' type='object'/>
    <Range value='480' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Weight' value='250'>
        <Modifier type='Extra Effect' value='250' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Telekinesis' level='5'>
    <description>This spell allows the caster to move an object at a distance by use of mental force. Maximum weight is 250  pounds. The maximum speed is 30' / round, and the motion occurs in movement phase. </description>
    <ZOE value='1' units='object' type=''/>
    <Range value='60' units='feet'/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Weight' value='250' units='pounds'>
        <Modifier type='Extra Weight' value='250' units='pounds' cost='.5' />
      </Property>
      <Property name='Extra Speed' value='30' units='feet/round' max='120'>
        <Modifier type='Extra Speed' value='30' units='feet/round' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Teleport' level='5' category='Movement'>
    <description>This spell allows practically instantaneous transportation without regard to distance. The caster may teleport himself,  his equipment and up to one other creature, provided the caster has the other's consent and grasps the creature with his free  arm. However, the mage may only teleport 250 lbs. in addition to his own weight (creature and equipment combined).  The destination must be in sight or given in relation to the mages current location or the mage must be attuned to it. An  attuned destination could be a place the mage is attuned to or the location of a person or object the mage is attuned to.  The mage will arrive at the chosen destination without error, however this is no guarantee of survival. If the destination is  filled with solid matter the result is death through explosion, destroying all traces of bodies and items carried. Generally,  the spell is forgiving, trying to place the mage in any available space at the destination. If one teleports into a room full  of people, one will arrive at any space large enough to accommodate one's self and load. If one teleports into a room full  of stone, one dies. The mage is assumed to be attuned to his home unless otherwise specified.  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='unlimited' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Affects Others' value='no' max='yes'>
        <Modifier type='Affects Others' value='yes' cost='2' />
      </Property>
      <Property name='Extra Weight' value='1' units='person'>
        <Modifier type='Extra Weight' value='1' units='person' cost='1' />
      </Property>
      <Property name='Concealment' value='no' max='yes'>
        <Modifier type='Concealment' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Toll' level='5' category='Environment'>
    <description>Produces a mystical disturbance that can be perceived by: angels, demons, free-willed elementals, Aerial Servants,  Conjured Servants, patrolling Invisible Stalkers, beings in the astral plane, and any spellcaster or cleric of level 12+.  Such beings can perceive the disturbance at a range of up to 5 miles regardless of intervening obstacles, and know its  exact location. The disturbance has no other effect beyond catching the attention of those beings able to perceive it.  </description>
    <ZOE value='5' units='mile' type='radius'/>
    <Range value='always zero' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra ZOE uses the Range progression rather than the ZOE progression' value='0'>
        <Modifier type='Extra ZOE uses the Range progression rather than the ZOE progression' value='2' cost='.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Trace Teleport' level='5' category='Senses'>
    <description>This spell will give the direction, without range restriction, to the origins (destinations) of all Teleports,  Dimension Doors, Words of Recall, etc. whether from a spell, prayer or item, which had destination (origin) within the  ZOE within 10 rounds of casting the spell. If there are multiple teleports involved, the caster will get the information  for each, but may trace only one at a time. If the teleport spell was concealed, then a Level Contest is required to trace.  </description>
    <ZOE value='30' units='foot' type='r'/>
    <Range value='120' units='feet'/>
    <Duration value='4' units='days'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Initial Duration' value='10' units='rounds'>
        <Modifier type='Extra Initial Duration' value='10' units='rounds' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Wall of Iron' level='5' category='Environment'>
    <description>Creates an iron wall three inches thick (Hardness 10, 90HP). The wall must lie in a vertical plane and has a  maximum area of 500 square feet. It may be battered down as one would a normal iron wall. Otherwise it will last until  dispelled or the duration ends. </description>
    <ZOE value='3” thick x 500 sq.ft.' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='4' units='days'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Harder' value='10'>
        <Modifier type='Harder' value='2' cost='.5' />
      </Property>
      <Property name='Thicker' value='90' units='HP'>
        <Modifier type='Thicker' value='30' units='HP' cost='.5' />
      </Property>
      <Property name='Bending' value='no' max='yes'>
        <Modifier type='Bending' value='yes' cost='1' />
      </Property>
      <Property name='Lasting' value='no' max='yes'>
        <Modifier type='Lasting' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Wall of Stone' level='5' category='Environment'>
    <description>Creates a stone wall one foot thick (Hardness 8, 180 HP). The maximum area is 1000 square feet, and the  wall must lie in a vertical plane. It may be battered down as one would a normal stone wall. Otherwise it will last until  dispelled or the duration ends. </description>
    <ZOE value='1’ thick x 1000 sq.ft.' units='' type='described'/>
    <Range value='60' units='feet'/>
    <Duration value='4' units='days'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Thickness' value='12' units='inches'>
        <Modifier type='Thicker' value='2' units='inches' cost='.5' />
      </Property>
      <Property name='Bending' value='no' max='yes'>
        <Modifier type='Bending' value='yes' cost='1' />
      </Property>
      <Property name='Lasting' value='no' max='yes'>
        <Modifier type='Lasting' value='yes' cost='1' />
      </Property>
    </Properties>
  </spell>
  <spell name='Anti-Magic Shell' level='6'>
    <description>Creates a 20' radius sphere centered on caster which inhibits magical spells or items. All magic will only  function with a successful Level Contest, with the caster getting a +2 level bonus against spells and a +4 bonus against  items. The caster can cast no spells except Dispel Magic at the shell, and then the spell only affects the shell. Magic items are only temporarily subjugated, and only one battle is to be fought between each one and the Shell. The shell moves with the caster, and the spell may never take Affects Others. </description>
    <modifiers_description> Full Shell (no Level Contest needed) +3,  Immobile Shell (shell will be centered on the caster initially) +2.  </modifiers_description>
    <ZOE value='20' units='foot' type='sphere'/>
    <Range value='always zero' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Full Shell' value='no' max='yes'>
        <Modifier type='Extra Duration' value='yes' cost='3'>
          ..........<Description>Full Shell (no Level Contest needed) +3</Description>
        </Modifier>
      </Property>
      <Property name='Immobile Shell' value='no' max='yes'>
        <Modifier type='Extra Duration' value='yes' cost='2'>
          ..........<Description>Immobile Shell (shell will be centered on the caster initially) +2</Description>
        </Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Death Spell' level='6'>
    <description>4d8 creatures die instantly. This spell has no effect on creatures that aren't alive (undead, constructs, etc.)  Creatures of less than 1 full HD also die but don't count against the total. Creatures of 4HD or more count as multiple  creatures: a creature counts as 1 extra target for each extra HD above 3. The spell targets lower levels first.</description>
    <modifiers_description> Extra Effect (+1d8 creatures dies) +1.  </modifiers_description>
    <ZOE value='60' units='foot' type='cube'/>
    <Range value='120' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Fortitude at -6' effect='negates'/>
    <Properties>
      <Property name='Extra Creatures' die='8' number='4' max='yes'>
        <Modifier type='Extra Effect' die='8' number='1' cost='1'>
          ..........<Description>Extra Effect (+1d8 creatures dies) +1.</Description>
        </Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Disintegrate' level='6'>
    <description>The spell emanates as a ray, requiring a ranged touch attack to hit the target. The target may be one living or  animated being of any size, one object or connected group of objects that fits into a 10' cube, or a 10' cubic piece of a  larger object. If the target fails to save it is reduced to a small amount of impalpable dust. A successful save means the  target only takes 5d6 damage.  </description>
    <ZOE value='1' units='being' type='or'/>
    <Range value='60' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='partial'/>
  </spell>
  <spell name='Flame Storm' level='6'>
    <description>This spell calls down a flame storm on an area. This spell requires at least a 40' ceiling. It ignites all inflammables  and exposes other objects to great heat. It will cause all creatures in the storm 2d6 hits per round, Reflex save  to half. Resistance to Fire protects normally, and some objects within the area may provide temporary protection. After  the duration expires, any remaining fuel will continue to burn normally. If cast indoors, the duration is halved. </description>
    <modifiers_description> Extra Duration (+4 rounds), Extra Damage (hotter flames cause +d6 more per round) +1.  </modifiers_description>
    <ZOE value='120' units='foot' type='H'/>
    <Range value='180' units='feet'/>
    <Duration value='8' units='rounds'/>
    <SavingThrow value='Reflex' effect='half'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='6' number = '1' cost='1' />
    </Modifiers>
    <Properties>
      <Property name='Extra Duration' value='8' units='rounds'>
        <Modifier type='Extra Duration' value='4' cost='1'>
          ..........<Description>Extra Duration, Extra Damage (hotter flames cause +d6 more per round) +1. (+4 rounds)</Description>
        </Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Geas' level='6'>
    <description>The victim must perform a task set out by the caster; otherwise his Strength will ebb at one point per day until death at  0. The task must be one that could be completed in 1 week and must not be utter suicide. The spell lasts until the task is  completed. No Strength loss is suffered as long as the victim makes some significant effort towards the task that day; all  Strength loss is recouped as soon as the victim starts working on the task again. </description>
    <modifiers_description>Extra Difficulty (double the  task completion time) +1.  </modifiers_description>
    <ZOE value='1' units='being' type=''/>
    <Range value='touch' units=''/>
    <Duration value='variable' units=''/>
    <SavingThrow value='Will' effect='negates'/>
    <Properties>
      <Property name='Extra Difficulty' value='0'>
        <Modifier type='Extra Difficulty' value='1' cost='1'>
          ..........<Description>Extra Difficulty (double the  task completion time) +1.</Description>
        </Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Invisible Stalker' level='6'>
    <description>The caster summons an invisible stalker (AC: 21/17/17; HD: 8d8+16; Attacks: +10 slam 2d6+4; Move:  180'/0'/0'; Special: Invisible, Darkvision) and can command it to perform a task which it will attempt regardless of the  difficulty. If the task is not completed at the end of the duration, the stalker will return to its plane without notice.  </description>
    <ZOE value='1' units='mission' type=''/>
    <Range value='none' units=''/>
    <Duration value='1' units='week'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Legend Lore' level='6'>
    <description>This spell provides some knowledge of a legendary item, place, or being (hereafter ”subject”). The base spell  provides only the most general and widespread knowledge about the subject. Additional levels grant more information;  model this as the GM answering 1 additional question about the subject per extra level. (This answer may go into as  much detail as the GM considers appropriate, and should always be at least one complete sentence. The GM should  volunteer an interesting fact if the the caster doesn't have an adequate question list.) For definiteness, a subject qualifies  as ”legendary” if either spoken or written stories about it existed prior to the caster's birth. The subject need not be  present; range is meaningless to this spell. Indeed, Legend Lore does not require that its subject still exist, or even that  its subject ever existed at all – although in the latter cases even a base level casting always reveals that the subject has  been destroyed or that it was fictional, as appropriate. This spell cannot be cast with reference to the same subject more  than once per day.  </description>
    <ZOE value='one subject' units='' type='described'/>
    <Range value='n/a' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Move Water' level='6'>
    <description>This spell will temporarily remove a 1 million cubic foot section of a body of water (exact dimensions up to the  caster; i.e., it could be 100' cubed, or 500'x100'x20'). During the spell's duration, the remaining body of water acts as if  the Moved water was still there; that is, water does not fill the space vacated. The spell only moves water; anything that  was in the Moved section of water will remain, and fall to the bottom of the ZOE.  </description>
    <ZOE value='1' units='million' type='cubic'/>
    <Range value='240' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Magic Jar' level='6'>
    <description>This spell allows the caster to house his life in an inanimate, non-magical object, the so-called “Soul Gem”. The  Soul Gem must be within 30 feet of his body at the time of casting. His body will then be lifeless, until or unless the  caster returns. However, his body will be preserved against ordinary decay so long as the Magic Jar spell lasts. The Soul  Gem must weigh at least 1 pound. The caster may then try to possess the body of any living creature that passes within  120 feet of his Soul Gem. Each such possession attempt uses the same spell point cost as the casting of the Magic Jar  spell would. The victim gets a Will save. If the victim fails, then the caster will have complete control over the body of  the victim, and complete access to the memories of the victim. The victim will know what is happening, although he will  be helpless at the time to take counteraction. The caster may not use any spell casting abilities of the possessed body;  however, he may use his own spell casting abilities if the body has hands and can make the proper motions and sounds.  If the possessed body is destroyed, the caster will return to the Soul Gem provided he is within 10 miles of it. Otherwise  it is as if he suffered a normal death. While within 10 miles he may return to the Soul Gem at will. He may return from  the Soul Gem to his body at will, thus ending the spell, provided it is within 30 feet of the Gem. From the Soul Gem he  may attempt new possessions. If the Soul Gem is destroyed, the caster is totally annihilated (whether he is in the Gem  or in a possessed body). If his body is destroyed while he is in the Soul Gem or a possessed body, he may obviously not  return to his body. The Extra Range modifier may affect any one of the three ranges in this spell. The spell lasts until the  caster returns to his body, or until the caster is destroyed.  </description>
    <ZOE value='1' units='object*' type=''/>
    <Range value='see description' units=''/>
    <Duration value='see description' units=''/>
    <SavingThrow value='Will' effect='negates'/>
  </spell>
  <spell name='Move Earth' level='6'>
    <description>Usable only outdoors, the spell can move a hill or ridge 5' per minute for up to 40 minutes. The spell takes 10  minutes to cast. The mage may have to move to keep the ZOE in range. The resulting terrain does not radiate magic.  Moving earth is quite destructive; only the strongest structures can survive even the base spell.  </description>
    <ZOE value='240' units='foot' type='cube'/>
    <Range value='240' units='feet'/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Projected Image' level='6'>
    <description>The caster may create an image of himself from which all his spells, etc. seem to emanate thereafter. Spells  that emanate from the image have their ranges calculated from it also. The image is at all times a mirror image of the  status of the caster. Thus, they will have the same appearance and magic on them. If the caster is scarred by a Fireball,  the same scars will appear on the image. The image is an illusion and cannot take damage or appear to take damage, nor  can it receive spells other than those cast on the caster. The image will move independently of the caster: the appearance  is transferred, but movement is not. The image can talk independently of the caster.  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='120' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Repulsion' level='6'>
    <description>This spell creates a 10' r invisible, mobile sphere which prevents creatures from approaching you. Any creature  within or entering the field must attempt a save. If it fails, it becomes unable to move toward you for the duration of  the spell. Repelled creatures' actions are not otherwise restricted. They can fight other creatures and can cast spells and  attack you with ranged weapons. If you move closer to an affected creature, nothing happens. (The creature is not forced  back.) The creature is free to make melee attacks against you if you come within reach. If a repelled creature moves  away from you and then tries to turn back toward you, it cannot move any closer if it is still within the spell's area.  </description>
    <ZOE value='10' units='’' type='r'/>
    <Range value='self' units=''/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='See True Form' level='6'>
    <description>This spell pierces all Disguises, Polymorphs, and Illusions to see the true form of the target. The true form is  what the target actually looks like. The caster must be able to see the target. This spell is considered a Detect type spell.  </description>
    <ZOE value='1' units='target' type=''/>
    <Range value='120' units='feet'/>
    <Duration value='instantaneous' units=''/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Shield Of Protection' level='6'>
    <description>Creates a large magical shield which will protect the caster from one attacker. Any damage from  physical attacks including missiles is done to the shield, which has hardness 6 and 30 h.p. It will still soak up all of the  damage from the attack that breaks it. The shield may be shifted to a different attacker each round. It lasts until brought  down. </description>
    <modifiers_description>Extra Effect (+1 hardness and +5 h.p.) +1.  </modifiers_description>
    <ZOE value='self' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='until destroyed' units=''/>
    <SavingThrow value='none'/>
    <Properties>
      <Property name='Extra Hardness' value='6'>
        <Modifier type='Extra Effect' value='1' cost='1'>
          ..........<Description>Extra Effect (+1 hardness and +5 h.p.) +1.</Description>
        </Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Teleport Attack' level='6'>
    <description>This spell teleports its victim to a random location within 50 miles. The victim gets a Reflex save to dodge  the effect. The victim will always be placed safely in a compatible environment. </description>
    <modifiers_description>Extra Effect (+50 miles).  </modifiers_description>
    <ZOE value='1' units='being' type=''/>
    <Range value='60' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='negates'/>
    <Properties>
      <Property name='Extra Distance' value='50' units='miles'>
        <Modifier type='Extra Effect' value='50' units='miles' cost='.5'>
          ..........<Description>Extra Effect (+50 miles).</Description>
        </Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Tremor' level='6'>
    <description>This spell causes an earthquake. The main ZOE is a 360' radius circle. During each round of the duration, fixed rigid  structures in the ZOE take 40 points of structural damage, ignoring hardness. Flexible structures or those not fixed to  the ground take no damage. Multi-story buildings collapse when their walls are reduced to 1/2 hit points (2-3 stories)  or 3/4 hit points (4+ stories); collapsing buildings damage those inside appropriately (10d6 crushing damage is typical).  Underground structures collapse as per 4 story buildings; treat them as having 10,000 structural hits divided by their  unsupported free span in feet.  Non-flying creatures who attempt to move within the ZOE must make a Reflex save or fall while the tremor continues.  There is a 1 in 6 chance each round that any being in the ZOE will fall into a crack or be struck by debris; this requires  another Reflex save, those failing take 2d6 damage. Outside the main ZOE, the tremor can still be felt and may cause  alarm but is essentially harmless; it can be felt to a distance of 15 times the ZOE radius. </description>
    <modifiers_description>Extra Duration (+2  rounds), Extra Effect (+10 structural damage per round, +1d6 debris damage per impact) +1.  </modifiers_description>
    <ZOE value='360' units='foot' type='radius'/>
    <Range value='960' units='feet'/>
    <Duration value='3' units='rounds'/>
    <SavingThrow value='see' effect='description'/>
    <Properties>
      <Property name='Extra Duration' value='3' units='rounds'>
        <Modifier type='Extra Duration' value='2' units='rounds' cost='1'>
          ..........<Description>Extra Duration (+2  rounds)</Description>
        </Modifier>
      </Property>
      <Property name='Extra Structural Damage' value='40' units='points'>
        <Modifier type='Extra Effect' value='10' cost='1'>
          ..........<Description>Extra Effect (+10 structural damage per round, +1d6 debris damage per impact) +1.</Description>
        </Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Damp Teleport' level='7'>
    <description>No Teleport, Dimension Door, or similar spell may depart nor arrive within the ZOE. No Level Contest will  be required.  </description>
    <ZOE value='120' units='foot' type='sphere'/>
    <Range value='none' units=''/>
    <Duration value='1' units='hour'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Mass Invisibility' level='7'>
    <description>This spell affects up to 200 men and horses, or up to 200 objects with an equivalent mass (about 200 tons).  They are turned invisible and will remain so until the spell expires or they break the spell, per Invisibility (page 66). All  must be in the initial ZOE, but can leave it invisibly.  </description>
    <ZOE value='120' units='foot' type='square'/>
    <Range value='60' units='feet'/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Permanent' level='7'>
    <description>This makes a spell that has lasting duration have permanent duration. In addition, the spell will be at twice normal  level against being dispelled. Only two spells of permanent or lasting duration may be on an individual at a time.  </description>
    <ZOE value='1' units='spell' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='permanent' units=''/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Phase Door' level='7'>
    <description>This spell is similar to Pass Wall (page 77) except that the door is invisible and can be used by the caster only.  The door is 6' by 8' and the wall can be up to 60 feet thick. It lasts for 7 uses, and may be dispelled by the caster at will. </description>
    <modifiers_description>Extra Length (+30 feet thickness) + 1/2 </modifiers_description>
    <ZOE value='6' units='foot' type='*'/>
    <Range value='10' units='feet'/>
    <Duration value='10' units='minutes'/>
    <SavingThrow value='none' effect=''/>
    <Properties>
      <Property name='Extra Length' value='no' max='yes'>
        <Modifier type='Extra Length' value='yes' cost='0.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Reincarnate' level='7'>
    <description>The spell requires a dead body to put the soul in. As with any resurrection magic the soul must consent to be  reincarnated. The target must succeed in a Fortitude save with a DC equal to the number of days the target has been  dead. The body can be of any species, but if it is not of the same species as the target then the resurrection save is at -4.  Physical statistics (including Str, Dex, Con) are drawn from the body after the Reincarnation succeeds; mental ones (Int,  Wis, Cha) from the soul. A failed roll means that the soul will never inhabit that body. </description>
    <modifiers_description> Bonus to Resurrection Roll (+2) + 1/2 </modifiers_description>
    <ZOE value='1' units='body,' type='1'/>
    <Range value='none' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='willing' effect='only'/>
    <Properties>
      <Property name='Bonus to Roll' value='0'>
        <Modifier type='Bonus to Roll' value='2' cost='0.5' />
      </Property>
    </Properties>
  </spell>
  <spell name='Reverse Gravity' level='7'>
    <description>Gravity within the zone of effect is reversed. If there is a roof overhead within the height of the ZOE  nonflying characters will fall to the roof taking normal falling damage. If there is no roof they will fall to the top of the  ZOE, above which normal gravity takes over, and spend the rest of the duration oscillating about this ?xed height. At the  end of the spell duration, affected objects and creatures fall downward. Provided it has something to hold onto, a creature  caught in the area can attempt a Reflex save to secure itself when the spell strikes. Creatures who can fly or levitate can  keep themselves from falling.  </description>
    <ZOE value='60' units='foot' type='cube'/>
    <Range value='120' units='feet'/>
    <Duration value='12' units='rounds'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='True Sight' level='7'>
    <description>The caster sees all things as their true selves, including invisible, disguised, polymorphed, illusions etc.  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='as sight' units=''/>
    <Duration value='90' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Warning' level='7'>
    <description>This spell acts as a tripwire against spells of Detection, Location and Tracing, and Sending. If the protected character  is the target of one of the spells covered by the Warning spell, then he will know. The spell is passive; a Warning spell  will never set off a Warning spell of the offensive mage. The spell will protect both the mage and his belongings. The  spell is tripped if: the mage is appraised by a Detect, the mage is the target of a Locate or Trace, or a creature magically  Sent against the caster approaches within 120 feet LOS of the mage. In the ?rst two cases, the mage will know the  offending scryer if he is within LOS. In the latter case, the mage will know which creature is the Sent one. The mage can  attempt to ?nd out more information using Trace Warning. Tripping the spell will not cancel it.  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='1' units='day'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Clone' level='8'>
    <description>A piece of living flesh may be used to create a duplicate of the person from whom the flesh was taken. If the Clone and  original are alive at the same time, the Clone will try to destroy the original or both will go insane. It takes 360 / L days  to complete a clone.  </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='none' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Cone of Feeblemind' level='8'>
    <description>All within cone, mages and non-mages alike, are subject to a Feeblemind (page 76) spell.  </description>
    <ZOE value='60' units='foot' type='cone'/>
    <Range value='always zero' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Will at -3' effect='negates'/>
  </spell>
  <spell name='Mind Blank' level='8'>
    <description>This spell protects the caster against all mental spells, without requiring a Level Contest or a saving throw.  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='1' units='day'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Phase Shift' level='8'>
    <description>The caster switches out of phase, becoming ethereal. He is unaffected by all weapons and spells in his original  plane. He can see in the original plane, but cannot hear or touch. He moves in the original plane, although he is not  constrained by any obstacle. He may be attacked as normal in his new plane, and he may be forced back to the old plane  by Phase In (page 77). Dispel Magic will not affect an out-of-phase mage. The mage moves at 120 feet and can move  in 3 dimensions. He is not made invisible by this spell. He may return to the normal phase at will, but may not switch  back.  </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='40' units='minutes'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Power Word Stun' level='8'>
    <description>This spell knocks unconscious one creature of up to 80 hit points. Only those with more than half of this  get saving throws. These numbers refer to the current hit points of the target. This spell automatically has the Power  Word modifier applied at no extra cost. </description>
    <modifiers_description>Extra Effect (+10 more hit points).  </modifiers_description>
    <ZOE value='1' units='being' type=''/>
    <Range value='60' units='feet'/>
    <Duration value='4d6' units='rounds'/>
    <SavingThrow value='special'/>
    <Properties>
      <Property name='Extra Hit Points' value='80'>
        <Modifier type='Extra Effect' value='10' cost='.5'>
          ..........<Description>Extra Effect (+10 more hit points).</Description>
        </Modifier>
      </Property>
    </Properties>
  </spell>
  <spell name='Symbol' level='8'>
    <description>This sets a trap for anyone touching, crossing or reading the symbol. Those whom the caster makes aware of the  symbol's exact location may avoid its effects. Types of Symbols are: Fear, Discord, Sleep, Stun, Insanity, Death. GM  creativity and discretion are encouraged.  </description>
    <ZOE value='1' units='symbol' type=''/>
    <Range value='touch' units=''/>
    <Duration value='until triggered' units=''/>
    <SavingThrow value='variable'/>
  </spell>
  <spell name='Alter True Self' level='9'>
    <description>This spell can only be cast when a Polymorph is in effect on the caster. The caster's True Self is then permanently altered to take the form of the Polymorph. Normally a Polymorphed creature tends magically to return to its normal form. When this spell is cast, the creature will forever forget its old form; it will truly become the new one. This spell is irreversible. Modi?ers: Affects Others (unwilling victims get +45% on their saving throws) +3. </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Astral Spell' level='9'>
    <description>This spell allows travel in Astral Plane. The caster's body remains on the original plane. Speed of Astral Body: 100 miles / hour. </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='1000' units='miles'/>
    <Duration value='6' units='hours'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Avalanche' level='9'>
    <description>This creates four 20d4 snowballs in square pattern with centers 20 feet apart. Each is like the spell Snowball (page 71) with 1 2 damage if physical saving throw is made. </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='240' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='half'/>
  </spell>
  <spell name='Great Barrier' level='9'>
    <description>This spell creates a magical barrier of immense power. No one and nothing may pass through the wall, including the caster. No magic may pass through the barrier. The barrier may take two forms, either a wall 60' by 20', or a 20' radius hemisphere centered on the caster. In the latter form, the spell will provide complete protection from Detects, Locates, and Scrying spells, and the enclosed area will be under the effect of a Damp Teleport spell. Only Dispel Magic (page 69) can bring down the barrier, and even on this, the caster gains a +4 level bonus in the Level Contest to dispel it. The spell is user-friendly; the caster and others will not suffocate inside it, nor will it block the ambient light of the area, although it will block poison gas or harmful radiation. </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='3' units='hours'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Mass Suggestion' level='9'>
    <description>The mage speaks a suggestion per the spell Suggestion (page 71) which affects all who can hear him. All saves are at -3. </description>
    <ZOE value='240' units='foot' type='sphere'/>
    <Range value='always zero' units=''/>
    <Duration value='1' units='day'/>
    <SavingThrow value='Will' effect='-3'/>
  </spell>
  <spell name='Meteor Swarm' level='9'>
    <description>This produces four 20d6 ?reballs in a square pattern with centers 20 feet feet apart. Each is per the spell Fireball (page 70) with 1 2 damage if re?ex saving throw is made. </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='240' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='Reflex' effect='half'/>
  </spell>
  <spell name='Power Word Kill' level='9'>
    <description>This kills 1 being with less than 90 hit points. Only those with more than half this amount get saving throws. These numbers refer to the current hit points of the target. This spell automatically has the Power Word modi?er applied at no extra cost. Modi?ers: Extra Effect (+10 hit points of target). </description>
    <ZOE value='1' units='being' type=''/>
    <Range value='60' units='feet'/>
    <Duration value='momentary' units=''/>
    <SavingThrow value='see description'/>
  </spell>
  <spell name='Time Stop' level='9'>
    <description>The caster is speeded up so greatly that from her view point, time stops for d4+2 rounds after casting. The caster cannot harm any person or object in normal time, but can move and rearrange objects not fastened down. The caster's cast spells but their durations and effects will be in normal time; effectively no spell will take effect until the Time Stop ends at which time they all go off at once. </description>
    <ZOE value='60' units='foot' type='cube'/>
    <Range value='always zero' units=''/>
    <Duration value='d4+2' units='rounds'/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Time Travel' level='9'>
    <description>Allows the caster to travel forward in time up to two weeks. Extra Effect (+2 weeks). </description>
    <ZOE value='self' units='' type='described'/>
    <Range value='none' units=''/>
    <Duration value='permanent, momentary' units=''/>
    <SavingThrow value='none'/>
  </spell>
  <spell name='Tsunami' level='9'>
    <description>This spell summons a 40' high wave. It requires a body of water at least 2 miles wide. The wave will be 720' long and will generally affect up to 540' inland. The effects of the wave at the shore line are disastrous, but they lessen as they move inland. Only the stoutest of castle walls can withstand the wave at full strength. The wave will arrive without notice d6 rounds after the casting of the spell. Modi?ers: Extra ZOE (affects length of wave), Extra Effect (+20' to height, +180' to inland effect region, power goes as square of height) +1. </description>
    <ZOE value='see description' units='' type='described'/>
    <Range value='480' units='feet'/>
    <Duration value='d6' units='rounds'/>
    <SavingThrow value='none'/>
  </spell>
</book>
