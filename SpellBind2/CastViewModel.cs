﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using ArzooSoftware.SpellBinder.spellbinder;
using Xceed.Wpf.Toolkit;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Data;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.SpellBind2.CastViewModel</name>
    /// <summary>
    /// ViewModel for handling CastWindow
    /// </summary>
    /// <version>1.0 - Initial Release - 2/12/2013 1:57:53 PM</version>
    /// <author>Greg Arzoomanian</author>    
    class CastViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        /// <summary>
        /// A delegate type for hooking up message notifications.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void MessageEventHandler(object sender, MessageEventArgs e);

        public event MessageEventHandler MessageEvent;

        public class MessageEventArgs : EventArgs
        {
            public string Message { get; internal set; }
            public MessageEventArgs(string message)
            {
                this.Message = message;
            }
        }

        private Cast _spellCast;

        internal CastViewModel(Cast spellCast)
        {
            _spellCast = spellCast;
            PropertyChanged += NullMessageHandler;  // remove need to check for no handler
            MessageEvent += NullMessageHandler;
        }

        /// <summary>
        /// Null event to call for event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>To avoid checking for null event</remarks>
        private void NullMessageHandler(object sender, EventArgs e)
        {
            ;
        }

        #region Properties
        public string Level
        {
            get
            {
                string ret = _spellCast.Level.ToString("0.#");
                if (_spellCast.Level == 0)
                    ret = "Morphic";
                return ret;
            }
        }

        public string SpellName
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.Name;
                return ret;
            }
        }

        public string SpellCost
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Cost.ToString();
                return ret;
            }
        }

        public string SpellDescription
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.Description;
                return ret;
            }
        }

        public string ModifiersDescription
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.ModifiersDescription;
                return ret;
            }
        }
        public string SpellZOE
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.ZOE.DisplayValue;
                return ret;
            }
        }

        public decimal ExtraZOECost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = _spellCast.Spell.ZOEProperty.Modifier;
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        public bool ZOEIsModifiable
        {
            get
            {
                return _spellCast.Spell.ZOEProperty.Modifier != null;
            }
        }

        public bool ZOEIsNotModifiable
        {
            get
            {
                return _spellCast.Spell.ZOEProperty.Modifier == null;
            }
        }

        public int NumZOEModifiers
        {
            get { return _spellCast.GetNumModifiers(_spellCast.Spell.ZOEProperty.Modifier); }
            set
            {
                IModifier mod = _spellCast.Spell.ZOEProperty.Modifier;
                int current_num = _spellCast.GetNumModifiers(mod);
                bool up_direction = current_num < value;  // get direction
                if (mod != null)
                {
                    // adjust number of damage modifiers
                    if (up_direction)
                    {
                        try
                        {
                            _spellCast.AddModifier(mod);
                        }
                        catch (Cast.BadStateException excp)
                        {
                            MessageEvent(this, new MessageEventArgs(excp.Message));
                            //RestoreUpDnControlValue(upDnCtl, cur_val);
                        }
                    }
                    else
                        _spellCast.RemoveModifier(mod);

                    // Raise event that property has changed
                    PropertyChanged(this, new PropertyChangedEventArgs("SpellZOE"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Level"));
                    PropertyChanged(this, new PropertyChangedEventArgs("SpellCost"));
                }
            }
        }

        public string ZOEProperty
        {
            get
            {
                return _spellCast.ZOE.ToString();
            }
        }

        public string SpellRange
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Range;
                return ret;
            }
        }

        public decimal ExtraRangeCost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = GetRangeModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetRangeModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is RangeModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        public bool RangeIsModifiable
        {
            get
            {
                return _spellCast.Spell.RangeProperty.Modifier != null;
            }
        }

        public int NumRngModifiers
        {
            get { return _spellCast.GetNumModifiers(_spellCast.Spell.RangeProperty.Modifier); }
            set
            {
                IModifier mod = _spellCast.Spell.RangeProperty.Modifier;
                int current_num = _spellCast.GetNumModifiers(mod);
                //Console.WriteLine(String.Format("NumRngModifiers -- current_num = {0}; value = {1}", current_num, value));
                if (current_num == value)   // we're seeing binding send down same value as target sometimes
                {
                    return;
                }
                bool up_direction = current_num < value;  // get direction
                if (mod != null)
                {
                    // adjust number of damage modifiers
                    if (up_direction)
                    {
                        try
                        {
                            _spellCast.AddModifier(mod);
                        }
                        catch (Cast.BadStateException excp)
                        {
                            MessageEvent(this, new MessageEventArgs(excp.Message));
                        }
                    }
                    else
                        _spellCast.RemoveModifier(mod);

                    // Raise event that property has changed
                    RaiseLevelPropertyEvents();
                    PropertyChanged(this, new PropertyChangedEventArgs("SpellRange"));
                }
            }
        }

        private void RaiseLevelPropertyEvents()
        {
            PropertyChanged(this, new PropertyChangedEventArgs("Level"));
            PropertyChanged(this, new PropertyChangedEventArgs("SpellCost"));
            PropertyChanged(this, new PropertyChangedEventArgs("SpellDifficultyClass"));
        }

        public string SpellDuration
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Duration;
                return ret;
            }
        }

        public decimal ExtraDurationCost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = GetDurationModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        public int NumDurModifiers
        {
            get { return _spellCast.GetNumModifiers(_spellCast.Spell.DurationProperty.Modifier); }
            set
            {
                IModifier mod = _spellCast.Spell.DurationProperty.Modifier;
                int current_num = _spellCast.GetNumModifiers(mod);
                if (value == current_num)
                {
                    return;
                }
                bool up_direction = current_num < value;  // get direction
                if (mod != null)
                {
                    // adjust number of modifiers
                    if (up_direction)
                    {
                        try
                        {
                            _spellCast.AddModifier(mod);
                        }
                        catch (Cast.BadStateException excp)
                        {
                            MessageEvent(this, new MessageEventArgs(excp.Message));
                        }
                    }
                    else
                        _spellCast.RemoveModifier(mod);

                    // Raise event that property has changed
                    RaiseLevelPropertyEvents();
                    PropertyChanged(this, new PropertyChangedEventArgs("SpellDuration"));
                    PropertyChanged(this, new PropertyChangedEventArgs("LastingIsAvailable"));
                }
            }
        }

        private IModifier GetDurationModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is DurationModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        public bool DurationIsModifiable
        {
            get
            {
                return _spellCast.Spell.DurationProperty.Modifier != null;
            }
        }

        public string SpellSavingThrow
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.SavingThrow;
                return ret;
            }
        }

        public decimal SpellDifficultyClass
        {
            get
            {
                decimal ret = 0M;

                ret = _spellCast.DifficultyClass;

                return ret;
            }
        }

        public bool HasSavingThrow
        {
            get
            {
                return _spellCast.DifficultyClass > 0M;
            }
        }

        public bool HasNoSavingThrow
        {
            get
            {
                return !HasSavingThrow;
            }
        }

        public decimal HardToSaveCost
        {
            get
            {
                decimal ret = 0M;

                IModifier modifier = GetSaveModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetSaveModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is SaveModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        /// <summary>
        /// Saving Throw property can be modified
        /// </summary>
        public bool SaveIsModifiable
        {
            get
            {
                return (_spellCast.Spell.SaveProperty.Modifier != null)
                    && HasSavingThrow; // not clear why a spell with no save has a modifier
            }
        }

        public int NumSavModifiers
        {
            get { return _spellCast.GetNumModifiers(_spellCast.Spell.SaveProperty.Modifier); }
            set
            {
                IModifier mod = _spellCast.Spell.SaveProperty.Modifier;
                int current_num = _spellCast.GetNumModifiers(mod);
                bool up_direction = current_num < value;  // get direction
                if (mod != null)
                {
                    // adjust number of damage modifiers
                    if (up_direction)
                    {
                        try
                        {
                            _spellCast.AddModifier(mod);
                        }
                        catch (Cast.BadStateException excp)
                        {
                            MessageEvent(this, new MessageEventArgs(excp.Message));
                        }
                    }
                    else
                        _spellCast.RemoveModifier(mod);

                    // Raise event that property has changed
                    PropertyChanged(this, new PropertyChangedEventArgs("Level"));
                    PropertyChanged(this, new PropertyChangedEventArgs("SpellCost"));
                    PropertyChanged(this, new PropertyChangedEventArgs("SpellDifficultyClass"));
                }
            }
        }

        public bool TakesAffectsOthersProperty
        {
            get
            {
                bool ret = _spellCast.Spell.PropertiesList.Any(p => p.Name == SpellProperty.StdPropertyNames.AffectsOthers);

                return ret;
            }
        }

        public bool AffectsOthersProperty
        {
            get
            {
                bool ret = false;

                if (TakesAffectsOthersProperty)
                {
                    SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == SpellProperty.StdPropertyNames.AffectsOthers);
                    if (_spellCast.GetNumModifiers(spell_prop.Modifier) > 0)
                        ret = true;
                }

                return ret;
            }
            set
            {
                SetProperty(value, "AffectsOthers");
            }
        }

        private void SetProperty(bool value, string propertyName)
        {
            IModifier mod = _spellCast.Spell.ModifierList.FirstOrDefault(m => m.Name == propertyName);

            if (value)
            {
                try
                {
                    bool ok = _spellCast.AddModifier(mod);
                }
                catch (Cast.BadStateException excp)
                {
                    MessageEvent(this, new MessageEventArgs(excp.Message));
                }
            }
            else
            {
                _spellCast.RemoveModifier(mod);
            }

            // Raise event that property has changed
            RaiseLevelPropertyEvents();
        }

        public decimal AffectsOthersCost
        {
            get
            {
                decimal ret = 1M;   // default cost
                // Check for spell-specific value
                if (_spellCast.Spell.ExtraPropertiesList.Any(p => p.Name == SpellProperty.StdPropertyNames.AffectsOthers))
                {
                    SpellProperty s_prop = _spellCast.Spell.ExtraPropertiesList.First(p => p.Name == SpellProperty.StdPropertyNames.AffectsOthers);
                    IModifier mod = s_prop.Modifier;
                    ret = mod.Cost;
                }
                return ret;
            }
        }


        public bool TakesAtRangeProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == SpellProperty.StdPropertyNames.AtRange))
                {
                    ret = true;
                }

                return ret;
            }
        }

        public bool AtRangeProperty
        {
            get
            {
                return GetProperty(SpellProperty.StdPropertyNames.AtRange);
            }
            set
            {
                SetProperty(value, SpellProperty.StdPropertyNames.AtRange);
            }

        }

        private bool GetProperty(string propertyName)
        {
            bool ret = false;

            if (_spellCast.Spell.PropertiesList.Any(p => p.Name == propertyName))
            {
                SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == propertyName);
                if (_spellCast.GetNumModifiers(spell_prop.Modifier) > 0)
                    ret = true;
            }

            return ret;
        }

        public bool CastOnTheRun
        {
            get
            {
                return GetProperty(SpellProperty.StdPropertyNames.CastOnTheRun);
            }
            set
            {
                SetProperty(value, SpellProperty.StdPropertyNames.CastOnTheRun);
            }
        }

        public bool Concealment
        {
            get
            {
                return GetProperty(SpellProperty.StdPropertyNames.Concealment);
            }
            set
            {
                SetProperty(value, SpellProperty.StdPropertyNames.Concealment);
            }
        }

        /// <summary>
        /// Is Power Word modifier on spell, and affordable
        /// </summary>
        public bool PowerWordIsAvailable
        {
            get
            {
                bool ret = false;
                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == SpellProperty.StdPropertyNames.PowerWord))
                {
                    SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == SpellProperty.StdPropertyNames.PowerWord);
                    // Check if modifier already applied (so we're only removing)
                    if (_spellCast.GetNumModifiers(spell_prop.Modifier) > 0)
                        ret = true;
                    else 
                        ret = ModifierIsAffordable(spell_prop.Modifier);
                }

                return ret;
            }
        }

        public bool PowerWord
        {
            get
            {
                return GetProperty(SpellProperty.StdPropertyNames.PowerWord);
            }
            set
            {
                SetProperty(value, SpellProperty.StdPropertyNames.PowerWord);
            }
        }

        /// <summary>
        /// Is Lasting modifier on spell, and affordable
        /// </summary>
        public bool LastingIsAvailable
        {
            get
            {
                bool ret = false;
                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == SpellProperty.StdPropertyNames.Lasting) && _spellCast.DurationPropertyValue.CompareTo(_spellCast.Spell.DurationProperty.MaxValue) >= 0)
                {
                    SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == SpellProperty.StdPropertyNames.Lasting);
                    // Check if modifier already applied (so we're only removing)
                    if (_spellCast.GetNumModifiers(spell_prop.Modifier) > 0)
                        ret = true;
                    else
                        ret = ModifierIsAffordable(spell_prop.Modifier);
                }

                return ret;
            }
        }

        public bool Lasting
        {
            get
            {
                return GetProperty(SpellProperty.StdPropertyNames.Lasting);
            }
            set
            {
                SetProperty(value, SpellProperty.StdPropertyNames.Lasting);
                PropertyChanged(this, new PropertyChangedEventArgs("SpellDuration"));
            }
        }
        
        /// <summary>
        /// Return index of ReducedGestures combobox
        /// </summary>
        public int ReducedGesturesProperty
        {
            get
            {
                int ret = 0;
                SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == SpellProperty.StdPropertyNames.ReducedGestures);
                ret = _spellCast.GetNumModifiers(spell_prop.Modifier);

                return ret;
            }
            set
            {
                IModifier mod = _spellCast.Spell.ModifierList.FirstOrDefault(m => m.Name == SpellProperty.StdPropertyNames.ReducedGestures);
                int selected_num = value; // number of times to apply this mod (0, 1 or 2)
                int num_mods = _spellCast.GetNumModifiers(mod); // how many times is this mod already applied?
                if (selected_num > num_mods)    // need to apply more?
                {
                    while (selected_num > num_mods)
                    {
                        try
                        {
                            bool ok = _spellCast.AddModifier(mod);
                        }
                        catch (Cast.BadStateException excp)
                        {
                            MessageEvent(this, new MessageEventArgs(excp.Message));

                            break;
                        }
                        num_mods++;
                    }
                }
                else if (selected_num < num_mods)   // need to remove a mod (or 2)?
                {
                    while (selected_num < num_mods)
                    {
                        _spellCast.RemoveModifier(mod);
                        num_mods--;
                    }
                }

                // Raise event that property has changed
                RaiseLevelPropertyEvents();
            }
        }

        /// <summary>
        /// Return index of ReducedGestures combobox
        /// </summary>
        public int ReducedIncantationsProperty
        {
            get
            {
                int ret = 0;
                SpellProperty spell_prop = _spellCast.Spell.PropertiesList.FirstOrDefault(p => p.Name == SpellProperty.StdPropertyNames.ReducedIncantation);
                ret = _spellCast.GetNumModifiers(spell_prop.Modifier);

                return ret;
            }
            set
            {
                IModifier mod = _spellCast.Spell.ModifierList.FirstOrDefault(m => m.Name == SpellProperty.StdPropertyNames.ReducedIncantation);
                int selected_num = value; // number of times to apply this mod (0, 1 or 2)
                int num_mods = _spellCast.GetNumModifiers(mod); // how many times is this mod already applied?
                if (selected_num > num_mods)    // need to apply more?
                {
                    while (selected_num > num_mods)
                    {
                        try
                        {
                            bool ok = _spellCast.AddModifier(mod);
                        }
                        catch (Cast.BadStateException excp)
                        {
                            MessageEvent(this, new MessageEventArgs(excp.Message));

                            break;
                        }
                        num_mods++;
                    }
                }
                else if (selected_num < num_mods)   // need to remove a mod (or 2)?
                {
                    while (selected_num < num_mods)
                    {
                        _spellCast.RemoveModifier(mod);
                        num_mods--;
                    }
                }

                // Raise event that property has changed
                PropertyChanged(this, new PropertyChangedEventArgs("Level"));
                PropertyChanged(this, new PropertyChangedEventArgs("SpellCost"));
            }
        }

        /// <summary>
        /// Number of dice for damage for this casting
        /// </summary>
        public string DamageNumber
        {
            get
            {
                string ret = "";
                if (_spellCast.Damage != null)
                    ret = _spellCast.Damage.DisplayValue;
                return ret;
            }
        }

        /// <summary>
        /// Expectation value for damage for this casting
        /// </summary>
        /// <remarks>Calculated from damage property value of Cast</remarks>
        public string DamageExpectation
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                {
                    //string str = _spellCast.Spell.DamageDie;
                    if (_spellCast.Damage != null)
                    {
                        int damageDie = _spellCast.Damage.Dice.Sides;
                        int damageNum = (int)_spellCast.Damage.Value;
                        decimal expectation = (((decimal)damageDie + 1) / 2) * damageNum;
                        expectation += _spellCast.Damage.DiePlus * damageNum;   // add plus per die
                        expectation += _spellCast.Damage.Plus;      // add plus per roll

                        ret = expectation.ToString();
                    }
                }
                return ret;
            }
        }

        /// <summary>
        /// Maximum value for damage for this casting
        /// </summary>
        /// <remarks>Calculated from damage property value of Cast</remarks>
        public string DamageMax
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                {
                    //string str = _spellCast.Spell.DamageDie;
                    if (_spellCast.Damage != null)
                    {
                        int damageDie = _spellCast.Damage.Dice.Sides;
                        int damageNum = (int)_spellCast.Damage.Value;   // number of dice
                        decimal max = (decimal)damageDie * damageNum;
                        max += _spellCast.Damage.DiePlus * damageNum;
                        max += _spellCast.Damage.Plus;      // add plus per roll

                        ret = max.ToString();
                    }
                }
                return ret;
            }
        }

        /// <summary>
        /// Maximum value for damage for this casting
        /// </summary>
        /// <remarks>Calculated from damage property value of Cast</remarks>
        public string DamageMin
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                {
                    //string str = _spellCast.Spell.DamageDie;
                    if (_spellCast.Damage != null)
                    {
                        // int damageDie = _spellCast.Damage.Units.Sides;
                        int damageNum = (int)_spellCast.Damage.Value;   // number of dice
                        decimal min = damageNum;
                        min += _spellCast.Damage.DiePlus * damageNum;
                        min += _spellCast.Damage.Plus;      // add plus per roll

                        ret = min.ToString();
                    }
                }
                return ret;
            }
        }

        public int ExtraDamageNum
        {
            get
            {
                int ret = 0;

                IModifier modifier = GetDamageModifier();
                ret = _spellCast.GetNumModifiers(modifier);

                return ret;
            }
        }

        public int NumDamageModifiers
        {
            get { return ExtraDamageNum; }
            set
            {
                IModifier mod = _spellCast.Spell.DamageProperty.Modifier;
                int current_num = _spellCast.GetNumModifiers(mod);
                bool up_direction = current_num < value;  // get direction
                if (mod != null)
                {
                    // adjust number of damage modifiers
                    if (up_direction)
                    {
                        try
                        {
                            _spellCast.AddModifier(mod);
                        }
                        catch (Cast.BadStateException excp)
                        {
                            MessageEvent(this, new MessageEventArgs(excp.Message));
                        }
                    }
                    else
                        _spellCast.RemoveModifier(mod);

                    // Raise event that property has changed
                    RaiseLevelPropertyEvents();
                    PropertyChanged(this, new PropertyChangedEventArgs("DamageNumber"));
                    PropertyChanged(this, new PropertyChangedEventArgs("DamageExpectation"));
                    PropertyChanged(this, new PropertyChangedEventArgs("DamageMin"));
                    PropertyChanged(this, new PropertyChangedEventArgs("DamageMax"));
                }
            }
        }

        /// <summary>
        /// Cost in levels for extra damage
        /// </summary>
        public decimal ExtraDamageCost
        {
            get
            {
                decimal ret = 0;

                IModifier modifier = GetDamageModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetDamageModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is DamageModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        /// <summary>
        /// Signify we can't bump spell level any more
        /// </summary>
        private bool ReachedMaxSpellLevel
        {
            get
            {
                return (_spellCast.Level >= _spellCast.MaxLevel);
            }
        }

        /// <summary>
        /// Check if caster can currently afford cost of modifier
        /// </summary>
        /// <param name="mod"></param>
        /// <returns></returns>
        public bool ModifierIsAffordable(IModifier mod)
        {
            bool ret = true;

            if (_spellCast.Level + mod.Cost > _spellCast.MaxLevel)
                ret = false;

            return ret;
        }

        /// <summary>
        /// Name of current base level for morphic spell
        /// </summary>
        private string _morphicBaseLevel = null;

        /// <summary>
        /// Name of current base level for morphic spell
        /// </summary>
        public string MorphicBaseLevel
        {
            get
            {
                if (_morphicBaseLevel == null)
                {
                    _morphicBaseLevel = _spellCast.Spell.MorphicBaseName;
                }
                return _morphicBaseLevel;
            }
            set
            {
                string old_val = _morphicBaseLevel; // store off in case something goes wrong
                _morphicBaseLevel = value;
                bool ok = onBaseLevelChanged();   // adjust spellcast modifier
                if (!ok)
                {
                    _morphicBaseLevel = old_val;        // restore previous value
                    RaiseEvent("MorphicBaseLevel");     // restore combobox
                }
            }
        }

        /// <summary>
        /// For Base Spell Levels on Morphic spells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal bool onBaseLevelChanged()
        {
            // Get modifier for current morphic spell
            bool ok = true;
            try
            {
                _spellCast.SetMorphicBase(_morphicBaseLevel);   // set in list for this cast (will use to build XMuteSpell)
            }
            catch (Cast.BadStateException excp)
            {
                MessageEvent(this, new MessageEventArgs(excp.Message));
                ok = false;
            }
            if (ok)
            {
                RaiseLevelPropertyEvents();
                PropertyChanged(this, new PropertyChangedEventArgs("SpellRange"));   // Locate base can change range
            }

            return ok;
        }

        public ObservableCollection<string> BaseLevelList
        {
            get
            {
                return new ObservableCollection<string>(_spellCast.Spell.MorphMinder.MorphicBaseNamesList);
            }
        }

        //private string _morphicApplication = null;
        //public string MorphicApplication
        //{
        //    get
        //    {
        //        if (_morphicApplication == null)
        //        {
        //            SpellProperty spell_prop = _spellCast.Spell.ExtraPropertiesList[0];
        //            _morphicApplication = spell_prop.ModifierList[0].IncrementValue.DisplayValue;
        //        }
        //        return _morphicApplication;
        //    }
        //    set
        //    {
        //        bool ok = onMorphicApplicationChanged(value);   // adjust spellcast modifier
        //        if (ok)
        //            _morphicApplication = value;
        //    }
        //}

        /// <summary>
        /// For Base Spell Levels on Morphic spells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal bool onMorphicApplicationChanged(string application)
        {
            SpellProperty spell_prop = _spellCast.Spell.ExtraPropertiesList[0];
            IModifier mod = spell_prop.ModifierList.First(m => m.IncrementValue.DisplayValue == application);
            bool ok = false;
            try
            {
                ok = _spellCast.SetModifier(mod);
            }
            catch (Cast.BadStateException excp)
            {
                MessageEvent(this, new MessageEventArgs(excp.Message));
            }
            RaiseLevelPropertyEvents();
            RaiseEvent("MorphicApplication");

            return ok;
        }

        public IList<string> MorphicAppList
        {
            get
            {
                SpellProperty spell_prop = _spellCast.Spell.ExtraPropertiesList[0];

                return new List<string>(spell_prop.ModifierList.Select(m => m.IncrementValue.DisplayValue));
            }
        }

        public IList<SpellProperty> MorphicPropertyList
        {
            get
            {
                IList<SpellProperty> prop_list = new List<SpellProperty>(_spellCast.Spell.ExtraPropertiesList.Where(p => p.ModifierList.Count > 1));
                return prop_list;
            }
        }

        /// <summary>
        /// Needed to restore old value for morphic spell after level change failure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MorphicComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combobox = sender as ComboBox;
            if (!combobox.IsEnabled) return;

            SpellProperty spell_prop = combobox.Tag as SpellProperty;
            string old_val = spell_prop.Value.DisplayValue;
            IModifier mod = spell_prop.ModifierList.First(m => m.IncrementValue.DisplayValue == (string)combobox.SelectedValue);
            bool ok = false;
            try
            {
                ok = _spellCast.SetModifier(mod);
            }
            catch (Cast.BadStateException excp)
            {
                MessageEvent(this, new MessageEventArgs(excp.Message));
            }
            if (ok)
            {
                RaiseLevelPropertyEvents();
            }
            else
            {
                MessageEvent(this, new MessageEventArgs("At max spell level"));
                combobox.IsEnabled = false;
                combobox.SelectedValue = old_val;
                combobox.IsEnabled = true;
            }

        }

        /// <summary>
        /// Needed to restore old value for morphic spell after level change failure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UpdateComboBox(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cmobobox = sender as ComboBox;
            BindingExpression binding = BindingOperations.GetBindingExpression(cmobobox, ComboBox.SelectedValueProperty);
            //if (binding.HasError)
            //{
            binding.UpdateTarget();
            //}
        }

        /// <summary>
        /// Return list of extra properties for display in list
        /// </summary>
        /// 
        [Obsolete]
        public IEnumerable<ExtraProperty> ExtraPropertiesReader
        {
            get
            {
                foreach (SpellProperty spell_prop in _spellCast.Spell.ExtraPropertiesList)
                {
                    yield return new ExtraProperty(spell_prop, _spellCast, this);
                }
            }
        }

        /// <summary>
        /// Return list of extra properties for display in list
        /// </summary>
        public ObservableCollection<ExtraProperty> ExtraProperties
        {
            get
            {
                ObservableCollection<ExtraProperty> ret = new ObservableCollection<ExtraProperty>();
                foreach (SpellProperty spell_prop in _spellCast.Spell.ExtraPropertiesList)
                {
                    // Check if these are going on Morphic list
                    if (_spellCast.Spell.IsMorphic && spell_prop.ModifierList.Count > 1)
                        continue;   // don't put in special tab

                    ret.Add(new ExtraProperty(spell_prop, _spellCast, this));
                }

                return ret;
            }
        }

        /// <summary>
        /// Invoke PropertyChanged event
        /// </summary>
        /// <param name="propertyName">Name of WPF property to update</param>
        /// <remarks>Invoking event directly from another class doesn't update WPF property</remarks>
        internal void RaiseEvent(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Display extra property in Special Modifiers tab
        /// </summary>
        public class ExtraProperty : DependencyObject
        {
            private SpellProperty _spellProp;   // spell property to display
            private Cast _spellCast;            // spell cast to adjust
            private CastViewModel _viewModel;
            public ExtraProperty(SpellProperty spellProp, Cast spellCast, CastViewModel viewModel)
            {
                _spellProp = spellProp;
                _spellCast = spellCast;
                _viewModel = viewModel;
                IPropertyValue val = _spellCast.GetPropertyValue(_spellProp);
                PropertyValue = val.DisplayValue;
            }

            public string Name { get { return _spellProp.Name; } }

            public static readonly DependencyProperty PropertyValueProperty =
              DependencyProperty.Register("PropertyValue", typeof(string),
              typeof(ExtraProperty), new UIPropertyMetadata(null));

            public string PropertyValue 
            { 
                get { return (string)GetValue(PropertyValueProperty); }
                set { SetValue(PropertyValueProperty, value); }
            }
            public string Increment { get { return _spellProp.Modifier.IncrementValue.DisplayValue; } }

            /// <summary>
            /// Extra amount for this property
            /// </summary>
            private int _extra = 0;
            public int Extra
            {
                get
                {
                    return _extra;
                }

                set
                {
                    int old_val = _extra;
                    _extra = value;

                    // Check direction
                    if (_extra > old_val)
                    {

                        try
                        {
                            // Add modifier
                            bool ok = _spellCast.AddModifier(_spellProp.Modifier);
                            //_viewModel.RaiseEvent("Level");
                            //_viewModel.RaiseEvent("SpellCost");
                            _viewModel.RaiseLevelPropertyEvents();
                            if (_spellCast.Damage.Value > 0)    // Do we have damage for this spell?
                            {
                                _viewModel.RaiseEvent("DamageNumber");    // Do for Magic Missile increasing plus
                                _viewModel.RaiseEvent("DamageExpectation");    // Do for Magic Missile increasing plus
                                _viewModel.RaiseEvent("DamageMin");    // Do for Magic Missile increasing plus
                                _viewModel.RaiseEvent("DamageMax");    // Do for Magic Missile increasing plus
                            }
                            IPropertyValue val = _spellCast.GetPropertyValue(_spellProp);
                            PropertyValue = val.DisplayValue;
                        }
                        catch (Cast.BadStateException excp)
                        {
                            _viewModel.MessageEvent(this, new MessageEventArgs(excp.Message));
                            _extra = old_val;
                        }
                    }
                    else if (_extra < old_val) // remove modifier
                    {
                        _spellCast.RemoveModifier(_spellProp.Modifier);
                        _viewModel.RaiseEvent("Level");
                        _viewModel.RaiseEvent("SpellCost");
                        _viewModel.RaiseEvent("DamageNumber");    // Do for Magic Missile increasing plus
                        IPropertyValue val = _spellCast.GetPropertyValue(_spellProp);
                        PropertyValue = val.DisplayValue;
                    }
                }
            }

            public decimal Cost { get { return _spellProp.Modifier.Cost; } }

            /// <summary>
            /// Get description of modifier
            /// </summary>
            public string Description { get { return _spellProp.Modifier.Description; } }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Modify property based on updown control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        //[Obsolete]
        //public void NumUpDn_ValueChanged(object sender, EventArgs e)
        //{
        //    IntegerUpDown upDnCtl = sender as IntegerUpDown;
        //    VMBinding binding = upDnCtl.Tag as VMBinding;
        //    SpellProperty spellProp = binding.SpellProperty;
        //    if (!upDnCtl.IsEnabled)   // prevent re-execution of handler if counter reset
        //        return;

        //    IModifier mod = spellProp.Modifier;
        //    bool up_direction = true;  // get direction
        //    int new_val = (int)upDnCtl.Value;
        //    if (mod != null)
        //    {
        //        int cur_val = _spellCast.GetNumModifiers(mod);
        //        if (cur_val > new_val)
        //            up_direction = false;

        //        // adjust number of damage modifiers
        //        if (up_direction)
        //        {
        //            //// Check we're not at max level
        //            //if (_spellCast.Level + mod.Cost > _spellCast.MaxLevel)
        //            //{
        //            //    MessageEvent(this, new MessageEventArgs("At max spell level"));
        //            //    RestoreUpDnControlValue(upDnCtl, cur_val);

        //            //    return;
        //            //}
        //            //// Check we're not at max value
        //            //if (_spellCast.GetPropertyValue(spellProp).Equals(spellProp.MaxValue))
        //            //{
        //            //    MessageEvent(this, new MessageEventArgs("At max property value"));
        //            //    RestoreUpDnControlValue(upDnCtl, cur_val);

        //            //    return;
        //            //}

        //            try
        //            {
        //                _spellCast.AddModifier(mod);
        //            }
        //            catch (Cast.BadStateException excp)
        //            {
        //                MessageEvent(this, new MessageEventArgs(excp.Message));
        //                RestoreUpDnControlValue(upDnCtl, cur_val);                    
        //            }
        //        }
        //        else
        //            _spellCast.RemoveModifier(mod);

        //        // Raise event that property has changed
        //        NotifyLevelChanged(binding);
        //    }

        //    return;
        //}

        /// <summary>
        /// Notify controls that the spell's level changed
        /// </summary>
        /// <param name="binding"></param>
        //private void NotifyLevelChanged(VMBinding binding)
        //{
        //    PropertyChanged(this, new PropertyChangedEventArgs("Level"));
        //    PropertyChanged(this, new PropertyChangedEventArgs("SpellCost"));

        //    // See if modifiers can still be applied
        //    PropertyChanged(this, new PropertyChangedEventArgs("PowerWordIsAvailable"));

        //    // Notify specified controls
        //    foreach (string prop_name in binding.BoundProperties)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(prop_name));
        //    }
        //}

        /// <summary>
        /// Restore NumericUpDown control w/o re-invoking handler
        /// </summary>
        /// <param name="upDnCtl"></param>
        /// <param name="cur_val"></param>
        private static void RestoreUpDnControlValue(IntegerUpDown upDnCtl, int cur_val)
        {
            // Set control back
            upDnCtl.IsEnabled = false;   // keep code from executing 2x
            try
            {
                upDnCtl.Value = cur_val;   // keep value the same
            }
            finally
            {
                upDnCtl.IsEnabled = true;
                upDnCtl.Focus();
            }

            return;
        }

        /// <summary>
        /// Modify spell based on checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //public void CheckBox_ValueChanged(object sender, EventArgs e)
        //{
        //    CheckBox check_box = sender as CheckBox;
        //    if (!check_box.IsEnabled)   // prevent re-execution of handler if counter reset
        //        return;
        //    VMBinding binding = check_box.Tag as VMBinding;
        //    SpellProperty spell_prop = binding.SpellProperty;
        //    IModifier mod = spell_prop.Modifier;

        //    if (check_box.IsChecked == true)
        //    {
        //        try
        //        {
        //            bool ok = _spellCast.AddModifier(mod);
        //        }
        //        catch (Cast.BadStateException excp)
        //        {
        //            MessageEvent(this, new MessageEventArgs(excp.Message));
        //            check_box.IsEnabled = false;
        //            try
        //            {
        //                check_box.IsChecked = false;
        //            }
        //            finally
        //            {
        //                check_box.IsEnabled = true;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        _spellCast.RemoveModifier(mod);
        //    }

        //    // Raise event that property has changed
        //    NotifyLevelChanged(binding);

        //    return;
        //}

        /// <summary>
        /// Hard to do w/generic control
        /// </summary>
        /// <param name="excp"></param>
        /// <param name="check_box"></param>
        private void HandleBadStateException(Cast.BadStateException excp, CheckBox check_box)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Modify spell based on checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //public void ComboBox_ValueChanged(object sender, EventArgs e)
        //{
        //    ComboBox combo_box = sender as ComboBox;
        //    if (!combo_box.IsEnabled)   // prevent re-execution of handler if counter reset
        //        return;
        //    VMBinding binding = combo_box.Tag as VMBinding;
        //    SpellProperty spell_prop = binding.SpellProperty;
        //    IModifier mod = spell_prop.Modifier;

        //    int selected_num = combo_box.SelectedIndex; // number of times to apply this mod (0, 1 or 2)
        //    int num_mods = _spellCast.GetNumModifiers(mod); // how many times is this mod already applied?
        //    if (selected_num > num_mods)    // need to apply more?
        //    {
        //        while (selected_num > num_mods)
        //        {
        //            try
        //            {
        //                bool ok = _spellCast.AddModifier(mod);
        //            }
        //            catch (Cast.BadStateException excp)
        //            {
        //                MessageEvent(this, new MessageEventArgs(excp.Message));
        //                combo_box.IsEnabled = false;
        //                try
        //                {
        //                    combo_box.SelectedIndex = _spellCast.GetNumModifiers(mod);
        //                }
        //                finally
        //                {
        //                    combo_box.IsEnabled = true;
        //                    combo_box.Focus();
        //                }

        //                break;
        //            }
        //            num_mods++;
        //        }
        //    }
        //    else if (selected_num < num_mods)   // need to remove a mod (or 2)?
        //    {
        //        while (selected_num < num_mods)
        //        {
        //            _spellCast.RemoveModifier(mod);
        //            num_mods--;
        //        }
        //    }

        //    // Raise event that property has changed
        //    NotifyLevelChanged(binding);

        //    return;
        //}
        
        #endregion

    }

    public class DecimalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            decimal dec_val = (decimal)value;

            return dec_val.ToString("0.#");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
