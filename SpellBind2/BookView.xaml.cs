﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using ArzooSoftware.SpellBinder.spellbinder;
using System.ComponentModel;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <summary>
    /// Interaction logic for BookView.xaml
    /// </summary>
    public partial class BookView : Window
    {
        public BookView()
        {
            InitializeComponent();
            int mageLevel = Properties.Settings.Default.MageLevel;
            int mageIntelligence = Properties.Settings.Default.MageIntelligence;
            _casterViewModel = new BookViewModel(new Mage(mageLevel, mageIntelligence), spellLstVw);
        }

        /// <summary>
        /// Holds caster info for BookView
        /// </summary>
        BookViewModel _casterViewModel;

        public class LVData
        {
            public string Name { get; set; }
            public string ZOE { get; set; }
            public string Range { get; set; }
            public string Duration { get; set; }
            public string SavingThrow { get; set; }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Get list of spells
            spellLstVw.ItemsSource = SpellBook.Book.List;

            // Set group levels
            CollectionView cv = (CollectionView)CollectionViewSource.GetDefaultView(spellLstVw.ItemsSource);
            PropertyGroupDescription pgd = new PropertyGroupDescription("Level");
            cv.GroupDescriptions.Add(pgd);
            var sd = new SortDescription("Level", ListSortDirection.Ascending);
            cv.SortDescriptions.Add(sd);
            sd = new SortDescription("Name", ListSortDirection.Ascending);
            cv.SortDescriptions.Add(sd);

            // casterGrpBx.DataContext = _casterViewModel;
            Grid grid = spellLstVw.Parent as Grid;
            grid.DataContext = _casterViewModel;
        }

        /// <summary>
        /// Display double-clicked spell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void spellLstVw_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //Get the ItemsControl and then get the item, and 
            //check there is an actual item, as if we are using 
            //a ListView we may have clicked the
            //headers which are not items
            // as per http://www.codeproject.com/Articles/42111/Selector-DoubleClick-Behaviour-calling-ViewModel-I
            ItemsControl listView = sender as ItemsControl;
            DependencyObject originalSender =
                e.OriginalSource as DependencyObject;
            if (listView == null || originalSender == null) return;

            DependencyObject container =
                ItemsControl.ContainerFromElement
                (sender as ItemsControl,
                e.OriginalSource as DependencyObject);

            if (container == null ||
                container == DependencyProperty.UnsetValue) return;

            // found a container, now find the item.
            object activatedItem =
                listView.ItemContainerGenerator.
                    ItemFromContainer(container);

            if (activatedItem == null)
            {
                return;
            }

            ISpell spell = spellLstVw.SelectedItem as ISpell;
            if (spell != null)  // scroll clicks being mis-interpreted
            {
                // See that spell is castable by mage
                if (CostCalc.GetMaxLevel(_casterViewModel.Mage.Level) < spell.Level)
                {
                    MessageBox.Show(String.Format("Mage of level {0} can't throw spell of level {1}", _casterViewModel.Mage.Level, spell.Level));
                }
                else
                {
                    CastWindow cast_win = new CastWindow(spell, _casterViewModel.Mage);
                    cast_win.Show();
                }
            }
        }

        private void ShowDurationCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void ShowDurationExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            UpdateViewModel();
            DurationView duration_view = new DurationView();
            duration_view.Show();
            e.Handled = true;
        }

        private void ShowRangeCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void ShowRangeExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            UpdateViewModel();
            RangeView duration_view = new RangeView();
            duration_view.Show();
            e.Handled = true;
        }

        private void ShowCategoryCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void ShowCategoryExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            UpdateViewModel();
            CategoryView duration_view = new CategoryView();
            duration_view.Show();
            e.Handled = true;
        }
        /// <summary>
        /// Update the view model for all bound controls
        /// </summary>
        private void UpdateViewModel()
        {
            FrameworkElement[] control_list = { casterIntTxtBx, casterLvlTxtBx, intBonusTxtBx };
            foreach (FrameworkElement control in control_list)
            {
                BindingExpression binding = control.GetBindingExpression(TextBox.TextProperty);
                binding.UpdateSource();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //string select_tag = ((ComboBoxItem)SelectShowCmbBx.SelectedItem).Tag as string;
            string select_tag = ((Button)sender).Tag as string;
            CommandBinding[] list = new CommandBinding[this.CommandBindings.Count];
            this.CommandBindings.CopyTo(list, 0);
            CommandBinding binding = list.FirstOrDefault(c => ((RoutedUICommand)c.Command).Text == select_tag);
            // CommandBinding binding = this.CommandBindings[0];
            binding.Command.Execute(this);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem property_item = sender as MenuItem;
            ShowPropertyBased.Tag = property_item.Tag;
            ShowPropertyBased.Content = "Show " + property_item.Header;
        }

    }

    /// <summary>
    /// Converter class to display Group
    /// </summary>
    public class GroupLevelConverter : IValueConverter
    {

        public object Convert(object value, System.Type targetType,
                              object parameter,
                              System.Globalization.CultureInfo culture)
        {
            if (null == value)
                return "null";


            int level = (int)(decimal)value;
            string ret = String.Format("Level {0}", level);
            if (level == 0)
                ret = "Morphic";

            return ret;
        }

        public object ConvertBack(object value, System.Type targetType,
                                  object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }

    public class SpellCastableConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool ret = true;

            decimal? level = (decimal?)value;

            if (CostCalc.GetMaxLevel(Properties.Settings.Default.MageLevel) < level)
            {
                ret = false;
            }

            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
