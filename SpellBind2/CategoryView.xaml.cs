﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using ArzooSoftware.SpellBinder.spellbinder;
using System.ComponentModel;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <summary>
    /// Interaction logic for DurationView.xaml
    /// </summary>
    public partial class CategoryView : Window, IPropertyView
    {
        public CategoryView()
        {
            InitializeComponent();
            int mageLevel = Properties.Settings.Default.MageLevel;
            int mageIntelligence = Properties.Settings.Default.MageIntelligence;
            _casterViewModel = new BookViewModel(new Mage(mageLevel, mageIntelligence), spellLstVw);
            _propertyView = new PropertyView(spellLstVw, _casterViewModel, this);
        }

        /// <summary>
        /// Holds caster info for BookView
        /// </summary>
        BookViewModel _casterViewModel;

        PropertyView _propertyView;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Create list of spells will all possible durations
            List<ISpell> castable_spell_list = SpellBook.Book.List.Where(s => CostCalc.GetMaxLevel(_casterViewModel.Mage.Level) >= s.Level).ToList();
            List<Cast> spell_list = castable_spell_list.Select(s => new Cast(s, _casterViewModel.Mage)).ToList();
            spellLstVw.ItemsSource = spell_list;

            // Set group levels
            CollectionView cv = (CollectionView)CollectionViewSource.GetDefaultView(spellLstVw.ItemsSource);
            PropertyGroupDescription pgd = new PropertyGroupDescription("Category");
            // pgd.Converter = new GroupDurationConverter();
            cv.GroupDescriptions.Add(pgd);

            //pgd = new PropertyGroupDescription("Category");
            //pgd.Converter = new GroupCategoryConverter();
            //cv.GroupDescriptions.Add(pgd);

            SortDescription sort_description = new SortDescription("Category", ListSortDirection.Ascending);
            cv.SortDescriptions.Add(sort_description);

            sort_description = new SortDescription("Level", ListSortDirection.Ascending);
            cv.SortDescriptions.Add(sort_description);

            sort_description = new SortDescription("Spell.Name", ListSortDirection.Ascending);
            cv.SortDescriptions.Add(sort_description);

        }

        /// <summary>
        /// Display double-clicked spell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void spellLstVw_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _propertyView.spellLstVw_MouseDoubleClick(sender, e);
        }


        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (spellLstVw != null)
            {
                _propertyView.ComboBox_SelectionChanged(sender, e);
            }
        }

        #region IPropertyView Members

        public bool AtMaxValue(Cast cast)
        {
            return false;
        }

        public IModifier GetPropertyModifier(ISpell spell)
        {
            IModifier ret = null;

            return ret;
        }

        #endregion
    }

    public class DurationCostConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string ret = "-";
            int num_ret = 0;
            string duration_descr = parameter as string;
            IPropertyValue target_duration = new NumericValue(12m, "hours");
            if (duration_descr == "1DAY")
            {
                target_duration = new NumericValue(1m, "day");
            }
            else if (duration_descr == "LASTING")
            {
                target_duration = new DescribedValue("Lasting");
            }

            Cast cast = value as Cast;
            if (cast.Spell.DurationProperty.Value is NumericValue)  // must be extendable
            {
                Cast mod_cast = new Cast(cast);     // create version to modify, (leaving original untouched)
                while (mod_cast.DurationPropertyValue.CompareTo(target_duration) < 0)
                {
                    if (CostCalc.GetMaxLevel(cast.Mage.Level) <= mod_cast.Level)
                        break;

                    IModifier mod = new DurationModifier(cast.Spell.DurationProperty.Value);
                    mod_cast.AddModifier(mod);
                }

                if (mod_cast.DurationPropertyValue.CompareTo(target_duration) >= 0)
                {
                    num_ret = mod_cast.Cost;
                }
            }

            if (num_ret > 0)
                ret = num_ret.ToString();

            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class LevelDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            decimal level_num = (decimal)value;
            string ret = level_num.ToString();
            if (level_num == 0) // Morphic spell
                ret = "M";

            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
