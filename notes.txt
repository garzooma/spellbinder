
3/11/11
SpellBinder needs
	Make distinction in spells
		Abstract spell
			info in book
			spellbook entry
			magic
			spellcraft
		Concrete spell
			thrown my MU of certain level
			w/specific modifiers
				new level
			as thrown
			spellwork
			cast
	Subclass damage spells
		distinguish from non-damage
			Dim Door
	Common spell considerations
		extra duration
		extra range
Need to consider similar distinction for modifiers
	as for spells
	SpellBookEntry modifier
		one each of whatever is available
	Cast modifier
		multiple modifiers
		different kinds
		stacked
		magnitude
			distance
			time
			damage
	Different kinds of modifiers
		Extra Duration
		Extra Range
		Extra effect
			Extra damage
		Other
Goal
	aid in putting together spell
		see options
		making calculations
	Show costs of doing what you want
		rather than showing how to calculate what you want

1/7/2013
Abstract instance
Is this a good case?
	Book spell does describe base info
		to which you just add modifiers
	What doesn't belong in ISpell? (book spell)
		cost
			depends on Mage
		expectation
			depends on mods
		cast spell level
			after mods
Modifier handling
	need for both abstract & concrete
		abstract: possible modifiers
			may define Modifier Factory
		concrete: actual list
Abstract vs concrete
	abstract
		defines possible values
	concrete
		actual values
		status
		id
		history
	InstanceBuilder
		StringBuilder, ConnectionBuilder

UI issues
	Showing book spell or cast?
	modifiers especially difficult
		want to show possible modifiers
		as well as what we have so far

Alternative to builder
	freezable objects
		modify while building
		then freeze
			all setters throw exceptions
			only have run-time check
				not build time

UI notes
	need to distinguish base from adjusted values
		level (always base + mods)
		cost (ditto)
		range/duration (may or may not be modifiable)
		damage (usually modifiable)
	fill text box based on mods
		gray -- not modifiable
		black -- base, unchanged
		green -- changed (base grey in parens)
	want to show 3 items
		base
		extra (in increments)
			cost due to extra (?)
				in levels (mainly .5/level)
				in spell points (depends on order -- may not be doable)
		total
		dice (for damage)
			for both base + increments
				could be nD8 base + nD4 extra (theoretically)
		?Popups for mods?
			only useful for items that don't matter after setting
			would like to see mods, ordinarily
	cost also could be expanded
		increments up and down
		min cost level
	damage expectation expansion
		min, max
Damage subclass
	for Spell (as per book)
	for Cast (?)
		needed to get modifiers right
	?Interface
		IDamage
		for both Spell & Cast?
		handle non-damage like non-duration/range
Modifier list on Cast
	hetero list
		different kinds of modifiers
			damage, duration, range
	adding ok
	removing a problem
		need to remove correct kind
		not so bad -- have reference

Damage value object
	number of dice
	sides on die
	base value (+/-)

Need to handle maximum setting for upping modifiers

Need to fix modifier xml
	modifier/type instead of name

1/9
Do we need immutable spell?
	App is for building
	no actual using
Helps for thinking about spell
	only contains what actually matters in spell
	build-time only components left for builder
		possible values

1/10
Generic Modifier
	has cost
		in (usually .5) levels
	increases parameter
		takes existing value
		returns next value up
			per formula
			per schedule
			combination
	type
		determines parameter to update
			and schedule
Value type
	for being modified by modifier
	ToString() for display
	IPropertyValue
	"Spell modifiers affect the normal properties of a spell"
	=> modifier deals w/property & property value
	examples
		damage
			die
			number
			plus d10+1
			expectation (calculated)
			min/max
		duration
			kind (momentary, permanent)
			permenant
			numeric value
			unit
			schedule (1)
		zoe
			kind (target, self, sphere)
			numeric (20)
			units (feet)
		range
			kind (touch, LOS)
			numeric
			units
			schedule (2)
		movement
			Expeditious retreat
			Nx faster
				starting w/N=2
		monster
			level
			critter list
		booleans
			At Range
			Cast on the Run
			Affects Others
			Power Word
	Using strings for values
		need to parse in modifier
	subvalues
		e.g. Damage
	max value
		describe range
		boolean is special case (0-1)
	general value types
		specified (momentary)
			unitless
		numeric
			w/units
			units can be objects (Dice)
		on schedule
			above schedule
Spell Property type
	ISpellProperty
	has name
	has value
		which gets displayed
	Can be modified predicate
		iff spell has appropriate modifiers

Modifier options
	modify Cast
		Currently Cast modifies itself
	modify Spell Property
	supply new property value

Default modifiers
	also, removing defaults

1/11
Saving throw modifiers
	numeric
	dimensionless (no units)
	affects DC

1/12
Adding Modifier
	e.g. Saving Throw
	steps
		create IModifier
			Bump() implementation
		set in Base Spell
			 if in xml 
			 or standard
		add to Cast
			on up tick
		bindings
			Extra cost
			Adjusted Value
		spellinfo xml handling
			 if special/nonstandard modifier

1/12
IPropertyValue
	Needed for saving throw modifier
		SavingModifier.Bump(IPropertyValue)
		need to increase number
		otherwise, need to parse numeric string
	kinds
		string
			used for non-numeric progression
			currently using this
		numeric
			can have units
			use for duration/range
			also save
				w/o units
		damage
			die/number
	Methods
		GetValue
			specified by Modifier
		need to be called in Cast.Bump()
		Next()
			otherwise, need if statement
	value types
		modifier specific
			Damage
			need this for dice
			!Make it DiceValue
		generic
			numeric
	Hard to make this polymorphic
		need to extract values
			display
				use string
			calculations
				damage expectations
				saving throw (DC)
		also, look into Match() method
			for checking specific values
1/14
UpDown Handlers
	differences
		Modifier
		updown control
		bound controls

Resolve IPropertyValue, IModifier interaction
	who's responsible for Next()/Bump()?
	already need Evaluation value
		for further calculations
			Expectation value
			Saving throw

Current To Do
	Disable controls for invalid properties
		no DC for non-save spells
	Refactoring
		Damage property
			also Range/Duration
		ISpell/BaseSpell/SpellInfo merge
		IModifier common base class
		PropertyValue/Modifier incrementing
			?IModifier.IncrementValue
		SpellValue xml rationalizing
			type, die, units, oh my!
		ZOE/Damage/Save property handling
	Lack of polymorphism
		Filter on modifiers
		casting PropertyValues
	WebBrowser for links in description
	Cost on boolean modifiers
	Comments
	prefix Units (+/1)
	Morphic spells
		Tables
	MVC version
	UML -- sequence diagram
	Modifier descriptions
	Multi-property modifiers (multi-value values)
		e.g. Bag of Holding
	Multiplier increments for modifier
		like ZOE
	Lastingable spells list
	Base level cost in SpellBook display
	Die value durations
	XML escape characters
		also w/HTML

Special properties
	per spell
	don't want spell-specific classes
	Generic property
		w/generic values
		& generic display
	examples
		Lance: Increased accuracy
		Confusion: extra duration (no standard duration modification)
		Displace Image: extra effect (additional -1 modifier for "to hit"; up to -5)
		Expeditious Retreat: extra effect (increase multiplier by 1)
Cost on modifiers
	put in label

1/19
Damage modification
	occasioned by Magic Missile
		Extra Damage
			=> more missiles
			regular damage specification
		has plusses
			incorporate into damage expectation
		has Extra Damage
			+1 per missile
			?How to integrate?
				As SpellProperty
					<= don't want special class
					DicePlus
				just a number
				DamageModifier can handle

1/20
Property max value handling
	for booleans
		Numeric value
		max = 1
	duration
		ends up Lasting
	Displace Image max (min) defensive bonus: -5
	Implementing
		on SpellProperty
		Max
			has Max
			Boolean (max = 1)
	Use
		keep controls from incrementing
		indicate on control

1/23
Non-schedule range/duration modifications
	double
	IModule.Bump()
	Need to rationalize IModule.Bump()/IPropertyValue Increment()
	Currently
		IModule.Bump() calls IPropertyValue.Increment()
		IModule references property
			Damage, Duration
		IPropertyValue references generic values
			Numeric, Scheduled, DiceValue
	Handling IPropertyValue phase change
		ScheduleValue -> NumericValue
			ScheduleValue get detect end of schedule
		NumericValue -> Schedule
			containing IModifier needs to detect

Spellbook ideas
	range schedule should only bump > 2x
		480' -> .25 = 1320' > 960'
		not 720' -> .25 < 1440'
		=>
		20�, 40�, 90�, 180�,...
		->
		20�, 50�, 120�, 240�,...
		catches up to 30�, 60�, 120� 240�,...

1/24
Display object
	combines bound control list w/property value
		for tagging control
		moving handler to PM
			get bindings to update
	also hold display property
		to bind extra properties

1/25
At Range trouble
	Hold Portal
		10' Range
		not on schedule
	also need to watch for described close range values
	base spell w/o modifier
		RangeProperty builds Modifier
		w/o schedule
	RangeProperty modifier goes to Range uptick
		no schedule
		after AtRange check
		despite being 60'
	XMute version
		gets range properly
		SpellForm.GetRangeModifier()
			gets different RangeModifier
				has schedule
			than numericUpDn.Tag
				no schedule
	Need to get modifier thru SpellCast
		to get XMuteSpell version
		=> need to use string name
	Multiple modifiers per SpellProperty
		in BaseSpell class
		e.g. Damage property
		separate id's for modifiers
		different aspects of property
		need separate id for UI
		Special for DamageProperty
			generic SpellProperty has a single modifier

1/28
NumericValue
	_increment field not used
	incrementing done w/increment value passed in
		by Modifier

1/31
UI markings
	Fixed
		Label
	Modifiable
		Program modifiable
			Textbox
				ReadOnly
		User modifiable
			 TextBox
	 Modified
		Last change
			Blue
		Previously modified
			Green

Morphic Spells
	no fixed level
		per Morphic Spells section on p. 51
	only spells of level 1 have cost
		per table
	=> use level 0
		xmute to other bases
			Attune
			Hallucination
			Illusion
			Locate
		adjust level to 1 for calculating costs
	Patterning
		needs hardness
	Monster Summoning
		multiple monsters from lower levels
	Base levels
		also descriptor
	UI changes
		replace Damage box
		Different controls
			checkboxes
			combobox
				for base levels
			updown
				increasing degrees
			attach to SpellProperties

2/2
Morphic spells
	base level handling
	XMute to different level
	XMute ctor in Cast.Spell property
		Currently checking AtRange
		takes BaseSpell, SpellProperty parameters
			SpellProperty for Range
			replaces BaseSpell.RangeProperty
		=> replace Level property
		Locate
			both level and range modification
	Named value schedule
		Hallucination
			target
				single or ZOE
			type of hallucination
				different pluses
		Shaping
			Abilities
				named (no value)

2/3
Spell holds properties
	along with  Logic
	Cast holds modifiers

Modifier/PropValue incrementing
	2 increment methods
		PropValue.Increment(incrementAmount)
			adds incrementAmount to current value
			usual method
		Cast increment
			get IModifier.IncrementValue
			build new value
			for Damage ExtraPlus

Multiple Modifier values
	Multiple modifiers?
		new IModifier
		multiple costs
	Single Modifier/multiple values?
		schedule
	Single modifier goes to Cast
		new modifier replaces previous

2/7
Morphic spell
	Applications
		not in list like regular SpellProperties
		handled by special UI controls
	Other spells
		different UI elements
Attune
	2 base levels
Counterspell
Enhance Ability
	Increase to 20
	Increase past 20
	! Would like starting value
Hallucination
	single target or 20' sphere
	damage: none, non-lethal, lethal
	incapacitating or not
	complex or not
Illusion
	visual levels
	aural levels
	programmed
	interactive
	traveling
	Double ZOE
Locate
	Base Level
		Attuned object
		Specific Person
		Specific Object
		Any Object of Specific Type
	Get distance
	All objects
	Affect others
Monster Summoning
	Monster table
		level
		monster
Patterning
	base level
	application
Pyromancy
	applications
	magical
	extra ZOE
Shaping
	base level
	changes
	abilities
Skylore
	Levels
	Traveling
Veil
	Base Level -- target
	Protection
	magic possessions
	increased difficulty to penetrate

UI change -- tabs
	Different sets
		Standard properties
		Damage
		Description
		Special Properties
		Morphic properties
		Spell specific

2/16
Morphic spells
	special tab
		based on Patterning
		widgets
			base level
			application
			doesn't include cost
				should have on combobox items
			combines value & selector
	special properties
		w/spinner
		name, value, increment, extra (spinner), cost
		?Can we put other controls in specials list?
		? will this serve for morphic spells?
		? Do we need "Incr" amount
			don't have on standard properties
			show final value
			show "Cost" as "@<val> per <incr>"

2/17
Morphic properties
	show in Morphic tab
	not in Specials
		need to make distinction
			number of options
			flag in xml
			set vs. add
		Don't show Specials Tab if empty
		might like controls on same tab
	Retaining Special tab
		indefinite incrementing
			Enhance Ability
			?CounterSpell
			Illusions modes
				maybe do options

Control binding problem
	restoring value not working
	Raising Event doesn't cause reread
	need to make sure value put back first
		wasn't happening on many experiments
	Need to test
		use examples on web
		add validation
		restore if not valid

Property categorizing
	Applicability
		Standard
			at bottom of each spell
		Pseudo-std:
			Damage (lots of spells)
		Special (per spell)
	Value kind
		incrementing
		schedule
		described list
	Cost kind
		incrementing
		specified

2/18
MVC
http://www.asp.net/mvc/tutorials/mvc-4/getting-started-with-aspnet-mvc4/intro-to-aspnet-mvc-4
Steps
	Create project
	Create controller
		?Map to CastViewModel/PresentationModel?
	Create View
	Create Model
		?Use existing SpellBinder?

Sequence diagram for Spellbinder
	Participants
		CastWindow.xaml
		SpellCast
		CastViewModel

2/24
Blinding flash
	DieValue duration

3/6
L'Artisan code: magenta1

3/9
Mage info in app-config
	Settings
	in Users\Application Data

3/13
Infosaic
	ozymajor.com
	simple
	!late
azure
	url's
		SITE URL
		http://spellbinder.azurewebsites.net
		COMPUTE MODE
		Free
		FTP HOSTNAME
		ftp://waws-prod-blu-001.ftp.azurewebsites.windows.net
		FTPS HOSTNAME
		ftps://waws-prod-blu-001.ftp.azurewebsites.windows.net
		DEPLOYMENT / FTP USER
		spellbinder\garzooma
	managing
		https://manage.windowsazure.com/#Workspaces/WebsiteExtension/Website/spellbinder/quickstart

3/15
XML issues
	odd chars in spellbook.xml
		apostrophe's, too
		Check how displayed by WPF
	display in browser

Doubling modifiers
	Quickmarch
	Growth
		Size Change
		Growth Animals
	Check ZOE modification

Die values for duration
	e.g. Blinding Flash
	UnitTest
		tBaseSpell.tDieValueProperty()
			calls BaseSpell.DurationProperty
			checks SpellInfo.DurationUnits
				if there, parses SpellInfo.DurationUnits
			Need generic way to get Die info from XML in property
				from node
				SpellInfo.GetPropertyDieValue(<prop name>)

Inconsisent location of parsing
	and XML access
	Do census
		XmlSelectSingleNode()
		TryParse()
	Properties
		w/Modifiers
		ZOE
			BaseSpell.ZOEProperty
				SpellInfo.ZOEValue
				SpellInfo.ZOEUnits
			ZOEModifier(decimal modAmount)
		Duration
			BaseSpell.DurationProperty
				SpellInfo.DurationValue
				SpellInfo.DurationUnits
			DurationModifier(XmlNode node) : base("Duration", 0.5M)
		Range
			BaseSpell.RangeProperty
				SpellInfo.RangeValue
				SpellInfo.RangeUnits
			RangeModifier(NumericValue[] schedule) : base("Range", 0.5M, "")
		Saving Throw
			BaseSpell.SaveProperty
				SpellInfo.SavingThrowValue
				SpellInfo.SavingThrowEffect
			SaveModifier() : base("Saving Throw", 0.5M) { }
		Damage
			DiceValue BaseSpell.Damage
				SpellInfo.Damage
			BaseSpell.DamageProperty
				SpellInfo.Damage
			DamageModifier(XmlNode node) : base ("Damage", 0.0M, "")
			ctor
			Modifiers
		Extra
			SpellInfo.ExtraProperties
			ctor
			Modifiers
				all from XmlNode SpellInfo._node
			Deals w/multiple property value types
				need to extract and use for other properties
Multivalue Values
	PassWall
		6'x8'x10'
		Mod: 3'x4'
		Length: 0x0x10'
	Implement as list of other values

3/19
Refactoring Damage
	For refactoring properties for die value durations
	Damage related items in classes
		SpellInfo
			string DamageDie (used in BaseSpell.DamageDie)
			string DamageNumber (used in BaseSpell.DamageNumber)
			DiceValue Damage (used in BaseSpell.Damage, BaseSpell.DamageProperty)
		BaseSpell
			int DamageNumber (used in Cast.DamageNumber)
			string DamageDie (used in CastViewModel)
			DiceValue Damage (used in Cast.Damage)
			SpellProperty DamageProperty (used in CastWindow.xaml)
		Cast
			int DamageNumber (only used in tests)
			DiceValue Damage (used in CastViewModel)
	Approach
		remove DamageNumber from SpellInfo/BaseSpell/Cast
			modify tests to use Cast.Damage
		replace BaseSpell.DamageDie in CastViewModel
			?also BaseSpell.Damage

3/20
Refactoring BaseSpell properties
	getting info from SpellInfo
	Doing parsing
		like to leave in SpellInfo
		As per ExtraProperties
	Have SpellInfo return IPropertyValue
		IPropertyValue currently being constructed in BaseSpell
		Determines Modifiers
			Need to check for DescribedValue in BaseSpell
	Approach
		Create SpellInfo method for creating IPropertyValue
			Extract code from ExtraProperties
		use in standard properties as well as specials
			Replace Range/Duration|Value/Units w/IPropertyValue RangeValue
		ZOE includes type
			=> return ZOEValue

3/22
Lastingable spells
	also 24/12 hours
	and duration N
		3 hours/90 min.
	Display
		w/spell book
			highlights
			problem showing cost
		separate display
			order by cost
			bring up w/lower right button
	Other ordering/filtering
		range
		save
		damage
			expectation/min/max
			w/ & w/o save
			factor in save likelihood

3/27
SpellMVC
	showing spinner
		need latest jquery/jqueryui
			update packages in VS
		load jqueryui script in header
			in _Layout.cshtml
		remove jquery script load in body

3/29
Ajax call
	From MVC Ajax book:
		http://www.manning.com/palermo3/ASPMVC4samplech7.pdf
	Steps
		Controller actions
			Index (page making Ajax call)
			PrivacyPolicy (return Ajax result)
				Return PartialView
				[HttpPost] attribute for passing value
		Ajax result view
			PrivacyPolicy
		Ajax caller view
			Index
			Include AjaxDemo.js
			Link to make Ajax call
			control to display result
		Javascript source
			AjaxDemo.js
			Set link handler
			make Ajax call
			load result in control

4/3
MVC/AJAX
	Walkthru
		http://geekswithblogs.net/blachniet/archive/2011/08/03/walkthrough-updating-partial-views-with-unobtrusive-ajax-in-mvc-3.aspx
	Pieces
		Controller
			Index method
				returns Index view
					renders ProductListControl partial view
			Add_Index method
				HttpPost attribute
				returns ProductionListControl partial view

4/18
Std Mod
	Power Word modifier
		checking disables
		leaves modifier applied
	Disabled when cost too great
		only want to disable if unchecked
		want to allow disable at any point

Changing controls in special modifiers grid
	Currently UpDown control
	Set in CastWindow.xaml
		specPropLstVw listview
	Need to have this changeable in code

8/31
Lasting Duration
16 Days
	not correct for starting durations > 2 days
		e.g. 1 week, 4 days (Wizard Lock)
More precise:
	4 bumps above 1 day (<= Cost == 2)
	or 4 bumps above start
		if start > 1 day
Integrating base spell duration
	problematic
		Don't have spell info in DurationModifier.Bump()
	avaialable in caller
		Cast.Duration property
Discrepancy w/sheet
	No morphic spells
		base morphic typically useless
Increasing duration after lasting
	need to prevent
	check for cost @CastViewModel.NumUpDn_ValueChanged()
		Also checks v. MaxValue (Lasting)
			Cast.GetPropertyValue(SpellProperty)
				returning 16 days
				not Lasting
	Approach
		Move checks to model
		add add'l checks
			e.g. extra duration to Lasting
	Need to check for hitting maximum in GetProperty
		Notifying observers
		Action: dialog box
		?Event
			Also max spell cost
Checking for MaxValue at multiple points
	CastViewModel.NumUpDn_ValueChanged()
	Cast.GetPropertyValue()
	Cast.Duration()
		IsBumpedToLasting()
			not gen'l
			doesn't reference SpellProperty.MaxValue
			set to consider case of initial duration > 1 day
	Cast.AddModifier()
		Only for SpellModifier
		not DurationModifier
			?Maybe check for SpellProperty.MaxValue
				Needs SpelModifier.Property
				IModifier doesn't have Property property
	DurationModifier.Bump()
		?Maybe just convert to Lasting
			Needs to account for initial duration > 1 day
		
Duration has multiple MaxValues
	standard: 16 days
	non-standard -- initial duration > 1 day (Wizard Lock):
		base duration + 4 bumps
		<= Lasting cost of 2
MaxValue used for Duration & booleans
	Booleans defined in spellbook.xml
MaxValue check incoherence
	Cast.AddModifer()
		prevents adding of modifier if @max
	Cast.GetPropertyValue()
		checks against list of modifiers
		Cast.AddModifier() should prevent
Need more unit tests for MaxValues
	Before major code refactoring
	ViewModel simulations
		Duration
		boolean
		described (Veil Protection mod)
		Also test ViewModel methods directly
Notification of modification failure
	cases:
		Max cost
		Max value
	Messages
		w/event handler
			UI-specific (not gen'l)
	Codes
		Thru event handler
		return value
		exception

9/12
Approach
	Value retrieval
		Remove Lasting check from DurationModifier.Bump()
			Bump() doesn't know about spell
			although it does know about Duration
		Lasting check in Cast.Duration & Cast.GetPropertyValue()
			change result to Lasting
			even in GetPropertyValue(), which doesn't know about duration
				will need hack
	Cast adjustment
		Cast.AddModifier() prevent adding modifier past max
			throw exception
		CastViewModel.UpDn() catches exception
			puts up dialog box as appropriate
Hard because GetPropertyValue() doesn't know about duration
	=> can't cleanly look at base value
Leave out long initial duration check
	filter out in Lasting spell display
2nd approach
	Value retrieval
		Lasting check in DurationModifier.Bump()
			Put base duration in DurationModifier
			in ctor
			check if > 1 day
		 No Lasting check in Cast.Duration & Cast.GetPropertyValue()
			AddModifier prevents extraneous modifier

10/21
Warnings in Output Window
	problems w/Binding
		"System.Windows.Data Error: 40 : BindingExpression path error: 'Level' property not found on 'object' ''CollectionViewGroupInternal' (HashCode=35766385)'. BindingExpression:Path=Level; DataItem='CollectionViewGroupInternal' (HashCode=35766385); target element is 'TextBlock' (Name=''); target property is 'IsEnabled' (type 'Boolean')"
	No "Level" for Spell group
		"Name" works (for some reason)
	Remove "Enabled" binding in DurationView
		not disabling either Time or Category

1/16/2014
Spell references in descriptions
	(page xxx)
		Find w/Regex
	Want to find preceding Spell Name
		one or two words
		Capitalized
		possible "of" in between
	Extracting from description
		Want RTF Run
		don't want to depend on WPF
		create own object
	Reference object
		includes Spell Name
		location in description
			for replacing w/hyperlink
	Extracting reference object from description
		build w/state machine
		working backwards thru text
		need name while creating
			to compare w/list

1/19
NUnit fail
	On XP machine
	SpellBind2 running
		can't stop

Hyperlink to do
	cleanup
	connector words
		Wall of Fire
	spellbook object
		speeding link

1/23
Display other views
	duration
		save
			getting DC's
			group by save type
		range
	Show button
		w/combobox type
Removing event handlers on changing MageLevel
	in BookViewModel
	Refresh() on changing Mage Level
		calling PropertyChanged() should solve
		doesn't
Approach
	duplicate DurationView.xaml
	change list generation
	change grouping in CollectionView
Invoking
	Context Menu
	Button in BookView
		w/dropdown

2/6/14
Formatting
	Spell Descriptions
	need CR's
		extra CR's between paragraphs
		don't confuse w/single CR in lists
			marked w/*
		!No distinction between 1 or 2 CR's
			w/plain copied text
			!see about getting doc
	Also, fix special quote marks
	Tables
		?How to id
			list spells
				Shaping
				Veil
				Size Change
					real table
				Monster Summoning

Filter on Duration/Range windows
	by Category
Can remove Category column
	in grouping

2/9/14
Affects Other curiosity
	where applicable?
	ZOE determines (?)
		self
		what about "target"
			Fly
			Specifically mentioned in Modifiers section
		what about "recipient"
			only Quickmarch
	Special specification
		in Properties element
		as per Expeditious Retreat
	BaseSpell.TakesAffectsOthers() for calc.

2/15
Further work
	Formatting
		need ConBook source
	Better binding
		relying on events an manual transfer
		VMBinding
	More filters on views
	Automated UI tests

2/18
Removing VMBinding
	bind to property
	e.g. Cast.AffectsOthers
		actually CastViewModel.AffectsOthersProperty
	replace code in CastViewModel.CheckBox_ValueChanged()
		later NumericUpDn_ValueChanged()
		code adds IModifier to Cast
			gets from sender->control->tag->VMBinding
		problem for WPF binding
			boolean TakesAffectsOthers doesn't have sender

2/20
Modified binding change
	values bound
	xfer explicit
		done w/existing event handler
		control w/UpdateSource()
	update target
		w/list of property names
		fire PropertyChanged event
			get control specific info

2/24
Showing if property available
	If never available -> disable
	if available for cost -> show w/message box
	What if not available even at base cost?
		esp. Power word

New issue
	Morphic spells
	w/o base settings
		start w/Level 0
			=> Std. Property modifications doesn't make sense
			=> disable?
			need to make Level 1
		-> Morphic
		E.g. Enhance Ability

3/4
Layout changes
currently uses Designer settings
	absolute positioning using margin settings
WPF alternatives
	stackpanel/dockpanels for rows
		hard to line up elements
	grid
		set column widths
		use styles for portability

3/6
Property names
	strings
	need to use constants
		Class variables
	Also, fix Reduced Incantation(s)

3/7
Classes for testing
	Run class
		encapsulates Process
	Window class
		encapsulates window automation element
	CastWindow class
		Cast UI window (and values)
	Compare class
		expected values
		mapped w/control name
			also need tab
Property properties
	name: Range, Affect Others
	value:
	value UI
	modification UI: Updown arrows, checkboxes, comboboxes
	connected values (UI's): level & cost
		DC
		Damage
	
Existing methods
	GetDisplayedValue(AutomationElement winElement, string uiControlName)
		called from [Test] method

What we want
	exercise all modifiers
	view all displays

Properties
	ZOE: Tx, UD
	Range: Tx, UD
	Duration: Tx, UD
	Saving Throw: Tx, Tx, UD
	Damage: 4x Tx, UD
	Affects Others: Cx
	At Range: Cx
	Power Word: Cx
	Reduced Gestures: Cmb
	Reduced Incantation: Cmb
	Special: Tx

Saving Throw trouble
	DC not increasing w/level w/modifiers
	need to update binding

3/9
Checking values
	after modification
	certain changed controls
		Level
		Cost
		Save (DC)
	modification controls
		checkboxes
		comboboxes
		don't change w/o direct modification
		enabled state

3/15
Locate trouble
	attuned base: range s.b. 1 mi.
	<BaseLevels>
		<Level>
			as property (modifier, actually)
				XMuteSpell ctor
				in Cast.Spell getter
				?should try chaining properties
					for Locate
						Level property overrides
						then Range
						?How to work name
			more like spell
				a "Morph"
					Name
					Level
				w/its own property
		Need to employ Range property
		xml -> SpellProperty
			=> lose Range information
		SpellProperty -> XMuteSpell
New approach
	On Morphic spell
		replace Cast._spell
		don't put in level modifier
		use XMuteSpell
			will contain original BaseSpell
		Cast ctor needs to change
	On changing Morphic dropdown
		bound to CastViewModel.MorphicBaseLevel
		change _spell to new XMuteSpell
			new method in Cast to mimic changed ctor
	Change Spell XML
		<BaseSpells>
			<Spell Name="Attuned" Level=1> 
				<Range value='1' units='mile'/>
May be problem w/getting enable info to UI
	want to enable controls if setting is "possible"
	envisioning tying "possible" to base spell
	this makes base spell a moving target
	May want BaseSpell property on cast
		just Spell if not morphic
		XMuteSpell.BaseSpell otherwise

Duration problem
Only increase duration to 1 day
	Then need to add Lasting for 2 levels
	Have UI prevent setting of 2 days
		only go to Lasting
		iff enough points

General modification principal
	enabled if conceivable
		can modify base (w/o other mods)
		disable if can't do even base
	enable if conceivable, but not with extant modifications
		show dialog if going over

spellbook.xml
	want common in SpellBind2, spellbinder
		also Test

3/23
Morphic trouble
	SpellInfo posting list of BaseSpell
		BaseSpell should wrap SpellInfo, not other way
	BaseSpell responsible for replacing self when morph base changes
		Requires list of possible morphs

11/13/2015
Git info
	url: https://garzooma@arzoosoftware.git.cloudforge.com/spellbinder.git
	GITHUB pw: Min 7; Simple; !Late

2/26/2016
Cloudforge
	Domain: arzoosoftware
	name: garzooma
	pw: Simple, !Late, Min 6

3/22
Lasting Duration
trouble adding
	not just adding DurationModifier's
	?Do you put LastingModifier on DurationProperty?
		or is it it's own property?

Need to specify relation between SpellProperty class
	& Std properties (Range, Duration, etc.) attached to Cast
	Both return IPropertyValue
2 lists of modifiers
	SpellProperty._modifierList
		list of modifiers that can apply
	Cast._modifiers
		list of modifiers applied to cast

3/23
Modifier types
	Modifies basic spell property
		Duration, Range, ZOE, DC, Damage, Extra Effect, Hard to Save
		bound to property that exists independent of modifier
		stack multiple times
	Sets spell property
		Affect Others, Power Word, At Range, Concealment
		property not set (false) w/o modifier
	Set multi state property
		Cast on the Run, Reduced Incantation, Reduced Gestures
	Lasting
		its own category
		like Duration modifier
			Modifies Duration
			other modifiers leave other properties alone
		like spell property
			Set or not
			See if AtRange modifier acts like this vis a vis Range

3/25
Modifier class properties
	Name/PropertyName
		used to id modifier in Cast for calculating Cast Property
Properties/Modifiers reference each other
	IModifier.PropertyName
	SpellProperty.ModifierList
Properties can have multiple Modifier
	Duration property
		Duration Modifier
		Lasting Modifier
	Range property
		Range Modifier
		AtRange Modifier
Modifiers can have multiple Properities
	Lasting Modifier
		Lasting Property
		Duration Property
Need to rationalize the above
	Adding Lasting Modifier
		both Duration & Lasting properties affected
	Need mechanism to get Duration settings as per Duration Modifier
		for verifying Lasting applicable
Dual nature of Lasting Modifier
	should reflect this
	Duration modifier
		Makes Duration Property return Lasting DescribedValue
	Singular Property
		Shows Lasting Property as set
Removing entangled modifiers
	duration w/lasting
		remove duration
		-> lasting also gets removed
	similar w/at range
		at range w/range modifiers
		remove at range
		-> range modifiers also removed
	secondary properties on Modifiers
		may not need gen'l solution for 2 cases
			Duration/Lasting
			Range/At Range
Use cases
	Duration incrementing in Cast
		Remove Lasting on Duration decrement
	Duration View
SpellProperty.MaxValue usages
	7 locations
	CastViewModel
		LastingIsAvailable property
	Cast (6)
		3 for reporting
		3 in AddModifier()

3/28
Duration property duality
	Taking Duration Modifier
	Taking Duration/Lasting Modifier
Need to distinguish max w/Duration only modifiers
	?Via DurationModifier
	v. max w/Lasting, too

4/2
Requiring base setting
Needed for Lasting show in Duration View
Can't have .5 level
	must be full Level 1
Need to prevent modification below level 1
	Need to allow Enhance Abilities special modifier to get to Level 1
		thru CastViewModel.ExtraProperty
		mark as Base property
			Base attribute
		?attribute on Property or Modifier
