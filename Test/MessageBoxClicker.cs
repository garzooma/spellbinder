﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using System.Timers;
using System.Diagnostics;

namespace ArzooSoftware.SpellBinder.Test
{
    /// <summary>
    /// Use Win32 calls to find and dismiss message box
    /// </summary>
    /// <remarks>Avoids hangs from WPF Automation window acquisition.
    /// From http://stackoverflow.com/questions/2659708/how-to-automate-response-to-msgbox</remarks>
    class MessageBoxClicker : IDisposable
    {
        private System.Timers.Timer mTimer;
        private Process _proc;
        public bool Found { get; private set; }

        public MessageBoxClicker(Process proc)
        {
            _proc = proc;
            Found = false;
            mTimer = new System.Timers.Timer();
            mTimer.Interval = 50;
            mTimer.AutoReset = true;
            mTimer.Elapsed += new ElapsedEventHandler(findDialog);
            mTimer.Enabled = true;
        }

        private void findDialog(object sender, EventArgs e)
        {
            // Enumerate windows to find the message box
            EnumThreadWndProc callback = new EnumThreadWndProc(checkWindow);
            foreach (ProcessThread thread in _proc.Threads)
            {
                EnumThreadWindows(thread.Id, callback, IntPtr.Zero);
            }
            GC.KeepAlive(callback);
        }

        private bool checkWindow(IntPtr hWnd, IntPtr lp)
        {
            // Checks if <hWnd> is a dialog
            StringBuilder sb = new StringBuilder(260);
            GetClassName(hWnd, sb, sb.Capacity);
            if (sb.ToString() != "#32770") return true;
            // Got it, send the BN_CLICKED message for the OK button
            SendMessage(hWnd, WM_COMMAND, (IntPtr)IDC_OK, IntPtr.Zero);
            Found = true;
            // Done
            return false;
        }

        public void Dispose()
        {
            mTimer.Enabled = false;
        }

        // P/Invoke declarations
        private const int WM_COMMAND = 0x111;
        private const int IDC_OK = 2;
        private delegate bool EnumThreadWndProc(IntPtr hWnd, IntPtr lp);
        [DllImport("user32.dll")]
        private static extern bool EnumThreadWindows(int tid, EnumThreadWndProc callback, IntPtr lp);
        [DllImport("kernel32.dll")]
        private static extern int GetCurrentThreadId();
        [DllImport("user32.dll")]
        private static extern int GetClassName(IntPtr hWnd, StringBuilder buffer, int buflen);
        [DllImport("user32.dll")]
        private static extern IntPtr GetDlgItem(IntPtr hWnd, int item);
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);
    }
}
