﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ArzooSoftware.SpellBinder.spellbinder;
using System.Windows.Forms;
using System.Xml;
using ArzooSoftware.SpellBinder.SpellForm;

namespace ArzooSoftware.SpellBinder.Test
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.Test.tPMBinder</name>
    /// <summary>
    /// Description for tPMBinder
    /// </summary>
    /// <version>1.0 - Initial Release - 1/24/2013 12:39:44 PM</version>
    /// <author>Greg Arzoomanian</author>    
    /// 
    [TestFixture]
    public class tPMBinding
    {
        /// <summary>
        /// Build the binding object
        /// </summary>
        [Test]
        public void tCtor()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Spell.ExtraPropertiesList.Count, String.Format("Incorrect number of extra properties for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Spell.ExtraPropertiesList.Count));
            SpellProperty spell_prop = spell_cast.Spell.ExtraPropertiesList[0];
            PresentationBinding binding = new PresentationBinding(spell_cast, spell_prop);

            Control text_box = new TextBox();
            binding.BoundControls.Add(text_box);

            Assert.AreEqual("1", binding.DisplayValue, "Wrong display value");

            return;
        }

        private const string _spellStr = @"  <spell name='Magic Missile' level='1'>
                <description>Magical missile(s) emanate from the caster’s fingers. Each missile hits and does damage exactly as if the caster had fired a +1 heavy crossbow bolt (Damage d10+1, Range Increment 100’, point-blank damage d10+2 within 30’). The Extra Range modifier increases the range increment, including point-blank range (which doubles from the base 30’ with each application). The base spell gives one missile, extra missiles are added as a modifier. Multiple missiles may be aimed at separate targets as long as all are within a 60 degree arc. Roll for each missile separately to see if it hits. A Shield (page 65) spell provides total defense.</description>
                <ZOE value='60' units='degrees' type='spread'/>
                <Range value='100' units='feet' />
                <Duration value='momentary' units='' />
                <SavingThrow value='none' effect='' />
                <Damage die='10' number='1' plus='1'/>
                <Modifiers>
                  <Modifier type='ExtraDamage' die='10' number = '1' cost='1' />
                </Modifiers>
                <Properties>
                  <Property name='Extra Damage' value='1'>
                    <Modifier type='ExtraPlus' value='1' cost='.5'></Modifier>
                  </Property>
                </Properties>
              </spell>
            ";

        /// <summary>
        /// Test the binding
        /// </summary>
        [Test]
        public void tBinding()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Spell.ExtraPropertiesList.Count, String.Format("Incorrect number of extra properties for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Spell.ExtraPropertiesList.Count));
            SpellProperty spell_prop = spell_cast.Spell.ExtraPropertiesList[0];
            PresentationBinding binding = new PresentationBinding(spell_cast, spell_prop);

            SpellForm.SpellForm form = new SpellForm.SpellForm(base_spell, mage);
            Control text_box = new TextBox();
            form.Controls.Add(text_box);
            text_box.Text = "2";
            form.Show();
            Binding db = text_box.DataBindings.Add("Text", binding, "DisplayValue");
            Assert.AreEqual("1", text_box.Text, "Wrong text in control");   // on setting binding
            text_box.Text = "2";    // set off again

            binding.BoundControls.Add(text_box);
            binding.UpdateControls();   // set to bound data again

            Assert.AreEqual("1", binding.DisplayValue, "Wrong display value");
            Assert.AreEqual("1", text_box.Text, "Wrong text in control");

            return;
        }
    }
}
