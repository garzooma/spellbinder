﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ArzooSoftware.SpellBinder.spellbinder;
using System.Xml;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;
using Automation = System.Windows.Automation;
using System.Windows.Automation;
using Microsoft.Test.Input;
using System.Drawing;
using System.Windows.Automation.Text;


namespace ArzooSoftware.SpellBinder.Test
{
    /// <copyright>Arzoo Software, 2014</copyright>
    /// <name>ArzooSoftware.SpellBinder.Test.tUI</name>
    /// <summary>
    /// Description for tUI
    /// </summary>
    /// <version>1.0 - Initial Release - 2/28/2014 7:54:54 PM</version>
    /// <author>Greg Arzoomanian</author>    
    [TestFixture]
    class tUI
    {
        const string pathToExe = @"..\..\..\SpellBind2\bin\Debug\SpellBind2.exe";

        /// <summary>
        /// Just run the app
        /// </summary>
        [Test]
        public void TestRun()
        {
            Process proc = StartSpellBinder();

            EndSpellBinder(proc);
        }

        /// <summary>
        /// Push the close button
        /// </summary>
        [Test]
        public void TestClose()
        {
            try
            {
                Process proc = StartSpellBinder();

                AutomationElement rootElement = AutomationElement.RootElement;

                Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
                AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
                Assert.IsNotNull(appElement, "Couldn't get app 'Spell Book'");

                condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
                AutomationElement bar_element = appElement.FindFirst(TreeScope.Children, condition);
                Assert.IsNotNull(bar_element, "Couldn't get title bar");

                condition = new PropertyCondition(AutomationElement.NameProperty, "Close");
                AutomationElement btn_element = bar_element.FindFirst(TreeScope.Children, condition);
                Assert.IsNotNull(btn_element, "Couldn't get close button");

                InvokePattern invoke_ptn = btn_element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                Assert.IsNotNull(invoke_ptn, "Couldn't get invoke pattern");

                invoke_ptn.Invoke();
            }
            catch (Exception excp)
            {
                Console.WriteLine("Exception: " + excp.Message);
            }

            // EndSpellBinder(proc);
        }

        /// <summary>
        /// Double-click on spell
        /// </summary>
        [Test]
        public void TestCast()
        {
            try
            {
                Process proc = StartSpellBinder();

                AutomationElement rootElement = AutomationElement.RootElement;

                Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
                AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
                Assert.IsNotNull(appElement, "Couldn't get app");

                condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
                AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
                Assert.IsNotNull(grid_element, "Couldn't get grid");

                condition = new PropertyCondition(AutomationElement.NameProperty, "0");
                AutomationElement element = grid_element.FindFirst(TreeScope.Children, condition);
                Assert.IsNotNull(element, "Couldn't get item");
                condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "dataitem"); 
                element = element.FindFirst(TreeScope.Children, condition);
                System.Windows.Point winPoint = element.GetClickablePoint();
                System.Drawing.Point click_point = new System.Drawing.Point((int)winPoint.X, (int)winPoint.Y);
                Mouse.MoveTo(click_point);
                Mouse.DoubleClick(MouseButton.Left);
                proc.WaitForInputIdle();

                string name = "Attune";
                AutomationElement winElement = GetWindowByName(name);
                Assert.IsNotNull(winElement, String.Format("Couldn't get window '{0}'", name));

                CloseNamedWindow("Attune");
                proc.WaitForInputIdle();

                CloseNamedWindow("Spell Book");
            }
            catch (Exception excp)
            {
                Console.WriteLine("Exception: " + excp.Message);
                Assert.Fail("Got exception: " + excp.Message);
            }

            // EndSpellBinder(proc);
        }

        private static AutomationElement GetWindowByName(string name)
        {
            AutomationElement rootElement = AutomationElement.RootElement;
            AutomationElement winElement = null;
            int loop_count = 0;
            while (winElement == null)
            {
                PropertyCondition condition = new PropertyCondition(AutomationElement.NameProperty, name);
                winElement = rootElement.FindFirst(TreeScope.Children, condition);
                if (winElement == null)
                    Thread.Sleep(500);
                if (loop_count++ > 2 * 30)
                    break;
            }
            return winElement;
        }

        /// <summary>
        /// Double-click on spell
        /// </summary>
        [Test]
        public void TestNonCastable()
        {
            Process proc = StartSpellBinder();

            AutomationElement rootElement = AutomationElement.RootElement;

            Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
            AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(appElement, "Couldn't get app");

            // Set level
            SetLevel(proc, appElement, "5");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(grid_element, "Couldn't get grid");

            ScrollPattern scroll_pattern = grid_element.GetCurrentPattern(ScrollPattern.Pattern) as ScrollPattern;
            string find_name = "2";
            AutomationElement group_element = null;
            bool can_click = false;
            AutomationElement row_element = null;
            do
            {
                try
                {
                    condition = new PropertyCondition(AutomationElement.NameProperty, find_name); // get level 2 spell (should  be displayed)
                    group_element = grid_element.FindFirst(TreeScope.Children, condition);
                    condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "dataitem");
                    row_element = group_element.FindFirst(TreeScope.Children, condition);
                    row_element.GetClickablePoint();
                    can_click = true;
                }
                catch (Exception excp)
                {
                    scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);
                    proc.WaitForInputIdle();
                }
            }
            while (can_click == false);
            Assert.IsTrue(can_click, "Couldn't get item");

            condition = new PropertyCondition(AutomationElement.IsEnabledProperty, true);
            AutomationElement text_element = row_element.FindFirst(TreeScope.Descendants, condition);
            Assert.IsNotNull(text_element, "Level 2 spells not enabled");

            find_name = "4";
            can_click = false;
            do
            {
                try
                {
                    condition = new PropertyCondition(AutomationElement.NameProperty, find_name); // get level 2 spell (should  be displayed)
                    group_element = grid_element.FindFirst(TreeScope.Children, condition);
                    condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "dataitem");
                    row_element = group_element.FindFirst(TreeScope.Children, condition);
                    row_element.GetClickablePoint();
                    can_click = true;
                }
                catch (Exception excp)
                {
                    scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);
                    proc.WaitForInputIdle();
                }
            }
            while ((can_click == false) && scroll_pattern.Current.VerticalScrollPercent < 100);
            Assert.IsNotNull(group_element);

            condition = new PropertyCondition(AutomationElement.IsEnabledProperty, true);
            text_element = row_element.FindFirst(TreeScope.Descendants, condition);
            Assert.IsNull(text_element, "Level 4 spells enabled");

            // Adjust level to get Level 4 spells
            SetLevel(proc, appElement, "7");
            grid_element.SetFocus();     // lose focus on level text box
            proc.WaitForInputIdle();
            scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);
            proc.WaitForInputIdle();
            condition = new PropertyCondition(AutomationElement.NameProperty, find_name); // get level 2 spell (should  be displayed)
            group_element = grid_element.FindFirst(TreeScope.Children, condition);
            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "dataitem");
            row_element = group_element.FindFirst(TreeScope.Children, condition);
            condition = new PropertyCondition(AutomationElement.IsEnabledProperty, true);
            text_element = row_element.FindFirst(TreeScope.Descendants, condition);
            Assert.IsNotNull(text_element, "Level 4 spells not enabled");

            CloseNamedWindow("Spell Book");
        }

        private static void SetLevel(Process proc, AutomationElement appElement, string level)
        {
            Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Caster Info");
            AutomationElement element = appElement.FindFirst(TreeScope.Children, condition);
            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "edit");
            AutomationElement level_element = element.FindFirst(TreeScope.Children, condition);
            ValuePattern valuePattern = level_element.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;

            level_element.SetFocus();   // Set focus for input functionality and begin.
            valuePattern.SetValue(level);
            proc.WaitForInputIdle();

            return;
        }

        /// <summary>
        /// Modify a spell and check results in UI
        /// </summary>
        [Test]
        public void TestModification()
        {
            Process proc = StartSpellBinder();

            AutomationElement rootElement = AutomationElement.RootElement;

            Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
            AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(appElement, "Couldn't get app");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(grid_element, "Couldn't get grid");

            // Set level
            SetLevel(proc, appElement, "5");

            //ScrollPattern scroll_pattern = grid_element.GetCurrentPattern(ScrollPattern.Pattern) as ScrollPattern;
            // scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);

            AutomationElement element;
            string name = "Confuse";
            AutomationElement winElement = CastSpell(proc, grid_element, "1", name);

            string level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1", level_val);
            string cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("2", cost_val);

            AutomationElement tab_element = GetTab(proc, winElement, "Std Prop");

            string duration_val = GetDisplayedValue(winElement, "durTxtBx");
            Assert.AreEqual("12 rounds", duration_val);

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "button");
            AutomationElementCollection element_list = tab_element.FindAll(TreeScope.Children, condition);
            AutomationElement btn_element = element_list[2];  // third button is duration increment
            InvokePattern invoke_ptn = btn_element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            invoke_ptn.Invoke();
            proc.WaitForInputIdle();

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1.5", level_val);
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("3", cost_val);

            duration_val = GetDisplayedValue(winElement, "durTxtBx");
            Assert.AreEqual("24 rounds", duration_val);

            CloseNamedWindow(name);

            CloseNamedWindow("Spell Book");
        }

        /// <summary>
        /// Modify a spell and check results in UI
        /// </summary>
        [Test]
        public void TestDamage()
        {
            Process proc = StartSpellBinder();

            AutomationElement rootElement = AutomationElement.RootElement;

            Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
            AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(appElement, "Couldn't get app");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(grid_element, "Couldn't get grid");

            // Set level
            SetLevel(proc, appElement, "5");

            //ScrollPattern scroll_pattern = grid_element.GetCurrentPattern(ScrollPattern.Pattern) as ScrollPattern;
            //scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);
            //scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);

            string name = "Lance of (Element)";
            AutomationElement winElement = CastSpell(proc, grid_element, "1", name);

            string level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1", level_val);
            string cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("2", cost_val);

            AutomationElement tab_element = GetTab(proc, winElement, "Std Prop");

            string duration_val = GetDisplayedValue(winElement, "durTxtBx");
            Assert.AreEqual("momentary", duration_val);

            tab_element = GetTab(proc, winElement, "Damage");
            string dmg_val = GetDisplayedValue(winElement, "damageTxtBx");
            Assert.AreEqual("2d6", dmg_val);

            dmg_val = GetDisplayedValue(winElement, "expectTxtBx");
            Assert.AreEqual("7.0", dmg_val);

            dmg_val = GetDisplayedValue(winElement, "minTxtBx");
            Assert.AreEqual("2", dmg_val);

            dmg_val = GetDisplayedValue(winElement, "maxTxtBx");
            Assert.AreEqual("12", dmg_val);

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "button");
            AutomationElementCollection element_list = tab_element.FindAll(TreeScope.Children, condition);
            AutomationElement btn_element = element_list[0];  // first button is duration increment
            InvokePattern invoke_ptn = btn_element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            invoke_ptn.Invoke();
            proc.WaitForInputIdle();

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1.5", level_val);
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("3", cost_val);

            dmg_val = GetDisplayedValue(winElement, "damageTxtBx");
            Assert.AreEqual("3d6", dmg_val);

            dmg_val = GetDisplayedValue(winElement, "expectTxtBx");
            Assert.AreEqual("10.5", dmg_val);

            dmg_val = GetDisplayedValue(winElement, "minTxtBx");
            Assert.AreEqual("3", dmg_val);

            dmg_val = GetDisplayedValue(winElement, "maxTxtBx");
            Assert.AreEqual("18", dmg_val);

            CloseNamedWindow(name);

            CloseNamedWindow("Spell Book");
        }

        /// <summary>
        /// Modify a spell and check results in UI
        /// </summary>
        [Test]
        public void TestSpecialMod()
        {
            Process proc = StartSpellBinder();

            AutomationElement rootElement = AutomationElement.RootElement;

            Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
            AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(appElement, "Couldn't get app");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(grid_element, "Couldn't get grid");

            // Set level
            SetLevel(proc, appElement, "5");

            string name = "Rope Trick";
            AutomationElement winElement = CastSpell(proc, grid_element, "3", name);

            string level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3", level_val);
            string cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("6", cost_val);

            AutomationElement tab_element = GetTab(proc, winElement, "Special");
            Assert.IsNotNull(tab_element, "Couldn't get Special tab");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement special_grid = tab_element.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(special_grid, "Couldn't find Special grid");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "dataitem");
            AutomationElement found_item = null;
            int loop_count = 0;
            string mod_name = "More People";
            foreach (AutomationElement item in special_grid.FindAll(TreeScope.Children, condition))
            {
                condition = new PropertyCondition(AutomationElement.NameProperty, mod_name);
                AutomationElement check_element = item.FindFirst(TreeScope.Children, condition);
                if (check_element != null)
                {
                    found_item = item;
                    break;
                }
                Assert.IsTrue(loop_count++ < 100, String.Format("Couldn't find special row for modifier '{0}'", mod_name));
            }
            Assert.IsNotNull(found_item, "Couldn't find special modifier row");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "custom");    // get updown control
            AutomationElement control_element = found_item.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(control_element, "Couldn't find updown control");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "button");
            AutomationElementCollection element_list = control_element.FindAll(TreeScope.Children, condition);
            AutomationElement btn_element = element_list[0];  // top button is increment
            InvokePattern invoke_ptn = btn_element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            invoke_ptn.Invoke();
            proc.WaitForInputIdle();

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3.5", level_val);
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("8", cost_val);

            CloseNamedWindow(name);

            CloseNamedWindow("Spell Book");
        }

        private static AutomationElement GetTab(Process proc, AutomationElement winElement, string tabName)
        {
            Condition condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "tab");
            AutomationElement element = winElement.FindFirst(TreeScope.Children, condition);
            condition = new PropertyCondition(AutomationElement.NameProperty, tabName);
            AutomationElement tab_element = element.FindFirst(TreeScope.Children, condition);
            SelectionItemPattern select_pattern = tab_element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            select_pattern.Select();
            proc.WaitForInputIdle();
            return tab_element;
        }

        private static AutomationElement CastSpell(Process proc, AutomationElement grid_element, string level, string name)
        {
            Condition condition = new PropertyCondition(AutomationElement.NameProperty, level);
            AutomationElement group_element = grid_element.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(group_element, "Couldn't get group");
            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "dataitem");
            AutomationElement found_row = null;
            ScrollPattern scroll_pattern = grid_element.GetCurrentPattern(ScrollPattern.Pattern) as ScrollPattern;

            double grid_bottom = grid_element.Current.BoundingRectangle.Bottom;

            int loop_count = 0;
            foreach (AutomationElement data_item in group_element.FindAll(TreeScope.Children, condition))
            {
                condition = new PropertyCondition(AutomationElement.NameProperty, name);
                AutomationElement check_element = data_item.FindFirst(TreeScope.Children, condition);
                if (check_element != null)
                {
                    found_row = data_item;
                    Console.WriteLine(String.Format("Found row: x={0}; y={1}; bottom={2}; right={3}",
                        found_row.Current.BoundingRectangle.Left,
                        found_row.Current.BoundingRectangle.Top,
                        found_row.Current.BoundingRectangle.Bottom,
                        found_row.Current.BoundingRectangle.Right));
                    break;
                }
                Assert.IsTrue(loop_count++ < 100, String.Format("Couldn't find spell '{0}'", name)); 
            }
            // System.Windows.Point winPoint = found_row.GetClickablePoint();
            System.Windows.Point winPoint;
            loop_count = 0;
            while (!found_row.TryGetClickablePoint(out winPoint))
            {
                // First double check by looking at bounding rectangle
                System.Windows.Point check_point = GetPointFromRectangle(found_row.Current.BoundingRectangle);
                if (check_point.Y > 0 && check_point.Y < grid_bottom)
                {
                    winPoint = check_point;
                    break;
                }
                scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);
                proc.WaitForInputIdle();
                Assert.IsTrue(loop_count++ < 100, String.Format("Couldn't click on spell '{0}'", name));
            }

            System.Drawing.Point click_point = new System.Drawing.Point((int)winPoint.X, (int)winPoint.Y);

            // Sometimes needs one more scroll
            if (grid_bottom < click_point.Y)  // Check for clickable point outside of grid
            {
                scroll_pattern.ScrollVertical(ScrollAmount.SmallIncrement);
                proc.WaitForInputIdle();
                bool ok = found_row.TryGetClickablePoint(out winPoint);
                if (!ok)
                {
                    winPoint = GetPointFromRectangle(found_row.Current.BoundingRectangle);
                }
                click_point = new System.Drawing.Point((int)winPoint.X, (int)winPoint.Y);
            }

            Assert.IsTrue(grid_bottom > click_point.Y, "Clicking off of grid");
            Mouse.MoveTo(click_point);
            Mouse.DoubleClick(MouseButton.Left);
            proc.WaitForInputIdle();

            AutomationElement winElement = GetWindowByName(name);
            Assert.IsNotNull(winElement, String.Format("Couldn't get window '{0}'", name));

            // Move cursor to avoid hiding controls w/tooltips
            winElement.SetFocus();
            System.Windows.Point loc = winElement.Current.BoundingRectangle.Location;
            click_point = new System.Drawing.Point((int)loc.X + 30, (int)loc.Y + 10);
            Mouse.MoveTo(click_point);
            Mouse.Click(MouseButton.Left);
            proc.WaitForInputIdle();

            return winElement;
        }

        private static System.Windows.Point GetPointFromRectangle(System.Windows.Rect rect)
        {
            double mid_x = (rect.Left + rect.Right) / 2;
            double mid_y = (rect.Top + rect.Bottom) / 2;
            System.Windows.Point ret = new System.Windows.Point(mid_x, mid_y);
            Console.WriteLine(String.Format("Rect point: x={0}; y={1}", mid_x, mid_y));
            //Console.Beep();

            return ret;
        }

        private static string GetDisplayedLevel(AutomationElement winElement)
        {
            string ret = String.Empty;
            ret = GetDisplayedValue(winElement, "lvlTxtBx");

            return ret;
        }

        private static string GetDisplayedCost(AutomationElement winElement)
        {
            string ret = String.Empty;
            ret = GetDisplayedValue(winElement, "costTxtBx");

            return ret;
        }

        private static string GetDisplayedValue(AutomationElement winElement, string uiControlName)
        {
            string ret = String.Empty;
            Condition condition = new PropertyCondition(AutomationElement.AutomationIdProperty, uiControlName);
            AutomationElement element = winElement.FindFirst(TreeScope.Descendants, condition);
            ValuePattern vpTextBox1 = (ValuePattern)element.GetCurrentPattern(ValuePattern.Pattern);
            ret = vpTextBox1.Current.Value.ToString();

            return ret;
        }

        private static Automation.Condition CloseNamedWindow(string name)
        {
            AutomationElement win_element = GetWindowByName(name);

            PropertyCondition condition = new PropertyCondition(AutomationElement.NameProperty, name);
            AutomationElement bar_element = win_element.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(bar_element, "Couldn't get title bar");

            condition = new PropertyCondition(AutomationElement.NameProperty, "Close");
            AutomationElement btn_element = bar_element.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(btn_element, "Couldn't get close button");

            InvokePattern invoke_ptn = btn_element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            Assert.IsNotNull(invoke_ptn, "Couldn't get invoke pattern");

            invoke_ptn.Invoke();
            return condition;
        }

        private static void EndSpellBinder(Process proc)
        {
            proc.CloseMainWindow();

            proc.Kill();

            proc.WaitForExit();
        }

        private static Process StartSpellBinder()
        {
            string cur_path = Directory.GetCurrentDirectory();
            string path = pathToExe;
            Assert.IsTrue(File.Exists(path), "Exe doesn't exist");

            Process proc = new Process();
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.FileName = path;
            proc.Start();

            while (!proc.Responding)
                Thread.Sleep(500);

            proc.WaitForInputIdle();

            DateTime start = DateTime.Now;
            IntPtr handle = IntPtr.Zero;
            while (handle == IntPtr.Zero && DateTime.Now - start <= TimeSpan.FromSeconds(2))
            {
                try
                {
                    // sleep a while to allow the MainWindow to open...
                    System.Threading.Thread.Sleep(50);
                    handle = proc.MainWindowHandle;
                }
                catch (Exception) { }
            }

            AutomationElement rootElement = AutomationElement.RootElement;
            AutomationElement appElement = null;
            int loop_count = 0;
            while (appElement == null && loop_count < 60)   // wait half a minute
            {
                Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
                appElement = rootElement.FindFirst(TreeScope.Children, condition);
                loop_count++;
                Thread.Sleep(500);
            }

            return proc;
        }

        /// <summary>
        /// Build a wrapper for controls for a given property
        /// </summary>
        /// <param name="name"></param>
        /// <param name="winElement">Cast window for spell</param>
        /// <returns></returns>
        private static PropertyUIWrapper BuildUIWrapper(string name, AutomationElement winElement, Process proc)
        {
            PropertyUIWrapper ret = null;

            PropertyControlGroup control_group = ControlDictionary[name];
            string tab_name = control_group.TabName;
            AutomationElement tab_element = GetTab(proc, winElement, "Std Prop");

            return ret;
        }

        /// <summary>
        /// Represents information about a Spell Property from the UI.
        /// </summary>
        internal class PropertyUIWrapper
        {
            internal PropertyUIWrapper(string propertyName, AutomationElement winElement, Process proc)
            {
                Name = propertyName;
                _winElement = winElement;
                string tab_name = ControlDictionary[Name].TabName;
                _tabElement = GetTab(proc, _winElement, tab_name);
                string control_name = ControlDictionary[Name].DisplayControlName;
                _displayControl = GetDisplayControl(_winElement, control_name);
                _modifyControl = BuildModifierControl(Name, _tabElement, proc);
                _proc = proc;
            }

            AutomationElement _winElement;
            AutomationElement _tabElement;
            private Process _proc;

            internal string Name { get; private set; }

            private AutomationElement _displayControl;
            internal string GetValue()
            {
                string ret = String.Empty;
                ValuePattern vpTextBox1 = (ValuePattern)_displayControl.GetCurrentPattern(ValuePattern.Pattern);
                ret = vpTextBox1.Current.Value.ToString();
                return ret;
            }

            private IModifierControl _modifyControl;
            internal void Modify(bool isUp)
            {
                if (isUp)
                    _modifyControl.IncrementValue();
                else
                    _modifyControl.DecrementValue();

                _proc.WaitForInputIdle();
                return;
            }
        }

        internal static AutomationElement GetDisplayControl(AutomationElement winElement, string control_name)
        {
            Condition condition = new PropertyCondition(AutomationElement.AutomationIdProperty, control_name);
            AutomationElement element = winElement.FindFirst(TreeScope.Descendants, condition);
            return element;
        }

        class PropertyControlGroup
        {
            internal string Name { get; set; }
            internal string DisplayControlName { get; set; }
            internal string TabName { get; set; }
            internal ControlType ControlType { get; set; }
            internal string ControlId { get; set; }
        }

        enum ControlType { UpDown, CheckBox, ComboBox };

        private static Dictionary<string, PropertyControlGroup> ControlDictionary = new Dictionary<string, PropertyControlGroup>(){
               {SpellProperty.StdPropertyNames.ZOE, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.ZOE, 
                       DisplayControlName = "zoeTxtBx", TabName = "Std Prop", 
                       ControlType = ControlType.UpDown, ControlId = "0"}},
               {SpellProperty.StdPropertyNames.Duration, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.Duration, 
                       DisplayControlName = "durTxtBx", TabName = "Std Prop", 
                       ControlType = ControlType.UpDown, ControlId = "1"}},
               {SpellProperty.StdPropertyNames.Range, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.Range, 
                       DisplayControlName = "rngTxtBx", TabName = "Std Prop", 
                       ControlType = ControlType.UpDown, ControlId = "2"}},
               {SpellProperty.StdPropertyNames.SavingThrow, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.SavingThrow, 
                       DisplayControlName = "dcTxtBx", TabName = "Std Prop", 
                       ControlType = ControlType.UpDown, ControlId = "3"}},
               {SpellProperty.StdPropertyNames.AffectsOthers, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.AffectsOthers, 
                       DisplayControlName = "affectOthersChkBx", TabName = "Std Mod", 
                       ControlType = ControlType.CheckBox, ControlId = "affectOthersChkBx"}},
               {SpellProperty.StdPropertyNames.AtRange, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.AtRange, 
                       DisplayControlName = "atRangeChkBx", TabName = "Std Mod", 
                       ControlType = ControlType.CheckBox, ControlId = "atRangeChkBx"}},
               {SpellProperty.StdPropertyNames.CastOnTheRun, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.CastOnTheRun, 
                       DisplayControlName = "castOnRunChkBx", TabName = "Std Mod", 
                       ControlType = ControlType.CheckBox, ControlId = "castOnRunChkBx"}},
               {SpellProperty.StdPropertyNames.ReducedGestures, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.ReducedGestures, 
                       DisplayControlName = "redGestCmbBx", TabName = "Std Mod", 
                       ControlType = ControlType.ComboBox, ControlId = "redGestCmbBx"}},
               {SpellProperty.StdPropertyNames.ReducedIncantation, 
                   new PropertyControlGroup{Name=SpellProperty.StdPropertyNames.ReducedIncantation, 
                       DisplayControlName = "redInCantCmbBx", TabName = "Std Mod", 
                       ControlType = ControlType.ComboBox, ControlId = "redInCantCmbBx"}}
               };

        internal interface IModifierControl
        {
            string GetCurrentValue();
            void IncrementValue();
            void DecrementValue();
            bool IncrementEnabled();
            bool DecrementEnabled();
        }

        class UpDownControl : IModifierControl
        {
            private AutomationElement _tabElement;
            private int _controlNum;    // vertical number of control on tab
            private Process _proc;
            /// <summary>
            /// Build updown control instance
            /// </summary>
            /// <param name="tabElement"></param>
            /// <param name="controlNum">nth up/down contol on page</param>
            public UpDownControl(AutomationElement tabElement, string controlNum, Process proc)
            {
                _tabElement = tabElement;
                Int32.TryParse(controlNum, out _controlNum);
                _proc = proc;
            }

            public string GetCurrentValue()
            {
                throw new NotImplementedException();
            }

            public void IncrementValue()
            {
                PropertyCondition condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "button");
                AutomationElementCollection element_list = _tabElement.FindAll(TreeScope.Children, condition);
                int up_btn_num = _controlNum * 2;
                AutomationElement btn_element = element_list[up_btn_num];  // third button is duration increment
                InvokePattern invoke_ptn = btn_element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                invoke_ptn.Invoke();
                _proc.WaitForInputIdle();
            }

            public void DecrementValue()
            {
                PropertyCondition condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "button");
                AutomationElementCollection element_list = _tabElement.FindAll(TreeScope.Children, condition);
                int btn_num = _controlNum * 2 + 1;  // down button
                AutomationElement btn_element = element_list[btn_num];  // third button is duration increment
                InvokePattern invoke_ptn = btn_element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                invoke_ptn.Invoke();
                _proc.WaitForInputIdle();
            }

            public bool IncrementEnabled()
            {
                throw new NotImplementedException();
            }

            public bool DecrementEnabled()
            {
                throw new NotImplementedException();
            }
        }

        class CheckboxControl : IModifierControl
        {
            private AutomationElement _tabElement;
            private string _controlName;    // vertical number of control on tab
            private Process _proc;

            /// <summary>
            /// Build updown control instance
            /// </summary>
            /// <param name="tabElement"></param>
            /// <param name="controlNum">nth up/down contol on page</param>
            public CheckboxControl(AutomationElement tabElement, string controlName, Process proc)
            {
                _tabElement = tabElement;
                _controlName = controlName;
                _proc = proc;
            }

            public string GetCurrentValue()
            {
                throw new NotImplementedException();
            }

            public void IncrementValue()
            {
                PropertyCondition condition = new PropertyCondition(AutomationElement.AutomationIdProperty, _controlName);
                AutomationElement element = _tabElement.FindFirst(TreeScope.Children, condition);
                object cur_pattern = element.GetCurrentPattern(TogglePattern.Pattern);
                TogglePattern invoke_ptn = cur_pattern as TogglePattern;
                invoke_ptn.Toggle();
                _proc.WaitForInputIdle();
            }

            public void DecrementValue()
            {
                PropertyCondition condition = new PropertyCondition(AutomationElement.AutomationIdProperty, _controlName);
                AutomationElement element = _tabElement.FindFirst(TreeScope.Children, condition);
                object cur_pattern = element.GetCurrentPattern(TogglePattern.Pattern);
                TogglePattern invoke_ptn = cur_pattern as TogglePattern;
                invoke_ptn.Toggle();
                _proc.WaitForInputIdle();
            }

            public bool IncrementEnabled()
            {
                throw new NotImplementedException();
            }

            public bool DecrementEnabled()
            {
                throw new NotImplementedException();
            }
        }

        class ComboboxControl : IModifierControl
        {
            private AutomationElement _tabElement;
            private string _controlName;    // vertical number of control on tab
            private Process _proc;
            /// <summary>
            /// Build updown control instance
            /// </summary>
            /// <param name="tabElement"></param>
            /// <param name="controlNum">nth up/down contol on page</param>
            public ComboboxControl(AutomationElement tabElement, string controlName, Process proc)
            {
                _tabElement = tabElement;
                _controlName = controlName;
                _proc = proc;
            }

            public string GetCurrentValue()
            {
                throw new NotImplementedException();
            }

            public void IncrementValue()
            {
                PropertyCondition condition = new PropertyCondition(AutomationElement.AutomationIdProperty, _controlName);
                AutomationElement combobox_element = _tabElement.FindFirst(TreeScope.Children, condition);
                ExpandCollapsePattern expandPattern = (ExpandCollapsePattern)combobox_element.GetCurrentPattern(ExpandCollapsePattern.Pattern);
                expandPattern.Expand();
                condition = new PropertyCondition(AutomationElement.ControlTypeProperty, Automation.ControlType.List);
                AutomationElement comboboxList = combobox_element.FindFirst(TreeScope.Descendants, condition);
                condition =  new PropertyCondition(AutomationElement.ControlTypeProperty, Automation.ControlType.ListItem);
                AutomationElementCollection comboboxItems = combobox_element.FindAll(TreeScope.Subtree, condition);
                AutomationElement itemToSelect = comboboxItems[1];
                SelectionItemPattern selectPattern = (SelectionItemPattern)itemToSelect.GetCurrentPattern(SelectionItemPattern.Pattern);
                selectPattern.Select();
                _proc.WaitForInputIdle();
            }

            public void DecrementValue()
            {
                throw new NotImplementedException();
            }

            public bool IncrementEnabled()
            {
                throw new NotImplementedException();
            }

            public bool DecrementEnabled()
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Return an IModifierControl wrapper for a given property
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        internal static IModifierControl BuildModifierControl(string propertyName, AutomationElement tabElement, Process proc)
        {
            IModifierControl ret = null;
            ControlType control_type = ControlDictionary[propertyName].ControlType;
            string control_id = ControlDictionary[propertyName].ControlId;
            switch (control_type)
            {
                case ControlType.CheckBox:
                    ret = new CheckboxControl(tabElement, control_id, proc);
                    break;

                case ControlType.ComboBox:
                    ret = new ComboboxControl(tabElement, control_id, proc);
                    break;

                case ControlType.UpDown:
                    ret = new UpDownControl(tabElement, control_id, proc);
                    break;
            }

            return ret;
        }

        /// <summary>
        /// Multiple Modifiers
        /// </summary>
        [Test]
        public void TestModMulti()
        {
            Process proc = StartSpellBinder();

            AutomationElement rootElement = AutomationElement.RootElement;

            Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
            AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(appElement, "Couldn't get app");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(grid_element, "Couldn't get grid");

            // Set level
            int mage_level = 6;
            SetLevel(proc, appElement, mage_level.ToString());

            //ScrollPattern scroll_pattern = grid_element.GetCurrentPattern(ScrollPattern.Pattern) as ScrollPattern;
            //scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);

            AutomationElement element;
            string name = "Confuse";
            AutomationElement winElement = CastSpell(proc, grid_element, "1", name);

            string level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1", level_val);
            string cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("2", cost_val);

            ExpectedValue expected = new ExpectedValue(name, mage_level);
            CompareVsExpected(expected, winElement, proc);

            AutomationElement tab_element = GetTab(proc, winElement, "Std Prop");

            string duration_val = GetDisplayedValue(winElement, "durTxtBx");
            Assert.AreEqual("12 rounds", duration_val);

            string prop_name = SpellProperty.StdPropertyNames.Range;
            PropertyUIWrapper prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(true);

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1.5", level_val);
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("2", cost_val);

            duration_val = GetDisplayedValue(winElement, "durTxtBx");
            Assert.AreEqual("12 rounds", duration_val);

            expected.SetExpectedValue(prop_name, "120'");
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "15.5");
            CompareVsExpected(expected, winElement, proc);

            // Set Standard Mod checkbox
            GetTab(proc, winElement, "Std Mod");
            prop_name = SpellProperty.StdPropertyNames.CastOnTheRun;
            prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(true);

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2.5", level_val);
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("4", cost_val);

            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "16.5");
            CompareVsExpected(expected, winElement, proc);

            // Set Combobox
            GetTab(proc, winElement, "Std Mod");
            prop_name = SpellProperty.StdPropertyNames.ReducedGestures;
            prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(true);

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3.5", level_val, "Incorrect spell level after reduced gestures");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("6", cost_val, "Incorrect spell cost");

            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "17.5");
            CompareVsExpected(expected, winElement, proc);

            // Decrease modifiers
            prop_name = SpellProperty.StdPropertyNames.CastOnTheRun;
            prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(false);  // uncheck

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2.5", level_val);
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("4", cost_val);

            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "16.5");
            CompareVsExpected(expected, winElement, proc);

            tab_element = GetTab(proc, winElement, "Std Prop");

            prop_name = SpellProperty.StdPropertyNames.Range;
            prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(false);  // adjust down

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("3", cost_val, "Incorrect cost");

            expected.SetExpectedValue(prop_name, "60'");
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "16");
            CompareVsExpected(expected, winElement, proc);

            CloseNamedWindow(name);

            CloseNamedWindow("Spell Book");
        }

        public delegate void MyDelegate();

        /// <summary>
        /// Increase mods until max spell points is reached
        /// </summary>
        [Test]
        public void TestLimit()
        {
            Process proc = StartSpellBinder();

            AutomationElement rootElement = AutomationElement.RootElement;

            Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
            AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(appElement, "Couldn't get app");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(grid_element, "Couldn't get grid");

            // Set level
            int mage_level = 4;
            SetLevel(proc, appElement, mage_level.ToString());

            //ScrollPattern scroll_pattern = grid_element.GetCurrentPattern(ScrollPattern.Pattern) as ScrollPattern;
            //scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);

            AutomationElement element;
            string name = "Confuse";
            AutomationElement winElement = CastSpell(proc, grid_element, "1", name);

            string level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1", level_val, "Incorrect starting level");
            string cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("3", cost_val, "Incorrect starting cost");

            ExpectedValue expected = new ExpectedValue(name, mage_level);
            CompareVsExpected(expected, winElement, proc);

            AutomationElement tab_element = GetTab(proc, winElement, "Std Prop");

            string prop_name = SpellProperty.StdPropertyNames.Duration;
            PropertyUIWrapper prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(true);

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1.5", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("4", cost_val, "Incorrect cost after 1 mod");

            expected.SetExpectedValue(prop_name, "24 rounds");
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "15.5");
            CompareVsExpected(expected, winElement, proc);

            prop_ui.Modify(true);   // mod 2x

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("5", cost_val, "Incorrect cost after 2 mods");

            expected.SetExpectedValue(prop_name, "5 minutes");
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "16");
            CompareVsExpected(expected, winElement, proc);

            prop_ui.Modify(true);   // mod 3x

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2.5", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("6", cost_val, "Incorrect cost after 3 mods");

            expected.SetExpectedValue(prop_name, "10 minutes");
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "16.5");
            CompareVsExpected(expected, winElement, proc);

            prop_ui.Modify(true);   // mod 4x

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("8", cost_val, "Incorrect cost after 3 mods");

            expected.SetExpectedValue(prop_name, "20 minutes");
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "17"); 
            CompareVsExpected(expected, winElement, proc);

            // The Work to perform on another thread 
            //ThreadStart start = delegate() { 
            //    // ... 
            //    // Sets the Text on a TextBlock Control. 
            //    // This will work as its using the dispatcher 
            //    Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Normal, new Action<AutomationElement>(CheckDialog), winElement); 
            //}; 
            //// Create the thread and kick it started! 
            //new Thread(start).Start();
            //MyDelegate delInstance = new MyDelegate(CheckDialog);
            //delInstance.BeginInvoke(null, null);

            using (MessageBoxClicker clicker = new MessageBoxClicker(proc))
            {
                prop_ui.Modify(true);   // mod 5x
                Assert.IsTrue(clicker.Found, "Didn't find messagebox");
            }

            // Check message box


            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("8", cost_val, "Incorrect cost after 3 mods");

            CompareVsExpected(expected, winElement, proc);

            CloseNamedWindow(name);

            CloseNamedWindow("Spell Book");
        }

        /// <summary>
        /// Go to limit & step back
        /// </summary>
        [Test]
        public void TestLimitStepBack()
        {
            Process proc = StartSpellBinder();

            AutomationElement rootElement = AutomationElement.RootElement;

            Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
            AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(appElement, "Couldn't get app");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(grid_element, "Couldn't get grid");

            // Set level
            int mage_level = 4;
            SetLevel(proc, appElement, mage_level.ToString());

            //ScrollPattern scroll_pattern = grid_element.GetCurrentPattern(ScrollPattern.Pattern) as ScrollPattern;
            //scroll_pattern.ScrollVertical(ScrollAmount.LargeIncrement);

            string name = "Confuse";
            AutomationElement winElement = CastSpell(proc, grid_element, "1", name);

            string level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1", level_val, "Incorrect starting level");
            string cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("3", cost_val, "Incorrect starting cost");

            ExpectedValue expected = new ExpectedValue(name, mage_level);
            CompareVsExpected(expected, winElement, proc);

            // Start w/Std Mod
            AutomationElement tab_element = GetTab(proc, winElement, "Std Mod");
            string prop_name = SpellProperty.StdPropertyNames.CastOnTheRun;
            PropertyUIWrapper prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(true);

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("5", cost_val, "Incorrect cost after 1 mod");

            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "16");
            CompareVsExpected(expected, winElement, proc);
            tab_element = GetTab(proc, winElement, "Std Prop");
            prop_name = SpellProperty.StdPropertyNames.Duration;
            prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(true);

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2.5", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("6", cost_val, "Incorrect cost after 1 mod");

            expected.SetExpectedValue(prop_name, "24 rounds"); 
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "16.5");
            CompareVsExpected(expected, winElement, proc);

            prop_ui.Modify(true);   // mod 2x

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("8", cost_val, "Incorrect cost after 2 mods");

            expected.SetExpectedValue(prop_name, "5 minutes");
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "17");
            CompareVsExpected(expected, winElement, proc);

            using (MessageBoxClicker clicker = new MessageBoxClicker(proc))
            {
                prop_ui.Modify(true);   // mod 3x
                Assert.IsTrue(clicker.Found, "Didn't find messagebox");
            }

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("8", cost_val, "Incorrect cost after 3 mods");

            // Remove Mod
            tab_element = GetTab(proc, winElement, "Std Mod");
            prop_name = SpellProperty.StdPropertyNames.CastOnTheRun;
            prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(false);

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("5", cost_val, "Incorrect cost after 3 mods");

            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "16");
            CompareVsExpected(expected, winElement, proc);

            prop_name = SpellProperty.StdPropertyNames.Duration;
            prop_ui = new PropertyUIWrapper(prop_name, winElement, proc);
            prop_ui.Modify(true);   // mod 4x

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("2.5", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("6", cost_val, "Incorrect cost after 3 mods");

            prop_ui.Modify(true);   // mod 4x

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("8", cost_val, "Incorrect cost after 3 mods");

            expected.SetExpectedValue(prop_name, "20 minutes");
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.SavingThrow, "17");
            CompareVsExpected(expected, winElement, proc);

            using (MessageBoxClicker clicker = new MessageBoxClicker(proc))
            {
                prop_ui.Modify(true);   // mod 5x
                Assert.IsTrue(clicker.Found, "Didn't find messagebox");
            }

            level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("3", level_val, "Incorrect level");
            cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("8", cost_val, "Incorrect cost after 3 mods");

            CompareVsExpected(expected, winElement, proc);

            CloseNamedWindow(name);

            CloseNamedWindow("Spell Book");
        }

        /// <summary>
        /// Go to limit & step back
        /// </summary>
        [Test]
        public void TestMorphic()
        {
            Process proc = StartSpellBinder();

            AutomationElement rootElement = AutomationElement.RootElement;

            Automation.Condition condition = new PropertyCondition(AutomationElement.NameProperty, "Spell Book");
            AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(appElement, "Couldn't get app");

            condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "datagrid");
            AutomationElement grid_element = appElement.FindFirst(TreeScope.Children, condition);
            Assert.IsNotNull(grid_element, "Couldn't get grid");

            // Set level
            int mage_level = 4;
            SetLevel(proc, appElement, mage_level.ToString());

            string name = "Locate";
            AutomationElement winElement = CastSpell(proc, grid_element, "0", name);

            string level_val = GetDisplayedLevel(winElement);
            Assert.AreEqual("1", level_val, "Incorrect starting level");
            string cost_val = GetDisplayedCost(winElement);
            Assert.AreEqual("3", cost_val, "Incorrect starting cost");

            ExpectedValue expected = new ExpectedValue(name, mage_level);
            expected.SetExpectedValue(SpellProperty.StdPropertyNames.Range, "1 mile");
            CompareVsExpected(expected, winElement, proc);

            CloseNamedWindow(name);

            CloseNamedWindow("Spell Book");
        }

        private void CheckDialog()
        {
            AutomationElement dlg_element = null;
            Thread.Sleep(3 * 1000);
            while (dlg_element == null)
            {
                AutomationElement desktopElement = AutomationElement.RootElement;
                PropertyCondition condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "dialog");
                AutomationElement winElement = desktopElement.FindFirst(TreeScope.Descendants, condition);
                condition = new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "dialog");
                dlg_element = winElement.FindFirst(TreeScope.Children, condition);
                Thread.Sleep(500);
            }
            Assert.IsNotNull(dlg_element);
        }

        private static void CompareVsExpected(ExpectedValue expected, AutomationElement winElement, Process proc)
        {
            foreach (string property in expected.Properties)
            {
                string expected_value = expected.GetExpectedValue(property);
                string display_control_name = ControlDictionary[property].DisplayControlName;
                string tab_name = ControlDictionary[property].TabName;
                AutomationElement element = GetTab(proc, winElement, tab_name);
                string display_value = GetDisplayedValue(winElement, display_control_name);
                Assert.AreEqual(expected_value, display_value, String.Format("Mismatch on property '{0}'", property));
            }
        }

        class ExpectedValue
        {
            private Dictionary<string, string> _expected = new Dictionary<string, string>();
            internal ExpectedValue(string spellName, int mageLevel)
            {
                ISpell spell = SpellBook.Book[spellName];
                Cast cast = new Cast(spell, new Mage(mageLevel, 18));
                _expected[SpellProperty.StdPropertyNames.ZOE] = spell.ZOEProperty.Value.DisplayValue;
                _expected[SpellProperty.StdPropertyNames.Duration] = spell.DurationProperty.Value.DisplayValue;
                _expected[SpellProperty.StdPropertyNames.Range] = spell.RangeProperty.Value.DisplayValue;
                _expected[SpellProperty.StdPropertyNames.SavingThrow] = cast.DifficultyClass.ToString();
                //_expected[SpellProperty.StdPropertyNames.AffectsOthers] = "false";
                //_expected[SpellProperty.StdPropertyNames.AtRange] = "false";
                //_expected[SpellProperty.StdPropertyNames.CastOnTheRun] = "false";
                //_expected[SpellProperty.StdPropertyNames.ReducedGestures] = "Normal";
                //_expected[SpellProperty.StdPropertyNames.ReducedIncantation] = "Normal";
            }

            internal string GetExpectedValue(string property)
            {
                return _expected[property];
            }

            internal void SetExpectedValue(string property, string propValue)
            {
                _expected[property] = propValue;
            }

            internal IEnumerable<string> Properties
            {
                get
                {
                    return _expected.Keys;
                }
            }
        }
    }
}
