﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ArzooSoftware.SpellBinder.spellbinder;
using System.Xml;

namespace ArzooSoftware.SpellBinder.Test
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.Test.tBaseSpell</name>
    /// <summary>
    /// Description for tBaseSpell
    /// </summary>
    /// <version>1.0 - Initial Release - 1/18/2013 6:40:56 PM</version>
    /// <author>Greg Arzoomanian</author>   
    /// 
    [TestFixture]
    class tBaseSpell
    {
        /// <summary>
        /// Create BaseSpell object
        /// </summary>
        [Test]
        public void tCtor()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            BaseSpell spell = new BaseSpell(doc.FirstChild);

            Assert.AreEqual("Detect Magic", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));
        }

        private string _spellStr = @"<spell name='Detect Magic' level='1'>
                                        <description>A confused creature will not be able to coordinate his actions with anyone else. (In the case of player characters, the players may not consult, and must submit orders in writing.) In addition there is a 1/3 chance each round that the creature will not be able to decide what to do that round, and thus will do absolutely nothing at all. Those creatures controlled by some outside source will not be affected, unless the controlling force also fails to save or fails to make other relevant control check. Only those of 4 HD or more will get saving throws. Those of 2 HD or less are affected immediately; others get a delay of d6 minus the level of the caster rounds.</description>
                                        <ZOE value='self' type='described'/>
                                        <Range value='as sight' />
                                        <Duration value='10' units='minutes' />
                                        <SavingThrow value='none' />
                                      </spell>";

        /// <summary>
        /// Check Properties
        /// </summary>
        [Test]
        public void tProperties()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_RFSpellStr);
            BaseSpell spell = new BaseSpell(doc.FirstChild);

            Assert.AreEqual("Range Finder", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            SpellProperty spell_prop = spell.ZOEProperty;
            Assert.AreEqual("ZOE", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));

            Assert.AreEqual(2, spell.ExtraPropertiesList.Count, String.Format("Incorrect number of extra spell property: '{0}'", spell.ExtraPropertiesList.Count));
            spell_prop = spell.ExtraPropertiesList[0];
            Assert.AreEqual("Increased Accuracy", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));
            spell_prop = spell.ExtraPropertiesList[1];
            Assert.AreEqual("Find Velocity", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));
            IPropertyValue prop_val = spell_prop.MaxValue;
            Assert.AreEqual(2, prop_val.CalculationValue, String.Format("Incorrect max value for spell property: '{0}'", spell_prop.Name));

            return;
        }

        internal const string _RFSpellStr = @"  <spell name='Range Finder' level='1'>
                                            <description>The recipient will know the precise range (but not velocity) of all objects which he can see. This gives +2 to hit on any ranged to-hit roll. This cancels a Range Loser, regardless of any disparity in bonus or duration. Modifiers: Extra Effect (+1 more bonus) 1/2 , Find Velocity (in addition to giving exact knowledge of objects’ direction and speed of movement, this grants an extra +2 to hit) +1.
                                            </description>
                                            <ZOE value='1' units='target'/>
                                            <Range value='touch' units=''/>
                                            <Duration value='10' units='minutes' />
                                            <SavingThrow value='Will negates' />
                                            <Properties>
                                              <Property name='Increased Accuracy' value='2'>
                                                <Modifier type='ExtraEffect' value='1' cost='.5'></Modifier>
                                              </Property>
                                              <Property name='Find Velocity' value='0' max='2'>
                                                <Modifier type='ExtraEffect' value='2' cost='1'></Modifier>
                                              </Property>
                                            </Properties>
                                          </spell>";


        /// <summary>
        /// Check standard modifiers
        /// </summary>
        [Test]
        public void tStdModifiers()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_HoldPortalSpellStr);
            BaseSpell spell = new BaseSpell(doc.FirstChild);

            Assert.AreEqual("Hold Portal", spell.Name, String.Format("Incorrect name for Spell: '{0}'", spell.Name));

            SpellProperty spell_prop = spell.ZOEProperty;
            Assert.AreEqual("ZOE", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));

            Assert.AreEqual(8, spell.ModifierList.Count, String.Format("Incorrect number of modifiers: '{0}'", spell.ExtraPropertiesList.Count));
            Assert.IsTrue(spell.ModifierList.Any(m => m is DurationModifier), String.Format("No duration modfier for spell: '{0}'", spell.Name));
            Assert.AreEqual(1, spell.ModifierList.Count(m => m is DurationModifier), String.Format("Incorrect number of duration modfiers for spell: '{0}'", spell.Name));

            return;
        }

        internal const string _HoldPortalSpellStr = @"  <spell name='Hold Portal' level='1'>
                                                <description>Holds closed a door, chest, panel, etc., which must be completely closed at the time of casting. A strongly anti-magical creature (e.g. Balrog) may shatter it. Dispel Magic (page 69) gets rid of it automatically, as does a Knock (page 67), which will open the door. Forcing the door open by brute strength requires a strength contest against the effective strength of the door’s construction. This is usually 25 for interior doors and 30 or more for gates but the GM may assign higher or lower values based on the condition of the door. Forcing the door destroys it.
                                                </description>
                                                <ZOE value='1' units='portal'/>
                                                <Range value='10' units='feet'/>
                                                <Duration value='1' units='hour' />
                                                <SavingThrow value='none' />
                                              </spell>
                                            ";

        /// <summary>
        /// Check Duration Property
        /// </summary>
        [Test]
        public void tDurationProperty()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_HoldPortalSpellStr);
            BaseSpell spell = new BaseSpell(doc.FirstChild);

            Assert.AreEqual("Hold Portal", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            SpellProperty spell_prop = spell.ZOEProperty;
            Assert.AreEqual("ZOE", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));
            IPropertyValue expected = new NumericValue(1, "portal");
            Assert.IsTrue(spell_prop.Value.Equals(expected), String.Format("Incorrect ZOE value for spell property: '{0}'", spell_prop.Name));

            spell_prop = spell.DurationProperty;
            Assert.AreEqual("Duration", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));
            expected = new NumericValue(1, "hour");
            Assert.IsTrue(spell_prop.Value.Equals(expected), String.Format("Incorrect Duration value for spell property: '{0}'", spell_prop.Name));
            expected = new DescribedValue("Lasting");
            Assert.IsTrue(spell_prop.MaxValue.Equals(expected), String.Format("Incorrect Duration maximum for spell property: '{0}'", spell_prop.Name));


            return;
        }

        /// <summary>
        /// Check Range Property
        /// </summary>
        [Test]
        public void tRangeProperty()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_rangeSpellStr);
            BaseSpell spell = new BaseSpell(doc.FirstChild);

            Assert.AreEqual("Confuse", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            SpellProperty spell_prop = spell.ZOEProperty;
            Assert.AreEqual("ZOE", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));
            IPropertyValue expected = new NumericValue(1, "being");
            Assert.IsTrue(spell_prop.Value.Equals(expected), String.Format("Incorrect ZOE value for spell property: '{0}'", spell_prop.Name));

            spell_prop = spell.RangeProperty;
            Assert.AreEqual("Range", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));
            expected = new NumericValue(60, "feet");
            Assert.IsTrue(spell_prop.Value.Equals(expected), String.Format("Incorrect Duration value for spell property: '{0}'", spell_prop.Name));

            return;
        }

        private const string _rangeSpellStr = @"  <spell name='Confuse' level='1'>
                                                    <description>A confused creature will not be able to coordinate his actions with anyone else. (In the case of player characters, the players may not consult, and must submit orders in writing.) In addition there is a 1/3 chance each round that the creature will not be able to decide what to do that round, and thus will do absolutely nothing at all. Those creatures controlled by some outside source will not be affected, unless the controlling force also fails to save or fails to make other relevant control check. Only those of 4 HD or more will get saving throws. Those of 2 HD or less are affected immediately; others get a delay of d6 minus the level of the caster rounds.</description>
                                                    <ZOE value='1' units='being' />
                                                    <Range value='60' units='feet' />
                                                    <Duration value='12' units='rounds' />
                                                    <SavingThrow value='Will' effect='negates' />
                                                    <Properties>
                                                      <Property name='Duration' die='10' number = '0'>
                                                        <Modifier type='ExtraEffect' die='10' number = '1' cost='.5'></Modifier>
                                                      </Property>
                                                    </Properties>
                                                  </spell>";

        /// <summary>
        /// Check Die Values for Properties
        /// </summary>
        [Test]
        public void tDieValueProperty()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_tsunamiSpellStr);
            BaseSpell spell = new BaseSpell(doc.FirstChild);

            Assert.AreEqual("Tsunami", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            SpellProperty spell_prop = spell.ZOEProperty;
            Assert.AreEqual("ZOE", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));
            IPropertyValue expected = new DescribedValue("see description");
            Assert.IsTrue(spell_prop.Value.Equals(expected), String.Format("Incorrect ZOE value for spell property: '{0}'", spell_prop.Name));

            spell_prop = spell.DurationProperty;
            Assert.AreEqual("Duration", spell_prop.Name, String.Format("Incorrect name for spell property: '{0}'", spell_prop.Name));
            expected = new DescribedValue("d6 rounds");
            Assert.IsTrue(spell_prop.Value.Equals(expected), String.Format("Incorrect Duration value for spell property: '{0}'", spell_prop.Name));

            return;
        }

        private const string _tsunamiSpellStr = @"<spell name='Tsunami' level='9'>
                <description>This spell summons a 40' high wave. It requires a body of water at least 2 miles wide. The wave will be 720' long and will generally affect up to 540' inland. The effects of the wave at the shore line are disastrous, but they lessen as they move inland. Only the stoutest of castle walls can withstand the wave at full strength. The wave will arrive without notice d6 rounds after the casting of the spell. Modifiers: Extra ZOE (affects length of wave), Extra Effect (+20' to height, +180' to inland effect region, power goes as square of height) +1. </description>
                <ZOE value='see description' units='' type='described'/>
                <Range value='480' units='feet'/>
                <Duration value ='d6 rounds'/>
                <SavingThrow value='none'/>
              </spell>";

        [Test]
        public void tMorphMinder()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tCast._morphicSpellStr);
            SpellInfo spell_info = new SpellInfo(doc.FirstChild);
            MorphMinder morph = new MorphMinder(spell_info);
            Assert.IsNotNull(morph);
            ISpell base_spell = morph.GetMorphicBase("Attuned object");
            Assert.IsNotNull(base_spell);
            Assert.AreEqual(1, base_spell.Level, "Wrong base level: " + base_spell.Level.ToString());

            return;
        }
    }
}
