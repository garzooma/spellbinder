﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ArzooSoftware.SpellBinder.spellbinder;
using ArzooSoftware.SpellBinder.SpellBind2;
using System.Xml;
using Xceed.Wpf.Toolkit;

namespace ArzooSoftware.SpellBinder.Test
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.Test.tCastViewModel</name>
    /// <summary>
    /// Test CastViewModel in SpellBind2
    /// </summary>
    /// <version>1.0 - Initial Release - 2/15/2013 2:16:37 PM</version>
    /// <author>Greg Arzoomanian</author>    
    [TestFixture]
    class tCastViewModel
    {

        [Test]
        public void tCtor()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tSpellInfo._PatterningSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 16);

            Cast spell_cast = new Cast(base_spell, mage);
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);
        }

        [Test]
        public void tSetBaseLevel()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tSpellInfo._PatterningSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 16);

            Cast spell_cast = new Cast(base_spell, mage);
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);

            // Set base level
            vm.MorphicBaseLevel = "Wood";
            Assert.AreEqual(2, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(3, spell_cast.Cost, "Wrong cost for morphic spell");

            return;
        }

        /// <summary>
        /// Muliple changes to base level
        /// </summary>
        [Test]
        public void tAdjustBaseLevel()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tSpellInfo._PatterningSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 16);

            Cast spell_cast = new Cast(base_spell, mage);
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);

            // Set message handler
            vm.MessageEvent += new CastViewModel.MessageEventHandler(vm_MessageEvent);
            _vmMsg = string.Empty;

            // Set base level
            vm.MorphicBaseLevel = "Wood";
            Assert.AreEqual(2, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(3, spell_cast.Cost, "Wrong cost for morphic spell");
            Assert.AreEqual(String.Empty, _vmMsg, "Incorrect Message received");

            // Set base level back down
            vm.MorphicBaseLevel = "Ice";
            Assert.AreEqual(1, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(2, spell_cast.Cost, "Wrong cost for morphic spell");
            Assert.AreEqual(String.Empty, _vmMsg, "Incorrect Message received");

            // Set base level too high
            vm.MorphicBaseLevel = "Mithril";
            Assert.AreEqual(1, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(2, spell_cast.Cost, "Wrong cost for morphic spell");
            Assert.AreEqual("Can't set morphic spell 'Patterning' to base 'Mithril'; resulting level '5' too high for mage of level '6'", _vmMsg, "Incorrect Message received");

            // Set base level ok
            _vmMsg = string.Empty;
            vm.MorphicBaseLevel = "Stone";
            Assert.AreEqual(3, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(5, spell_cast.Cost, "Wrong cost for morphic spell");
            Assert.AreEqual(String.Empty, _vmMsg, "Incorrect Message received");

            return;
        }

        private string _vmMsg = "";

        void vm_MessageEvent(object sender, CastViewModel.MessageEventArgs e)
        {
            _vmMsg = e.Message;
        }

        /// <summary>
        /// Muliple changes to base level
        /// </summary>
        [Test]
        public void tAdjustApplications()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tSpellInfo._PatterningSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 16);

            Cast spell_cast = new Cast(base_spell, mage);
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);

            // Check base settings
            Assert.AreEqual(1, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(2, spell_cast.Cost, "Wrong cost for morphic spell");
            Assert.AreEqual("Paper, fabric, rope", vm.MorphicBaseLevel, "Wrong base level for morphic spell");
            Assert.AreEqual(1, vm.MorphicPropertyList.Count, "Wrong number of morphic properties");
            SpellProperty spell_prop = vm.MorphicPropertyList[0];
            Assert.AreEqual("Applications", spell_prop.Name);
            Assert.AreEqual(5, spell_prop.ModifierList.Count, "Wrong number of modifiers for Application property");

            // Set message handler
            vm.MessageEvent += new CastViewModel.MessageEventHandler(vm_MessageEvent);
            _vmMsg = string.Empty;

            // Change application
            System.Windows.Controls.ComboBox combobox = new System.Windows.Controls.ComboBox();
            combobox.IsEnabled = true;
            combobox.Tag = spell_prop;
            combobox.SelectedValue = "Disintegrate";
            vm.MorphicComboBox_SelectionChanged(combobox, null);
            Assert.AreEqual(3, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(5, spell_cast.Cost, "Wrong cost for morphic spell");

            // Set base level
            vm.MorphicBaseLevel = "Wood";
            Assert.AreEqual(4, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(8, spell_cast.Cost, "Wrong cost for morphic spell");
            Assert.AreEqual(String.Empty, _vmMsg, "Incorrect Message received");

            // Set base level too high
            vm.MorphicBaseLevel = "Iron";
            Assert.AreEqual(4, spell_cast.Level, "Wrong level for morphic spell");  // should be unchanged
            Assert.AreEqual(8, spell_cast.Cost, "Wrong cost for morphic spell");
            Assert.AreEqual("Can't set morphic spell 'Patterning' to base 'Iron'; resulting level '6' too high for mage of level '6'", 
                                _vmMsg, "Incorrect Message received");

            return;
        }

        private const string _IllusionSpellStr = @"
                      <spell name='Illusion' level='morphic'>
                        <description>This is a very powerful tool. It can be used to create illusions by warping air to reflect light or sound. Illusions are not  artifacts of mind control. An illusion will never cause damage. The instant that an illusion would have caused damage,  the spell will be broken. All modes except programmed (see below) automatically have the Concealment modifier cast  upon them at no extra cost.  It has two modes, visual and aural. It has two modifiers, programmed and interactive. Use of each mode allows the caster  to construct an illusion using that sense.  Visual: This is of course the most common. It may be used to create or hide a door, disguise a person, or create a false  image of something threatening, or any other purpose imaginable.  Aural: Most illusions will be far more convincing when used with sound. Many animals will not be fooled by any  illusion without sound; in fact, some animals will not even detect an illusion without sound.  The Visual and Aural modes can be bought any number of times each, proportional to the complexity of the illusion  desired. A single tone or blank wall would be one level, a voice or body two, a specific voice or body three or four, a  symphony or army five, etc.  The programmed modifier allows the mage to set a specific set of circumstances that would trigger the illusion. The  illusion then will perform some prearranged show. It will only work once, unless a Permanence is cast upon it. The site  where the illusion is to take place will detect as magic unless it is concealed.  The interactive modifier will allow the caster, if concentrating, to shape the illusion's responses and actions. Note that  unless the illusion is interactive, the entire script of the illusion must be chosen at the time of casting. Hence, shadow  fighters must be interactive, and thus require concentration.  There is no such thing as �disbelieving an illusion�. Illusions are really there: illusory walls do block sight, loud noises  will obscure other sounds. Creatures can ignore them just as they can ignore anything else. Remember that illusions will  never cause damage. In addition, most illusions are dispelled by touch of flesh; all illusions can be dispelled by Dispel  Magic (page 69).  Light and Darkness: Aside from some cantrips and highly specialized spells such as Ventriloquism and Magic Mouth,  Illusions are the mage's main power over light and darkness, noise and silence. Illusory light sources create real light  which can illuminate areas beyond the spell's ZOE. Illusory shadows genuinely do block vision. (However, if they are  to affect people inside them, they must be cast to survive the touch of flesh, unlike light sources which can simply be  placed out of the way. Unlike Darkness prayers, they do not block Darkvision.) Similar concerns apply to sounds and  silence: real sounds can be heard at a distance, while an anechoic zone will only deafen people inside it and must be built  to withstand their presence. Note that neither light nor sound can ever be made bright enough to be harmful, nor can an  illusion's glow have the properties that make sunlight deadly to some Undead.  Cost: 1  2 level per level of visual or aural mode. +2 spell levels for programmed. +1 for interactive . +1 for the illusion  to not be dispelled by touch of flesh. +1 for a �traveling� illusion with a movable ZOE. The ZOE is centered on some  object or being and moves with it. Of course, it must either be �touchproof� or else the actual illusions must be restricted  to a part of the ZOE that doesn't come into contact with anyone. In addition, an illusion cast on an unwilling person or  an object carried by an unwilling person allows a Will save to negate the spell. Extra ZOE doubles the ZOE for +1. The  progression is geometric  </description>
                        <ZOE value='20 foot sphere' units='' type='described'/>
                        <Range value='120' units='feet'/>
                        <Duration value='10' units='minutes'/>
                        <SavingThrow value='none'/>
                        <Properties>
                          <Property name='Visual' value='0'>
                            <Modifier type='Visual' value='1' cost='.5'></Modifier>
                          </Property>
                          <Property name='Aural' value='0'>
                            <Modifier type='Aural' value='1' cost='.5'></Modifier>
                          </Property>
                          <Property name='Programmed' value='no' max='yes'>
                            <Modifier type='Programmed' value='no' cost='0'></Modifier>
                            <Modifier type='Programmed' value='yes' cost='2'></Modifier>
                          </Property>
                          <Property name='Interactive' value='no' max='yes'>
                            <Modifier type='Interactive' value='no' cost='0'></Modifier>
                            <Modifier type='Interactive' value='yes' cost='1'></Modifier>
                          </Property>
                          <Property name='Dispellable' value='yes' max='no'>
                            <Modifier type='Dispellable' value='yes' cost='0'></Modifier>
                            <Modifier type='Dispellable' value='no' cost='1'></Modifier>
                          </Property>
                          <Property name='Traveling' value='no' max='yes'>
                            <Modifier type='Traveling' value='no' cost='0'></Modifier>
                            <Modifier type='Traveling' value='yes' cost='1'></Modifier>
                          </Property>
                          <Property name='ZOE Multiplier' value='1'>
                            <Modifier type='ExtraZOE' value='1' cost='1'></Modifier>
                          </Property>
                        </Properties>
                      </spell>
                    ";

        /// <summary>
        /// Muliple changes to base level
        /// </summary>
        [Test]
        public void tMorphicProperties()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_IllusionSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(5, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);

            // Check base settings
            Assert.AreEqual(0, spell_cast.Level, "Wrong level for morphic spell");
            Assert.AreEqual(2, spell_cast.Cost, "Wrong cost for morphic spell");
            Assert.AreEqual(0, vm.BaseLevelList.Count, "Wrong base level for morphic spell");
            Assert.AreEqual(4, vm.MorphicPropertyList.Count, "Wrong number of morphic properties");
            SpellProperty spell_prop = vm.MorphicPropertyList[0];
            Assert.AreEqual("Programmed", spell_prop.Name);
            Assert.AreEqual(2, spell_prop.ModifierList.Count, "Wrong number of modifiers for Application property");

            // Set message handler
            vm.MessageEvent += new CastViewModel.MessageEventHandler(vm_MessageEvent);
            _vmMsg = string.Empty;

            return;
        }

        /// <summary>
        /// Simple adding of modifiers
        /// </summary>
        [Test]
        public void tAddModifiers()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tCast._makeLastingSpellStr);    // Telescopic vision
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 16);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual("3 hours", spell_cast.Duration, "Incorrect duration");
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);

            // Add modifier
            IModifier modifier = spell_cast.Spell.DurationProperty.Modifier;
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("6 hours", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("1.5", vm.Level, "Incorrect level");

            //// Add another modifier
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("12 hours", spell_cast.Duration, "Incorrect duration");

            return;
        }
        
        /// <summary>
        /// Simple adding of modifiers
        /// </summary>
        [Test]
        public void tAddModifiers2()
        {
            ISpell base_spell = SpellBook.Book["Confuse"];
            Mage mage = new Mage(4, 17);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual("12 rounds", spell_cast.Duration, "Incorrect duration");
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);
            Assert.AreEqual("1", vm.Level, "Incorrect level");

            // Add modifier
            IModifier modifier = spell_cast.Spell.DurationProperty.Modifier;
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("24 rounds", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("1.5", vm.Level, "Incorrect level");

            // Add another modifier
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("5 minutes", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("2", vm.Level, "Incorrect level");

            return;
        }

        /// <summary>
        /// Add different modifiers
        /// </summary>
        [Test]
        public void tAddModifiers3()
        {
            ISpell base_spell = SpellBook.Book["Confuse"];
            Mage mage = new Mage(9, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual("12 rounds", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("60'", spell_cast.Range, "Incorrect range");
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);
            Assert.AreEqual("1", vm.Level, "Incorrect level");

            // Add modifier
            IModifier modifier = spell_cast.Spell.DurationProperty.Modifier;
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("24 rounds", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("60'", spell_cast.Range, "Incorrect range");
            Assert.AreEqual("1.5", vm.Level, "Incorrect level");

            // Add another modifier
            modifier = spell_cast.Spell.RangeProperty.Modifier;
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("24 rounds", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("120'", spell_cast.Range, "Incorrect range");
            Assert.AreEqual("2", vm.Level, "Incorrect level");
            Assert.IsFalse(vm.CastOnTheRun);
            Assert.IsFalse(vm.Concealment);

            // Add another modifier
            SpellProperty spell_prop = spell_cast.Spell.PropertiesList.First(p => p.Name == SpellProperty.StdPropertyNames.CastOnTheRun);
            modifier = spell_prop.Modifier;
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("24 rounds", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("120'", spell_cast.Range, "Incorrect range");
            Assert.IsTrue(vm.CastOnTheRun);
            Assert.IsFalse(vm.Concealment);
            Assert.AreEqual("3", vm.Level, "Incorrect level");

            // Add another modifier
            spell_prop = spell_cast.Spell.PropertiesList.First(p => p.Name == SpellProperty.StdPropertyNames.Concealment);
            modifier = spell_prop.Modifier;
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("24 rounds", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("120'", spell_cast.Range, "Incorrect range");
            Assert.IsTrue(vm.CastOnTheRun);
            Assert.IsTrue(vm.Concealment);
            Assert.AreEqual("4", vm.Level, "Incorrect level");

            return;
        }

        /// <summary>
        /// Add different modifiers
        /// </summary>
        [Test]
        public void tAddPowerWord()
        {
            ISpell base_spell = SpellBook.Book["Web"];
            Mage mage = new Mage(9, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual("40 minutes", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("60'", spell_cast.Range, "Incorrect range");
            CastViewModel vm = new CastViewModel(spell_cast);
            Assert.IsNotNull(vm);
            Assert.AreEqual("2", vm.Level, "Incorrect level");

            // Add modifier
            SpellProperty spell_prop = spell_cast.Spell.PropertiesList.FirstOrDefault(p => p.Name == SpellProperty.StdPropertyNames.PowerWord);
            IModifier modifier = spell_prop.Modifier;
            spell_cast.AddModifier(modifier);
            Assert.AreEqual("40 minutes", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("60'", spell_cast.Range, "Incorrect range");
            Assert.AreEqual("5", vm.Level, "Incorrect level");
            Assert.IsFalse(vm.CastOnTheRun);
            Assert.IsFalse(vm.Concealment);

            // Add another modifier
            spell_prop = spell_cast.Spell.PropertiesList.First(p => p.Name == SpellProperty.StdPropertyNames.CastOnTheRun);
            modifier = spell_prop.Modifier;
            bool caught_excp = false;
            try
            {
                spell_cast.AddModifier(modifier);
            }
            catch (Cast.BadStateException excp)
            {
                caught_excp = true;
            }
            Assert.IsTrue(caught_excp);
            Assert.AreEqual("40 minutes", spell_cast.Duration, "Incorrect duration");
            Assert.AreEqual("60'", spell_cast.Range, "Incorrect range");
            Assert.IsFalse(vm.CastOnTheRun);
            Assert.IsFalse(vm.Concealment);
            Assert.AreEqual("5", vm.Level, "Incorrect level");

            return;
        }
    }
}
