﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ArzooSoftware.SpellBinder.spellbinder;
using ArzooSoftware.SpellBinder.SpellForm;
using System.Xml;
using System.Windows.Forms;

namespace ArzooSoftware.SpellBinder.Test
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.Test.tPM</name>
    /// <summary>
    /// Test PresentationModel class
    /// </summary>
    /// <version>1.0 - Initial Release - 1/21/2013 7:01:53 PM</version>
    /// <author>Greg Arzoomanian</author>    
    [TestFixture]
    class tPM
    {
        /// <summary>
        /// Create PresentationModel object
        /// </summary>
        [Test]
        public void tCtor()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);

            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            SpellForm.PresentationModel pm = new SpellForm.PresentationModel(spell_cast, form);

            return;
        }

        private string _spellStr = @"<spell name='Detect Magic' level='1'>
                                        <description>A confused creature will not be able to coordinate his actions with anyone else. (In the case of player characters, the players may not consult, and must submit orders in writing.) In addition there is a 1/3 chance each round that the creature will not be able to decide what to do that round, and thus will do absolutely nothing at all. Those creatures controlled by some outside source will not be affected, unless the controlling force also fails to save or fails to make other relevant control check. Only those of 4 HD or more will get saving throws. Those of 2 HD or less are affected immediately; others get a delay of d6 minus the level of the caster rounds.</description>
                                        <ZOE value='self' type='described'/>
                                        <Range value='as sight' />
                                        <Duration value='10' units='minutes' />
                                        <SavingThrow value='none' />
                                      </spell>";

        /// <summary>
        /// Send update messages from combo box
        /// </summary>
        [Test]
        public void tComboBox()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            SpellForm.PresentationModel pm = new SpellForm.PresentationModel(spell_cast, form);
            ComboBox gest_combo_box = new ComboBox();
            gest_combo_box.Items.AddRange(new object[] {
                "Normal",
                "+1 disguise gestures",
                "+2 without gestures"});
            gest_combo_box.SelectedIndex = 1;
            pm.redGesturesCmbBx_SelectedIndexChanged(gest_combo_box, new EventArgs());
            Assert.AreEqual(2, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            ComboBox incant_combo_box = new ComboBox();
            incant_combo_box.Items.AddRange(new object[] {
                "Normal",
                "+1 quiet",
                "+2 silent"});
            incant_combo_box.SelectedIndex = 2;
            pm.redIncantCmbBx_SelectedIndexChanged(incant_combo_box, new EventArgs());
            Assert.AreEqual(4, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            gest_combo_box.SelectedIndex = 0;   // remove "Reduced Gestures" modifier
            pm.redGesturesCmbBx_SelectedIndexChanged(gest_combo_box, new EventArgs());
            Assert.AreEqual(3, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            return;
        }

        /// <summary>
        /// Add too many modifiers for maximum spell level
        /// </summary>
        [Test]
        public void tTooManyMods()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            // Add modifiers
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No duration modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(2, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("20 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // Now a lot of modifiers
            spell_cast.AddModifier(modifier);
            spell_cast.AddModifier(modifier);
            spell_cast.AddModifier(modifier);
            spell_cast.AddModifier(modifier);
            Assert.AreEqual(3.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            // Add one more modifier to get past maximum spell level
            SpellForm.PresentationModel pm = new SpellForm.PresentationModel(spell_cast, form);
            pm.MessageEvent -= new SpellForm.PresentationModel.MessageEventHandler(form.HandlePresentationMessage);
            ComboBox combo_box = new ComboBox();
            combo_box.Items.AddRange(new object[] {
                "Normal",
                "+1 disguise gestures",
                "+2 without gestures"});
            combo_box.SelectedIndex = 1;
            bool caught_excp = false;
            try
            {
                pm.redGesturesCmbBx_SelectedIndexChanged(combo_box, new EventArgs());
            }
            catch (Cast.BadStateException excp)
            {
                caught_excp = true;
            }
            Assert.AreEqual(true, caught_excp, "Didn't catch exception");
            Assert.AreEqual(3.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            return;
        }

        /// <summary>
        /// Send update messages from saving throw control
        /// </summary>
        [Test]
        public void tSave()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_tripSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            SpellForm.PresentationModel pm = new SpellForm.PresentationModel(spell_cast, form);
            //ComboBox combo_box = new ComboBox();
            NumericUpDown updn_ctl = new NumericUpDown();
            updn_ctl.Value = 1;     // bump up one

            // Call PresentationModel method
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is SaveModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            return;
        }

        private const string _tripSpellStr = @"  <spell name='Trip' level='1'>
    <description>
      This spell knocks the victim prone if he fails his save. Prone combatants are at a disadvantage. It usually takes one round to get up. This spell has a built-in saving throw penalty.
    </description>
    <ZOE value='1' units='biped' />
    <Range value='120' units='feet' />
    <Duration value='momentary' />
    <SavingThrow value='Reflex at -3' effect='negates' />
  </spell>";

        /// <summary>
        /// Send update messages from saving throw control
        /// </summary>
        [Test]
        public void tDuration()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tBaseSpell._RFSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("10 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            PresentationModel pm = new PresentationModel(spell_cast, form);
            //ComboBox combo_box = new ComboBox();
            NumericUpDown updn_ctl = new NumericUpDown();
            updn_ctl.Value = 1;     // bump up one

            // Call PresentationModel method
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("20 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("40 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            return;
        }

        /// <summary>
        /// Send update messages from saving throw control
        /// </summary>
        [Test]
        public void tRange()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_ventriloquismSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("40 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("60'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            PresentationModel pm = new PresentationModel(spell_cast, form);
            //ComboBox combo_box = new ComboBox();
            NumericUpDown updn_ctl = new NumericUpDown();
            updn_ctl.Value = 1;     // bump up one

            // Call PresentationModel method
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is RangeModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("40 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("120'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("40 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("240'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            return;
        }

        private const string _ventriloquismSpellStr = @"  <spell name='Ventriloquism' level='1'>
                                                            <description>The mage may make the sound of his voice come from somewhere else up to the spell range distant. He may also use it to imitate the voices of others. The difference will not be detected if he has heard the voice before. This function of the spell may be used in conjunction with Magic Mouth (page 67), Long Talk (page 67), or Disguise (page 69).
                                                            </description>
                                                            <ZOE value='self' type='described'/>
                                                            <Range value='60' units='feet'/>
                                                            <Duration value='40' units='minutes' />
                                                            <SavingThrow value='none' />
                                                          </spell>";

        /// <summary>
        /// Send update messages from saving throw control
        /// </summary>
        [Test]
        public void tAtRange()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tBaseSpell._HoldPortalSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("1 hour", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("10'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            // Start testing on PresentationModel
            PresentationModel pm = new PresentationModel(spell_cast, form);

            // Simulate checkbox for At Range
            CheckBox check_box = new CheckBox();
            check_box.Checked = true;
            SpellProperty spell_prop = spell_cast.Spell.PropertiesList.First(p => p.Name == "AtRange");
            PresentationBinding pm_binding = new PresentationBinding(spell_cast, spell_prop);
            check_box.Tag = pm_binding;
            pm.atRangeChkBx_CheckedChanged(check_box, null);
            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("1 hour", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("60'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            
            NumericUpDown updn_ctl = new NumericUpDown();
            updn_ctl.Value = 1;     // bump up one

            // Call PresentationModel method
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is RangeModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("1 hour", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("120'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(2.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("240'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            return;
        }

        /// <summary>
        /// Send update messages from damage control
        /// </summary>
        [Test]
        public void tDamage()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_LanceSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("60'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("2d6", spell_cast.Damage.DisplayValue, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));
            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            // Start testing on PresentationModel
            PresentationModel pm = new PresentationModel(spell_cast, form);
            Assert.AreEqual("2d6", pm.DamageNumber, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            // Simulate updown control for Damage
            NumericUpDown updn_ctl = new NumericUpDown();
            updn_ctl.Value = 1;     // bump up one
            //SpellProperty spell_prop = spell_cast.Spell.PropertiesList.First(p => p.Name == "AtRange");
            //PresentationBinding pm_binding = new PresentationBinding(spell_cast, spell_prop);
            //updn_ctl.Tag = pm_binding;


            // Call PresentationModel method
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DamageModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("60'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("3d6", pm.DamageNumber, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual("60'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("4d6", pm.DamageNumber, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            return;
        }

        private const string _LanceSpellStr = @"  <spell name='Lance of (Element)' level='1'>
		                                <description>Exists in three different elemental versions: Fire, Ice, and Lightning (elemental fire, water, and air respectively). The Earth element equivalent is the Magic Missile spell (see below). Each Lance creates the given element and flings it at a single target. The element is nonmagical once created, so there is no saving throw but the appropriate form of Resistance grants complete immunity. (Ice Lance is stopped by Resist Cold). The Lance spell is a ranged touch attack which ignores cover penalties, including the 'cover' provided by a friendly character in melee: therefore it can never hit an unintended target, though it can simply miss. The base damage is 2d6.</description> 
		                                <ZOE value='1' units='being' /> 
		                                <Range value='60' units='feet' /> 
		                                <Duration value='momentary' units='' /> 
		                                <SavingThrow value='none' effect='' /> 
		                                <Damage die='6' number='2'/>
		                                <Modifiers> 
			                                <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
		                                </Modifiers>
                                        <Properties>
                                          <Property name='Increased Accuracy' value='0'>
                                            <Modifier type='IncreasedAccuracy' value='2' cost='.5'></Modifier>
                                          </Property>
                                        </Properties>
	                                </spell>";

        /// <summary>
        /// Send update messages from damage control
        /// </summary>
        [Test]
        public void tDamagePlus()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tCast._magicMissileSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("100'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("1d10+1", spell_cast.Damage.DisplayValue, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));
            SpellForm.SpellForm form;
            try
            {
                form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            }
            catch (Exception excp) // to check stack
            {
                Console.WriteLine("Exception: " + excp.Message);
                throw;
            }

            // Start testing on PresentationModel
            PresentationModel pm = new PresentationModel(spell_cast, form);
            Assert.AreEqual("1d10+1", pm.DamageNumber, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            // Simulate updown control for Damage
            NumericUpDown updn_ctl = new NumericUpDown();
            updn_ctl.Value = 1;     // bump up one
            //SpellProperty spell_prop = spell_cast.Spell.PropertiesList.First(p => p.Name == "AtRange");
            //PresentationBinding pm_binding = new PresentationBinding(spell_cast, spell_prop);
            //updn_ctl.Tag = pm_binding;

            // Call PresentationModel method
            IModifier modifier = null;
            // Damage modifier
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DamageModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("100'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("2d10+1", pm.DamageNumber, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            pm.HandleUpDownEvent(modifier, updn_ctl, form.CommonControls);
            Assert.AreEqual(3M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("100'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("3d10+1", pm.DamageNumber, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            // Damage plus modifier
            dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is SpellModifier && m.Name == "ExtraPlus"));
            if (dm_list.Count > 0)
                modifier = dm_list[0];

            SpellProperty spell_prop = spell_cast.Spell.PropertiesList.First(p => p.Modifier.Name == "ExtraPlus");

            pm.HandleUpDownEvent(spell_prop, updn_ctl, form.CommonControls);
            Assert.AreEqual(3.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("100'", spell_cast.Range, String.Format("Incorrect range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("3d10+2", pm.DamageNumber, String.Format("Incorrect damage for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            return;
        }

        /// <summary>
        /// Test boolean modifier for extra property
        /// </summary>
        [Test]
        public void tBoolMod()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tCast._boolModSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Spell.ExtraPropertiesList.Count, String.Format("Incorrect number of extra properties for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Spell.ExtraPropertiesList.Count));
            SpellProperty spell_prop = spell_cast.Spell.ExtraPropertiesList[0];
            PresentationBinding binding = new PresentationBinding(spell_cast, spell_prop);

            SpellForm.SpellForm form = new SpellForm.SpellForm(base_spell, mage);   // uninitialized form
            PresentationModel pm = new PresentationModel(spell_cast, form);
            NumericUpDown updn_ctl = new NumericUpDown();
            updn_ctl.Tag = binding;
            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("no", spell_cast.GetPropertyValue(spell_prop).DisplayValue, String.Format("Incorrect value for Spell '{0}': '{1}'", base_spell.Name, spell_cast.GetPropertyValue(spell_prop).DisplayValue));

            // Call PresentationModel method
            IModifier modifier = spell_prop.Modifier;

            updn_ctl.Value = 1;
            pm.HandleUpDownEvent(spell_prop, updn_ctl, form.CommonControls);
            Assert.AreEqual(3M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("yes", spell_cast.GetPropertyValue(spell_prop).DisplayValue, String.Format("Incorrect value for Spell '{0}': '{1}'", base_spell.Name, spell_cast.GetPropertyValue(spell_prop).DisplayValue));

            updn_ctl.Value = 2;
            pm.HandleUpDownEvent(spell_prop, updn_ctl, form.CommonControls);
            Assert.AreEqual(3M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual("yes", spell_cast.GetPropertyValue(spell_prop).DisplayValue, String.Format("Incorrect value for Spell '{0}': '{1}'", base_spell.Name, spell_cast.GetPropertyValue(spell_prop).DisplayValue));

            return;
        }

    }
}
