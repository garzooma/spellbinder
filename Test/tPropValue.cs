﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ArzooSoftware.SpellBinder.spellbinder;

namespace ArzooSoftware.SpellBinder.Test
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.Test.tPropValue</name>
    /// <summary>
    /// Tests for IPropertyValue
    /// </summary>
    /// <version>1.0 - Initial Release - 9/15/2013 1:33:10 PM</version>
    /// <author>Greg Arzoomanian</author>    
    /// 
    [TestFixture]
    public class tPropValue
    {
        [Test]
        public void TestNumericCompare()
        {
            // Simple compare
            NumericValue val1 = new NumericValue(10, "minutes");
            NumericValue val2 = new NumericValue(20, "minutes");
            int compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val < 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val > 0);

            // Hour/minute
            val1 = new NumericValue(90, "minutes");
            val2 = new NumericValue(3, "hours");
            compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val < 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val > 0);

            // Single hour/90 minutes
            val1 = new NumericValue(1, "hour");
            val2 = new NumericValue(90, "minutes");
            compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val < 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val > 0);

            // hour/day
            val1 = new NumericValue(1, "day");
            val2 = new NumericValue(32, "hours");
            compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val < 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val > 0);

            // 8 day/256 hours
            val1 = new NumericValue(8, "days");
            val2 = new NumericValue(256, "hours");
            compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val < 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val > 0);

            return;
        }

        [Test]
        public void TestRangeCompare()
        {
            // Simple compare
            NumericValue val1 = new NumericValue(60, "feet");
            NumericValue val2 = new NumericValue(30, "feet");
            Assert.IsFalse(val1.Equals(val2));
            Assert.IsFalse(val2.Equals(val1));
            int compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val > 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val < 0);

            val2 = new NumericValue(60, "feet");
            Assert.IsTrue(val1.Equals(val2));
            Assert.IsTrue(val2.Equals(val1));
            compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val == 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val == 0);

            val2 = new NumericValue(0.25m, "mile");
            Assert.IsFalse(val1.Equals(val2));
            Assert.IsFalse(val2.Equals(val1));
            compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val < 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val > 0);

            // Check large number of feet
            val1 = new NumericValue(1m, "mile");
            val2 = new NumericValue(10000m, "feet");
            compare_val = val1.CompareTo(val2);
            Assert.IsTrue(compare_val < 0);
            compare_val = val2.CompareTo(val1);
            Assert.IsTrue(compare_val > 0);

            return;
        }
    }
}
