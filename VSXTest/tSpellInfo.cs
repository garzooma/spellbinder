﻿using System;
using System.Xml;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArzooSoftware.SpellBinder.spellbinder;

namespace VSXTest
{
    /// <summary>
    /// Test reading spell info from xml
    /// </summary>
    /// 
    [TestClass]
    public class tSpellInfo
    {
        /// <summary>
        /// Create SpellInfo object
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tCtor()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Detect Magic", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));
        }

        private string _spellStr = @"<spell name='Detect Magic' level='1'>
                                        <description>A confused creature will not be able to coordinate his actions with anyone else. (In the case of player characters, the players may not consult, and must submit orders in writing.) In addition there is a 1/3 chance each round that the creature will not be able to decide what to do that round, and thus will do absolutely nothing at all. Those creatures controlled by some outside source will not be affected, unless the controlling force also fails to save or fails to make other relevant control check. Only those of 4 HD or more will get saving throws. Those of 2 HD or less are affected immediately; others get a delay of d6 minus the level of the caster rounds.</description>
                                        <ZOE value='self' type='described'/>
                                        <Range value='as sight' />
                                        <Duration value='10' units='minutes' />
                                        <SavingThrow value='none' />
                                      </spell>";

        /// <summary>
        /// Check reading a spell with properties
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tSpellProperties()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellPropStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Confuse", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            // Check properties
            Assert.AreEqual(1, spell.ExtraProperties.Count, "Wrong number of properties: " + spell.ExtraProperties.Count);
            SpellProperty prop = spell.ExtraProperties[0];
            Assert.AreEqual("Duration", prop.Name, String.Format("Wrong name for property: '{0}'", prop.Name));
            return;
        }

        private string _spellPropStr = @"<spell name='Confuse' level='1'>
    <description>A confused creature will not be able to coordinate his actions with anyone else. (In the case of player characters, the players may not consult, and must submit orders in writing.) In addition there is a 1/3 chance each round that the creature will not be able to decide what to do that round, and thus will do absolutely nothing at all. Those creatures controlled by some outside source will not be affected, unless the controlling force also fails to save or fails to make other relevant control check. Only those of 4 HD or more will get saving throws. Those of 2 HD or less are affected immediately; others get a delay of d6 minus the level of the caster rounds.</description>
    <ZOE value='1' units='being' />
    <Range value='60' units='feet' />
    <Duration value='12' units='rounds' />
    <SavingThrow value='Will' effect='negates' />
    <Properties>
      <Property name='Duration' die='10' number = '0'>
        <Modifier type='ExtraEffect' die='10' number = '1' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>";

        /// <summary>
        /// Check reading a property values
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tSpellPropValues()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_sleepSpellStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Sleep", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            // Check properties
            Assert.AreEqual(2, spell.ExtraProperties.Count, "Wrong number of properties: " + spell.ExtraProperties.Count);
            SpellProperty prop = spell.ExtraProperties[1];  // 2nd property (HD)
            Assert.AreEqual("Extra Effect", prop.Name, String.Format("Wrong name for property: '{0}'", prop.Name));
            Assert.IsTrue(prop.Value is NumericValue, "Not numeric value");
            NumericValue num_val = prop.Value as NumericValue;
            Assert.AreEqual(12, num_val.Value, "Wrong value");
            Assert.AreEqual("HD", num_val.Units, "Wrong units");
            Assert.AreEqual("12 HD", num_val.DisplayValue, "Wrong display for value");

            return;
        }

        private const string _sleepSpellStr = @"
          <spell name='Sleep' level='1'>
            <description>A Sleep spell can affect a maximum of 12 HD of creatures within the ZOE. The spell will affect the lowest-level targets first, continuing until the strength of the spell is used up or no more creatures can be affected. Larger creatures use up spell strength disproportionately: beings of 1 or 2 HD count at face value, but a 3HD creature counts as 4, 4HD counts as 8, 5HD counts as 16, etc. Undead or other non-living entities can not be slept regardless of level, nor can living organisms that do not naturally sleep. Creatures that fail their save cannot be awakened by non-magical means for 10 rounds. If they are not disturbed they will sleep for 2 hours.
            </description>
            <ZOE value='60' units='foot' type='cone'/>
            <Range value='Always zero' units='' />
            <Duration value='see description' units='' />
            <SavingThrow value='Will' effect='negates' />
            <Properties>
              <Property name='Duration' value='10' units = 'rounds'>
                <Modifier type='ExtraDuration' value='5' units = 'rounds' cost='.5'></Modifier>
              </Property>
              <Property name='Extra Effect' value='12' units = 'HD'>
                <Modifier type='ExtraEffect' value='3' units = 'HD' cost='.5'></Modifier>
              </Property>
            </Properties>
          </spell>";

        /// <summary>
        /// Check reading a boolean property values
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tSpellBoolValues()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tCast._boolModSpellStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Invisibility", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            // Check properties
            Assert.AreEqual(1, spell.ExtraProperties.Count, "Wrong number of properties: " + spell.ExtraProperties.Count);
            SpellProperty prop = spell.ExtraProperties[0];  // Improved Invisibility property
            Assert.AreEqual("Improved Invisibility", prop.Name, String.Format("Wrong name for property: '{0}'", prop.Name));
            Assert.IsTrue(prop.Value is DescribedValue, "Not described value");
            DescribedValue descr_val = prop.Value as DescribedValue;
            Assert.AreEqual("no", descr_val.DisplayValue, "Wrong value");
            Assert.AreEqual("yes", prop.MaxValue.DisplayValue, "Wrong max value");

            // Check property modifier
            IModifier mod = prop.Modifier;
            Assert.AreEqual("Improved", mod.Name, String.Format("Wrong name for modifier: '{0}'", mod.Name));
            Assert.AreEqual("yes", mod.IncrementValue.DisplayValue, String.Format("Wrong value for modifier: '{0}'", mod.Name));

            return;
        }

        /// <summary>
        /// Check reading Morphic spell base values
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tMorphicBaseValues()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_AttuneSpellStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Attune", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            // Check base levels
            Assert.AreEqual(2, spell.BaseLevel.ModifierList.Count, "Wrong number of properties: " + spell.BaseLevel.ModifierList.Count);
            IModifier level_mod = spell.BaseLevel.ModifierList[0];  // Level 1
            Assert.AreEqual("object/place", level_mod.Name, String.Format("Wrong name for spell: '{0}'", spell.Name));
            Assert.AreEqual(0, level_mod.Cost, "Wrong cost");
            Assert.AreEqual(1M, level_mod.IncrementValue.CalculationValue, "Wrong cost");

            return;
        }

        private const string _AttuneSpellStr = @"
                  <spell name='Attune' level='morphic'>
                    <description>This spell attunes the mage to an object creating a magic bond between the mage and the object. The target of the  spell may be a literal object, or a place or a person. If the object is a person, that person must either drop their saving  throw or fail to save twice for the attunement to work.  Once attuned to the target, the mage enjoys a connection which enhances the operation of some spells:  Locate (page 59) works better with attuned objects.  Message (page 64) works at much greater range with attuned persons.  ESP (page 66) works at greater range and effectiveness with attuned persons.  Clairsentience (page 69) works at greater range and effectiveness with attuned persons.  Telepathy (page 74) may be forced on attuned targets and used at greater range than normal.  Summon (page 78) only works with attuned objects.  Teleport (page 78) to distant locations is only safe with attuned places or objects.  Aside from these specific spell effects, any spell cast on an attuned target allows the target only half its normal saving  throw bonus.  Attuning to a literal object or a place is a Level 1 base spell.  Attuning to a person is a Level 2 base spell.  It takes an hour to attune to an object. A mage can be attuned to a maximum of 7 objects without penalty. Attuning to  more objects makes the spell more difficult: attempting to attune an 8th object requires a +1  2 modifier, the 9th attunement  requires +1, etc. Note that these modifiers are not required if the mage drops one of his existing attunements in the  process of casting the new one, which is the standard action for a mage already holding 7 attunements and requires no  extra time or casting cost.  Attuning to an object which another mage has already attuned breaks the other mage's bond to the object, but it requires  a Level Contest with the previously attuned mage, who gets a 2 level bonus. Players should keep track of the objects their  mages are attuned to on their character sheet. It is assumed that every mage is attuned to their home unless otherwise  specified.  </description>
	                  <BaseLevels>
		                  <Level name='object/place' level='1'/>
		                  <Level name='person' level='2'/>
	                  </BaseLevels>
	                  <ZOE value='1' units='object' type=''/>
                    <Range value='touch' units=''/>
                    <Duration value='permanent' units=''/>
                    <SavingThrow value='Will' effect='negates'/>
                  </spell>
                ";

        /// <summary>
        /// Check reading Morphic spell application values
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tMorphicApplications()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_PatterningSpellStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Patterning", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            // Check base levels
            Assert.AreEqual(8, spell.MorphList.Count, "Wrong number of base levels: " + spell.MorphList.Count);
            SpellInfo morph_info = spell.MorphList[0];  // Level 1
            Assert.AreEqual("Paper, fabric, rope", morph_info.Name, String.Format("Wrong name for spell: '{0}'", spell.Name));
            Assert.AreEqual(1, morph_info.Level, "Wrong level");

            // Check Applications
            Assert.AreEqual(1, spell.ExtraProperties.Count, "Wrong number of spell properties: " + spell.ExtraProperties.Count);
            SpellProperty spell_prop = spell.ExtraProperties[0];
            Assert.AreEqual(5, spell_prop.ModifierList.Count, "Wrong number of spell properties: " + spell_prop.ModifierList.Count);
            IModifier mod = spell_prop.ModifierList[0];
            Assert.AreEqual("Bind/Weaken", mod.IncrementValue.DisplayValue, "Wrong modifier value: " + mod.IncrementValue.DisplayValue);

            return;
        }

        public const string _PatterningSpellStr = @"
                      <spell name='Patterning' level='morphic' category='Environment'>
                        <description>This strengthens or weakens, but does not transmute, nonliving substances and objects. The base spell level  depends on how strong the material is, as defined by its Hardness (see p.42). The base spell level is 1/3 of the Hardness  of the target, rounding up to the nearest half-level. The final spell level, including base plus modifiers, must be at least  1. Patterning cast on a composite object (e.g. a hafted weapon) can be cast at a level too low to affect the strongest  materials, in which case it will affect only the weaker materials. If a substance has multiple Hardness ratings against  different attack types (e.g. rope), use the lowest.  Patterning has several applications which may raise the base level of the spell. All applications have duration �momentary�:  their effects are real and permanent and cannot be dispelled. Note, however, that Strengthened and Grand Patterned  materials do detect as magical: the magic is innate to the substance and cannot be removed except by destroying it or  undoing the Patterning.  Bind: makes something a seamless whole, strengthening any weak points or flaws. Fastening a tool to its handle, or a  weapon blade into its haft, need only affect the weaker material. Bind can splice ropes. When Binding a complex  object, such as a lock, parts that are intended to move retain their freedom of movement. Bind makes an object as  strong as it can be without introducing preternatural properties to the materials. It incidentally repairs all scuffs,  scratches, and other minor cosmetic flaws. It cannot repair major holes or gaps. Level +0.  Weaken: simply weakens the material or object. Its Hardness, hit points, and strength (load capacity) can be lowered  to half their normal values. Weaken undoes Strengthen; when weakening a Strengthened material the material  properties can be lowered to half their natural (pre-Strengthening) values. Level +0.  Seal: has all the effects of Bind, and in addition will make things watertight or even airtight. It can also be reversed  (Unseal) to make a normally watertight or airtight substance leaky. Level +1  2 .  Mend: has all the effects of Bind, plus Seal/Unseal if desired, plus it can repair major damage. Holes up to half the size  of the ZOE can be repaired; an object significantly smaller than the ZOE can be reassembled from a few scattered  fragments. Mend cannot build brand-new objects from raw materials; the thing you are trying to mend must have  previously existed in the form you are trying to restore. +1 level.  Break: is the obvious opposite of Mend. An object that more or less fills the ZOE can be broken in half or have a hole  half its size punched through it. Smaller objects can be broken into more pieces proportionately. +1 level.  Disintegrate: is just a more extreme form of Break. The object is reduced to a fine powder from which it cannot be  reconstituted by Mending. +2 levels.  Strengthen: allows you to increase the Hardness, hit points, and strength/loadbearing of a material. Simply cast the  spell at the base level for the Hardness you want to achieve instead of the current Hardness of the material. When  strengthening flexible material like rope or fabric, you choose whether the increase in Hardness merely makes it  more resistant to damage or actually makes it rigid and hard. Load strength and hit points can be increased by  a factor of either the proportion by which you are increasing Hardness, or the base level of the Patterning spell,  whichever is less.  Grand: enchants a material with magical permanence and unity. Hardness per se is not altered, but it acquires DR50/-  against all forms of physical or magical attack, including energy attacks such as fire, lightning, acid, etc. Its  strength or load capacity increases tenfold. Note that Grand Patterned clothing does not extend its invulnerability  to the wearer. Once an object has been Grand Patterned, to affect it further with Patterning requires the caster to  apply the Grand modifier and win a Level Contest with the original caster. All mitems (save scrolls, potions,  and naturally magical materials) have been Grand Patterned as part of their construction.+4 levels.  The ZOE for any form of Patterning is one object (or portion of a larger object) that may weigh up to 200 lbs and fit into  a cube not more than 10 feet on a side. This can be doubled for +1 (geometric progression)  </description>
		                <MorphList>
	                      <spell name='Paper, fabric, rope' level='1'/>
                          <spell name='Glass' level='1'/>
		                  <spell name='Ice' level='1'/>
		                  <spell name='Leather' level='1'/>
	                      <spell name='Wood' level='2'/>
	                      <spell name='Stone' level='3'/>
	                      <spell name='Iron' level='4'/>
	                      <spell name='Mithril' level='5'/>
	                    </MorphList>
                        <ZOE value='see' units='description' type=''/>
                        <Range value='60' units='feet'/>
                        <Duration value='momentary' units=''/>
                        <SavingThrow value='none'/>
	                      <Properties>
	                        <Property name='Applications' value='Bind/Weaken'>
                              <Modifier type='Application' value='Bind/Weaken' cost='0'></Modifier>
                              <Modifier type='Application' value='Seal' cost='.5'></Modifier>
                              <Modifier type='Application' value='Mend/Break' cost='1'></Modifier>
                              <Modifier type='Application' value='Disintegrate' cost='2'></Modifier>
                              <Modifier type='Application' value='Grand' cost='4'></Modifier>
	                        </Property>
	                      </Properties>
                      </spell>
                    ";

        /// <summary>
        /// Check reading Morphic spell application values
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tMorphicBases()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tCast._morphicSpellStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Locate", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            // Check base levels
            int loop_count = 0;
            foreach (SpellInfo morph in spell.MorphList)
            {
                loop_count++;
                switch (morph.Name)
                {
                    case "Attuned object":
                        Assert.AreEqual(1, morph.Level, String.Format("Incorrect level for '{0}': {1}", morph.Name, morph.Level));
                        break;

                    case "Person":
                        Assert.AreEqual(2, morph.Level, String.Format("Incorrect level for '{0}': {1}", morph.Name, morph.Level));
                        break;

                    case "Specific object":
                        Assert.AreEqual(2, morph.Level, String.Format("Incorrect level for '{0}': {1}", morph.Name, morph.Level));
                        break;

                    case "Any object":
                        Assert.AreEqual(1, morph.Level, String.Format("Incorrect level for '{0}': {1}", morph.Name, morph.Level));
                        break;

                    default:
                        Assert.Fail(String.Format("Invalid morph '{0}'", morph.Name));
                        break;
                }
            }
            Assert.AreEqual(4, loop_count, "Wrong number of base levels");

            return;
        }

        /// <summary>
        /// Check reading a property w/modifiers
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tSpellModifiers()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_tremorSpellStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Tremor", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));

            // Check properties
            Assert.AreEqual(2, spell.ExtraProperties.Count, "Wrong number of properties: " + spell.ExtraProperties.Count);
            SpellProperty prop = spell.ExtraProperties[0];  // 1st property
            Assert.AreEqual("Extra Duration", prop.Name, String.Format("Wrong name for property: '{0}'", prop.Name));
            IModifier mod = prop.Modifier;  // check (1st) modifier
            Assert.AreEqual("Extra Duration", mod.Name, String.Format("Wrong name for modifier: '{0}'", mod.Name));
            Assert.AreEqual("2 rounds", mod.IncrementValue.DisplayValue, String.Format("Wrong value for modifier: '{0}'", mod.IncrementValue.DisplayValue));
            Assert.AreEqual("Extra Duration (+2  rounds)", mod.Description, "Bad modifier description");

            return;
        }

        private const string _tremorSpellStr = @"
              <spell name='Tremor' level='6'>
                <description>This spell causes an earthquake. The main ZOE is a 360' radius circle. During each round of the duration, fixed rigid  structures in the ZOE take 40 points of structural damage, ignoring hardness. Flexible structures or those not fixed to  the ground take no damage. Multi-story buildings collapse when their walls are reduced to 1/2 hit points (2-3 stories)  or 3/4 hit points (4+ stories); collapsing buildings damage those inside appropriately (10d6 crushing damage is typical).  Underground structures collapse as per 4 story buildings; treat them as having 10,000 structural hits divided by their  unsupported free span in feet.  Non-flying creatures who attempt to move within the ZOE must make a Reflex save or fall while the tremor continues.  There is a 1 in 6 chance each round that any being in the ZOE will fall into a crack or be struck by debris; this requires  another Reflex save, those failing take 2d6 damage. Outside the main ZOE, the tremor can still be felt and may cause  alarm but is essentially harmless; it can be felt to a distance of 15 times the ZOE radius. </description>
                <modifiers_description>Extra Duration (+2  rounds), Extra Effect (+10 structural damage per round, +1d6 debris damage per impact) +1.  </modifiers_description>
                <ZOE value='360' units='foot' type='radius'/>
                <Range value='960' units='feet'/>
                <Duration value='3' units='rounds'/>
                <SavingThrow value='see' effect='description'/>
                <Properties>
                  <Property name='Extra Duration' value='3' units='rounds'>
                    <Modifier type='Extra Duration' value='2' units='rounds' cost='1'>
                      ..........<Description>Extra Duration (+2  rounds)</Description>
                    </Modifier>
                  </Property>
                  <Property name='Extra Structural Damage' value='40' units='points'>
                    <Modifier type='Extra Effect' value='10' cost='1'>
                      ..........<Description>Extra Effect (+10 structural damage per round, +1d6 debris damage per impact) +1.</Description>
                    </Modifier>
                  </Property>
                </Properties>
              </spell>";

        /// <summary>
        /// Check spell category
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tCategories()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_PatterningSpellStr);
            SpellInfo spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Patterning", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));
            Assert.AreEqual("Environment", spell.Category, String.Format("Incorrect category for SpellInfo: '{0}'", spell.Category));

            doc = new XmlDocument();
            doc.LoadXml(_tremorSpellStr);   // Check if nothing set
            spell = new SpellInfo(doc.FirstChild);

            Assert.AreEqual("Tremor", spell.Name, String.Format("Incorrect name for SpellInfo: '{0}'", spell.Name));
            Assert.AreEqual("Misc", spell.Category, String.Format("Incorrect category for SpellInfo: '{0}'", spell.Category));

            return;
        }

        /// <summary>
        /// Check we can read all the spells in spellbook.xml
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void ReadSpellBook()
        {
            List<ISpell> spell_list = new List<ISpell>();
            XmlDocument doc = new XmlDocument();
            doc.Load("spellbook.xml");
            XmlNode book_node = doc.FirstChild;
            Assert.IsNotNull(book_node, "No first child in spellbook.xml");
            Assert.IsTrue(book_node.ChildNodes.Count > 0, "No spells in spellbook.xml");
            foreach (XmlNode spell_node in book_node.ChildNodes)
            {
                SpellInfo spell = new SpellInfo(spell_node);
                Assert.IsTrue(!String.IsNullOrEmpty(spell.Name), "Empty spell name");
                Assert.IsTrue(spell.Level >= 0, "Spell level not >= 0");
                if (spell.Level == 0)
                    Assert.IsTrue(spell.IsMorphic, "Level 0 spell not Morphic");
                Assert.IsTrue(spell.Level < 10, "Spell level not < 10");
                Assert.IsTrue(!String.IsNullOrEmpty(spell.ZOEValue), "empty ZOE");
                Assert.IsTrue(!String.IsNullOrEmpty(spell.RangeValue.DisplayValue), "empty Range");
                Assert.IsTrue(!String.IsNullOrEmpty(spell.DurationValue.DisplayValue), "empty Duration");
                Assert.IsTrue(!String.IsNullOrEmpty(spell.SavingThrowValue), "empty Saving Throw");
                Assert.IsTrue(BaseSpell.CategoryList.Contains(spell.Category), String.Format("Incorrect category '{0}'", spell.Category));
            }

        }
    }
}
