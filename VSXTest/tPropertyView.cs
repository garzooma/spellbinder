﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArzooSoftware.SpellBinder.spellbinder;
using System.Xml;

namespace ArzooSoftware.SpellBinder.Test
{
    [TestClass]
    public class tPropertyView
    {
        /// <summary>
        /// Create SpellInfo object
        /// </summary>
        [TestMethod]
        [TestCategory("PropertyView")]
        public void tCastsDuration()
        {
            Mage mage = new Mage(7, 18);
            GroupedSpellList spell_list = new GroupedSpellList(SpellBook.Book);
            GroupedSpellList.ISpellGroup spell_group = new GroupedSpellList.DurationGroup();
            int cast_count = 0;
            List<Cast> cast_list = spell_list.GetCasts(spell_group, mage).ToList();
            foreach (Cast cast in cast_list)
            {
                switch (++cast_count)
                {
                    case 1:
                        Assert.AreEqual("Enhance Ability", cast.Spell.Name);
                        Assert.AreEqual(1, cast.Cost);
                        Assert.AreEqual("3 hours", cast.Duration);
                        Assert.AreEqual("touch", cast.Range);
                        break;

                    case 2:
                        Assert.AreEqual("Enhance Ability", cast.Spell.Name);
                        Assert.AreEqual(2, cast.Cost);
                        Assert.AreEqual("6 hours", cast.Duration);
                        Assert.AreEqual("touch", cast.Range);
                        break;

                }
            }
            List<Cast> ventriloquism_list = cast_list.Where(c => c.Spell.Name == "Ventriloquism").ToList();
            Assert.AreEqual(3, ventriloquism_list.Count);
            Cast v_cast = ventriloquism_list[0];
            Assert.AreEqual("Ventriloquism", v_cast.Spell.Name);
            Assert.AreEqual(1, v_cast.Cost);
            Assert.AreEqual("40 minutes", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);
            v_cast = ventriloquism_list[1];
            Assert.AreEqual("Ventriloquism", v_cast.Spell.Name);
            Assert.AreEqual(2, v_cast.Cost);
            Assert.AreEqual("3 hours", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);
            v_cast = ventriloquism_list[2];
            Assert.AreEqual("Ventriloquism", v_cast.Spell.Name);
            Assert.AreEqual(3, v_cast.Cost);
            Assert.AreEqual("6 hours", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);

            List<Cast> confuse_list = cast_list.Where(c => c.Spell.Name == "Confuse").ToList();
            Assert.AreEqual(7, confuse_list.Count);
            v_cast = confuse_list[0];
            Assert.AreEqual("Confuse", v_cast.Spell.Name);
            Assert.AreEqual(1, v_cast.Cost);
            Assert.AreEqual("12 rounds", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);
            v_cast = confuse_list[1];
            Assert.AreEqual("Confuse", v_cast.Spell.Name);
            Assert.AreEqual(2, v_cast.Cost);
            Assert.AreEqual("5 minutes", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);
            v_cast = confuse_list[2];
            Assert.AreEqual("Confuse", v_cast.Spell.Name);
            Assert.AreEqual(3, v_cast.Cost);
            Assert.AreEqual("10 minutes", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);
            v_cast = confuse_list[6];
            Assert.AreEqual("Confuse", v_cast.Spell.Name);
            Assert.AreEqual(8, v_cast.Cost);
            Assert.AreEqual("3 hours", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);

            List<Cast> disguise_list = cast_list.Where(c => c.Spell.Name == "Disguise").ToList();
            Assert.AreEqual(1, disguise_list.Count);
            v_cast = disguise_list[0];
            Assert.AreEqual("Disguise", v_cast.Spell.Name);
            Assert.AreEqual(4, v_cast.Cost);
            Assert.AreEqual("1 day", v_cast.Duration);
            Assert.AreEqual("10'", v_cast.Range);

        }

        /// <summary>
        /// Check on casting Lasting Duration spells
        /// </summary>
        [TestMethod]
        [TestCategory("PropertyView")]
        //[Ignore]
        public void tDurationLasting()
        {
            Mage mage = new Mage(9, 18);
            GroupedSpellList spell_list = new GroupedSpellList(SpellBook.Book);
            GroupedSpellList.ISpellGroup spell_group = new GroupedSpellList.DurationGroup();
            int cast_count = 0;
            List<Cast> cast_list = spell_list.GetCasts(spell_group, mage).ToList();
            foreach (Cast cast in cast_list)
            {
                switch (++cast_count)
                {
                    case 1:
                        Assert.AreEqual("Enhance Ability", cast.Spell.Name);
                        Assert.AreEqual(1, cast.Cost);
                        Assert.AreEqual("6 hours", cast.Duration);
                        Assert.AreEqual("touch", cast.Range);
                        break;

                    case 2:
                        Assert.AreEqual("Enhance Ability", cast.Spell.Name);
                        Assert.AreEqual(5, cast.Cost);
                        Assert.AreEqual("Lasting", cast.Duration);
                        Assert.AreEqual("touch", cast.Range);
                        break;

                }
            }
            List<Cast> ventriloquism_list = cast_list.Where(c => c.Spell.Name == "Ventriloquism").ToList();
            Assert.AreEqual(3, ventriloquism_list.Count);
            Cast v_cast = ventriloquism_list[0];
            Assert.AreEqual("Ventriloquism", v_cast.Spell.Name);
            Assert.AreEqual(1, v_cast.Cost);
            Assert.AreEqual("3 hours", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);

            List<Cast> confuse_list = cast_list.Where(c => c.Spell.Name == "Confuse").ToList();
            Assert.AreEqual(6, confuse_list.Count);
            v_cast = confuse_list[0];
            Assert.AreEqual("Confuse", v_cast.Spell.Name);
            Assert.AreEqual(1, v_cast.Cost);
            Assert.AreEqual("5 minutes", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);
            v_cast = confuse_list[1];
            Assert.AreEqual("Confuse", v_cast.Spell.Name);
            Assert.AreEqual(2, v_cast.Cost);
            Assert.AreEqual("20 minutes", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);
            v_cast = confuse_list[5];
            Assert.AreEqual("Confuse", v_cast.Spell.Name);
            Assert.AreEqual(6, v_cast.Cost);
            Assert.AreEqual("6 hours", v_cast.Duration);
            Assert.AreEqual("60'", v_cast.Range);

            List<Cast> disguise_list = cast_list.Where(c => c.Spell.Name == "Disguise").ToList();
            Assert.AreEqual(1, disguise_list.Count);
            v_cast = disguise_list[0];
            Assert.AreEqual("Disguise", v_cast.Spell.Name);
            Assert.AreEqual(2, v_cast.Cost);
            Assert.AreEqual("1 day", v_cast.Duration);
            Assert.AreEqual("10'", v_cast.Range);

            List<Cast> tv_list = cast_list.Where(c => c.Spell.Name == "Telescopic Vision").ToList();
            Assert.AreEqual(2, tv_list.Count);
            v_cast = tv_list[0];
            Assert.AreEqual("Telescopic Vision", v_cast.Spell.Name);
            Assert.AreEqual(1, v_cast.Cost);
            Assert.AreEqual("6 hours", v_cast.Duration);
            Assert.AreEqual("touch", v_cast.Range);
            v_cast = tv_list[1];
            Assert.AreEqual("Telescopic Vision", v_cast.Spell.Name);
            Assert.AreEqual(5, v_cast.Cost);
            Assert.AreEqual("Lasting", v_cast.Duration);
            Assert.AreEqual("touch", v_cast.Range);

        }

        [TestMethod]
        [TestCategory("PropertyView")]
        public void tDurationGroup()
        {
            Mage mage = new Mage(6, 18);
            GroupedSpellList.ISpellGroup spell_group = new GroupedSpellList.DurationGroup();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_TVspellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(2, spell_cast.Cost);
            Assert.AreEqual("3 hours", spell_cast.Duration);
            Assert.IsFalse(spell_group.AtMaxValue(spell_cast));

            spell_cast.AddModifier(base_spell.DurationProperty.Modifier);
            Assert.AreEqual(2, spell_cast.Cost);
            Assert.AreEqual("6 hours", spell_cast.Duration);
            Assert.IsTrue(spell_group.AtMaxValue(spell_cast));

            // Go with Mage who can cast lasting
            mage = new Mage(7, 18);
            spell_cast = new Cast(base_spell, mage);
            Assert.AreEqual(1, spell_cast.Cost);
            Assert.AreEqual("3 hours", spell_cast.Duration);
            Assert.IsFalse(spell_group.AtMaxValue(spell_cast));

            spell_cast.AddModifier(base_spell.DurationProperty.Modifier);
            Assert.AreEqual(2, spell_cast.Cost);
            Assert.AreEqual("6 hours", spell_cast.Duration);
            Assert.IsFalse(spell_group.AtMaxValue(spell_cast));

            spell_cast.AddModifier(base_spell.PropertiesList.FirstOrDefault(p => p.Name == "Lasting").Modifier);
            Assert.AreEqual(8, spell_cast.Cost);
            //Assert.AreEqual("Lasting", spell_cast.Duration);
            //Assert.IsTrue(spell_group.AtMaxValue(spell_cast));

            return;
        }

        private string _TVspellStr = @"<spell name='Telescopic Vision' level='1' category='Senses'>
    <description>While the spell lasts, recipient may switch at will between normal vision and up to x6 magnification. This is only useful to scrutinize a particular location at any given moment: trying to ""scan"" with the magnified view produces only a headache-inducing blur.</description>
    <ZOE value = '1' units='target' />
    <Range value = 'touch' units='' />
    <Duration value = '3' units='hours' />
    <SavingThrow value = 'Will' effect='negates' />
    <Properties>
      <Property name = 'Extra Effect' value='6' units = 'x'>
        <Modifier type = 'ExtraEffect' value='3' units = 'x' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>";

        [TestMethod]
        [TestCategory("PropertyView")]
        public void tLastingTest()
        {
            string book_str = String.Format("<book>{0}</book>", _TVspellStr);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(book_str);
            SpellBook book = new SpellBook(doc);
            GroupedSpellList spell_list_bldr = new GroupedSpellList(book);
            Mage mage = new Mage(9, 18);
            List<Cast> spell_list = spell_list_bldr.GetCasts(new GroupedSpellList.DurationGroup(), mage).ToList();
            int cast_count = 0;
            foreach (Cast cast in spell_list)
            {
                switch (++cast_count)
                {
                    case 1:
                        Assert.AreEqual("Telescopic Vision", cast.Spell.Name);
                        Assert.AreEqual(1, cast.Cost);
                        Assert.AreEqual("6 hours", cast.Duration);
                        Assert.AreEqual("touch", cast.Range);
                        break;

                    case 2:
                        Assert.AreEqual("Telescopic Vision", cast.Spell.Name);
                        Assert.AreEqual(5, cast.Cost);
                        Assert.AreEqual("Lasting", cast.Duration);
                        Assert.AreEqual("touch", cast.Range);
                        break;

                }
            }

        }

        private string _wizardLockSpellStr = @"  <spell name='Wizard Lock' level='2' category='Environment'>
    <description>Wizard Lock holds closed a door, chest, drawer, etc., which must be completely closed at the time of casting.  A strong anti-magical creature (e.g. a Balrog) may shatter it. A Knock spell will automatically open it unless it is also  physically barred. A mage three levels higher than the caster, or the caster himself, will not be affected by the spell.  Forcing the door open by brute strength requires a strength contest against a difficulty representing the strength of the  door's construction. This difficulty is usually 25 for dungeon and castle doors but the GM may assign higher or lower  values based on the condition of the door. Forcing the door destroys it. </description>
    <ZOE value='1' units='portal' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='4' units='days'/>
    <Properties>
      <Property name='Hard to Knock' value = 'no' units = ''>
        <Modifier type='Hard to Knock' value='yes' units = '' cost='2' />
      </Property>
      <Property name='Extra People' value = '0' units = 'people'>
        <Modifier type='Extra People' value='1' units = 'person' cost='0.5' />
      </Property>
    </Properties>
    <SavingThrow value='none'/>
  </spell>
";

        /// <summary>
        /// Test odd duration spells
        /// </summary>
        [TestMethod]
        [TestCategory("PropertyView")]
        public void tLasting2Test()
        {
            string book_str = String.Format("<book>{0}</book>", _wizardLockSpellStr);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(book_str);
            SpellBook book = new SpellBook(doc);
            GroupedSpellList spell_list_bldr = new GroupedSpellList(book);
            Mage mage = new Mage(9, 18);
            List<Cast> spell_list = spell_list_bldr.GetCasts(new GroupedSpellList.DurationGroup(), mage).ToList();
            Assert.AreEqual(2, spell_list.Count);
            int cast_count = 0;
            foreach (Cast cast in spell_list)
            {
                switch (++cast_count)
                {
                    case 1:
                        Assert.AreEqual("Wizard Lock", cast.Spell.Name);
                        Assert.AreEqual(1, cast.Cost);
                        Assert.AreEqual("4 days", cast.Duration);
                        Assert.AreEqual("10'", cast.Range);
                        break;

                    case 2:
                        Assert.AreEqual("Wizard Lock", cast.Spell.Name);
                        Assert.AreEqual(6, cast.Cost);
                        Assert.AreEqual("Lasting", cast.Duration);
                        Assert.AreEqual("10'", cast.Range);
                        break;

                }
            }
        }

    }
}
