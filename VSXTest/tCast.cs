﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArzooSoftware.SpellBinder.spellbinder;
using System.Xml;

namespace VSXTest
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.Test.tCast</name>
    /// <summary>
    /// Description for tCast
    /// </summary>
    /// <version>1.0 - Initial Release - 1/18/2013 9:28:52 PM</version>
    /// <author>Greg Arzoomanian</author>    
    [TestClass]
    public class tCast
    {
        /// <summary>
        /// Create Cast object
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tCtor()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(2, spell_cast.Cost, String.Format("Incorrect cost for Spell: '{0}'", base_spell.Name));
        }

        private string _spellStr = @"<spell name='Detect Magic' level='1'>
                                        <description>A confused creature will not be able to coordinate his actions with anyone else. (In the case of player characters, the players may not consult, and must submit orders in writing.) In addition there is a 1/3 chance each round that the creature will not be able to decide what to do that round, and thus will do absolutely nothing at all. Those creatures controlled by some outside source will not be affected, unless the controlling force also fails to save or fails to make other relevant control check. Only those of 4 HD or more will get saving throws. Those of 2 HD or less are affected immediately; others get a delay of d6 minus the level of the caster rounds.</description>
                                        <ZOE value='self' type='described'/>
                                        <Range value='as sight' />
                                        <Duration value='10' units='minutes' />
                                        <SavingThrow value='none' />
                                      </spell>";

        /// <summary>
        /// Test adding modifier
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tModifiers()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_spellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);

            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(2, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("10 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // Add modifier
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No duration modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(2, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("20 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            return;
        }

        /// <summary>
        /// Test handling damage
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tDamage()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_damageSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);
            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(3, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            //Assert.AreEqual(3, spell_cast.DamageNumber, String.Format("Incorrect Damage Number for Spell '{0}': '{1}'", base_spell.Name, spell_cast.DamageNumber));
            Assert.AreEqual(3, spell_cast.Damage.Value, String.Format("Incorrect Damage Value for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.Value));
            Assert.AreEqual("3d6", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));


            // Add modifier
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            Assert.IsTrue(dm_list.Count == 0, String.Format("Duration modifier for momentary Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DamageModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No Damage modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(2.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(4, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            //Assert.AreEqual(4, spell_cast.DamageNumber, String.Format("Incorrect Damage Number for Spell '{0}': '{1}'", base_spell.Name, spell_cast.DamageNumber));
            Assert.AreEqual(4, spell_cast.Damage.Value, String.Format("Incorrect Damage Value for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.Value));
            Assert.AreEqual("4d6", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));
            return;
        }

        private string _damageSpellStr = @"  <spell name='Pain' level='2'>
                                                <description>This spell inflicts wracking pains, causing 3d6 points of damage unless the victim saves vs. Will.</description>
                                                <ZOE value='1' units='target' />
                                                <Range value='120' units='feet' />
                                                <Duration value='momentary' />
                                                <SavingThrow value='Will' effect='negates' />
                                                <Damage die='6' number='3'/>
                                                <Modifiers>
                                                  <Modifier type='ExtraDamage' die='6' number = '1' cost='.5' />
                                                </Modifiers>
                                              </spell>";

        /// <summary>
        /// Test handling damage w/special plus modifier.
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tDamagePlus()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_magicMissileSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);
            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(1M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(2, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            //Assert.AreEqual(1, spell_cast.DamageNumber, String.Format("Incorrect Damage Number for Spell '{0}': '{1}'", base_spell.Name, spell_cast.DamageNumber));
            Assert.AreEqual(1, spell_cast.Damage.Value, String.Format("Incorrect Damage Value for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.Value));
            Assert.AreEqual("1d10+1", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            // Add modifier
            IModifier modifier = null;
            IList<SpellProperty> prop_list = new List<SpellProperty>(spell_cast.Spell.PropertiesList);
            Assert.IsTrue(prop_list.Count > 0, String.Format("No Damage modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            SpellProperty plus_prop = prop_list.First(p => p.Modifier.Name == "ExtraPlus");
            modifier = plus_prop.Modifier;
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(2, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            //Assert.AreEqual(1, spell_cast.DamageNumber, String.Format("Incorrect Damage Number for Spell '{0}': '{1}'", base_spell.Name, spell_cast.DamageNumber));
            Assert.AreEqual(1, spell_cast.Damage.Value, String.Format("Incorrect Damage Value for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.Value));
            Assert.AreEqual("1d10+2", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            // Restart and increate number of dice 1st
            spell_cast = new Cast(base_spell, mage);

            // Add modifier
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DamageModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No damage modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            spell_cast.AddModifier(modifier);
            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(3, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("2d10+1", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            // Add plus modifier
            spell_cast.AddModifier(plus_prop.Modifier);
            Assert.AreEqual(2.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(4, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("2d10+2", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            return;
        }

        internal const string _magicMissileSpellStr = @"  <spell name='Magic Missile' level='1'>
    <description>Magical missile(s) emanate from the caster’s fingers. Each missile hits and does damage exactly as if the caster had fired a +1 heavy crossbow bolt (Damage d10+1, Range Increment 100’, point-blank damage d10+2 within 30’). The Extra Range modifier increases the range increment, including point-blank range (which doubles from the base 30’ with each application). The base spell gives one missile, extra missiles are added as a modifier. Multiple missiles may be aimed at separate targets as long as all are within a 60 degree arc. Roll for each missile separately to see if it hits. A Shield (page 65) spell provides total defense.</description>
    <ZOE value='60' units='degrees' type='spread'/>
    <Range value='100' units='feet' />
    <Duration value='momentary' units='' />
    <SavingThrow value='none' effect='' />
    <Damage die='10' number='1' plus='1'/>
    <Modifiers>
      <Modifier type='ExtraDamage' die='10' number = '1' plus='1' cost='1' />
    </Modifiers>
    <Properties>
      <Property name='Extra Damage' value='1'>
        <Modifier type='ExtraPlus' die='10' number = '0' plus='1' cost='.5'></Modifier>
      </Property>
    </Properties>
  </spell>";

        /// <summary>
        /// Test handling damage
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tRange()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_damageSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);
            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(3, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("120'", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("3d6", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            // Add modifier
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            Assert.IsTrue(dm_list.Count == 0, String.Format("Duration modifier for momentary Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DamageModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No Damage modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is RangeModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No Range modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(2.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(4, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("240'", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("3d6", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            spell_cast.AddModifier(modifier);
            Assert.AreEqual("480'", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            return;
        }

        /// <summary>
        /// Test handling maximum value
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tLongRange()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_damageSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(11, 18);
            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(1, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("120'", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("3d6", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            // Add modifier
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            Assert.IsTrue(dm_list.Count == 0, String.Format("Duration modifier for momentary Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DamageModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No Damage modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));

            dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is RangeModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No Range modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(2.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(1, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("momentary", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("240'", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));
            Assert.AreEqual("3d6", spell_cast.Damage.DisplayValue, String.Format("Incorrect Damage Display for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Damage.DisplayValue));

            spell_cast.AddModifier(modifier);
            Assert.AreEqual("480'", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            spell_cast.AddModifier(modifier);
            Assert.AreEqual("0.25 mile", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            spell_cast.AddModifier(modifier);
            Assert.AreEqual("0.5 mile", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            spell_cast.AddModifier(modifier);
            Assert.AreEqual("1 mile", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            spell_cast.AddModifier(modifier);
            Assert.AreEqual("2 miles", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            return;
        }
        
        /// <summary>
        /// Test handling maximum value
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tMaxValue()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_maxSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);
            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(1M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(2, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));

            // Get extra property
            Assert.AreEqual(1, spell_cast.Spell.ExtraPropertiesList.Count, String.Format("Incorrect number of extra properties for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Spell.ExtraPropertiesList.Count));
            SpellProperty spell_prop = spell_cast.Spell.ExtraPropertiesList[0];
            NumericValue num_val = spell_cast.GetPropertyValue(spell_prop) as NumericValue;
            Assert.AreEqual(-3, num_val.Value, String.Format("Incorrect value for Spell '{0}': '{1}'", base_spell.Name, num_val.Value));

            // Add modifier
            IModifier modifier = spell_prop.Modifier;
            spell_cast.AddModifier(modifier);

            num_val = spell_cast.GetPropertyValue(spell_prop) as NumericValue;
            Assert.AreEqual(-4, num_val.Value, String.Format("Incorrect value for Spell '{0}': '{1}'", base_spell.Name, num_val.Value));

            return;
        }
        private string _maxSpellStr = @"
                          <spell name='Displace Image' level='1'>
                            <description>Displaces the subject’s image against all forms of vision including Darksight. The subject gains the combat benefits of Partial Concealment from this (attacks are -3 to hit). Stealth and other effects of partial concealment are not provided, since the subject’s image is still in plain sight, just in the wrong place. Modifiers: Extra Effect (additional -1 modifier) +1. The defensive benefit can’t be made bigger than -5; this spell cannot provide Total Concealment.
                            </description>
                            <ZOE value='1' units='target'/>
                            <Range value='touch' />
                            <Duration value='10' units='minutes' />
                            <SavingThrow value='Will negates' />
                            <Properties>
                              <Property name='Additional -1' value='-3' max='-5'>
                                <Modifier type='ExtraEffect' value='-1' cost='1'></Modifier>
                              </Property>
                            </Properties>
                          </spell>
                        ";

        /// <summary>
        /// Test adding duration modifiers
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tDurationModifiers()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_makeLastingSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(10, 18);   // see that we can get to lasting

            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(1, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("3 hours", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // Add modifier
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No duration modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(1, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("6 hours", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // try past max
            bool ok = true;
            try
            {
                ok = spell_cast.AddModifier(modifier);   // beyond Lasting
            }
            catch (Cast.BadStateException excp)
            {
                Assert.AreEqual(Cast.BadState.MaxValue, excp.State, "Wrong state");
                ok = false;
            }
            Assert.IsFalse(ok, "Tried to add modifier");

            //spell_cast.AddModifier(modifier);   // 1 day
            //Assert.AreEqual("1 day", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            //// get to max non-lasting (+1.5 levels)
            //spell_cast.AddModifier(modifier);   // 2 days
            //spell_cast.AddModifier(modifier);   // 4 days
            //spell_cast.AddModifier(modifier);   // 8 days
            //Assert.AreEqual("8 days", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // get to lasting
            IModifier lastingModifier = spell_cast.Spell.ModifierList.First(m => m.Name == "Lasting");
            ok = spell_cast.AddModifier(lastingModifier);   // Lasting
            Assert.IsTrue(ok, "Couldn't add modifier");
            Assert.AreEqual(1, spell_cast.GetNumModifiers(lastingModifier));
            Assert.AreEqual("Lasting", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            try
            {
                ok = spell_cast.AddModifier(modifier);   // beyond Lasting
            }
            catch (Cast.BadStateException excp)
            {
                Assert.AreEqual(Cast.BadState.MaxValue, excp.State, "Wrong state");
                ok = false;
            }
            Assert.IsFalse(ok, "Tried to add modifier");
            Assert.AreEqual("Lasting", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            return;
        }
        internal const string _makeLastingSpellStr = @"  <spell name='Telescopic Vision' level='1'>
                                            <description>While the spell lasts, recipient may switch at will between normal vision and up to x6 magnification. This is only useful to scrutinize a particular location at any given moment: trying to 'scan' with the magnified view produces only a headache-inducing blur.</description>
                                            <ZOE value='1' units='target' />
                                            <Range value='touch' units='' />
                                            <Duration value='3' units='hours' />
                                            <SavingThrow value='Will' effect='negates' />
                                          </spell>";

        /// <summary>
        /// Test calculating modifications to get to Lasting Duration
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tToLastingBumps()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_makeLastingSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(10, 18);   // see that we can get to lasting

            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(1, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("3 hours", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // Calculate modifications needed to get to Lasting
            IPropertyValue duration_value = base_spell.DurationProperty.Value;
            IPropertyValue lasting_value = new DescribedValue("Lasting");
            int num_mods = DurationModifier.GetNumMods(duration_value, lasting_value);
            Assert.AreEqual(7, num_mods, String.Format("Incorrect number of mods for Spell '{0}': '{1}'", base_spell.Name, num_mods));

            return;
        }

        /// <summary>
        /// Test calculating modifications without getting to Lasting Duration
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tToNotLastingBumps()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_messageSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(10, 18);   // see that we can get to lasting

            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(1, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(1, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("1 round", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // Add modifier
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No duration modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(1.5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(1, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("2 rounds", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // get to days (can make lasting)
            spell_cast.AddModifier(modifier);   // 4 round
            spell_cast.AddModifier(modifier);   // 8 round
            Assert.AreEqual("8 rounds", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // get to max non-lasting (+1.5 levels)
            spell_cast.AddModifier(modifier);   // 16 round
            bool ok = spell_cast.AddModifier(modifier);   // 32 round
            Assert.IsTrue(ok, "Couldn't add modifier");
            Assert.AreEqual("32 rounds", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            return;
        }
        internal const string _messageSpellStr = @"  <spell name='Message' level='1'>
    <description>The Mage sends a telepathic message of up to 25 words per round to any recipient in range. There is no saving throw unless the recipient is trying to avoid the message. If the recipient is trying to avoid the message, Will negates. The message cannot be overheard, and background noise and Silence have no effect on it, although they may prevent the spell itself. If the recipient is attuned by the mage their save is halved, and the base distance is 1/2 mile instead of 480’
    </description>
    <ZOE value='1' units='person'/>
    <Range value='480' units='feet'/>
    <Duration value='1' units='round' />
    <SavingThrow value='none or Will negates' />
  </spell>
";

        /// <summary>
        /// Test adding duration modifiers to lasting
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tLastingDurationModifiers()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_longLastingSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(10, 18);   // see that we can get to lasting

            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(2, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(1, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("4 days", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // Add modifier
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(spell_cast.Spell.ModifierList.Where(m => m is DurationModifier));
            Assert.IsTrue(dm_list.Count > 0, String.Format("No duration modifier for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            modifier = dm_list[0];
            try
            {
                spell_cast.AddModifier(modifier);
                Assert.Fail("Should throw exception");
            }
            catch (Cast.BadStateException excp)
            {
                Assert.AreEqual(Cast.BadState.Invalid, excp.State);
            }

            IModifier lastingModifier = spell_cast.Spell.ModifierList.First(m => m.Name == "Lasting");
            bool ok = spell_cast.AddModifier(lastingModifier);   // Lasting
            Assert.IsTrue(ok, "Couldn't add modifier");
            Assert.AreEqual(1, spell_cast.GetNumModifiers(lastingModifier));
            Assert.AreEqual("Lasting", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            return;
        }
        internal const string _longLastingSpellStr = @"    <spell name='Wizard Lock' level='2'>
    <description>Wizard Lock holds closed a door, chest, drawer, etc., which must be completely closed at the time of casting.  A strong anti-magical creature (e.g. a Balrog) may shatter it. A Knock spell will automatically open it unless it is also  physically barred. A mage three levels higher than the caster, or the caster himself, will not be affected by the spell.  Forcing the door open by brute strength requires a strength contest against a difficulty representing the strength of the  door's construction. This difficulty is usually 25 for dungeon and castle doors but the GM may assign higher or lower  values based on the condition of the door. Forcing the door destroys it. </description>
    <ZOE value='1' units='portal' type=''/>
    <Range value='10' units='feet'/>
    <Duration value='4' units='days'/>
    <Properties>
      <Property name='Hard to Knock' value = 'no' units = ''>
        <Modifier type='Hard to Knock' value='yes' units = '' cost='2' />
      </Property>
      <Property name='Extra People' value = '0' units = 'people'>
        <Modifier type='Extra People' value='1' units = 'person' cost='0.5' />
      </Property>
    </Properties>
    <SavingThrow value='none'/>
  </spell>";
        
        /// <summary>
        /// Test adding duration die value modifiers
        /// </summary>
        [TestMethod]
        [TestCategory("Cast")]
        public void tDurationDieModifiers()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_blindingFlashSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(10, 18);   // see that we can get to lasting

            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(4, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(3, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("2d6 rounds", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            // Add modifier
            IModifier modifier = spell_cast.Spell.DurationProperty.Modifier;
            spell_cast.AddModifier(modifier);

            Assert.AreEqual(5M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(5, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("3d6 rounds", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));

            return;
        }
        internal const string _blindingFlashSpellStr = @"  <spell name='Blinding Flash' level='4'>
            <description>The caster may create a flash of light. All within the ZOE must save or be temporarily blinded. Those who  are facing away from the center or who have their eyes closed, get +5 to save. </description>
            <ZOE value='20' units='foot' type='burst'/>
            <Range value='60' units='feet'/>
            <Duration die='6' number='2' units='rounds'>
                <Modifier type='Extra Duration' die='6' number='1' cost='1' />
            </Duration>
            <SavingThrow value='Reflex' effect='negates'/>
          </spell>
        ";

        /// <summary>
        /// Test modifier that's on or off
        /// </summary>
        [TestMethod]
        [TestCategory("SpellInfo")]
        public void tBoolModifier()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_boolModSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Mage mage = new Mage(6, 18);
            Cast spell_cast = new Cast(base_spell, mage);

            Assert.AreEqual(2M, spell_cast.Level, String.Format("Incorrect level for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Level));
            Assert.AreEqual(3, spell_cast.Cost, String.Format("Incorrect cost for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Cost));
            Assert.AreEqual("90 minutes", spell_cast.Duration, String.Format("Incorrect duration for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Duration));
            Assert.AreEqual("10'", spell_cast.Range, String.Format("Incorrect Range for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Range));

            // Get extra property
            Assert.AreEqual(1, spell_cast.Spell.ExtraPropertiesList.Count, String.Format("Incorrect number of extra properties for Spell '{0}': '{1}'", base_spell.Name, spell_cast.Spell.ExtraPropertiesList.Count));
            SpellProperty spell_prop = spell_cast.Spell.ExtraPropertiesList[0];
            DescribedValue descr_val = spell_cast.GetPropertyValue(spell_prop) as DescribedValue;
            Assert.AreEqual("no", descr_val.DisplayValue, String.Format("Incorrect value for Spell '{0}': '{1}'", base_spell.Name, descr_val.DisplayValue));

            // Add modifier
            IModifier modifier = spell_prop.Modifier;
            spell_cast.AddModifier(modifier);

            descr_val = spell_cast.GetPropertyValue(spell_prop) as DescribedValue;
            Assert.AreEqual("yes", descr_val.DisplayValue, String.Format("Incorrect value for Spell '{0}': '{1}'", base_spell.Name, descr_val.DisplayValue));

            return;
        }
        internal const string _boolModSpellStr = @"  <spell name='Invisibility' level='2'>
                                                <description>It makes something not visible, including to those using Darkvision. The spell will be broken the instant that the  recipient: completes casting a spell, actively uses a magical device, opens a door, becomes immersed in water, engages  in melee, attempts to grapple, or fires a missile. The recipient may always break the spell if he chooses. If a being is  made invisible, objects he is carrying at the time become invisible. A group of related objects (as a pile of coins) may  be treated as one object, but the object, being, or objects must fit in the ZOE. An illusion, or an object concealed by an  illusion, cannot be made invisible. </description>
                                                <ZOE value='One target that fits into a 10’ cube' units='' type='described'/>
                                                <Range value='10' units='feet'/>
                                                <Duration value='90' units='minutes'/>
                                                <SavingThrow value='willing only' effect=''/>
                                                <Properties>
                                                  <Property name='Improved Invisibility' value='no' units = '' max='yes'>
                                                    <Modifier type='Improved' value='yes' units = '' cost='1'></Modifier>
                                                  </Property>
                                                </Properties>
                                              </spell>
                                            ";

        [TestMethod]
        [TestCategory("Cast")]
        public void TestMorphic()
        {
            // ISpell spell = SpellBook.Book["Locate"];
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_morphicSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Cast cast = new Cast(base_spell, new Mage(5, 17));
            Assert.AreEqual(1, cast.Level);
            Assert.AreEqual("Locate", cast.Spell.Name);
            Assert.AreEqual("Attuned object", cast.Spell.MorphicBaseName);
            Assert.AreEqual(new NumericValue(10, "minutes"), cast.DurationPropertyValue);
            Assert.AreEqual(new NumericValue(1, "mile"), cast.RangePropertyValue);

            cast.SetMorphicBase("Person");
            Assert.AreEqual(2, cast.Level);
            Assert.AreEqual("Person", cast.Spell.MorphicBaseName);
            Assert.AreEqual(new NumericValue(10, "minutes"), cast.DurationPropertyValue);
            Assert.AreEqual(new NumericValue(480, "feet"), cast.RangePropertyValue);

            return;
        }

        [TestMethod]
        [TestCategory("Cast")]
        public void TestMorphicMod()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tSpellInfo._PatterningSpellStr);
            BaseSpell base_spell = new BaseSpell(doc.FirstChild);
            Cast cast = new Cast(base_spell, new Mage(5, 17));
            Assert.AreEqual(1, cast.Level);
            Assert.AreEqual("Patterning", cast.Spell.Name);
            Assert.AreEqual("Paper, fabric, rope", cast.Spell.MorphicBaseName);
            Assert.AreEqual(new DescribedValue("momentary"), cast.DurationPropertyValue);
            Assert.AreEqual(new NumericValue(60, "feet"), cast.RangePropertyValue);

            Assert.AreEqual(1, cast.Spell.ExtraPropertiesList.Count);
            SpellProperty app_prop = cast.Spell.ExtraPropertiesList.FirstOrDefault(p => p.Name == "Applications");
            Assert.IsNotNull(app_prop);
            Assert.AreEqual("Application", app_prop.Modifier.Name);
            Assert.IsTrue(app_prop.Modifier.IncrementValue is NamedValue);
            NamedValue prop_val = app_prop.Modifier.IncrementValue as NamedValue;
            // Assert.AreEqual(0, prop_val.CompareTo(new DescribedValue("Bind/Weaken")));
            Assert.AreEqual(new NamedValue("Bind/Weaken", prop_val.Schedule), prop_val);
            IModifier mod = app_prop.ModifierList.FirstOrDefault(m => m.IncrementValue.Equals(new NamedValue("Mend/Break", prop_val.Schedule)));
            Assert.IsNotNull(mod);
            cast.SetModifier(mod);   // set in list for this cast
            Assert.AreEqual(2, cast.Level);
            Assert.AreEqual("Paper, fabric, rope", cast.Spell.MorphicBaseName);
            Assert.AreEqual(new DescribedValue("momentary"), cast.DurationPropertyValue);
            Assert.AreEqual(new NumericValue(60, "feet"), cast.RangePropertyValue);

            // Change base
            bool ok = true;
            try
            {
                cast.SetMorphicBase("Stone");
            }
            catch (Cast.BadStateException excp)
            {
                ok = false;
                Assert.AreEqual(Cast.BadState.MaxLevel, excp.State, "Wrong error");
            }
            Assert.IsFalse(ok);
            //Assert.AreEqual("Mend/Break", app_prop.Modifier.Name);
            Assert.AreEqual(2, cast.Level);
            Assert.AreEqual("Paper, fabric, rope", cast.Spell.MorphicBaseName);

            // Roll back modifier
            mod = app_prop.ModifierList.FirstOrDefault(m => m.IncrementValue.Equals(new NamedValue("Bind/Weaken", prop_val.Schedule)));
            Assert.IsNotNull(mod);
            cast.SetModifier(mod);   // set in list for this cast
            Assert.AreEqual(1, cast.Level);
            Assert.AreEqual("Paper, fabric, rope", cast.Spell.MorphicBaseName);
            Assert.AreEqual(new DescribedValue("momentary"), cast.DurationPropertyValue);
            Assert.AreEqual(new NumericValue(60, "feet"), cast.RangePropertyValue);

            // Change base
            ok = true;
            try
            {
                cast.SetMorphicBase("Stone");
            }
            catch (Cast.BadStateException excp)
            {
                ok = false;
                Assert.AreEqual(Cast.BadState.MaxValue, excp.State, "Wrong error");
            }
            Assert.IsTrue(ok);
            Assert.AreEqual(3, cast.Level);
            Assert.AreEqual("Stone", cast.Spell.MorphicBaseName);
            Assert.AreEqual(new DescribedValue("momentary"), cast.DurationPropertyValue);
            Assert.AreEqual(new NumericValue(60, "feet"), cast.RangePropertyValue);

            return;
        }

        internal const string _morphicSpellStr = @"<spell name='Locate' level='morphic' category='Senses'>
    <description>This spell is used to find the direction and distance to a specified target, if it is within range. The base spell will give the direction to the target, or the nearest target if it is not unique. The distance to the detected target may be known for +1 spell level. The possible targets are:  
    
    Attuned object: Base level 1 and base range 1 mile. The object may be a person, place or literal object.  
    Person: Base level 2. A specific sentient creature is targeted. The creature must be named in a unique fashion.  
    Specific Object: Base level 2. A specific object is targeted. The object must be described enough to distinguish it from  all items that are not completely identical, or named in the case of named artifacts.  
    Any Object of a Specific Type: Base level 1. A specific type of object (again, this can include a category of person or  creature, or a place meeting certain specifications) is targeted. The type must be described in terms of its current physical  state, i.e. described so that an ordinary person who could see, touch, hear, and smell the object where it currently is could answer the question, 'Is this the object?' based solely on the description given, without resort to unusual senses, skill, or expertise. The description may not include past or future locations or conditions. The description may not include  properties such as ownership, purpose, good, or evil. The description may include references to other co-located objects,  i.e. 'a sword in a red sheath lying on an altar'. The type can be as broad or narrow as the caster wants. The caster could  locate a collection of books, i.e. a library. The caster could locate a book with a specific word in its title. The caster can  not select a type of object that they are unfamiliar with. For instance, if the caster heard of a left-handed smoke shifter  and tried to locate one, the locate would fail. The caster can not locate a type of object that requires information they do  not have. For instance, 'the objects that were taken from this room' could not be located unless the caster knew what they were. For the same reason, a caster could not locate something like 'a clue that we have overlooked'.  
    
    The range is 480' extendible along the following progression at the cost of +1/2 per step:  480', 1/4 mile, 1/2 mile, 1 mile, 2 miles, 4 miles, doubling.  Extra Effect +1: Range is reduced to 60' but all targets in range are located. Range may be doubled for +1/2 .  Affects Others will give the knowledge given by the spell to another. The detection range is then computed from the  recipient. Concealment will hide the fact that a person has a Locate spell running.  </description>
    <modifiers_description>The range is 480' extendible along the following progression at the cost of +1/2 per step: 480', 1/4 mile, 1/2 mile, 1 mile, 2 miles, 4 miles, doubling. Extra Effect +1: Range is reduced to 60' but all targets in range are located. Range may be doubled for +1/2. Affects Others will give the knowledge given by the spell to another. The detection range is then computed from the recipient. Concealment will hide the fact that a person has a Locate spell running.</modifiers_description>
		<MorphList>
	      <spell name='Attuned object' level='1'> 
			<Range value='1' units='mile'/>
          </spell>
          <spell name='Person' level='2'/>
		  <spell name='Specific object' level='2'/>
		  <spell name='Any object' level='1'/>
	    </MorphList>
        <ZOE value='self' units='' type='described'/>
        <Range value='480' units='feet'/>
        <Duration value='10' units='minutes'/>
        <SavingThrow value='none'/>
	    <Properties>
	      <Property name='Get Distance' value='no' max='yes'>
            <Modifier type='DetermineRange' value='yes' cost='1'></Modifier>
	      </Property>
	      <Property name='All Targets' value='no' max='yes'>
            <Modifier type='ExtraEffect' value='yes' cost='1'></Modifier>
	      </Property>
	    </Properties>
      </spell>
";
    }
}
