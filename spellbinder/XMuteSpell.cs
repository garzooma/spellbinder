﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.spellbinder.XMuteSpell</name>
    /// <summary>
    /// Apply morphic modifications to base spell
    /// </summary>
    /// <version>1.0 - Initial Release - 1/26/2013 9:28:14 AM</version>
    /// <author>Greg Arzoomanian</author>  
    /// <remarks>The idea is that you get the base of a morphic spell by combining
    /// properties from the base spell (as from the book) with the properties from
    /// one of the morphs of the spell.  The morph provides the Level and in some
    /// cases values for the Range property.</remarks>
    public class XMuteSpell : ISpell
    {
        private ISpell _baseSpell;  // base spell as defined in book
        private MorphMinder.MorphicBaseSpell _morph;    // selected morph for this spell

        private SpellProperty _modifiedRange = null;    // Range is currently the only property
                                                        // that's modified in a morph

        public XMuteSpell(ISpell spell, MorphMinder.MorphicBaseSpell morph)
        {
            _baseSpell = spell;
            _morph = morph;
        }

        #region ISpell Members

        public decimal Level
        {
            get 
            {
                decimal ret = _morph.Level;

                return ret; 
            }
        }

        public string Name
        {
            get { return _baseSpell.Name; }
        }

        public string Description
        {
            get { return _baseSpell.Description; }
        }

        public string ModifiersDescription
        {
            get { return _baseSpell.ModifiersDescription; }
        }

        public bool IsMorphic
        {
            get
            {
                return _baseSpell.IsMorphic;
            }
        }

        public SpellProperty ZOEProperty
        {
            get { return _baseSpell.ZOEProperty; }
        }

        /// <summary>
        /// Return modified range rather than base range
        /// </summary>
        public SpellProperty RangeProperty
        {
            get 
            {
                SpellProperty ret = _baseSpell.RangeProperty;
                if (_morph.ProvidesProperty(SpellProperty.StdPropertyNames.Range))
                    ret = _morph.RangeProperty;

                return ret; 
            }
        }

        public SpellProperty DurationProperty
        {
            get { return _baseSpell.DurationProperty; }
        }

        public string SavingThrow
        {
            get { return _baseSpell.SavingThrow; }
        }

        public SpellProperty SaveProperty
        {
            get { return _baseSpell.SaveProperty; }
        }

        public DiceValue Damage
        {
            get { return _baseSpell.Damage; }
        }

        public SpellProperty DamageProperty
        {
            get { return _baseSpell.DamageProperty; }
        }

        /// <summary>
        /// Add range modifier
        /// </summary>
        public IList<IModifier> ModifierList
        {
            get
            {
                List<IModifier> ret = new List<IModifier>(_baseSpell.ModifierList);
                if (_modifiedRange != null)
                    ret.Add(_modifiedRange.Modifier);
                return ret; 
            }
        }

        public IList<SpellProperty> PropertiesList
        {
            get { return _baseSpell.PropertiesList; }
        }

        public IList<SpellProperty> ExtraPropertiesList
        {
            get { return _baseSpell.ExtraPropertiesList; }
        }

        public string Category
        {
            get { return _baseSpell.Category; }
        }

        public string MorphicBaseName
        {
            get
            {
                string ret = _morph.Name;

                return ret;
            }
        }

        public MorphMinder MorphMinder
        {
            get
            {
                MorphMinder ret = null;

                ret = _baseSpell.MorphMinder;

                return ret;
            }
        }

        #endregion

    }
}
