﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.ZOEValue</name>
    /// <summary>
    /// Describes Zone of Effect geometry
    /// </summary>
    /// <version>1.0 - Initial Release - 1/14/2013 4:19:31 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class ZOEValue : NumericValue
    {
        /// <summary>
        /// Describes shape of ZOE
        /// </summary>
        private string _type;   // types of ZOE

        /// <summary>
        /// Shape of ZOE
        /// </summary>
        public override string TypeValue
        {
            get
            {
                return _type;
            }
        }

        public ZOEValue(decimal value, string units, string zoeType)
            : base(value, units)
        {
            _type = zoeType;
        }

        #region IPropertyValue Members

        public override string DisplayValue
        {
            get 
            {
                string ret = base.DisplayValue;
                ret += " " + _type;

                return ret;
            }
        }

        #endregion

        private static string[] _individualTargets = {
                                                  "target",
                                                  "biped",
                                                  "self",
                                                  "caster",
                                                  "being"
                                              };
        internal static bool IsIndividual(string zoeType)
        {
            bool ret = false;

            if (_individualTargets.Contains(zoeType))
                ret = true;

            return ret;
        }
    }
}
