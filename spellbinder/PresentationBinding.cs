﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.spellbinder.PresentationBinding</name>
    /// <summary>
    /// Binds controls to objects in Model
    /// </summary>
    /// <version>1.0 - Initial Release - 1/24/2013 9:55:37 AM</version>
    /// <author>Greg Arzoomanian</author>    
    public class PresentationBinding
    {
        private Cast _spellCast;
        private SpellProperty _spellProperty;
        public SpellProperty SpellProperty { get { return _spellProperty; } }

        private List<Control> _boundControls = new List<Control>();

        /// <summary>
        /// List of controls whose data is bound to the property value.
        /// </summary>
        public IList<Control> BoundControls
        {
            get
            {
                return _boundControls;
            }
        }

        public PresentationBinding(Cast spellCast, SpellProperty spellProperty)
        {
            _spellCast = spellCast;
            _spellProperty = spellProperty;
        }

        /// <summary>
        /// String to display value
        /// </summary>
        public string DisplayValue
        {
            get
            {
                IPropertyValue val = _spellCast.GetPropertyValue(_spellProperty);
                return val.DisplayValue;
            }
        }

        public void UpdateControls()
        {
            foreach (Control ctl in _boundControls)
                ctl.DataBindings[0].ReadValue();

            return;
        }
    }
}
