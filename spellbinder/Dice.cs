﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.Dice</name>
    /// <summary>
    /// Description for Dice
    /// </summary>
    /// <version>1.0 - Initial Release - 1/15/2013 12:10:04 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class Dice
    {
        int _sides;
        public int Sides { get { return _sides; } }
        public Dice(int sides)
        {
            _sides = sides;
        }

        public override bool Equals(object obj)
        {
            bool ret = false;
            if (obj is Dice)
            {
                Dice in_val = obj as Dice;
                if (in_val._sides == _sides)
                {
                    ret = true;
                }
            }
            return ret;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + _sides.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return String.Format("d{0}", _sides);
        }
    }
}
