using System;

namespace ArzooSoftware.SpellBinder.spellbinder
{
	/// <summary>
	/// A modifier for a spell that increases a spell property (and usually also the cost).
	/// </summary>
    /// <remarks>May be attached to a spell to show the possible modifications, or to a cast to actually change the properties</remarks>
	public interface IModifier
	{
        /// <summary>
        /// Name of modifier
        /// </summary>
		string Name {get;}

        /// <summary>
        /// Name of property to be modified
        /// </summary>
        /// <remarks>Used in Cast class to count modifiers for a property</remarks>
        string PropertyName { get; }

        /// <summary>
        /// Amount modifier increments current value
        /// </summary>
        /// <remarks>Only used for display; Bump handles actual increment</remarks>
        IPropertyValue IncrementValue { get; }

        /// <summary>
        /// Cost in spell levels
        /// </summary>
        /// <remarks>Default of .5</remarks>
		decimal Cost { get; }

        /// <summary>
        /// Increase value of property
        /// </summary>
        /// <param name="startValue"></param>
        /// <returns></returns>
        IPropertyValue Bump(IPropertyValue startValue);

        /// <summary>
        /// Description of modifier from spellbook entry for spell.
        /// </summary>
        string Description { get; }
    }
}
