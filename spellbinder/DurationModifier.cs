﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.DurationModifier</name>
    /// <summary>
    /// Description for DurationModifier
    /// </summary>
    /// <version>1.0 - Initial Release - 1/10/2013 10:52:32 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class DurationModifier : Modifier
    {
        //private string _name = "Duration";
        //private decimal _cost = 0.5M;   // default cost is 1/2 a level

        //public DurationModifier(XmlNode node) : base("Duration", 0.5M)
        //{
        //    decimal cost = 0.0M;
        //    XmlAttribute attr = node.Attributes["cost"];
        //    if (attr != null)
        //    {
        //        decimal.TryParse(attr.Value, out cost);
        //    }
        //    _cost = cost;
        //}

        /// <summary>
        /// Store off base duration if > 1 day for seeing about lasting duration.
        /// </summary>
        private NumericValue _lengthyBaseValue;

        /// <summary>
        /// Create default modifier
        /// </summary>
        /// <param name="doc"></param>
        public DurationModifier(IPropertyValue startValue)
            : base("Duration", 0.5M)
        {
            // Only set if >1 day
            if (startValue is NumericValue)
            {
                if (startValue.CompareTo(new NumericValue(6, "hours")) == 1)
                {
                    _lengthyBaseValue = startValue as NumericValue; // store off for checking lasting
                }
            }
        }

		#region IModifier Members

        public override IPropertyValue Bump(IPropertyValue startValue)
        {
            IPropertyValue ret = startValue;    // default -- don't bump

            if (startValue is NumericValue)
            {
                NumericValue num_val = startValue as NumericValue;

                if (num_val.Units.StartsWith("week")) // special handing of week
                {
                    ret = new DescribedValue("Lasting");
                }
                // Check for max value
                else if (_timeSchedule.Contains(num_val) && num_val != _timeSchedule.Last())
                {
                    bool match = false;
                    foreach (NumericValue val in _timeSchedule)
                    {
                        if (match)  // previous value matched
                        {
                            ret = val;  // return next value
                            break;
                        }
                        if (val.Equals(startValue))
                            match = true;
                    }
                }
                else if (!_timeSchedule.Contains(num_val))
                {
                    //// Check for bumping to Lasting
                    //NumericValue penultimate_value = new NumericValue(8M, "days");
                    //if (_lengthyBaseValue != null)  // do we need to check > 1 day?
                    //{
                    //    penultimate_value = _lengthyBaseValue;
                    //    for (int i = 0; i < 3; i++) // bump 3x
                    //    {
                    //        penultimate_value = new NumericValue(penultimate_value.Value * 2, penultimate_value.Units);
                    //    }
                    //}
                    //if (num_val.CompareTo(penultimate_value) == 0
                    //    || num_val.CompareTo(penultimate_value) > 0)    // Lasting duration > 8 days
                    //    ret = new DescribedValue("Lasting");    // bump brings you to lasting
                    //else
                    //{
                        string unit_str = num_val.Units;
                        if (!unit_str.EndsWith("s"))    // see about making plural
                            unit_str += "s";
                        ret = new NumericValue(num_val.Value * 2, unit_str);
                    //}
                }
            }

            return ret;
        }

        #endregion

        /// <summary>
        /// Check value if on schedule
        /// </summary>
        /// <param name="Duration"></param>
        /// <returns></returns>
        internal static bool IsOnSchedule(IPropertyValue duration)
        {
            bool ret = false;
            if (duration is NumericValue && _timeSchedule.Contains(duration))
                ret = true;

            return ret;
        }

        private static NumericValue[] _timeSchedule = {
                                                    new NumericValue(6, "rounds"),
                                                    new NumericValue(12, "rounds"),
                                                    new NumericValue(24, "rounds"), 
                                                    new NumericValue(5, "minutes"),
                                                    new NumericValue(10, "minutes"),
                                                    new NumericValue(20, "minutes"),
                                                    new NumericValue(40, "minutes"),
                                                    new NumericValue(90, "minutes"), 
                                                    new NumericValue(3, "hours"),
                                                    new NumericValue(6, "hours") //,
                                                    //new NumericValue(12, "hours"),
                                                    //new NumericValue(1, "day"),
                                                    //new NumericValue(2, "days")// get plural
                                                    };   


        /// <summary>
        /// Calculate the number of increments needed to get from current property value
        /// to target value.
        /// </summary>
        /// <param name="currentValue">Value to be incremented</param>
        /// <param name="targetValue">Value to be incrmented to</param>
        /// <returns></returns>
        internal static int GetNumMods(IPropertyValue currentValue, IPropertyValue targetValue)
        {
            int ret = -1;   // not applicable

            if (currentValue is NumericValue)
            {
                NumericValue target_numeric = targetValue as NumericValue;
                if (target_numeric == null)
                {
                    if (targetValue is DescribedValue && targetValue.Equals(new DescribedValue("Lasting")))
                    {
                        target_numeric = new NumericValue(6, "hours");     // bumping 6 hours w/+3 levels
                    }
                }
                if (target_numeric != null) // have valid target value
                {
                    int index = Array.IndexOf(_timeSchedule, currentValue);
                    int target_index = 0;
                    if (target_numeric.Units.StartsWith("day"))
                    {
                        int one_day_idx = Array.IndexOf(_timeSchedule, new NumericValue(1, "day"));
                        int num_bumps_over_day = (int)Math.Log((double)target_numeric.Value, (double)2);
                        target_index = one_day_idx + num_bumps_over_day;
                    }
                    else // in schedule
                    {
                        target_index = Array.IndexOf(_timeSchedule, target_numeric);
                    }

                    ret = target_index - index;
                    if (targetValue is DescribedValue && targetValue.Equals(new DescribedValue("Lasting")))
                    {
                        ret += 6;     // bumping 6x w/+3 levels
                    }
                }
            }

            return ret;
        }

        public static IPropertyValue LastingValue { get { return new DescribedValue("Lasting"); } }
    }
}
