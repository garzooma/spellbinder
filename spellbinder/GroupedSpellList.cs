﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    public class GroupedSpellList
    {
        private SpellBook _spellBook;
        public GroupedSpellList(SpellBook spellBook)
        {
            _spellBook = spellBook;
        }

        public IEnumerable<Cast> GetCasts(ISpellGroup group, Mage mage)
        {
            foreach (ISpell base_spell in _spellBook.List)  // loop on spells in book
            {
                if (CostCalc.GetMaxLevel(mage.Level) >= base_spell.Level)
                {
                    IModifier mod = group.GetPropertyModifier(base_spell);
                    if (mod != null)  // must be extendable
                    {
                        // List<Cast> add_list = new List<Cast>(); // temp list 
                        Cast cast = new Cast(base_spell, mage);
                        //yield return new Cast(cast);

                        // Add modified version of spell if less than max
                        Cast ret_cast = null;       // for storing off returned cast for check after loop
                        while (cast.Level < cast.MaxLevel && !group.AtMaxValue(cast))
                        {
                            Cast mod_cast = new Cast(cast);  // build another copy
                            mod = group.GetPropertyModifier(mod_cast);
                            try
                            {
                                mod_cast.AddModifier(mod);
                            }
                            catch (Cast.BadStateException excp)
                            {
                                break;
                            }

                            if (mod_cast.Cost > cast.Cost)
                            {
                                yield return cast;
                                ret_cast = cast;
                                cast = mod_cast;    // for next time
                            }
                            else // cost didn't increment
                            {
                                cast.AddModifier(mod);  // modify existing cast
                            }
                        }
                        if (ret_cast != null)
                        {
                            if (cast.Cost > ret_cast.Cost)
                            {
                                yield return cast;
                            }
                        }
                        else
                        {
                            // Nothing returned
                            yield return cast;      // just return original cast
                        }
                    }
                }
            }
        }

        public interface ISpellGroup
        {
            bool AtMaxValue(Cast cast);
            IModifier GetPropertyModifier(ISpell spell);
            IModifier GetPropertyModifier(Cast cast);
        }

        public class DurationGroup : ISpellGroup
        {
            /// <summary>
            /// Is spell cast at maximum duration value.
            /// </summary>
            /// <param name="cast"></param>
            /// <returns></returns>
            public bool AtMaxValue(Cast cast)
            {
                if (cast.Level == cast.MaxLevel) return true;
                IPropertyValue lasting_value = new DescribedValue("Lasting");
                if (cast.DurationPropertyValue.CompareTo(lasting_value) == 0) return true;

                IPropertyValue max_value = new NumericValue(6, "hours");
                if (cast.DurationPropertyValue.CompareTo(max_value) < 0)
                    return false;

                if (cast.Level + 3 <= cast.MaxLevel)
                {
                    return false;
                }
                return true;
            }

            public IModifier GetPropertyModifier(Cast cast)
            {
                IModifier ret = null;
                if (cast.DurationPropertyValue is NumericValue)
                {
                    if (cast.DurationPropertyValue.CompareTo(new NumericValue(6, "hours")) >= 0)
                    {
                        ret = cast.Spell.ModifierList.FirstOrDefault(p => p.Name == SpellProperty.StdPropertyNames.Lasting);
                    }
                    else
                    {
                        ret = new DurationModifier(cast.DurationPropertyValue);
                    }
                }
                return ret;
            }

            public IModifier GetPropertyModifier(ISpell spell)
            {
                IModifier ret = null;
                if (spell.DurationProperty.Value is NumericValue)
                {
                    if (spell.DurationProperty.Value.Equals(new NumericValue(6, "hours")))
                    {
                        ret = new DurationModifier(new DescribedValue("Lasting"));
                    }
                    else
                    {
                        ret = new DurationModifier(spell.DurationProperty.Value);
                    }
                }
                return ret;
            }
        }
    }
}
