﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.ZOEModifier</name>
    /// <summary>
    /// Description for ZOEModifier
    /// </summary>
    /// <version>1.0 - Initial Release - 1/14/2013 4:34:59 PM</version>
    /// <author>Greg Arzoomanian</author>    
    class ZOEModifier : Modifier
    {
        private decimal _modAmount;
        public ZOEModifier(decimal modAmount) : base("ZOE", 0.5M)
        {
            _modAmount = modAmount;
        }

        #region IModifier Members

        //private string _name = "ZOE";
        //private decimal _cost = 0.5M;

        //public string Name
        //{
        //    get { return _name; }
        //}

        //public string PropertyName
        //{
        //    get
        //    {
        //        string ret = _name;
        //        return ret;
        //    }
        //}

        //public decimal Cost
        //{
        //    get { return _cost; }
        //}

        public string Bump(string startValue)
        {
            throw new NotImplementedException();
        }

        public override IPropertyValue Bump(IPropertyValue startValue)
        {
            IPropertyValue ret = null;

            NumericValue cur_val = startValue as NumericValue;

            decimal cur_num = startValue.CalculationValue;

            decimal new_num = cur_num + _modAmount;

            ret = new ZOEValue(new_num, cur_val.Units, startValue.TypeValue);

            return ret;
        }

        public override IPropertyValue IncrementValue
        {
            get { return new NumericValue(_modAmount, null); }
        }

        #endregion
    }
}
