using System;

namespace spellbinder
{
	/// <summary>
	/// Interface for a spell.
	/// </summary>
	public interface ISpell
	{
		string Level{get;}
		string Name{get;}
		string Cost{get;}
		string Description{get;}
		string ZOE{get;}
		string Range{get;}
		string Duration{get;}
		string SavingThrow{get;}
		string DamageDie{get;}
		string DamageNumber{get;}
		string ExpectationValue{get;}
	}
}
