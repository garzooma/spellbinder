﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.Modifier</name>
    /// <summary>
    /// Base for modifier to implement common features of a modifier
    /// </summary>
    /// <version>1.0 - Initial Release - 1/9/2013 3:05:59 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public abstract class Modifier : IModifier
    {
        /// <summary>
        /// Name of modifier
        /// </summary>
        protected string _name = "Range";
        protected decimal _cost = 0.5M;
        protected string _description;

        public Modifier(string name, decimal cost, string description)
        {
            _name = name;
            _cost = cost;
            _description = description;
        }

        public Modifier(string name, decimal cost) : this(name, cost, String.Empty) { }

        #region IModifier Members

        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Property for this modifier.
        /// </summary>
        /// <remarks>Overridden in SpellModifier class</remarks>
        public virtual string PropertyName
        {
            get
            {
                string ret = _name;
                return ret;
            }
        }

        public virtual string Die
        {
            get { throw new NotImplementedException(); }
        }

        public virtual string Number
        {
            get { throw new NotImplementedException(); }
        }

        public virtual decimal Cost
        {
            get { return _cost; }
        }

        public abstract IPropertyValue Bump(IPropertyValue startValue);

        public virtual IPropertyValue IncrementValue
        {
            get { throw new NotImplementedException(); }
        }

        public string Description
        {
            get
            {
                return _description;
            }

        }

        #endregion
    }
}
