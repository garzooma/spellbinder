﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.BooleanValue</name>
    /// <summary>
    /// Description for BooleanValue
    /// </summary>
    /// <version>1.0 - Initial Release - 1/16/2013 8:14:47 PM</version>
    /// <author>Greg Arzoomanian</author>    
    class BooleanValue : IPropertyValue
    {
        private bool _value;
        internal bool Value { get { return _value; } }
        public BooleanValue(bool val)
        {
            _value = val;
        }

        #region IPropertyValue Members

        public string DisplayValue
        {
            get { return _value ? "yes" : "no"; }
        }

        public string TypeValue
        {
            get { throw new NotImplementedException(); }
        }

        public decimal CalculationValue
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Go from off to on
        /// </summary>
        /// <param name="incrAmount"></param>
        /// <returns></returns>
        public IPropertyValue Increment(IPropertyValue incrAmount)
        {
            return incrAmount;
        }

        #endregion

        #region IComparable Members

        public int CompareTo(object obj)
        {
            return 0;
        }

        #endregion
    }
}
