﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.SpellProperty</name>
    /// <summary>
    /// Generic property of a spell
    /// </summary>
    /// <version>1.0 - Initial Release - 1/15/2013 10:37:57 AM</version>
    /// <author>Greg Arzoomanian</author>    
    public class SpellProperty
    {
        private string _name;
        public string Name { get { return _name; } }
        private IPropertyValue _value;
        public IPropertyValue Value { get { return _value; } }
        private IPropertyValue _maxValue;
        public IPropertyValue MaxValue { get { return _maxValue; } }

        /// <summary>
        /// List of modifiers that apply to property
        /// </summary>
        /// 
        public IList<IModifier> ModifierList { get { return _modifierList; } }
        private List<IModifier> _modifierList = new List<IModifier>();
        // private IModifier _modifier;

        /// <summary>
        /// Single modifier that applies to property.
        /// </summary>
        /// <remarks>Normal case</remarks>
        public IModifier Modifier 
        { 
            get 
            { 
                IModifier ret = null;

                if (_modifierList.Count > 0)
                    ret = _modifierList[0];

                return ret;
            }
            set 
            { 
                _modifierList.Add(value); 
            }
        }

        public void AddModifier(IModifier modifier)
        {
            _modifierList.Add(modifier);
        }

        public SpellProperty(string name, IPropertyValue value, IModifier modifier, IPropertyValue maxValue)
        {
            _name = name;
            _value = value;
            //_modifier = modifier;
            if (modifier != null)
                _modifierList.Add(modifier);
            _maxValue = maxValue;
        }

        public SpellProperty(string name, IPropertyValue value, IModifier modifier)
            : this(name, value, modifier, null)
        {
        }

        public SpellProperty(string name, IPropertyValue value)
            : this(name, value, null)
        {
        }

        public override string ToString()
        {
            return _name;
        }

        /// <summary>
        /// Standard spell properties (besides range, duration, etc.)
        /// </summary>
        /// 
        [Obsolete]
        public static IList<SpellProperty> StandardProperties
        {
            get
            {
                List<SpellProperty> ret = new List<SpellProperty>();

                // Add standard properties
                SpellProperty property = new SpellProperty("AffectsOthers", new DescribedValue("no"));
                SpellModifier modifier = new SpellModifier(property.Name, 1M, new DescribedValue("yes"), property);
                property.Modifier = modifier;
                ret.Add(property);

                property = new SpellProperty("AtRange", new DescribedValue("no"));
                modifier = new SpellModifier(property.Name, 1M, new DescribedValue("yes"), property);
                property.Modifier = modifier;
                ret.Add(property);

                return ret;
            }
        }

        /// <summary>
        /// Names of standard properties
        /// </summary>
        public class StdPropertyNames
        {
            public const string AffectsOthers = "AffectsOthers";
            public const string AtRange = "AtRange";
            public const string CastOnTheRun = "CastOnTheRun";
            public const string Concealment = "Concealment";
            public const string PowerWord = "PowerWord";
            public const string Lasting = "Lasting";
            public const string ReducedGestures = "ReducedGestures";
            public const string ReducedIncantation = "ReducedIncantation";
            public const string ZOE = "ZOE";
            public const string Duration = "Duration";
            public const string Range = "Range";
            public const string SavingThrow = "SavingThrow";
            public const string Damage = "Damage";
        }
    }
}
