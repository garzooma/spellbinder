using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
	/// <summary>
	/// Spell as it's written up in the spellbook.
	/// </summary>
	public class BaseSpell : ISpell
	{
		protected SpellInfo _spell;
		public BaseSpell(XmlNode node)
		{
			_spell = new SpellInfo(node);
		}

        public BaseSpell(SpellInfo spell_info)
        {
            this._spell = spell_info;
        }

		#region ISpell Members

		public decimal Level
		{
			get
			{
                decimal ret = 0.0M;
				// Get Level node
				ret = _spell.Level;
				return ret;
			}
		}

		public string Name
		{
			get
			{
				string ret = "";
				// Get Level node
				ret = _spell.Name;
				return ret;
			}
		}

		public string Description
		{
			get
			{
				string ret = "";

				ret = this._spell.Description;

				return ret;
			}
		}

        /// <summary>
        /// Modifiers section of spell description
        /// </summary>
        public string ModifiersDescription
        {
            get
            {
                string ret = "";

                ret = this._spell.ModifiersDescription;

                return ret;
            }
        }

        public bool IsMorphic
        {
            get
            {
                return _spell.IsMorphic;
            }
        }

        public SpellProperty ZOEProperty
        {
            get
            {
                IPropertyValue value = new DescribedValue(_spell.ZOEValue);
                IModifier modifier = null;      // standard modifier
                if (_spell.ZOEType != "described")
                {
                    decimal val;
                    decimal.TryParse(_spell.ZOEValue, out val);
                    value = new ZOEValue(val, _spell.ZOEUnits, _spell.ZOEType);

                    // Add ZOE modifier
                    if (!ZOEValue.IsIndividual(_spell.ZOEType)
                        && _spell.ZOEValue != "1"
                        )
                    {
                        // See if there are explicitly defined modifiers
                        if (_spell.Modifiers.Any(m => m is ZOEModifier))
                        {
                            modifier = _spell.Modifiers.First(m => m is ZOEModifier);
                        }
                        else // use default modifier
                        {
                            decimal calc_val;
                            decimal.TryParse(_spell.ZOEValue, out calc_val);
                            modifier = new ZOEModifier(calc_val / 2);
                        }
                    }
                }

                SpellProperty prop = new SpellProperty("ZOE", value, modifier);

                return prop;
            }
        }

        public virtual SpellProperty RangeProperty
        {
            get
            {
                //IPropertyValue value = new DescribedValue(_spell.RangeValue);
                IPropertyValue value = _spell.RangeValue;
                IModifier modifier = null;      // standard modifier

                // Add Duration modifier
                if (!(value is DescribedValue)) // don't add modifier if described
                {
                    // See if there are explicitly defined modifiers
                    if (_spell.Modifiers.Any(m => m is RangeModifier))
                    {
                        modifier = _spell.Modifiers.First(m => m is RangeModifier);
                    }
                    else // use default modifier
                    {
                        //decimal calc_val;
                        //decimal.TryParse(_spell.DurationValue, out calc_val);
                        modifier = new RangeModifier(RangeModifier.GetSchedule(value));
                    }
                }

                SpellProperty prop = new SpellProperty("Range", value, modifier);

                return prop;
            }
        }

        public SpellProperty DurationProperty
        {
            get
            {
                IPropertyValue value = _spell.DurationValue;

                IPropertyValue max_value = new NumericValue(6, "hours");
                SpellProperty prop = new SpellProperty("Duration", value, null, max_value);

                // Add Duration modifier
                if (!(value is DescribedValue))
                {
                    IModifier modifier = _spell.GetDurationModifier(prop);      // check for modifier in duration
                    if (modifier == null) // use default modifier
                    {
                        modifier = new DurationModifier(_spell.DurationValue);
                    }
                    prop.AddModifier(modifier);
                }

                return prop;
            }
        }

		public string SavingThrow
		{
			get
			{
				string ret = "";
				ret = this._spell.SavingThrowValue;
                if (!String.IsNullOrEmpty(_spell.SavingThrowEffect))
				    ret += (" " + this._spell.SavingThrowEffect);
				return ret;
			}
		}

        public SpellProperty SaveProperty
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(_spell.SavingThrowValue);
                if (!String.IsNullOrEmpty(_spell.SavingThrowEffect))    // look for "negates" usually
                {
                    sb.Append(" ");
                    sb.Append(_spell.SavingThrowEffect);
                }
                IPropertyValue value = new DescribedValue(sb.ToString());
                IModifier modifier = null;      // standard modifier
                if (_spell.Modifiers.Any(m => m is SaveModifier)) // Don't think this actually happens (just DamageModifiers)
                {
                    modifier = _spell.Modifiers.First(m => m is SaveModifier);
                }
                else // use default modifier
                {
                    modifier = new SaveModifier();
                }

                SpellProperty prop = new SpellProperty("SavingThrow", value, modifier);

                return prop;
            }
        }

        /// <summary>
        /// Base damage in dice (number, kind and plusses)
        /// </summary>
        public DiceValue Damage
        {
            get
            {
                return _spell.Damage;
            }
        }

        /// <summary>
        /// Encapsulate Damage info as a SpellProperty
        /// </summary>
        public SpellProperty DamageProperty
        {
            get
            {
                IPropertyValue value = _spell.Damage;
                IModifier modifier = null;      // standard modifier
                if (_spell.Modifiers.Any(m => m is DamageModifier))
                {
                    modifier = _spell.Modifiers.First(m => m is DamageModifier);
                }

                SpellProperty prop = new SpellProperty("Damage", value, modifier);

                return prop;
            }
        }

        /// <summary>
        /// List of modifiers for this spell
        /// </summary>
        /// <remarks>Lists the different kinds of modifiers that can be applied</remarks>
        public virtual IList<IModifier> ModifierList
        {
            get 
            {
                List<IModifier> ret = new List<IModifier>(_spell.Modifiers);    // start w/modifiers in definition

                // Add defaults
                IModifier mod;
                if (DurationProperty.Value is NumericValue /* DurationModifier.IsOnSchedule(Duration)*/
                    && !PropertiesList.Any(p => p.Name == "Duration"))
                {
                    mod = new DurationModifier(_spell.DurationValue);
                    ret.Add(mod);
                }

                // Add default Range, depending on the schedule
                NumericValue[] schedule = RangeModifier.GetSchedule(RangeProperty.Value);
                if (schedule != null)
                {
                    mod = new RangeModifier(schedule);
                    ret.Add(mod);
                }

                // Add default Saving Throw modifier
                mod = new SaveModifier();
                ret.Add(mod);

                // Add default ZOE modifier
                if (!ZOEValue.IsIndividual(_spell.ZOEType)
                    && _spell.ZOEValue != "1")
                {
                    decimal calc_val;
                    decimal.TryParse(_spell.ZOEValue, out calc_val);
                    mod = new ZOEModifier(calc_val/2);
                    ret.Add(mod);
                }

                // Add modifiers from property list
                foreach (SpellProperty property in PropertiesList)
                {
                    ret.Add(property.Modifier);
                }

                return ret; 
            }
        }

        /// <summary>
        /// Additional standard properties
        /// </summary>
        /// <remarks>Properties besides the standard ones</remarks>
        private List<SpellProperty> _propertiesList = new List<SpellProperty>();
        public IList<SpellProperty> PropertiesList
        {
            get
            {
                if (_propertiesList.Count == 0)
                {
                    // Start with standard properties
                    SpellProperty spell_prop = new SpellProperty("AffectsOthers", new DescribedValue("no"));
                    // Make sure modifier not explicitly listed in spell
                    if (TakesAffectsOthers(ZOEProperty.Value)
                        && !ExtraPropertiesList.Any(p => p.Name == spell_prop.Name))
                    {
                        decimal cost = 1M;
                        DescribedValue val = new DescribedValue("yes");
                        IModifier mod = new SpellModifier(spell_prop.Name, cost, val, spell_prop);
                        spell_prop.Modifier = mod;

                        _propertiesList.Add(spell_prop);
                    }

                    spell_prop = new SpellProperty("AtRange", new DescribedValue("no"));
                    // only add for spells w/"close" range (not always 0, not 60')
                    // Make sure modifier not explicitly listed in spell
                    if (IsCloseRange(RangeProperty.Value)
                        && !ExtraPropertiesList.Any(p => p.Name == spell_prop.Name))
                    {
                        decimal cost = 0.5M;
                        DescribedValue val = new DescribedValue("yes");
                        IModifier mod = new SpellModifier(spell_prop.Name, cost, val, spell_prop);
                        spell_prop.Modifier = mod;

                        _propertiesList.Add(spell_prop);
                    }

                    spell_prop = new SpellProperty("CastOnTheRun", new DescribedValue("no"));
                    // Make sure modifier not explicitly listed in spell
                    if (!ExtraPropertiesList.Any(p => p.Name == spell_prop.Name))
                    {
                        decimal cost = 1M;
                        DescribedValue val = new DescribedValue("yes");
                        IModifier mod = new SpellModifier(spell_prop.Name, cost, val, spell_prop);
                        spell_prop.Modifier = mod;

                        _propertiesList.Add(spell_prop);
                    }

                    spell_prop = new SpellProperty("Concealment", new DescribedValue("no"));
                    // Make sure modifier not explicitly listed in spell
                    if (!ExtraPropertiesList.Any(p => p.Name == spell_prop.Name))
                    {
                        decimal cost = 1M;
                        DescribedValue val = new DescribedValue("yes");
                        IModifier mod = new SpellModifier(spell_prop.Name, cost, val, spell_prop);
                        spell_prop.Modifier = mod;

                        _propertiesList.Add(spell_prop);
                    }

                    spell_prop = new SpellProperty("PowerWord", new DescribedValue("no"));
                    // Make sure modifier not explicitly listed in spell
                    if (!ExtraPropertiesList.Any(p => p.Name == spell_prop.Name))
                    {
                        decimal cost = 3M;
                        DescribedValue val = new DescribedValue("yes");
                        IModifier mod = new SpellModifier(spell_prop.Name, cost, val, spell_prop);
                        spell_prop.Modifier = mod;

                        _propertiesList.Add(spell_prop);
                    }

                    spell_prop = new SpellProperty("Lasting", new DescribedValue("no"));
                    // Make sure modifier not explicitly listed in spell
                    if (!ExtraPropertiesList.Any(p => p.Name == spell_prop.Name))
                    {
                        decimal cost = 3M;
                        DescribedValue val = new DescribedValue("yes");
                        IModifier mod = new SpellModifier(spell_prop.Name, cost, val, spell_prop);
                        spell_prop.Modifier = mod;

                        _propertiesList.Add(spell_prop);
                    }

                    spell_prop = new SpellProperty("ReducedGestures", new NumericValue(0), null, new NumericValue(2));
                    // Make sure modifier not explicitly listed in spell
                    if (!ExtraPropertiesList.Any(p => p.Name == spell_prop.Name))
                    {
                        decimal cost = 1M;
                        NumericValue val = new NumericValue(1);
                        IModifier mod = new SpellModifier(spell_prop.Name, cost, val, spell_prop);
                        spell_prop.Modifier = mod;

                        _propertiesList.Add(spell_prop);
                    }

                    spell_prop = new SpellProperty("ReducedIncantation", new NumericValue(0), null, new NumericValue(2));
                    // Make sure modifier not explicitly listed in spell
                    if (!ExtraPropertiesList.Any(p => p.Name == spell_prop.Name))
                    {
                        decimal cost = 1M;
                        NumericValue val = new NumericValue(1);
                        IModifier mod = new SpellModifier(spell_prop.Name, cost, val, spell_prop);
                        spell_prop.Modifier = mod;

                        _propertiesList.Add(spell_prop);
                    }

                    // Add extra properties explicitly listed in spell definition for a particular spell
                    _propertiesList.AddRange(ExtraPropertiesList);
                }
                return _propertiesList;
            }
        }
 
        /// <summary>
        /// Extra properties attached to spell
        /// </summary>
        /// <remarks>Properties mentioned in spell description for a particular spell</remarks>
        public IList<SpellProperty> ExtraPropertiesList
        {
            get 
            {
                return _spell.ExtraProperties; 
            }
        }

        /// <summary>
        /// Base levels for Morphic spells
        /// </summary>
        public SpellProperty BaseLevel
        {
            get { return _spell.BaseLevel; }
        }

        public string Category
        {
            get { return _spell.Category; }
        }

        public IList<string> MorphicBaseNamesList
        {
            get
            {
                List<string> ret = new List<string>();
                foreach (SpellInfo morph_info in _spell.MorphList)
                {
                    ret.Add(morph_info.Name);
                }

                return ret;
            }
        }

        public string MorphicBaseName
        {
            get { return null; }
        }

        /// <summary>
        /// Class to handle the transforming of a Morphic spell by modifying its base.
        /// </summary>
        public MorphMinder MorphMinder
        {
            get
            {
                MorphMinder ret = null;

                ret = new MorphMinder(_spell);

                return ret;
            }
        }
        #endregion

        /// <summary>
        /// Checks for range == self
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        private bool TakesAffectsOthers(IPropertyValue zoe)
        {
            bool ret = false;

            if (zoe.Equals(new DescribedValue("self")))
                ret = true;
            else if (zoe.Equals(new DescribedValue("caster")))
                ret = true;
            else if (zoe.Equals(new NumericValue(1, "target")))
                ret = true;

            return ret;
        }

        /// <summary>
        /// Check if range is not "always zero" but also not 60'
        /// </summary>
        /// <param name="Range"></param>
        /// <returns></returns>
        /// <remarks>Per spell book: touch, none or 10'</remarks>
        private bool IsCloseRange(IPropertyValue Range)
        {
            bool ret = false;
            IPropertyValue touch_range = new DescribedValue("touch");
            IPropertyValue none_range = new DescribedValue("none");
            IPropertyValue tenfoot_range = new NumericValue(10, "feet");
            if (Range.Equals(touch_range)
                || Range.Equals(none_range)
                || Range.Equals(tenfoot_range))
                ret = true;

            return ret;
        }

        static public IList<String> CategoryList
        {
            get { return _categoryList; }
        }

        static private List<String> _categoryList = new List<string>() { 
            "Appearance",
            "Combat",
            "Communication",
            "Conjuring",
            "Counterspells",
            "Enhancement",
            "Environment",
            "Misc",
            "Movement",
            "Senses"
        };

    }
}
