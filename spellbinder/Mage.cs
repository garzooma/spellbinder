﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.Mage</name>
    /// <summary>
    /// Information on casting mage
    /// </summary>
    /// <version>1.0 - Initial Release - 1/14/2013 12:14:02 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class Mage
    {
        private int _level;
        public int Level { get { return _level; } }

        private int _intelligence;
        public int Intelligence { get { return _intelligence; } }

        public Mage(int level, int intelligence)
        {
            _level = level;
            _intelligence = intelligence;
        }

        /// <summary>
        /// Bonus due to intelligence
        /// </summary>
        public decimal AbilityBonus
        {
            get
            {
                decimal ret = 0;

                // 1/2 of the difference between intellgence & 10
                ret = ((_intelligence - 10)/2);

                return ret;
            }
        }
    }
}
