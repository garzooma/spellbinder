﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <summary>
    /// For extracting information from Spell Descriptions
    /// </summary>
    public class SpellDescription
    {
        private string _descriptionText;    // text from Spell Book

        public SpellDescription(string description)
        {
            _descriptionText = description;
        }

        public IList<SpellDescription.Reference> FindReferences()
        {
            List<SpellDescription.Reference> ret = new List<Reference>();

            // Match page_match = Regex.Match(_descriptionText, @"\((page \d+)\)");
            Regex rgx = new Regex(@"\((page \d+)\)");
            foreach (Match page_match in rgx.Matches(_descriptionText))
            {
                int match_point = page_match.Groups[0].Index;
                string pre_text = _descriptionText.Substring(0, match_point);

                // Check for special name, first
                SpellDescription.Reference spell_ref = GetSpecialSpellName(pre_text);
                if (spell_ref == null)
                {
                    // Check for spell name
                    int start = GetSpellName(pre_text);
                    if (start > 0)
                    {
                        spell_ref = new Reference(pre_text, start, pre_text.Length);
                    }
                }
                if (spell_ref != null)
                    ret.Add(spell_ref);
            }

            return ret;
        }

        Dictionary<string, string> _specialSpellNames = new Dictionary<string,string>(){
            {"Attuned ", "Attune"},
            {"Prot / Normal Missiles ", "Prot / Normal Missiles"},
            {"Wall of Electricity ", "Wall of Electricity"}
        };
        private Reference GetSpecialSpellName(string pre_text)
        {
            Reference ret = null;

            foreach (KeyValuePair<string, string> entry in _specialSpellNames)
            {
                string special_name = entry.Key;
                if (pre_text.EndsWith(special_name))
                {
                    int text_len = pre_text.Length;
                    int start = text_len - special_name.Length;
                    ret = new Reference(start, text_len, entry.Value);
                }
            }

            return ret;
        }

        /// <summary>
        /// See if end of text is a spell name
        /// </summary>
        /// <param name="pre_text"></param>
        /// <returns>Index within description of start of spell name, if spellname; -1 otherwise.</returns>
        private int GetSpellName(string pre_text)
        {
            int ret = -1;

            // Walk backwards looking for Spell Name
            int char_pos = pre_text.Length;

            while(Char.IsWhiteSpace(pre_text[--char_pos]))  // skip any whitespace
                ;
            int word_end = char_pos;

            while (Char.IsLower(pre_text[--char_pos]) || pre_text[char_pos] == '/')  // skip letters before start
                ;

            if (char_pos == word_end   // must have some lower case letters
                || !Char.IsUpper(pre_text[char_pos])) // must start w/cap
                return ret; // fail
            int word_start = char_pos;

            if (!char.IsWhiteSpace(pre_text[--char_pos]))
                return ret;

            ret = word_start;

            // See if there are two words
            while (Char.IsWhiteSpace(pre_text[--char_pos]))  // skip any whitespace
                ;
            word_end = char_pos;

            while (Char.IsLower(pre_text[--char_pos]) || pre_text[char_pos] == '/')  // skip letters before start
                ;

            if (char_pos == word_end   // must have some lower case letters
                || !Char.IsUpper(pre_text[char_pos])) // must start w/cap
                return ret; // fail
            word_start = char_pos;

            if (!char.IsWhiteSpace(pre_text[--char_pos]))
                return ret;

            ret = word_start;

            return ret;
        }

        /// <summary>
        /// See if end of text is a spell name
        /// </summary>
        /// <param name="pre_text"></param>
        /// <returns>Index within description of start of spell name, if spellname; -1 otherwise.</returns>
        enum RecogStates { Init, Space, Word, Success, Fail };

        [Obsolete]
        private int GetSpellNameStateMachine(string pre_text)
        {
            int ret = -1;
            int char_pos = pre_text.Length;

            RecogStates state = RecogStates.Init;
            while (state != RecogStates.Fail && state != RecogStates.Success && char_pos > 0)
            {
                char_pos--;
                char rec_char = pre_text[char_pos];
                switch (state)
                {
                    case RecogStates.Init:
                        if (Char.IsWhiteSpace(rec_char))
                            state = RecogStates.Space;
                        else if (char.IsUpper(rec_char))
                            state = RecogStates.Fail;
                        else
                            state = RecogStates.Word;
                        break;

                    case RecogStates.Space:
                        if (Char.IsWhiteSpace(rec_char))
                            state = RecogStates.Space;
                        else if (char.IsUpper(rec_char))
                            state = RecogStates.Fail;
                        else
                            state = RecogStates.Word;
                        break;

                    case RecogStates.Word:
                        if (Char.IsWhiteSpace(rec_char))
                            state = RecogStates.Space;
                        else if (char.IsUpper(rec_char))
                            state = RecogStates.Fail;
                        else
                            state = RecogStates.Word;
                        break;


                }
            }

            return ret;
        }
        /// <summary>
        /// Reference in text to another spell
        /// </summary>
        public class Reference
        {
            public string SpellName { get { return _spellName; } }
            private string _spellName;

            private string _descriptionText;

            public int Start { get { return _start; } }
            private int _start;     // start index of reference section in description
            public int End { get { return _end; } }
            private int _end;       // end index of above

            internal Reference(string description, int start, int end)
            {
                _descriptionText = description;
                _start = start;
                while (char.IsWhiteSpace(description[end-1])) // strip trailing spaces
                    end--;
                _end = end;

                string descr_string = _descriptionText.Substring(_start, _end - _start);
                string[] text_array = descr_string.Split(" ".ToCharArray()).Where(w => !String.IsNullOrEmpty(w.Trim())).ToArray();
                _spellName = String.Join(" ", text_array);
            }

            /// <summary>
            /// Set name directly
            /// </summary>
            /// <param name="start"></param>
            /// <param name="end"></param>
            /// <param name="name"></param>
            internal Reference(int start, int end, string name)
            {
                _start = start;
                _end = end;
                _spellName = name;
            }

            public override string ToString()
            {
                return _spellName;
            }
        }

    }
}
