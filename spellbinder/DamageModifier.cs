using System;
using System.Xml;

namespace ArzooSoftware.SpellBinder.spellbinder
{
	/// <summary>
	/// Summary description for DamageModifier.
	/// </summary>
	public class DamageModifier : Modifier
	{
		private XmlNode _node;
        public DamageModifier(XmlNode node)
            : base("ExtraDamage", 0.0M, "")
		{
			_node = node;

            int number = 1;
            int die = 6;
            XmlAttribute attr = node.Attributes["number"];
            if (attr != null)
                int.TryParse(attr.Value, out number);
            attr = node.Attributes["die"];
            if (attr != null)
                int.TryParse(attr.Value, out die);
            _diceValue = new DiceValue(number, die);
		}

        private DiceValue _diceValue;

		#region IModifier Members

        /// <summary>
        /// Kind of die (how many sides)
        /// </summary>
        public override string Die
		{
			get
			{
				string ret = "";
				XmlNode modNode = _node;
				XmlAttribute attr = modNode.Attributes["die"];
				if (attr != null)
					ret = attr.Value;

				return ret;
			}
		}

        /// <summary>
        /// Number of dice
        /// </summary>
        public override string Number
		{
			get
			{
				string ret = "";
				XmlNode modNode = _node;
				XmlAttribute attr = modNode.Attributes["number"];
				if (attr != null)
					ret = attr.Value;

				return ret;
			}
		}

        public override decimal Cost
		{
			get
			{
                decimal ret = 0.0M;
				XmlNode modNode = _node;
				XmlAttribute attr = modNode.Attributes["cost"];
                if (attr != null)
                {
                    decimal.TryParse(attr.Value, out ret);
                }

				return ret;
			}
		}

        [Obsolete]
        public string Bump(string startValue)
        {
            throw new NotImplementedException();
        }

        public override IPropertyValue Bump(IPropertyValue startValue)
        {
            return startValue.Increment(_diceValue);
        }

        public override IPropertyValue IncrementValue
        {
            get { return _diceValue; }
        }

        #endregion
    }
}
