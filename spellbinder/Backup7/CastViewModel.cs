﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using ArzooSoftware.SpellBinder.spellbinder;
using Xceed.Wpf.Toolkit;
using System.Windows.Controls;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.SpellBind2.CastViewModel</name>
    /// <summary>
    /// ViewModel for handling CastWindow
    /// </summary>
    /// <version>1.0 - Initial Release - 2/12/2013 1:57:53 PM</version>
    /// <author>Greg Arzoomanian</author>    
    class CastViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        /// <summary>
        /// A delegate type for hooking up message notifications.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void MessageEventHandler(object sender, MessageEventArgs e);

        public event MessageEventHandler MessageEvent;

        public class MessageEventArgs : EventArgs
        {
            public string Message { get; internal set; }
            public MessageEventArgs(string message)
            {
                this.Message = message;
            }
        }

        private Cast _spellCast;

        internal CastViewModel(Cast spellCast)
        {
            _spellCast = spellCast;
            PropertyChanged += NullMessageHandler;  // remove need to check for no handler
            MessageEvent += NullMessageHandler;
        }

        /// <summary>
        /// Null event to call for event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>To avoid checking for null event</remarks>
        private void NullMessageHandler(object sender, EventArgs e)
        {
            ;
        }

        #region Properties
        public string Level
        {
            get
            {
                string ret = _spellCast.Level.ToString();
                if (_spellCast.Level == 0)
                    ret = "Morphic";
                return ret;
            }
        }

        public string SpellName
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.Name;
                return ret;
            }
        }

        public string SpellCost
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Cost.ToString();
                return ret;
            }
        }

        public string SpellDescription
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.Description;
                return ret;
            }
        }

        public string SpellZOE
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.ZOE.DisplayValue;
                return ret;
            }
        }

        public decimal ExtraZOECost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = _spellCast.Spell.ZOEProperty.Modifier;
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        public bool ZOEIsModifiable
        {
            get
            {
                return _spellCast.Spell.ZOEProperty.Modifier != null;
            }
        }

        public bool ZOEIsNotModifiable
        {
            get
            {
                return _spellCast.Spell.ZOEProperty.Modifier == null;
            }
        }

        public string SpellRange
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Range;
                return ret;
            }
        }

        public decimal ExtraRangeCost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = GetRangeModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetRangeModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is RangeModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        public string SpellDuration
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Duration;
                return ret;
            }
        }

        public decimal ExtraDurationCost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = GetDurationModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetDurationModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is DurationModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        public string SpellSavingThrow
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.SavingThrow;
                return ret;
            }
        }

        public decimal SpellDifficultyClass
        {
            get
            {
                decimal ret = 0M;

                ret = _spellCast.DifficultyClass;

                return ret;
            }
        }

        public bool HasSavingThrow
        {
            get
            {
                return _spellCast.DifficultyClass > 0M;
            }
        }

        public bool HasNoSavingThrow
        {
            get
            {
                return !HasSavingThrow;
            }
        }

        public decimal HardToSaveCost
        {
            get
            {
                decimal ret = 0M;

                IModifier modifier = GetSaveModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetSaveModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is SaveModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        public bool TakesAffectsOthersProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == "AffectsOthers"))
                {
                    ret = true;
                }

                return ret;
            }
        }
        public bool AffectsOthersProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == "AffectsOthers"))
                {
                    SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AffectsOthers");
                    if (_spellCast.GetNumModifiers(spell_prop.Modifier) > 0)
                        ret = true;
                }

                return ret;
            }
        }

        public decimal AffectsOthersCost
        {
            get
            {
                decimal ret = 1M;   // default cost
                // Check for spell-specific value
                if (_spellCast.Spell.ExtraPropertiesList.Any(p => p.Name == "AffectsOthers"))
                {
                    SpellProperty s_prop = _spellCast.Spell.ExtraPropertiesList.First(p => p.Name == "AffectsOthers");
                    IModifier mod = s_prop.Modifier;
                    ret = mod.Cost;
                }
                return ret;
            }
        }


        public bool TakesAtRangeProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == "AtRange"))
                {
                    ret = true;
                }

                return ret;
            }
        }
        public bool AtRangeProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == "AtRange"))
                {
                    SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AtRange");
                    if (_spellCast.GetNumModifiers(spell_prop.Modifier) > 0)
                        ret = true;
                }

                return ret;
            }
        }

        public bool CastOnTheRun
        {
            get
            {
                return false;
            }
        }

        public bool Concealment
        {
            get
            {
                return false;
            }
        }

        public bool PowerWord
        {
            get
            {
                return false;
            }
        }

        public string ReducedGesturesProperty
        {
            get
            {
                return "Normal";
            }
        }

        /// <summary>
        /// Number of dice for damage for this casting
        /// </summary>
        public string DamageNumber
        {
            get
            {
                string ret = "";
                if (_spellCast.Damage != null)
                    ret = _spellCast.Damage.DisplayValue;
                return ret;
            }
        }

        /// <summary>
        /// Expectation value for damage for this casting
        /// </summary>
        /// <remarks>Calculated from damage property value of Cast</remarks>
        public string DamageExpectation
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                {
                    //string str = _spellCast.Spell.DamageDie;
                    string str = _spellCast.Spell.DamageDie;
                    if (_spellCast.Damage != null)
                    {
                        int damageDie = _spellCast.Damage.Units.Sides;
                        int damageNum = (int)_spellCast.Damage.Value;
                        decimal expectation = (((decimal)damageDie + 1) / 2) * damageNum;
                        expectation += _spellCast.Damage.Plus;

                        ret = expectation.ToString();
                    }
                }
                return ret;
            }
        }

        public int ExtraDamageNum
        {
            get
            {
                int ret = 0;

                IModifier modifier = GetDamageModifier();
                ret = _spellCast.GetNumModifiers(modifier);

                return ret;
            }
        }

        public decimal ExtraDamageCost
        {
            get
            {
                decimal ret = 0;

                IModifier modifier = GetDamageModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetDamageModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is DamageModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        /// <summary>
        /// Signify we can't bump spell level any more
        /// </summary>
        private bool ReachedMaxSpellLevel
        {
            get
            {
                return (_spellCast.Level >= _spellCast.MaxLevel);
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Modify property based on updown control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void NumUpDn_ValueChanged(object sender, EventArgs e)
        {
            IntegerUpDown upDnCtl = sender as IntegerUpDown;
            VMBinding binding = upDnCtl.Tag as VMBinding;
            SpellProperty spellProp = binding.SpellProperty;
            if (!upDnCtl.IsEnabled)   // prevent re-execution of handler if counter reset
                return;

            IModifier mod = spellProp.Modifier;
            bool up_direction = true;  // get direction
            int new_val = (int)upDnCtl.Value;
            if (mod != null)
            {
                int cur_val = _spellCast.GetNumModifiers(mod);
                if (cur_val > new_val)
                    up_direction = false;

                // adjust number of damage modifiers
                if (up_direction)
                {
                    // Check we're not at max level
                    if (_spellCast.Level + mod.Cost > _spellCast.MaxLevel)
                    {
                        MessageEvent(this, new MessageEventArgs("At max spell level"));
                        RestoreUpDnControlValue(upDnCtl, cur_val);

                        return;
                    }
                    // Check we're not at max value
                    if (_spellCast.GetPropertyValue(spellProp).Equals(spellProp.MaxValue))
                    {
                        MessageEvent(this, new MessageEventArgs("At max property value"));
                        RestoreUpDnControlValue(upDnCtl, cur_val);

                        return;
                    }
                    _spellCast.AddModifier(mod);
                }
                else
                    _spellCast.RemoveModifier(mod);

                // Raise event that property has changed
                // PropertyChanged(this, new PropertyChangedEventArgs("SpellDuration"));
                PropertyChanged(this, new PropertyChangedEventArgs("Level"));
                PropertyChanged(this, new PropertyChangedEventArgs("SpellCost"));
                foreach (string prop_name in binding.BoundProperties)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(prop_name));
                }
            }

            return;
        }

        /// <summary>
        /// Restore NumericUpDown control w/o re-invoking handler
        /// </summary>
        /// <param name="upDnCtl"></param>
        /// <param name="cur_val"></param>
        private static void RestoreUpDnControlValue(IntegerUpDown upDnCtl, int cur_val)
        {
            // Set control back
            upDnCtl.IsEnabled = false;   // keep code from executing 2x
            try
            {
                upDnCtl.Value = cur_val;   // keep value the same
            }
            finally
            {
                upDnCtl.IsEnabled = true;
                upDnCtl.Focus();
            }

            return;
        }

        /// <summary>
        /// Modify spell based on checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CheckBox_ValueChanged(object sender, EventArgs e)
        {
            CheckBox check_box = sender as CheckBox;
            if (!check_box.IsEnabled)   // prevent re-execution of handler if counter reset
                return;
            VMBinding binding = check_box.Tag as VMBinding;
            SpellProperty spell_prop = binding.SpellProperty;
            IModifier mod = spell_prop.Modifier;

            if (check_box.IsChecked == true)
            {
                bool ok = _spellCast.AddModifier(mod);
                if (!ok)
                {
                    MessageBox.Show("At max spell level");
                    check_box.IsEnabled = false;
                    try
                    {
                        check_box.IsChecked = false;
                    }
                    finally
                    {
                        check_box.IsEnabled = true;
                    }
                }
            }
            else
            {
                _spellCast.RemoveModifier(mod);
            }

            // Raise event that property has changed
            // PropertyChanged(this, new PropertyChangedEventArgs("SpellDuration"));
            PropertyChanged(this, new PropertyChangedEventArgs("Level"));
            PropertyChanged(this, new PropertyChangedEventArgs("SpellCost"));
            foreach (string prop_name in binding.BoundProperties)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop_name));
            }

            return;
        }

        /// <summary>
        /// Modify spell based on checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ComboBox_ValueChanged(object sender, EventArgs e)
        {
            ComboBox combo_box = sender as ComboBox;
            if (!combo_box.IsEnabled)   // prevent re-execution of handler if counter reset
                return;
            VMBinding binding = combo_box.Tag as VMBinding;
            SpellProperty spell_prop = binding.SpellProperty;
            IModifier mod = spell_prop.Modifier;

            int selected_num = combo_box.SelectedIndex; // number of times to apply this mod (0, 1 or 2)
            int num_mods = _spellCast.GetNumModifiers(mod); // how many times is this mod already applied?
            if (selected_num > num_mods)    // need to apply more?
            {
                while (selected_num > num_mods)
                {
                    bool ok = _spellCast.AddModifier(mod);
                    if (!ok)
                    {
                        MessageEvent(this, new MessageEventArgs("At max spell level"));
                        combo_box.IsEnabled = false;
                        try
                        {
                            combo_box.SelectedIndex = _spellCast.GetNumModifiers(mod);
                        }
                        finally
                        {
                            combo_box.IsEnabled = true;
                            combo_box.Focus();
                        }

                        break;
                    }
                    num_mods++;
                }
            }
            else if (selected_num < num_mods)   // need to remove a mod (or 2)?
            {
                while (selected_num < num_mods)
                {
                    _spellCast.RemoveModifier(mod);
                    num_mods--;
                }
            }

            // Raise event that property has changed
            // PropertyChanged(this, new PropertyChangedEventArgs("SpellDuration"));
            PropertyChanged(this, new PropertyChangedEventArgs("Level"));
            PropertyChanged(this, new PropertyChangedEventArgs("SpellCost"));
            foreach (string prop_name in binding.BoundProperties)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop_name));
            }

            return;
        }
        
        #endregion
    }
}
