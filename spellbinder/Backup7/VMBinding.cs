﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ArzooSoftware.SpellBinder.spellbinder;
using System.Windows.Controls;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.SpellBind2.VMBinding</name>
    /// <summary>
    /// Binds controls to objects in Model for ViewModel
    /// </summary>
    /// <version>1.0 - Initial Release - 2/12/2013 5:07:43 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class VMBinding
    {
        private Cast _spellCast;
        private SpellProperty _spellProperty;
        public SpellProperty SpellProperty { get { return _spellProperty; } }

        private List<string> _boundProperties = new List<string>();

        /// <summary>
        /// List of names of properties whose data is bound to the property value.
        /// </summary>
        public IList<string> BoundProperties
        {
            get
            {
                return _boundProperties;
            }
        }

        private List<Control> _boundControls = new List<Control>();

        public IList<Control> BoundControls
        {
            get
            {
                return _boundControls;
            }
        }

        public VMBinding(Cast spellCast, SpellProperty spellProperty)
        {
            _spellCast = spellCast;
            _spellProperty = spellProperty;
        }

        /// <summary>
        /// String to display value
        /// </summary>
        public string DisplayValue
        {
            get
            {
                IPropertyValue val = _spellCast.GetPropertyValue(_spellProperty);
                return val.DisplayValue;
            }
        }
    }
}
