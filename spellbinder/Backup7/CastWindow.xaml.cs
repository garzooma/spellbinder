﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ArzooSoftware.SpellBinder.spellbinder;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <summary>
    /// Interaction logic for CastWindow.xaml
    /// </summary>
    public partial class CastWindow : Window
    {
        private Cast _spellCast;
        private Mage _mage;
        private CastViewModel _viewModel;

        public CastWindow(ISpell spell, Mage mage)
        {
            InitializeComponent();

            _spellCast = new Cast(spell, mage);
            _mage = mage;
            _viewModel = new CastViewModel(_spellCast);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Binding lvl_bind = new Binding();
            lvl_bind.Source = _viewModel;
            lvl_bind.Path = new PropertyPath("Level");
            lvl_bind.Mode = BindingMode.OneWay;

            lvlTxtBx.SetBinding(TextBox.TextProperty, lvl_bind);

            Binding cost_bind = new Binding();
            cost_bind.Source = _viewModel;
            cost_bind.Path = new PropertyPath("SpellCost");
            cost_bind.Mode = BindingMode.OneWay;

            costTxtBx.SetBinding(TextBox.TextProperty, cost_bind);

            Binding bind = new Binding();
            bind.Source = _spellCast;
            bind.Path = new PropertyPath("Spell.Description");
            bind.Mode = BindingMode.OneWay;
            descrTxtBx.SetBinding(TextBox.TextProperty, bind);

            // See if we need Morphic tab
            if (!_spellCast.Spell.IsMorphic)
            {
                tabControl1.Items.Remove(morphTabItem);
            }

            CastViewGrid.DataContext = _viewModel;

            // Standard properties
            this.izoeupdn.ValueChanged += _viewModel.NumUpDn_ValueChanged;
            VMBinding vm_bind = new VMBinding(_spellCast, _spellCast.Spell.ZOEProperty);
            vm_bind.BoundProperties.Add("SpellZOE");   // name of property for duration text box
            izoeupdn.Tag = vm_bind;

            iDurUpDown.ValueChanged += _viewModel.NumUpDn_ValueChanged;
            vm_bind = new VMBinding(_spellCast, _spellCast.Spell.DurationProperty);
            vm_bind.BoundProperties.Add("SpellDuration");   // name of property for duration text box
            iDurUpDown.Tag = vm_bind;

            this.irngupdn.ValueChanged += _viewModel.NumUpDn_ValueChanged;
            vm_bind = new VMBinding(_spellCast, _spellCast.Spell.RangeProperty);
            vm_bind.BoundProperties.Add("SpellRange");   // name of property for duration text box
            irngupdn.Tag = vm_bind;

            this.savIUpDn.ValueChanged += _viewModel.NumUpDn_ValueChanged;
            vm_bind = new VMBinding(_spellCast, _spellCast.Spell.SaveProperty);
            vm_bind.BoundProperties.Add("SpellRange");   // name of property for duration text box
            irngupdn.Tag = vm_bind;

            // Standard modifiers
            SpellProperty spell_prop;
            if (_viewModel.TakesAtRangeProperty)
            {
                this.atRangeChkBx.Checked += _viewModel.CheckBox_ValueChanged;
                spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AtRange");
                vm_bind = new VMBinding(_spellCast, spell_prop);
                vm_bind.BoundProperties.Add("SpellRange");  // range property changes
                atRangeChkBx.Tag = vm_bind;
            }

            this.redGestCmbBx.SelectionChanged += _viewModel.ComboBox_ValueChanged;
            spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "ReducedGestures");
            vm_bind = new VMBinding(_spellCast, spell_prop);
            redGestCmbBx.Tag = vm_bind;

            _viewModel.MessageEvent += _viewModel_MessageEvent;

            return;
        }

        private void _viewModel_MessageEvent(object sender, CastViewModel.MessageEventArgs e)
        {
            MessageBox.Show(e.Message);
        }
    }
}
