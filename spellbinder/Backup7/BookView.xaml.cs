﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using ArzooSoftware.SpellBinder.spellbinder;

namespace ArzooSoftware.SpellBinder.SpellBind2
{
    /// <summary>
    /// Interaction logic for BookView.xaml
    /// </summary>
    public partial class BookView : Window
    {
        public BookView()
        {
            InitializeComponent();
        }

        private Mage _mage = new Mage(6, 18);
        public Mage Mage
        {
            get
            {
                if (_mage == null)
                {
                    int level = 6;
                    //int.TryParse(mageLevelTxtBx.Text, out level);
                    int intelligence = 18;
                    //int.TryParse(intelligenceTxtBx.Text, out intelligence);
                    _mage = new Mage(level, intelligence);
                }
                return _mage;
            }
        }
        public int MageLevel { get { return Mage.Level; } }
        public int MageIntelligence { get { return Mage.Intelligence; } }
        public decimal MageBonus { get { return Mage.AbilityBonus; } }

        public class LVData
        {
            public string Name { get; set; }
            public string ZOE { get; set; }
            public string Range { get; set; }
            public string Duration { get; set; }
            public string SavingThrow { get; set; }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Create list of spells
            List<ISpell> spell_list = new List<ISpell>();
            XmlDocument doc = new XmlDocument();
            // doc.LoadXml(_bookStr);
            doc.Load("spellbook.xml");
            XmlNode book_node = doc.FirstChild;
            foreach (XmlNode spell_node in book_node.ChildNodes)
            {
                BaseSpell base_spell = new BaseSpell(spell_node);
                spell_list.Add(base_spell);
            }

            // Set level groups
            int current_level = 0;  // start w/Morphic spells
            //ListViewGroup level_group = new ListViewGroup("Morphic");
            //spellLstVw.Groups.Add(level_group);
            // Put spells in listview
            foreach (ISpell spell in spell_list)
            {
                // See if we're at next level
                if (spell.Level > current_level)
                {
                    current_level = (int)spell.Level;
                    //level_group = new ListViewGroup("Level " + current_level.ToString());
                    //spellLstVw.Groups.Add(level_group);
                }
                // ListViewItem listItem = new ListViewItem(spell.Name);
                //ListViewItem listItem = new ListViewItem();
                //listItem.ImageIndex = 0;

                //listItem.SubItems.Add(spell.ZOEProperty.Value.DisplayValue);
                //listItem.SubItems.Add(spell.RangeProperty.Value.DisplayValue);
                //listItem.SubItems.Add(spell.DurationProperty.Value.DisplayValue);
                //listItem.SubItems.Add(spell.SavingThrow);

                //listItem.Group = level_group;

                //listItem.Tag = spell;

                //this.spellLstVw.Items.Add(listItem);
                //spellLstVw.Items.Add(spell);
            }

            spellLstVw.ItemsSource = spell_list;

            CollectionView cv = (CollectionView)CollectionViewSource.GetDefaultView(spellLstVw.ItemsSource);
            PropertyGroupDescription pgd = new PropertyGroupDescription("Level");
            cv.GroupDescriptions.Add(pgd);
        }

        /// <summary>
        /// Display double-clicked spell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void spellLstVw_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ISpell spell = spellLstVw.SelectedItem as ISpell;
            if (spell != null)  // scroll clicks being mis-interpreted
            {
                CastWindow cast_win = new CastWindow(spell, _mage);
                cast_win.Show();
            }
        }

    }

    /// <summary>
    /// Converter class to display Group
    /// </summary>
    public class GroupLevelConverter : IValueConverter
    {

        public object Convert(object value, System.Type targetType,
                              object parameter,
                              System.Globalization.CultureInfo culture)
        {
            if (null == value)
                return "null";


            int level = (int)(decimal)value;
            string ret = String.Format("Level {0}", level);
            if (level == 0)
                ret = "Morphic";

            return ret;
        }

        public object ConvertBack(object value, System.Type targetType,
                                  object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}
