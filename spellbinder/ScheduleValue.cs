﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.ScheduleValue</name>
    /// <summary>
    /// Property value based on schedule
    /// </summary>
    /// <version>1.0 - Initial Release - 1/14/2013 10:40:16 AM</version>
    /// <author>Greg Arzoomanian</author>    
    class ScheduleValue : IPropertyValue
    {
        private string _value;
        private string[] _schedule;
        public ScheduleValue(string value, string[] schedule)
        {
            _value = value;
            _schedule = schedule;
        }

        #region IPropertyValue Members

        public string DisplayValue
        {
            get { return _value; }
        }

        /// <summary>
        /// Check value against schedule, and return next value
        /// </summary>
        public IPropertyValue Next
        {
            get 
            {
                IPropertyValue ret = null;
                bool match = false;
                foreach (string val in _schedule)
                {
                    if (match)  // previous value matched
                    {
                        ret = new ScheduleValue(val, _schedule);  // return next value
                        break;
                    }
                    if (val == _value)
                        match = true;
                }

                return ret;
            }
        }

        public string TypeValue
        {
            get { throw new NotImplementedException(); }
        }

        public decimal CalculationValue
        {
            get { throw new NotImplementedException(); }
        }

        public IPropertyValue Increment(IPropertyValue incrAmount)
        {
            return Next;
        }

        #endregion

        #region IComparable Members

        public int CompareTo(object obj)
        {
            int ret = 0;
            if (obj is ScheduleValue)
            {
                ScheduleValue in_val = obj as ScheduleValue;
                int index = Array.IndexOf(this._schedule, this._value);
                int in_index = Array.IndexOf(in_val._schedule, in_val._value);
                ret = index.CompareTo(in_index);
            }
            return ret;
        }

        #endregion
    }
}
