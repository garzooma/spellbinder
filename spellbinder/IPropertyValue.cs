﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    public interface IPropertyValue : IComparable
    {
        /// <summary>
        /// Get string to show in form
        /// </summary>
        string DisplayValue
        {
            get;
        }

        /// <summary>
        /// String that describes type of value
        /// </summary>
        /// <remarks>Used to describe type of Zone</remarks>
        string TypeValue
        {
            get;
        }

        /// <summary>
        /// Numeric value to participate in calculations
        /// </summary>
        decimal CalculationValue
        {
            get;
        }

        /// <summary>
        /// Add increment value to current value
        /// </summary>
        /// <param name="incrementValue"></param>
        /// <returns></returns>
        IPropertyValue Increment(IPropertyValue incrAmount);

    }
}
