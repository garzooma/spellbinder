﻿namespace ArzooSoftware.SpellBinder.spellbinder
{
    partial class MorphicUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.morphicLvlCmbBx = new System.Windows.Forms.ComboBox();
            this.patAppCmbBx = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Level";
            // 
            // morphicLvlCmbBx
            // 
            this.morphicLvlCmbBx.FormattingEnabled = true;
            this.morphicLvlCmbBx.Location = new System.Drawing.Point(46, 8);
            this.morphicLvlCmbBx.Name = "morphicLvlCmbBx";
            this.morphicLvlCmbBx.Size = new System.Drawing.Size(97, 21);
            this.morphicLvlCmbBx.TabIndex = 1;
            // 
            // patAppCmbBx
            // 
            this.patAppCmbBx.FormattingEnabled = true;
            this.patAppCmbBx.Location = new System.Drawing.Point(229, 8);
            this.patAppCmbBx.Name = "patAppCmbBx";
            this.patAppCmbBx.Size = new System.Drawing.Size(102, 21);
            this.patAppCmbBx.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(168, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Application";
            // 
            // MorphicUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.patAppCmbBx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.morphicLvlCmbBx);
            this.Controls.Add(this.label1);
            this.Name = "MorphicUC";
            this.Size = new System.Drawing.Size(350, 35);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox morphicLvlCmbBx;
        private System.Windows.Forms.ComboBox patAppCmbBx;
        private System.Windows.Forms.Label label2;
    }
}
