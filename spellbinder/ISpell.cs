using System;
using System.Collections.Generic;

namespace ArzooSoftware.SpellBinder.spellbinder
{
	/// <summary>
	/// Interface for a spell.
	/// </summary>
	public interface ISpell
	{
		decimal Level{get;}
		string Name{get;}
        string Description{get;}

        string ModifiersDescription { get; }

        bool IsMorphic { get; }

        /// <summary>
        /// ZOE property of base spell
        /// </summary>
        SpellProperty ZOEProperty { get; }

        SpellProperty RangeProperty { get; }

        /// <summary>
        /// Duration of base spell
        /// </summary>
        SpellProperty DurationProperty { get; }

        /// <summary>
        /// Type of saving throw (Reflex, Will, etc.)
        /// </summary>
		string SavingThrow{get;}

        /// <summary>
        /// Save DC of base spell
        /// </summary>
        SpellProperty SaveProperty { get; }

        /// <summary>
        /// Base damage for spell
        /// </summary>
        /// <remarks>Number of dice, size, and plusses</remarks>
        DiceValue Damage { get; }

        /// <summary>
        /// Base damage for spell
        /// </summary>
        /// <remarks>Number of dice, size, and plusses</remarks>
        SpellProperty DamageProperty { get; }

        /// <summary>
        /// List of possible modifiers for spell
        /// </summary>
        IList<IModifier> ModifierList { get; }

        /// <summary>
        /// List of properties
        /// </summary>
        IList<SpellProperty> PropertiesList { get; }

        /// <summary>
        /// List of extra properties (besides standards)
        /// </summary>
        IList<SpellProperty> ExtraPropertiesList { get; }

        /// <summary>
        /// Category of spell as per sheet
        /// </summary>
        string Category { get; }

        /// <summary>
        /// For Morphic Spell, the currently selected morph
        /// </summary>
        string MorphicBaseName { get; }

        /// <summary>
        /// To change a Morphic spell into a new one.
        /// </summary>
        MorphMinder MorphMinder { get; }

    }
}
