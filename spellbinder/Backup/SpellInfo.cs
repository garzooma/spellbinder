using System;
using System.Xml;

namespace spellbinder
{
	/// <summary>
	/// Summary description for SpellInfo.
	/// </summary>
	public class SpellInfo
	{
		XmlNode _node;
		public SpellInfo(XmlNode node)
		{
			_node = node;
		}

		public string Level
		{
			get
			{
				string ret = "";
				// Get Level node
				XmlAttribute attr = _node.Attributes["level"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string Name
		{
			get
			{
				string ret = "";
				// Get Level node
				XmlAttribute attr = _node.Attributes["name"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string Description
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/description");
				XmlText txtNode = descNode.FirstChild as XmlText;
				if (txtNode != null)
					ret = txtNode.Value;
				return ret;
			}
		}

		public string ZOEValue
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/ZOE");
				XmlAttribute attr = descNode.Attributes["value"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string ZOEUnits
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/ZOE");

				XmlAttribute attr = descNode.Attributes["units"];
				if (attr != null)
					ret = attr.Value;

				return ret;
			}
		}

		public string RangeValue
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/Range");
				XmlAttribute attr = descNode.Attributes["value"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string RangeUnits
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/Range");

				XmlAttribute attr = descNode.Attributes["units"];
				if (attr != null)
					ret = attr.Value;

				return ret;
			}
		}

		public string DurationValue
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/Duration");
				XmlAttribute attr = descNode.Attributes["value"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string DurationUnits
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/Duration");
				XmlAttribute attr = descNode.Attributes["units"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string SavingThrowValue
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/SavingThrow");
				XmlAttribute attr = descNode.Attributes["value"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string SavingThrowEffect
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/SavingThrow");
				XmlAttribute attr = descNode.Attributes["effect"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string DamageDie
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/Damage");
				XmlAttribute attr = descNode.Attributes["die"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string DamageNumber
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("/spell/Damage");
				XmlAttribute attr = descNode.Attributes["number"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

	}
}
