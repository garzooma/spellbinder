using System;

namespace spellbinder
{
	/// <summary>
	/// Summary description for IModifier.
	/// </summary>
	public interface IModifier
	{
		string Name {get;}
		string Die { get;}
		string Number { get;}
		string Cost { get; }
	}
}
