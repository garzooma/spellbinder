using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace spellbinder
{
	/// <summary>
	/// Summary description for ExtraDamageForm.
	/// </summary>
	public class ExtraDamageForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox dieTxtBx;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox dieNumberTxtBx;
		private System.Windows.Forms.TextBox costTxtBx;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown incrementUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox totalCostTxtBx;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ISpell _spell;
		private IModifier _modifier;
		private int _increment;
		public ExtraDamageForm(ISpell spell, IModifier mod)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			_spell = spell;
			_modifier = mod;
			this._increment = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.dieTxtBx = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.dieNumberTxtBx = new System.Windows.Forms.TextBox();
			this.costTxtBx = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.incrementUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.totalCostTxtBx = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.incrementUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Die:";
			// 
			// dieTxtBx
			// 
			this.dieTxtBx.Location = new System.Drawing.Point(72, 24);
			this.dieTxtBx.Name = "dieTxtBx";
			this.dieTxtBx.Size = new System.Drawing.Size(40, 20);
			this.dieTxtBx.TabIndex = 1;
			this.dieTxtBx.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(128, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(48, 23);
			this.label2.TabIndex = 2;
			this.label2.Text = "Number:";
			// 
			// dieNumberTxtBx
			// 
			this.dieNumberTxtBx.Location = new System.Drawing.Point(176, 24);
			this.dieNumberTxtBx.Name = "dieNumberTxtBx";
			this.dieNumberTxtBx.Size = new System.Drawing.Size(56, 20);
			this.dieNumberTxtBx.TabIndex = 3;
			this.dieNumberTxtBx.Text = "";
			// 
			// costTxtBx
			// 
			this.costTxtBx.Location = new System.Drawing.Point(304, 24);
			this.costTxtBx.Name = "costTxtBx";
			this.costTxtBx.Size = new System.Drawing.Size(80, 20);
			this.costTxtBx.TabIndex = 5;
			this.costTxtBx.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(256, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 23);
			this.label3.TabIndex = 4;
			this.label3.Text = "Cost:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(24, 64);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 23);
			this.label4.TabIndex = 6;
			this.label4.Text = "Increment:";
			// 
			// incrementUpDown
			// 
			this.incrementUpDown.Location = new System.Drawing.Point(96, 64);
			this.incrementUpDown.Name = "incrementUpDown";
			this.incrementUpDown.Size = new System.Drawing.Size(48, 20);
			this.incrementUpDown.TabIndex = 8;
			this.incrementUpDown.ValueChanged += new System.EventHandler(this.incrementUpDown_ValueChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(176, 64);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 23);
			this.label5.TabIndex = 9;
			this.label5.Text = "Total cost:";
			// 
			// totalCostTxtBx
			// 
			this.totalCostTxtBx.Location = new System.Drawing.Point(256, 64);
			this.totalCostTxtBx.Name = "totalCostTxtBx";
			this.totalCostTxtBx.TabIndex = 10;
			this.totalCostTxtBx.Text = "";
			// 
			// ExtraDamageForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(408, 266);
			this.Controls.Add(this.totalCostTxtBx);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.incrementUpDown);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.costTxtBx);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.dieNumberTxtBx);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.dieTxtBx);
			this.Controls.Add(this.label1);
			this.Name = "ExtraDamageForm";
			this.Text = "ExtraDamageForm";
			this.Load += new System.EventHandler(this.ExtraDamageForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.incrementUpDown)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void ExtraDamageForm_Load(object sender, System.EventArgs e)
		{
			Binding binding = null;
			binding = this.dieTxtBx.DataBindings.Add("Text", this, "Die");
			binding = this.dieNumberTxtBx.DataBindings.Add("Text", this, "Number");
			binding = this.costTxtBx.DataBindings.Add("Text", this, "Cost");
			binding = this.incrementUpDown.DataBindings.Add("Value", this, "Increment");
			binding = this.totalCostTxtBx.DataBindings.Add("Text", this, "TotalCost");
			if (binding == null)
				MessageBox.Show("Binding failed");

		}

		private void incrementUpDown_ValueChanged(object sender, System.EventArgs e)
		{
			this._increment = (int)this.incrementUpDown.Value;
			double cost = Convert.ToDouble(this.Cost);
			double totalCost = this._increment * cost;
			this.totalCostTxtBx.Text = totalCost.ToString();

			return;
		}

		public string Die
		{
			get
			{
				string ret = "";
				if (_modifier != null)
					ret = _modifier.Die;

				return ret;
			}
		}

		public string Number
		{
			get
			{
				string ret = "";
				if (_modifier != null)
					ret = _modifier.Number;

				return ret;
			}
		}

		public string Cost
		{
			get
			{
				string ret = "";
				if (_modifier != null)
					ret = _modifier.Cost;

				return ret;
			}
		}

		public string Increment
		{
			get
			{
				string ret = "";
				ret = this._increment.ToString();
				return ret;
			}
			set
			{
				this._increment = Convert.ToInt32(value);
			}
		}

		public string TotalCost
		{
			get
			{
				string ret = "";
				double cost = Convert.ToDouble(this.Cost);
				double total = this._increment * cost;

				ret = total.ToString();

				return ret;
			}
		}
	}
}
