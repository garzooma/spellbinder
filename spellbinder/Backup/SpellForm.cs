using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;

namespace spellbinder
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class SpellForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox nameTxtBx;
		private System.Windows.Forms.TextBox levelTxtBx;
		private System.Windows.Forms.TextBox descTxtBx;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox ZOETxtBx;
		private System.Windows.Forms.TextBox savingThrowTxtBx;
		private System.Windows.Forms.TextBox durationTxtBx;
		private System.Windows.Forms.GroupBox damageGrpBx;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox damageDieTxtBx;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox damageNumberTxtBx;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox expectTxtBx;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox costTxtBx;
		private System.Windows.Forms.TextBox rangeTxtBx;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SpellForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SpellForm));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.nameTxtBx = new System.Windows.Forms.TextBox();
			this.levelTxtBx = new System.Windows.Forms.TextBox();
			this.descTxtBx = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.ZOETxtBx = new System.Windows.Forms.TextBox();
			this.savingThrowTxtBx = new System.Windows.Forms.TextBox();
			this.durationTxtBx = new System.Windows.Forms.TextBox();
			this.rangeTxtBx = new System.Windows.Forms.TextBox();
			this.damageGrpBx = new System.Windows.Forms.GroupBox();
			this.expectTxtBx = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.damageNumberTxtBx = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.damageDieTxtBx = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.costTxtBx = new System.Windows.Forms.TextBox();
			this.damageGrpBx.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(128, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 24);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(40, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "Level";
			// 
			// nameTxtBx
			// 
			this.nameTxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.nameTxtBx.Location = new System.Drawing.Point(168, 8);
			this.nameTxtBx.Name = "nameTxtBx";
			this.nameTxtBx.Size = new System.Drawing.Size(272, 20);
			this.nameTxtBx.TabIndex = 2;
			this.nameTxtBx.Text = "";
			// 
			// levelTxtBx
			// 
			this.levelTxtBx.Location = new System.Drawing.Point(56, 8);
			this.levelTxtBx.Name = "levelTxtBx";
			this.levelTxtBx.Size = new System.Drawing.Size(56, 20);
			this.levelTxtBx.TabIndex = 3;
			this.levelTxtBx.Text = "";
			// 
			// descTxtBx
			// 
			this.descTxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.descTxtBx.Location = new System.Drawing.Point(24, 64);
			this.descTxtBx.Multiline = true;
			this.descTxtBx.Name = "descTxtBx";
			this.descTxtBx.Size = new System.Drawing.Size(416, 224);
			this.descTxtBx.TabIndex = 4;
			this.descTxtBx.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(24, 40);
			this.label3.Name = "label3";
			this.label3.TabIndex = 5;
			this.label3.Text = "Description:";
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label4.Location = new System.Drawing.Point(24, 304);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(48, 23);
			this.label4.TabIndex = 6;
			this.label4.Text = "ZOE:";
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label5.Location = new System.Drawing.Point(24, 336);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(48, 23);
			this.label5.TabIndex = 7;
			this.label5.Text = "Range:";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.Location = new System.Drawing.Point(224, 304);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 23);
			this.label6.TabIndex = 8;
			this.label6.Text = "Duration:";
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label7.Location = new System.Drawing.Point(224, 336);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(80, 23);
			this.label7.TabIndex = 9;
			this.label7.Text = "Saving Throw:";
			// 
			// ZOETxtBx
			// 
			this.ZOETxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.ZOETxtBx.Location = new System.Drawing.Point(80, 304);
			this.ZOETxtBx.Name = "ZOETxtBx";
			this.ZOETxtBx.Size = new System.Drawing.Size(120, 20);
			this.ZOETxtBx.TabIndex = 10;
			this.ZOETxtBx.Text = "";
			// 
			// savingThrowTxtBx
			// 
			this.savingThrowTxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.savingThrowTxtBx.Location = new System.Drawing.Point(312, 336);
			this.savingThrowTxtBx.Name = "savingThrowTxtBx";
			this.savingThrowTxtBx.Size = new System.Drawing.Size(128, 20);
			this.savingThrowTxtBx.TabIndex = 11;
			this.savingThrowTxtBx.Text = "";
			// 
			// durationTxtBx
			// 
			this.durationTxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.durationTxtBx.Location = new System.Drawing.Point(312, 304);
			this.durationTxtBx.Name = "durationTxtBx";
			this.durationTxtBx.Size = new System.Drawing.Size(128, 20);
			this.durationTxtBx.TabIndex = 12;
			this.durationTxtBx.Text = "";
			// 
			// rangeTxtBx
			// 
			this.rangeTxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.rangeTxtBx.Location = new System.Drawing.Point(80, 336);
			this.rangeTxtBx.Name = "rangeTxtBx";
			this.rangeTxtBx.Size = new System.Drawing.Size(120, 20);
			this.rangeTxtBx.TabIndex = 13;
			this.rangeTxtBx.Text = "";
			// 
			// damageGrpBx
			// 
			this.damageGrpBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.damageGrpBx.Controls.Add(this.expectTxtBx);
			this.damageGrpBx.Controls.Add(this.label10);
			this.damageGrpBx.Controls.Add(this.damageNumberTxtBx);
			this.damageGrpBx.Controls.Add(this.label9);
			this.damageGrpBx.Controls.Add(this.damageDieTxtBx);
			this.damageGrpBx.Controls.Add(this.label8);
			this.damageGrpBx.Location = new System.Drawing.Point(24, 368);
			this.damageGrpBx.Name = "damageGrpBx";
			this.damageGrpBx.Size = new System.Drawing.Size(416, 56);
			this.damageGrpBx.TabIndex = 14;
			this.damageGrpBx.TabStop = false;
			this.damageGrpBx.Text = "Damage";
			// 
			// expectTxtBx
			// 
			this.expectTxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.expectTxtBx.Location = new System.Drawing.Point(288, 16);
			this.expectTxtBx.Name = "expectTxtBx";
			this.expectTxtBx.Size = new System.Drawing.Size(112, 20);
			this.expectTxtBx.TabIndex = 5;
			this.expectTxtBx.Text = "";
			// 
			// label10
			// 
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label10.Location = new System.Drawing.Point(224, 16);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(64, 23);
			this.label10.TabIndex = 4;
			this.label10.Text = "Expect:";
			// 
			// damageNumberTxtBx
			// 
			this.damageNumberTxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.damageNumberTxtBx.Location = new System.Drawing.Point(80, 16);
			this.damageNumberTxtBx.Name = "damageNumberTxtBx";
			this.damageNumberTxtBx.Size = new System.Drawing.Size(40, 20);
			this.damageNumberTxtBx.TabIndex = 3;
			this.damageNumberTxtBx.Text = "";
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label9.Location = new System.Drawing.Point(16, 16);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(56, 23);
			this.label9.TabIndex = 2;
			this.label9.Text = "Number:";
			// 
			// damageDieTxtBx
			// 
			this.damageDieTxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.damageDieTxtBx.Location = new System.Drawing.Point(168, 16);
			this.damageDieTxtBx.Name = "damageDieTxtBx";
			this.damageDieTxtBx.Size = new System.Drawing.Size(40, 20);
			this.damageDieTxtBx.TabIndex = 1;
			this.damageDieTxtBx.Text = "";
			// 
			// label8
			// 
			this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label8.Location = new System.Drawing.Point(128, 16);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(40, 23);
			this.label8.TabIndex = 0;
			this.label8.Text = "Die:";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(200, 40);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(40, 23);
			this.label11.TabIndex = 15;
			this.label11.Text = "Cost:";
			// 
			// costTxtBx
			// 
			this.costTxtBx.Location = new System.Drawing.Point(256, 40);
			this.costTxtBx.Name = "costTxtBx";
			this.costTxtBx.TabIndex = 16;
			this.costTxtBx.Text = "";
			// 
			// SpellForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(464, 486);
			this.Controls.Add(this.costTxtBx);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.damageGrpBx);
			this.Controls.Add(this.rangeTxtBx);
			this.Controls.Add(this.durationTxtBx);
			this.Controls.Add(this.savingThrowTxtBx);
			this.Controls.Add(this.ZOETxtBx);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.descTxtBx);
			this.Controls.Add(this.levelTxtBx);
			this.Controls.Add(this.nameTxtBx);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SpellForm";
			this.Text = "SpellBinder";
			this.Load += new System.EventHandler(this.SpellForm_Load);
			this.damageGrpBx.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new SpellForm());
		}

		private ISpell _spell;
		private string _spellStr = @"
		<spell name='Lance of (Element)' level='1'>
			<description>'Exists in three different elemental versions: Fire, Ice, and Lightning (elemental fire, water, and air respectively). The Earth element equivalent is the Magic Missile spell (see below). Each Lance creates the given element and flings it at a single target. The element is nonmagical once created, so there is no saving throw but the appropriate form of Resistance grants complete immunity. (Ice Lance is stopped by Resist Cold). The Lance spell is a ranged touch attack which ignores cover penalties, including the 'cover' provided by a friendly character in melee: therefore it can never hit an unintended target, though it can simply miss. The base damage is 2d4.'</description> 
			<Modifiers>
				<ExtraEffect>+d10 rounds duration</ExtraEffect> 
				<ExtraDuration /> 
			</Modifiers>
			<ZOE value='1' units='being' /> 
			<Range value='60' units='feet' /> 
			<Duration value='12' units='rounds' /> 
			<SavingThrow value='Will' effect='negates' /> 
			<Damage die='4' number='2'/>
			<Modifiers> 
				<ExtraDamage die='4' number = '1' cost='.5' />
			</Modifiers>
		</spell>
		";

		ExtraDamageForm _xtraDmgForm;

		private void SpellForm_Load(object sender, System.EventArgs e)
		{
		
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(_spellStr);
			XmlNode node = doc.FirstChild;
			this._spell = new BaseSpell(node, 6);
			Binding binding = null;
			binding = this.levelTxtBx.DataBindings.Add("Text", this, "Level");
			binding = this.nameTxtBx.DataBindings.Add("Text", this, "SpellName");
			binding = this.costTxtBx.DataBindings.Add("Text", this, "SpellCost");
			binding = this.descTxtBx.DataBindings.Add("Text", this, "SpellDescription");
			binding = this.ZOETxtBx.DataBindings.Add("Text", this, "SpellZOE");
			binding = this.rangeTxtBx.DataBindings.Add("Text", this, "SpellRange");
			binding = this.durationTxtBx.DataBindings.Add("Text", this, "SpellDuration");
			binding = this.savingThrowTxtBx.DataBindings.Add("Text", this, "SpellSavingThrow");
			binding = this.damageDieTxtBx.DataBindings.Add("Text", this, "DamageDie");
			binding = this.damageNumberTxtBx.DataBindings.Add("Text", this, "DamageNumber");
			binding = this.expectTxtBx.DataBindings.Add("Text", this, "DamageExpectation");
			if (binding == null)
				MessageBox.Show("Binding failed");

			// Put up ExtraDamage form
			DamageModifier mod = new DamageModifier(node);
			_xtraDmgForm = new ExtraDamageForm(this._spell, mod);
			_xtraDmgForm.Show();

		}
	
		public string Level
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.Level;
				return ret;
			}
		}
	
		public string SpellName
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.Name;
				return ret;
			}
		}

		public string SpellCost
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.Cost;
				return ret;
			}
		}

		public string SpellDescription
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.Description;
				return ret;
			}
		}

		public string SpellZOE
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.ZOE;
				return ret;
			}
		}

		public string SpellRange
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.Range;
				return ret;
			}
		}

		public string SpellDuration
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.Duration;
				return ret;
			}
		}

		public string SpellSavingThrow
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.SavingThrow;
				return ret;
			}
		}

		public string DamageDie
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.DamageDie;
				return ret;
			}
		}

		public string DamageNumber
		{
			get
			{
				string ret = "";
				if (_spell != null)
					ret = _spell.DamageNumber;
				return ret;
			}
		}

		public string DamageExpectation
		{
			get
			{
				string ret = "";
				if (_spell != null)
				{
					string str = _spell.DamageDie;
					int damageDie = Convert.ToInt32(str);
					str = _spell.DamageNumber;
					int damageNum = Convert.ToInt32(str);
					float expectation = (((float)damageDie+1)/2) * damageNum;
					ret = expectation.ToString();
				}
				return ret;
			}
		}
	}
}
