using System;

namespace spellbinder
{
	/// <summary>
	/// Summary description for CostCalc.
	/// </summary>
	public class CostCalc
	{
		int _level;
		bool _half;
		public CostCalc(int level, bool half)
		{
			_level = level;
			_half = half;
		}

		// Spell cost
		public string Cost(int mageLevel)
		{
			string ret = "";

			// Get index for table
			int[] levelTbl = levelTables[mageLevel];
			int idx = (this._level-1) * 2 + (this._half ? 1 : 0);
			int cost = levelTbl[idx];

			ret = cost.ToString();
			return ret;
		}

		// Spell cost table
		private static int[] level0 = {8};
		private static int[] level1 = {6,8};
		private static int[] level2 = {5,6,8};
		private static int[] level3 = {4,5,6,8};
		private static int[] level4 = {3,4,5,6,8};
		private static int[] level5 = {2,3,4,5,6,8};
		private static int[] level6 = {2,2,3,4,5,6,8};
		private static int[] level7 = {1,2,2,3,4,5,6,8};
		private static int[] level8 = {1,2,2,3,4,5,6,8};
		private static int[] level9 = {1,2,2,3,4,5,6,8};
		private static int[] level10 = {1,2,2,3,4,5,6,8};
		private static int[] level11 = {1,2,2,3,4,5,6,8};
		private static int[] level12 = {1,2,2,3,4,5,6,8};
		private static int[] level13 = {1,2,2,3,4,5,6,8};
		private static int[] level14 = {1,2,2,3,4,5,6,8};
		private static int[] level15 = {1,2,2,3,4,5,6,8};
		private static int[] level16 = {1,2,2,3,4,5,6,8};
		private static int[] level17 = {1,2,2,3,4,5,6,8};
		private static int[] level18 = {1,2,2,3,4,5,6,8};

		private static int[][] levelTables = {
										  level0,
										  level1,
										  level2,
										  level3,
										  level4,
										  level5,
										  level6,
										  level7,
										  level8,
										  level9,
										  level10,
										  level11,
										  level12,
										  level13,
										  level14,
										  level15,
										  level16,
										  level17,
										  level18
		};
	}	// CostCalc
}
