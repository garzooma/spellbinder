using System;
using System.Xml;

namespace spellbinder
{
	/// <summary>
	/// Summary description for BaseSpell.
	/// </summary>
	public class BaseSpell : ISpell
	{
		SpellInfo _spell;
		int _mageLevel;
		public BaseSpell(XmlNode node, int mageLevel)
		{
			_spell = new SpellInfo(node);
			_mageLevel = mageLevel;
		}
		#region ISpell Members

		public string Level
		{
			get
			{
				string ret = "";
				// Get Level node
				ret = _spell.Level;
				return ret;
			}
		}

		public string Name
		{
			get
			{
				string ret = "";
				// Get Level node
				ret = _spell.Name;
				return ret;
			}
		}

		public string Cost
		{
			get
			{
				string ret = "";
				// Get Level node
				int level = Convert.ToInt32(this._spell.Level);

				CostCalc calc = new CostCalc(level, false);
				ret = calc.Cost(this._mageLevel);

				return ret;
			}
		}

		public string Description
		{
			get
			{
				string ret = "";

				ret = this._spell.Description;

				return ret;
			}
		}

		public string ZOE
		{
			get
			{
				string ret = "";
				ret = this._spell.ZOEValue;
				ret += (" " + this._spell.ZOEUnits);
				return ret;
			}
		}

		public string Range
		{
			get
			{
				string ret = "";
				ret = this._spell.RangeValue;
				ret += (" " + this._spell.RangeUnits);
				return ret;
			}
		}

		public string Duration
		{
			get
			{
				string ret = "";
				ret = this._spell.DurationValue;
				ret += (" " + this._spell.DurationUnits);
				return ret;
			}
		}

		public string SavingThrow
		{
			get
			{
				string ret = "";
				ret = this._spell.SavingThrowValue;
				ret += (" " + this._spell.SavingThrowEffect);
				return ret;
			}
		}

		public string DamageDie
		{
			get
			{
				string ret = "";
				ret = this._spell.DamageDie;
				return ret;
			}
		}

		public string DamageNumber
		{
			get
			{
				string ret = "";
				ret = this._spell.DamageNumber;
				return ret;
			}
		}

		public string ExpectationValue
		{
			get
			{
				// TODO:  Add BaseSpell.ExpectationValue getter implementation
				return null;
			}
		}

		#endregion
	}
}
