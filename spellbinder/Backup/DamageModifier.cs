using System;
using System.Xml;

namespace spellbinder
{
	/// <summary>
	/// Summary description for DamageModifier.
	/// </summary>
	public class DamageModifier : IModifier
	{
		private XmlNode _node;
		public DamageModifier(XmlNode node)
		{
			_node = node;
		}
		#region IModifier Members

		public string Name
		{
			get
			{
				string ret = "ExtraDamage";
				return ret;
			}
		}

		public string Die
		{
			get
			{
				string ret = "";
				XmlNode modNode = _node.SelectSingleNode("/spell/Modifiers/ExtraDamage");
				XmlAttribute attr = modNode.Attributes["die"];
				if (attr != null)
					ret = attr.Value;

				return ret;
			}
		}

		public string Number
		{
			get
			{
				string ret = "";
				XmlNode modNode = _node.SelectSingleNode("/spell/Modifiers/ExtraDamage");
				XmlAttribute attr = modNode.Attributes["number"];
				if (attr != null)
					ret = attr.Value;

				return ret;
			}
		}

		public string Cost
		{
			get
			{
				string ret = "";
				XmlNode modNode = _node.SelectSingleNode("/spell/Modifiers/ExtraDamage");
				XmlAttribute attr = modNode.Attributes["cost"];
				if (attr != null)
					ret = attr.Value;

				return ret;
			}
		}

		#endregion
	}
}
