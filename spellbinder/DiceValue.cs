﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.DiceValue</name>
    /// <summary>
    /// Description for DiceValue
    /// </summary>
    /// <version>1.0 - Initial Release - 1/15/2013 12:11:44 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class DiceValue : IPropertyValue
    {
        public DiceValue(XmlNode node)
        {
            XmlAttribute attr = node.Attributes["die"];
            if (attr == null)
                throw new ArgumentException("No 'die' attribute in dice node");
            int sides;
            int.TryParse(attr.Value, out sides);
            int num_dice;
            attr = node.Attributes["number"];
            if (attr == null)
                throw new ArgumentException("No 'number' attribute in dice modifier");
            int.TryParse(attr.Value, out num_dice);

            attr = node.Attributes["die_plus"];
            int die_plus = 0;
            if (attr != null)
            {
                int.TryParse(attr.Value, out die_plus);
            }

            attr = node.Attributes["plus"];
            int plus = 0;
            if (attr != null)
            {
                int.TryParse(attr.Value, out plus);
            }

            attr = node.Attributes["units"];
            string units = String.Empty;
            if (attr != null)
            {
                units = node.Attributes["units"].Value;
            }

            _value = num_dice;
            _dice = new Dice(sides);
            _plus = plus;
            _diePlus = die_plus;
            _units = units;
        }

        public DiceValue(int value, int sides) : this(value, sides, 0)
        {
        }

        public DiceValue(int value, int sides, int plus)
            : this(value, sides, plus, String.Empty)
        {
        }

        public DiceValue(int value, int sides, int plus, string units)
            : this(value, sides, plus, 0, units)
        {
        }

        public DiceValue(int value, int sides, int plus, int die_plus, string units)
        {
            _value = value;
            _dice = new Dice(sides);
            _plus = plus;
            _diePlus = die_plus;
            _units = units;
        }

        private int _value;

        /// <summary>
        /// Number of dice
        /// </summary>
        public decimal Value { get { return _value; } }
        //private int _incrementAmount = 1;

        /// <summary>
        /// Kinds of dice (sides)
        /// </summary>
        public Dice Dice { get { return _dice; } }
        private Dice _dice = null;

        private int _plus = 0;     // additional value per die

        /// <summary>
        /// Additional value per roll (after all dice rolled)
        /// </summary>
        public int Plus { get { return _plus; } }

        private int _diePlus = 0;     // additional value per die

        /// <summary>
        /// Additional value per die
        /// </summary>
        public int DiePlus { get { return _diePlus; } }

        /// <summary>
        /// Units for special spells that use dice to determine
        /// non-damage values (Blinding Flash)
        /// </summary>
        public string Units { get { return _units; } }
        private string _units = null;

        #region IPropertyValue Members

        public string DisplayValue
        {
            get 
            {
                StringBuilder ret = new StringBuilder();
                if (_value > 0) // not just plus indicator
                {
                    ret.Append(String.Format("{0}{1}", _value, _dice));
                    if (_plus > 0 || _diePlus > 0)  // add additional value per die
                        ret.Append(String.Format("+{0}", _plus + (_diePlus * _value)));
                }
                else // plus indicator
                {
                    if (_plus > 0)
                        ret.Append(String.Format("+{0}", _plus));
                    else
                        ret.Append(String.Format("+{0}", _diePlus));
                }
                // Check for units
                if (!String.IsNullOrEmpty(_units))
                {
                    ret.Append(" ");
                    ret.Append(_units);
                }

                return ret.ToString(); 
            }
        }

        public string TypeValue
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Return the number of dice
        /// </summary>
        public decimal CalculationValue
        {
            get { return _value; }
        }

        /// <summary>
        /// Increment the number of dice and/or the plus
        /// </summary>
        /// <param name="incrAmount"></param>
        /// <returns></returns>
        public IPropertyValue Increment(IPropertyValue incrAmount)
        {
            DiceValue incr_val = incrAmount as DiceValue;
            return new DiceValue(_value + incr_val._value, _dice.Sides, _plus + incr_val._plus, _diePlus + incr_val._diePlus, _units);
        }

        #endregion

        public override bool Equals(object obj)
        {
            bool ret = false;
            if (obj is DiceValue)
            {
                DiceValue in_val = obj as DiceValue;
                if ((in_val._dice.Equals(_dice))
                    && (in_val._value == _value)
                    && (in_val._plus == _plus)
                    && (in_val._units == _units)
                    )
                {
                    ret = true;
                }
            }
            return ret;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + _dice.GetHashCode();
                hash = hash * 23 + this._diePlus.GetHashCode();
                hash = hash * 23 + this._plus.GetHashCode();
                hash = hash * 23 + this._units.GetHashCode();
                hash = hash * 23 + this._value.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return DisplayValue;
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            int ret = 0;
            if (obj is DiceValue)
            {
                DiceValue in_val = obj as DiceValue;
                ret = decimal.Compare(this.ExpectationValue, in_val.ExpectationValue);
            }
            return ret;
        }

        #endregion

        public decimal ExpectationValue
        {
            get
            {
                decimal expectation = ((Dice.Sides + 1) / 2) * Value;
                expectation += Plus;

                return expectation;
            }
        }
    }
}
