﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.SpellModifier</name>
    /// <summary>
    /// Generic modifier of a spell's property
    /// </summary>
    /// <remarks>Used in single or enumerated value properties.  As distinct from standard modifiers</remarks>
    /// <version>1.0 - Initial Release - 1/15/2013 10:45:42 AM</version>
    /// <author>Greg Arzoomanian</author>    
    class SpellModifier : Modifier
    {
        public SpellModifier(string name, decimal cost, IPropertyValue increment, SpellProperty property, string description) : base(name, cost, description)
        {
            _incrementAmount = increment;
            _spellProperty = property;
        }

        /// <summary>
        /// Create SpellModifier property
        /// </summary>
        /// <param name="name">Name of modifier</param>
        /// <param name="cost"></param>
        /// <param name="increment"></param>
        /// <param name="property">Property to be modified</param>
        public SpellModifier(string name, decimal cost, IPropertyValue increment, SpellProperty property)
            : this(name, cost, increment, property, String.Empty)
        {
        }

        #region IModifier Members
        //private string _name = "Modifier";
        //private decimal _cost = 0.5M;
        private IPropertyValue _incrementAmount;

        /// <summary>
        /// Property to be modified
        /// </summary>
        private SpellProperty _spellProperty;

        /// <summary>
        /// Property to be modified
        /// </summary>
        public SpellProperty Property { get { return _spellProperty; } }

        public override string PropertyName
        {
            get
            {
                string ret = _spellProperty.Name;
                return ret;
            }
        }

        [Obsolete]
        public string Bump(string startValue)
        {
            throw new NotImplementedException();
        }

        public override IPropertyValue Bump(IPropertyValue startValue)
        {
            return startValue.Increment(_incrementAmount);
        }

        public override IPropertyValue IncrementValue
        {
            get { return _incrementAmount; }
        }
        #endregion
    }
}
