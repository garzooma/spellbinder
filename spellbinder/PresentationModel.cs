﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;
using System.Linq;
using SynapticEffect.Forms;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.spellbinder.PresentationModel</name>
    /// <summary>
    /// Handles presentation of spell information to SpellForm
    /// </summary>
    /// <version>1.0 - Initial Release - 1/21/2013 12:14:56 PM</version>
    /// <author>Greg Arzoomanian</author>  
    /// <remarks>See http://martinfowler.com/eaaDev/PresentationModel.html </remarks>
    public class PresentationModel
    {
        /// <summary>
        /// A delegate type for hooking up message notifications.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void MessageEventHandler(object sender, MessageEventArgs e);

        public event MessageEventHandler MessageEvent;

        public class MessageEventArgs : EventArgs
        {
            public string Message { get; internal set; }
            public MessageEventArgs(string message)
            {
                this.Message = message;
            }
        }

        private Cast _spellCast;
        private SpellForm _form;
        public PresentationModel(Cast spellCast, SpellForm form)
        {
            _spellCast = spellCast;
            _form = form;
            MessageEvent += NullMessageHandler; // to avoid checking for null event
        }

        /// <summary>
        /// Null event to call for message event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>To avoid checking for null event</remarks>
        private void NullMessageHandler(object sender, EventArgs e)
        {
            ;
        }

        #region Properties
        public string Level
        {
            get
            {
                string ret = _spellCast.Level.ToString();
                if (_spellCast.Level == 0)
                    ret = "Morphic";
                return ret;
            }
        }

        public string SpellName
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.Name;
                return ret;
            }
        }

        public string SpellCost
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Cost.ToString();
                return ret;
            }
        }

        public string SpellDescription
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.Description;
                return ret;
            }
        }

        public string SpellZOE
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.ZOE.DisplayValue;
                return ret;
            }
        }

        public decimal ExtraZOECost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = _spellCast.Spell.ZOEProperty.Modifier;
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        public bool ZOEIsModifiable
        {
            get
            {
                return _spellCast.Spell.ZOEProperty.Modifier != null;
            }
        }

        public bool ZOEIsNotModifiable
        {
            get
            {
                return _spellCast.Spell.ZOEProperty.Modifier == null;
            }
        }

        public string SpellRange
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Range;
                return ret;
            }
        }

        public decimal ExtraRangeCost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = GetRangeModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetRangeModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is RangeModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        public string SpellDuration
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Duration;
                return ret;
            }
        }

        public decimal ExtraDurationCost
        {
            get
            {
                decimal ret = 0.0M;

                IModifier modifier = GetDurationModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetDurationModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is DurationModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        public string SpellSavingThrow
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                    ret = _spellCast.Spell.SavingThrow;
                return ret;
            }
        }

        public decimal SpellDifficultyClass
        {
            get
            {
                decimal ret = 0M;

                ret = _spellCast.DifficultyClass;

                return ret;
            }
        }

        public bool HasSavingThrow
        {
            get
            {
                return _spellCast.DifficultyClass > 0M;
            }
        }

        public bool HasNoSavingThrow
        {
            get
            {
                return !HasSavingThrow;
            }
        }

        public decimal HardToSaveCost
        {
            get
            {
                decimal ret = 0M;

                IModifier modifier = GetSaveModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetSaveModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is SaveModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        public bool TakesAffectsOthersProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == "AffectsOthers"))
                {
                    ret = true;
                }

                return ret;
            }
        }
        public bool AffectsOthersProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == "AffectsOthers"))
                {
                    SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AffectsOthers");
                    if (_spellCast.GetNumModifiers(spell_prop.Modifier) > 0)
                        ret = true;
                }

                return ret;
            }
        }

        public decimal AffectsOthersCost
        {
            get
            {
                decimal ret = 1M;   // default cost
                // Check for spell-specific value
                if (_spellCast.Spell.ExtraPropertiesList.Any(p => p.Name == "AffectsOthers"))
                {
                    SpellProperty s_prop = _spellCast.Spell.ExtraPropertiesList.First(p => p.Name == "AffectsOthers");
                    IModifier mod = s_prop.Modifier;
                    ret = mod.Cost;
                }
                return ret;
            }
        }


        public bool TakesAtRangeProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == "AtRange"))
                {
                    ret = true;
                }

                return ret;
            }
        }
        public bool AtRangeProperty
        {
            get
            {
                bool ret = false;

                if (_spellCast.Spell.PropertiesList.Any(p => p.Name == "AtRange"))
                {
                    SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AtRange");
                    if (_spellCast.GetNumModifiers(spell_prop.Modifier) > 0)
                        ret = true;
                }

                return ret;
            }
        }

        public bool CastOnTheRun
        {
            get
            {
                return false;
            }
        }

        public bool Concealment
        {
            get
            {
                return false;
            }
        }

        public bool PowerWord
        {
            get
            {
                return false;
            }
        }

        public string ReducedGesturesProperty
        {
            get
            {
                return "Normal";
            }
        }

        /// <summary>
        /// Number of dice for damage for this casting
        /// </summary>
        public string DamageNumber
        {
            get
            {
                string ret = "";
                if (_spellCast.Damage != null)
                    ret = _spellCast.Damage.DisplayValue;
                return ret;
            }
        }

        /// <summary>
        /// Expectation value for damage for this casting
        /// </summary>
        /// <remarks>Calculated from damage property value of Cast</remarks>
        public string DamageExpectation
        {
            get
            {
                string ret = "";
                if (_spellCast != null)
                {
                    //string str = _spellCast.Spell.DamageDie;
                    if (_spellCast.Damage != null)
                    {
                        int damageDie = _spellCast.Damage.Dice.Sides;
                        int damageNum = (int)_spellCast.Damage.Value;
                        decimal expectation = (((decimal)damageDie + 1) / 2) * damageNum;
                        expectation += _spellCast.Damage.Plus;

                        ret = expectation.ToString();
                    }
                }
                return ret;
            }
        }

        public int ExtraDamageNum
        {
            get
            {
                int ret = 0;

                IModifier modifier = GetDamageModifier();
                ret = _spellCast.GetNumModifiers(modifier);

                return ret;
            }
        }

        public decimal ExtraDamageCost
        {
            get
            {
                decimal ret = 0;

                IModifier modifier = GetDamageModifier();
                if (modifier != null)
                    ret = modifier.Cost;

                return ret;
            }
        }

        private IModifier GetDamageModifier()
        {
            IModifier modifier = null;
            IList<IModifier> dm_list = new List<IModifier>(_spellCast.Spell.ModifierList.Where(m => m is DamageModifier));
            if (dm_list.Count > 0)
                modifier = dm_list[0];
            return modifier;
        }

        /// <summary>
        /// Signify we can't bump spell level any more
        /// </summary>
        private bool ReachedMaxSpellLevel
        {
            get
            {
                return (_spellCast.Level >= _spellCast.MaxLevel);
            }
        }

        #endregion

        /// <summary>
        /// Handle NumericUpDown counter for given property
        /// </summary>
        /// <param name="mod"></param>
        /// <param name="upDnCtl"></param>
        /// <param name="boundControlList">List of controls to update (besides standards)</param>
        /// <remarks>Used by standard properties</remarks>
        internal void HandleUpDownEvent(IModifier mod, NumericUpDown upDnCtl, IList<Control> boundControlList)
        {
            if (!upDnCtl.Enabled)   // prevent re-execution of handler if counter reset
                return;

            bool up_direction = true;  // get direction
            int new_val = (int)upDnCtl.Value;
            if (mod != null)
            {
                int cur_val = _spellCast.GetNumModifiers(mod);
                if (cur_val > new_val)
                    up_direction = false;

                // adjust number of damage modifiers
                if (up_direction)
                {
                    // Check we're not at max level
                    if (_spellCast.Level + mod.Cost > _spellCast.MaxLevel)
                    {
                        // MessageBox.Show("At max spell level");
                        MessageEvent(this, new MessageEventArgs("At max spell level"));

                        // Set control back
                        upDnCtl.Enabled = false;   // keep code from executing 2x
                        try
                        {
                            upDnCtl.Value = cur_val;   // keep value the same
                        }
                        finally
                        {
                            upDnCtl.Enabled = true;
                            upDnCtl.Focus();
                        }

                        return;
                    }
                    _spellCast.AddModifier(mod);
                }
                else
                    _spellCast.RemoveModifier(mod);

                // Adjust controls based on modifiers
                //_form.levelTxtBx.DataBindings[0].ReadValue();
                //_form.costTxtBx.DataBindings[0].ReadValue();
                //this.dcTxtBx.DataBindings[0].ReadValue();
                foreach (Control ctl in boundControlList)
                    ctl.DataBindings[0].ReadValue();
            }

            return;
        }

        /// <summary>
        /// Handle NumericUpDown counter for given property
        /// </summary>
        /// <param name="mod"></param>
        /// <param name="upDnCtl"></param>
        /// <param name="boundControlList">List of controls to update (besides standards)</param>
        /// <remarks>Property version -- needed for max value</remarks>
        internal void HandleUpDownEvent(SpellProperty spellProp, NumericUpDown upDnCtl, IList<Control> boundControlList)
        {
            if (!upDnCtl.Enabled)   // prevent re-execution of handler if counter reset
                return;

            IModifier mod = spellProp.Modifier;
            bool up_direction = true;  // get direction
            int new_val = (int)upDnCtl.Value;
            if (mod != null)
            {
                int cur_val = _spellCast.GetNumModifiers(mod);
                if (cur_val > new_val)
                    up_direction = false;

                // adjust number of damage modifiers
                if (up_direction)
                {
                    // Check we're not at max level
                    if (_spellCast.Level + mod.Cost > _spellCast.MaxLevel)
                    {
                        MessageEvent(this, new MessageEventArgs("At max spell level"));
                        RestoreUpDnControlValue(upDnCtl, cur_val);

                        return;
                    }
                    // Check we're not at max value
                    if (_spellCast.GetPropertyValue(spellProp).Equals(spellProp.MaxValue))
                    {
                        MessageEvent(this, new MessageEventArgs("At max property value"));
                        RestoreUpDnControlValue(upDnCtl, cur_val);

                        return;
                    }
                    _spellCast.AddModifier(mod);
                }
                else
                    _spellCast.RemoveModifier(mod);

                // Adjust controls based on modifiers
                //_form.levelTxtBx.DataBindings[0].ReadValue();
                //_form.costTxtBx.DataBindings[0].ReadValue();
                //this.dcTxtBx.DataBindings[0].ReadValue();
                foreach (Control ctl in boundControlList)
                    ctl.DataBindings[0].ReadValue();
            }

            return;
        }

        /// <summary>
        /// Restore NumericUpDown control w/o re-invoking handler
        /// </summary>
        /// <param name="upDnCtl"></param>
        /// <param name="cur_val"></param>
        private static void RestoreUpDnControlValue(NumericUpDown upDnCtl, int cur_val)
        {
            // Set control back
            upDnCtl.Enabled = false;   // keep code from executing 2x
            try
            {
                upDnCtl.Value = cur_val;   // keep value the same
            }
            finally
            {
                upDnCtl.Enabled = true;
                upDnCtl.Focus();
            }
        }

        /// <summary>
        /// Adjust for checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void affectOthersChkBx_CheckedChanged(object sender, EventArgs e)
        {
            // get modifier
            CheckBox check_box = sender as CheckBox;
            if (!check_box.Enabled)
                return;

            SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AffectsOthers");
            IModifier mod = spell_prop.Modifier;

            if (check_box.Checked)
            {
                bool ok = _spellCast.AddModifier(mod);
                if (!ok)
                {
                    MessageBox.Show("At max spell level");
                    check_box.Enabled = false;
                    try
                    {
                        check_box.Checked = false;
                    }
                    finally
                    {
                        check_box.Enabled = true;
                    }
                }
            }
            else
                _spellCast.RemoveModifier(mod);

            // Adjust controls based on modifiers
            foreach (Control ctl in _form.CommonControls)
            {
                ctl.DataBindings[0].ReadValue();
            }

            return;
        }

        internal void atRangeChkBx_CheckedChanged(object sender, EventArgs e)
        {
            // get modifier
            CheckBox check_box = sender as CheckBox;
            if (!check_box.Enabled)
                return;

            PresentationBinding binding = check_box.Tag as PresentationBinding;
            // SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "AtRange");
            SpellProperty spell_prop = binding.SpellProperty;
            IModifier mod = spell_prop.Modifier;

            if (check_box.Checked)
            {
                bool ok = _spellCast.AddModifier(mod);
                if (!ok)
                {
                    MessageBox.Show("At max spell level");
                    check_box.Enabled = false;
                    try
                    {
                        check_box.Checked = false;
                    }
                    finally
                    {
                        check_box.Enabled = true;
                    }
                }
            }
            else
                _spellCast.RemoveModifier(mod);

            // Adjust controls based on modifiers
            //foreach (Control ctl in _form.CommonControls)
            //{
            //    ctl.DataBindings[0].ReadValue();
            //}
            binding.UpdateControls();
        }

        internal void onRunChkBx_CheckedChanged(object sender, EventArgs e)
        {
            // get modifier
            CheckBox check_box = sender as CheckBox;
            if (check_box.Enabled == false) return;     // Don't handle if control disabled (keep from invoking 2x)

            SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "CastOnTheRun");
            IModifier mod = spell_prop.Modifier;

            if (check_box.Checked)
            {
                bool ok = _spellCast.AddModifier(mod);
                if (!ok)
                {
                    MessageBox.Show("At max spell level");
                    check_box.Enabled = false;
                    try
                    {
                        check_box.Checked = false;
                    }
                    finally
                    {
                        check_box.Enabled = true;
                    }
                }
            }
            else
                _spellCast.RemoveModifier(mod);

            // Adjust controls based on modifiers
            foreach (Control ctl in _form.CommonControls)
            {
                ctl.DataBindings[0].ReadValue();
            }
        }

        internal void concealChkBx_CheckedChanged(object sender, EventArgs e)
        {
            // get modifier
            CheckBox check_box = sender as CheckBox;
            if (check_box.Enabled == false) return;     // Don't handle if control disabled (keep from invoking 2x)

            SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "Concealment");
            IModifier mod = spell_prop.Modifier;

            if (check_box.Checked)
            {
                bool ok = _spellCast.AddModifier(mod);
                if (!ok)
                {
                    MessageBox.Show("At max spell level");
                    check_box.Enabled = false;
                    try
                    {
                        check_box.Checked = false;
                    }
                    finally
                    {
                        check_box.Enabled = true;
                    }
                }
            }
            else
                _spellCast.RemoveModifier(mod);

            // Adjust controls based on modifiers
            foreach (Control ctl in _form.CommonControls)
            {
                ctl.DataBindings[0].ReadValue();
            }
        }

        internal void powerWordChkBx_CheckedChanged(object sender, EventArgs e)
        {
            // get modifier
            CheckBox check_box = sender as CheckBox;
            if (check_box.Enabled == false) return;     // Don't handle if control disabled (keep from invoking 2x)

            SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "PowerWord");
            IModifier mod = spell_prop.Modifier;

            if (check_box.Checked)
            {
                bool ok = _spellCast.AddModifier(mod);
                if (!ok)
                {
                    MessageBox.Show("At max spell level");
                    check_box.Enabled = false;
                    try
                    {
                        check_box.Checked = false;
                    }
                    finally
                    {
                        check_box.Enabled = true;
                    }
                }
            }
            else
                _spellCast.RemoveModifier(mod);

            // Adjust controls based on modifiers
            foreach (Control ctl in _form.CommonControls)
            {
                ctl.DataBindings[0].ReadValue();
            }
        }

        /// <summary>
        /// Handle Reduced Gestures combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void redGesturesCmbBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo_box = sender as ComboBox;
            if (!combo_box.Enabled) return;

            int selected_num = combo_box.SelectedIndex; // number of times to apply this mod (0, 1 or 2)
            SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "ReducedGestures");
            IModifier mod = spell_prop.Modifier;
            int num_mods = _spellCast.GetNumModifiers(mod); // how many times is this mod already applied?
            if (selected_num > num_mods)    // need to apply more?
            {
                while (selected_num > num_mods)
                {
                    bool ok = _spellCast.AddModifier(mod);
                    if (!ok)
                    {
                        MessageEvent(this, new MessageEventArgs("At max spell level"));
                        combo_box.Enabled = false;
                        try
                        {
                            combo_box.SelectedIndex = _spellCast.GetNumModifiers(mod);
                        }
                        finally
                        {
                            combo_box.Enabled = true;
                            combo_box.Focus();
                        }

                        break;
                    }
                    num_mods++;
                }
            }
            else if (selected_num < num_mods)   // need to remove a mod (or 2)?
            {
                while (selected_num < num_mods)
                {
                    _spellCast.RemoveModifier(mod);
                    num_mods--;
                }
            }

            // Adjust controls based on modifiers
            foreach (Control ctl in _form.CommonControls)
            {
                ctl.DataBindings[0].ReadValue();
            }
        }

        internal void redIncantCmbBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo_box = sender as ComboBox;
            if (!combo_box.Enabled) return;

            int selected_num = combo_box.SelectedIndex;
            SpellProperty spell_prop = _spellCast.Spell.PropertiesList.First(p => p.Name == "ReducedIncantation");
            IModifier mod = spell_prop.Modifier;
            int num_mods = _spellCast.GetNumModifiers(mod);
            if (selected_num > num_mods)
            {
                while (selected_num > num_mods)
                {
                    bool ok = _spellCast.AddModifier(mod);
                    if (!ok)
                    {
                        MessageEvent(this, new MessageEventArgs("At max spell level"));
                        combo_box.Enabled = false;
                        try
                        {
                            combo_box.SelectedIndex = _spellCast.GetNumModifiers(mod);
                        }
                        finally
                        {
                            combo_box.Enabled = true;
                            combo_box.Focus();
                        }

                        break;
                    } 
                    num_mods++;
                }
            }
            else if (selected_num < num_mods)
            {
                while (selected_num < num_mods)
                {
                    _spellCast.RemoveModifier(mod);
                    num_mods--;
                }
            }

            // Adjust controls based on modifiers
            foreach (Control ctl in _form.CommonControls)
            {
                ctl.DataBindings[0].ReadValue();
            }
        }

        /// <summary>
        /// Update extra spell properties from listview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void extraPropNumUpDn_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown upDnCtl = sender as NumericUpDown;
            PresentationBinding pm_binding = upDnCtl.Tag as PresentationBinding;
            SpellProperty spell_prop = pm_binding.SpellProperty;
            IModifier mod = spell_prop.Modifier;
#if false
            bool up_direction = true;  // get direction
            int new_val = (int)upDnCtl.Value;
            if (mod != null)
            {
                int cur_val = _spellCast.GetNumModifiers(mod);
                if (cur_val > new_val)
                    up_direction = false;

                // adjust number of damage modifiers
                if (up_direction)
                {
                    // Check for maximum value
                    if (spell_prop.MaxValue != null)
                    {
                        if (_spellCast.GetPropertyValue(spell_prop).Equals(spell_prop.MaxValue))
                        {
                            MessageBox.Show("At max spell value");
                            upDnCtl.ValueChanged -= extraPropNumUpDn_ValueChanged;
                            upDnCtl.Value = cur_val;   // keep value the same
                            upDnCtl.ValueChanged += extraPropNumUpDn_ValueChanged;
                            return;
                        }
                    }

                    // Check we're not at max level
                    if (_spellCast.Level >= _spellCast.MaxLevel)
                    {
                        MessageBox.Show("At max spell level");
                        upDnCtl.ValueChanged -= extraPropNumUpDn_ValueChanged;
                        upDnCtl.Value = cur_val;   // keep value the same
                        upDnCtl.ValueChanged += extraPropNumUpDn_ValueChanged;
                        return;
                    }
                    _spellCast.AddModifier(mod);
                }
                else
                    _spellCast.RemoveModifier(mod);

                // Adjust controls based on modifiers
                //this.levelTxtBx.DataBindings[0].ReadValue();
                //this.costTxtBx.DataBindings[0].ReadValue();
                foreach (Control ctl in _commonControls)
                    ctl.DataBindings[0].ReadValue();

                // Also damage values (magic missle)
                this.damageNumberTxtBx.DataBindings[0].ReadValue();
                this.expectTxtBx.DataBindings[0].ReadValue();
                // boundControlList.ForEach(ctl => ctl.DataBindings[0].ReadValue());

                // Get property value control in listbox
                // ContainerListViewItem clvi = upDnCtl.Parent as ContainerListViewItem;
                // ContainerListViewItem match_item = null;
                foreach (ContainerListViewItem item in _clv.Items)
                {
                    if (item.SubItems[2].ItemControl == upDnCtl)    // get list item containing this updown control
                    {
                        item.SubItems[0].Text = _spellCast.GetPropertyValue(spell_prop).DisplayValue;
                        break;
                    }
                }
            }
#endif

            NumericUpDown updn_ctl = sender as NumericUpDown;
            HandleUpDownEvent(spell_prop, updn_ctl, pm_binding.BoundControls);
            //foreach (ContainerListViewItem item in _clv.Items)
            //{
            //    if (item.SubItems[2].ItemControl == upDnCtl)    // get list item containing this updown control
            //    {
            //        item.SubItems[0].Text = _spellCast.GetPropertyValue(spell_prop).DisplayValue;
            //        //item.SubItems[0].ItemControl.DataBindings[0].ReadValue();
            //        break;
            //    }
            //}
            //pm_binding.UpdateControls();
        }

        /// <summary>
        /// For Base Spell Levels on Morphic spells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void onBaseLevelChanged(object sender, EventArgs e)
        {
            ComboBox combo_box = sender as ComboBox;
            if (!combo_box.Enabled) return;

            int selected_num = combo_box.SelectedIndex;
            SpellProperty spell_prop = _spellCast.Spell.BaseLevel;
            IModifier mod = spell_prop.ModifierList.First(m => m.Name == combo_box.Text);
            bool ok = _spellCast.SetModifier(mod);

            // Adjust controls based on modifiers
            foreach (Control ctl in _form.CommonControls)
            {
                ctl.DataBindings[0].ReadValue();
            }
        }

        /// <summary>
        /// For Base Spell Levels on Morphic spells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void onAppChanged(object sender, EventArgs e)
        {
            ComboBox combo_box = sender as ComboBox;
            if (!combo_box.Enabled) return;

            int selected_num = combo_box.SelectedIndex;
            SpellProperty spell_prop = _spellCast.Spell.ExtraPropertiesList.First(p => p.Name == "Applications");
            IModifier mod = spell_prop.ModifierList.First(m => m.IncrementValue.DisplayValue == combo_box.Text);
            bool ok = _spellCast.SetModifier(mod);

            // Adjust controls based on modifiers
            foreach (Control ctl in _form.CommonControls)
            {
                ctl.DataBindings[0].ReadValue();
            }
        }


        /// <summary>
        /// Spell properties handled specially in UI
        /// </summary>
        public static IList<SpellProperty> SpecialHandlerProperties
        {
            get
            {
                List<SpellProperty> ret = new List<SpellProperty>();

                // Add standard properties
                SpellProperty property = new SpellProperty("Applications", new DescribedValue("no"));
                ret.Add(property);

                return ret;
            }
        }
    }
}
