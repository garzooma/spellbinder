﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.RangeModifier</name>
    /// <summary>
    /// Description for RangeModifier
    /// </summary>
    /// <version>1.0 - Initial Release - 1/11/2013 4:12:33 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class RangeModifier : Modifier
    {
        #region IModifier Members
        private NumericValue[] _schedule;

        /// <summary>
        /// Create default modifier
        /// </summary>
        /// <param name="doc"></param>
        public RangeModifier(NumericValue[] schedule) : base("Range", 0.5M, "")
        {
            _schedule = schedule;
        }

        /// <summary>
        /// Create default modifier
        /// </summary>
        /// <param name="doc"></param>
        public RangeModifier(IPropertyValue startValue)
            : base("Duration", 0.5M)
        {
            if (startValue is NumericValue)
            {
                _schedule = GetSchedule(startValue);
            }

            return;
        }

        public override IPropertyValue Bump(IPropertyValue startValue)
        {
            IPropertyValue ret = startValue;
            if (startValue is NumericValue)
            {
                NumericValue num_val = startValue as NumericValue;

                if ((_schedule == null)  // any schedule?
                    || isOverMax(_schedule, startValue))
                {
                    // double if not on schedule
                    string unit_val = num_val.Units;
                    if (unit_val != "feet" && !unit_val.EndsWith("s"))   // pluralize
                        unit_val += "s";
                    ret = new NumericValue(num_val.Value * 2, unit_val);
                }
                else
                {
                    bool match = false;
                    foreach (NumericValue val in _schedule)
                    {
                        if (match)  // previous value matched
                        {
                            ret = val;  // return next value
                            break;
                        }
                        if (val.Equals(startValue))
                            match = true;
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// At or over max value on schedule
        /// </summary>
        /// <param name="_schedule"></param>
        /// <param name="startValue"></param>
        /// <returns></returns>
        private bool isOverMax(NumericValue[] _schedule, IPropertyValue startValue)
        {
            bool ret = true;

            NumericValue max_value = _schedule[_schedule.Length - 1];
            if (startValue.CompareTo(max_value) < 0)
                ret = false;

            return ret;
        }

        public override IPropertyValue IncrementValue
        {
            get { return new NumericValue(0, null); }
        }

        #endregion

        internal static NumericValue[] Schedule1 { get { return _schedule1; } }
        private static NumericValue[] _schedule1 = {
                            new NumericValue(30, "feet"),
                            new NumericValue(60, "feet"),
                            new NumericValue(120, "feet"),
                            new NumericValue(240, "feet"),
                            new NumericValue(480, "feet"), 
                            new NumericValue(.25M, "mile"),
                            new NumericValue(.5M, "mile"),
                            new NumericValue(1, "mile")};
        private static NumericValue[] _schedule2 = {
                            new NumericValue(20, "feet"),
                            new NumericValue(40, "feet"),
                            new NumericValue(90, "feet"),
                            new NumericValue(180, "feet"),
                            new NumericValue(360, "feet"), 
                            new NumericValue(720, "feet"), 
                            new NumericValue(.25M, "mile"),
                            new NumericValue(.5M, "mile"),
                            new NumericValue(1, "mile")};

        /// <summary>
        /// Check value if on schedule
        /// </summary>
        /// <param name="Duration"></param>
        /// <returns></returns>
        internal static NumericValue[] GetSchedule(IPropertyValue range)
        {
            NumericValue[] ret = null;
            if (_schedule1.Contains(range))
                ret = _schedule1;
            else if (_schedule2.Contains(range))
                ret = _schedule2;

            return ret;
        }
    }
}
