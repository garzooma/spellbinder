﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    public partial class MorphicUC : UserControl
    {
        public MorphicUC()
        {
            InitializeComponent();
        }

        internal void AddLevels(SpellProperty levelProp)
        {
            foreach (IModifier mod in levelProp.ModifierList)
            {
                this.morphicLvlCmbBx.Items.Add(mod.Name);
            }

            morphicLvlCmbBx.Text = levelProp.ModifierList[0].Name;
        }

        internal void AddApplications(IList<IModifier> iList)
        {
            foreach (IModifier mod in iList)
            {
                this.patAppCmbBx.Items.Add(mod.IncrementValue.DisplayValue);
            }

            patAppCmbBx.Text = iList[0].IncrementValue.DisplayValue;
        }

        internal ComboBox LevelCmbBx
        {
            get
            {
                return morphicLvlCmbBx;
            }
        }

        internal ComboBox AppCmbBx
        {
            get
            {
                return patAppCmbBx;
            }
        }
    }
}
