﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    public class SpellBook
    {
        private Dictionary<string, ISpell> _spellMap = new Dictionary<string, ISpell>();
        public SpellBook(string fileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            PopulateSpellMap(doc);
        }

        /// <summary>
        /// For testing
        /// </summary>
        /// <param name="doc"></param>
        internal SpellBook(XmlDocument doc)
        {
            PopulateSpellMap(doc);
        }

        private void PopulateSpellMap(XmlDocument doc)
        {
            XmlNode book_node = doc.FirstChild;
            foreach (XmlNode spell_node in book_node.ChildNodes)
            {
                BaseSpell base_spell = new BaseSpell(spell_node);
                _spellMap[base_spell.Name] = base_spell;
            }
        }

        public ISpell this[string name] { get { return _spellMap[name]; } }

        public IList<ISpell> List { get { return _spellMap.Values.ToList(); } }

        static public SpellBook Book
        {
            get
            {
                if (_book == null)
                    _book = new SpellBook("spellbook.xml");

                return _book;
            }
        }
        static private SpellBook _book = null;

        public bool Contains(string spellName)
        {
            return _spellMap.ContainsKey(spellName);
        }
    }
}
