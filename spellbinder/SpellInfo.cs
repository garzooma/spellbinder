using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;

namespace ArzooSoftware.SpellBinder.spellbinder
{
	/// <summary>
	/// Wrapper for spell info
	/// </summary>
	public class SpellInfo
	{
		XmlNode _node;
		public SpellInfo(XmlNode node)
		{
			_node = node;
		}

		public decimal Level
		{
			get
			{
                decimal ret = 0.0M;
				// Get Level node
				XmlAttribute attr = _node.Attributes["level"];
                if (attr != null)
                {
                    decimal.TryParse(attr.Value, out ret);
                }
				return ret;
			}
		}

        public bool IsMorphic
        {
            get
            {
                bool ret = false;
                // Get Level node
                XmlAttribute attr = _node.Attributes["level"];
                if (attr != null)
                {
                    ret = (attr.Value.ToUpper() == "MORPHIC");
                }
                return ret;
            }
        }

		public string Name
		{
			get
			{
				string ret = "";
				// Get Level node
				XmlAttribute attr = _node.Attributes["name"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string Description
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("description");
				XmlText txtNode = descNode.FirstChild as XmlText;
				if (txtNode != null)
					ret = txtNode.Value;
				return ret;
			}
		}

        /// <summary>
        /// Spell category
        /// </summary>
        /// <remarks>As described on character sheets</remarks>
        public string Category
        {
            get
            {
                string ret = "Misc";
                // Get Level node
                XmlAttribute attr = _node.Attributes["category"];
                if (attr != null)
                {
                    ret = attr.Value;
                }
                return ret;
            }
        }

        /// <summary>
        /// Modifiers section of spell description
        /// </summary>
        public string ModifiersDescription
        {
            get
            {
                string ret = "";
                XmlNode descNode = _node.SelectSingleNode("modifiers_description");
                if (descNode != null)
                {
                    XmlText txtNode = descNode.FirstChild as XmlText;
                    if (txtNode != null)
                        ret = txtNode.Value;
                }
                return ret;
            }
        }

		public string ZOEValue
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("ZOE");
				XmlAttribute attr = descNode.Attributes["value"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

        public string ZOEType
        {
            get
            {
                string ret = "";
                XmlNode descNode = _node.SelectSingleNode("ZOE");
                XmlAttribute attr = descNode.Attributes["type"];
                if (attr != null)
                    ret = attr.Value;
                return ret;
            }
        }

		public string ZOEUnits
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("ZOE");

				XmlAttribute attr = descNode.Attributes["units"];
				if (attr != null)
					ret = attr.Value;

				return ret;
			}
		}

		public IPropertyValue RangeValue
		{
			get
			{
				XmlNode descNode = _node.SelectSingleNode("Range");

				return GetPropertyValue(descNode);
			}
		}

		public IPropertyValue DurationValue
		{
			get
			{
				XmlNode descNode = _node.SelectSingleNode("Duration");
				return GetPropertyValue(descNode);
			}
		}

        /// <summary>
        /// Get modifier for duration
        /// </summary>
        public IModifier GetDurationModifier(SpellProperty spell_prop)
        {
            IModifier ret = null;
            XmlNode modNode = _node.SelectSingleNode("Duration/Modifier");
            if (modNode != null)
            {
                XmlAttribute attr = modNode.Attributes["type"];
                string mod_type = attr.Value;
                decimal cost = 0M;
                attr = modNode.Attributes["cost"];
                if (attr != null)
                {
                    decimal.TryParse(attr.Value, out cost);
                }

                // Get increment value from node
                IPropertyValue inc_val = GetPropertyValue(modNode);
                // Check for Modifier Description
                XmlElement mod_desc_node = modNode.SelectSingleNode("Description") as XmlElement;
                string mod_descr = String.Empty;
                if (mod_desc_node != null)
                {
                    mod_descr = mod_desc_node.FirstChild.Value;
                }
                ret = new SpellModifier(mod_type, cost, inc_val, spell_prop, mod_descr);
            }

            return ret;
        }

		public string SavingThrowValue
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("SavingThrow");
				XmlAttribute attr = descNode.Attributes["value"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

		public string SavingThrowEffect
		{
			get
			{
				string ret = "";
				XmlNode descNode = _node.SelectSingleNode("SavingThrow");
				XmlAttribute attr = descNode.Attributes["effect"];
				if (attr != null)
					ret = attr.Value;
				return ret;
			}
		}

        /// <summary>
        /// Get base value for damage
        /// </summary>
        public DiceValue Damage
        {
            get
            {
                DiceValue ret = null;

                XmlNode node = _node.SelectSingleNode("Damage");
                if (node != null)
                {
                    ret = new DiceValue(node);
                }

                return ret;
            }
        }

        /// <summary>
        /// List of modifiers explicitly defined in spell xml
        /// </summary>
        public IList<IModifier> Modifiers
        {
            get
            {
                List<IModifier> ret = new List<IModifier>();

                // For now, just set for lance
                foreach (XmlNode modNode in _node.SelectNodes("Modifiers/Modifier"))
                {
                    IModifier mod = new DamageModifier(modNode);
                    ret.Add(mod);
                }

                return ret;
            }
        }

        /// <summary>
        /// List of special properties of spell
        /// </summary>
        public IList<SpellProperty> ExtraProperties
        {
            get
            {
                List<SpellProperty> ret = new List<SpellProperty>();

                // Loop on named properties
                foreach (XmlNode propNode in _node.SelectNodes("Properties/Property"))
                {
                    string name = String.Empty;
                    XmlAttribute attr = propNode.Attributes["name"];
                    if (attr != null)
                        name = attr.Value;

                    // Get value from node
                    IPropertyValue prop_val = GetPropertyValue(propNode);

                    // Check for max
                    attr = propNode.Attributes["max"];
                    IPropertyValue max_propval = null;
                    if (attr != null)
                    {
                        int max_val = 0;
                        bool is_num = int.TryParse(attr.Value, out max_val);
                        if (is_num)
                            max_propval = new NumericValue(max_val, null);
                        else
                            max_propval = new DescribedValue(attr.Value);
                    }

                    SpellProperty spell_prop = new SpellProperty(name, prop_val, null, max_propval);
                    ret.Add(spell_prop);

                    // Now take Modifier node of SpellProperty
                    // XmlNode modNode = propNode.SelectSingleNode("Modifier");
                    List<IModifier> mod_list = new List<IModifier>();
                    IList<IPropertyValue> val_list = new List<IPropertyValue>();
                    foreach (XmlNode modNode in propNode.SelectNodes("Modifier"))   // multiple modifiers if enumerated (e.g. Hallucination)
                    {
                        attr = modNode.Attributes["type"];
                        string mod_type = attr.Value;
                        decimal cost = 0M;
                        attr = modNode.Attributes["cost"];
                        if (attr != null)
                        {
                            decimal.TryParse(attr.Value, out cost);
                        }
                        
                        // Get increment value from node
                        IPropertyValue inc_val = GetPropertyValue(modNode);
                        val_list.Add(inc_val);  // add to list to make schedule
                        // Check for Modifier Description
                        XmlElement mod_desc_node = modNode.SelectSingleNode("Description") as XmlElement;
                        string mod_descr = String.Empty;
                        if (mod_desc_node != null)
                        {
                            mod_descr = mod_desc_node.FirstChild.Value;
                        }
                        IModifier mod = new SpellModifier(mod_type, cost, inc_val, spell_prop, mod_descr);
                        mod_list.Add(mod);
                    }
                    if (mod_list.Count > 0)
                    {
                        if (mod_list.Count == 1)    // normal case
                        {
                            spell_prop.Modifier = mod_list[0];
                        }
                        else
                        {
                            IList<DescribedValue> d_list = new List<DescribedValue>(val_list.Select(v => v as DescribedValue));
                            DescribedValue[] schedule = NamedValue.CreateSchedule(d_list);
                            foreach (IModifier mod in mod_list)
                            {
                                NamedValue named_val = new NamedValue(mod.IncrementValue.DisplayValue, schedule);
                                SpellModifier morphic_mod = new SpellModifier(mod.Name, mod.Cost, named_val, spell_prop);
                                spell_prop.AddModifier(morphic_mod);
                            }
                        }
                    }
                }

                return ret;
            }
        }

        /// <summary>
        /// Return a property value for given node.
        /// </summary>
        /// <param name="propNode"></param>
        /// <returns></returns>
        private static IPropertyValue GetPropertyValue(XmlNode propNode)
        {
            IPropertyValue prop_val = null;
            if (propNode != null)
            {
                XmlAttribute die_attr = propNode.Attributes["die"];
                if (die_attr != null)
                {
                    prop_val = new DiceValue(propNode);
                }
                else
                {
                    XmlAttribute val_attr = propNode.Attributes["value"];
                    if (val_attr != null)
                    {
                        int num_val;
                        bool is_num = int.TryParse(val_attr.Value, out num_val);
                        if (is_num)
                        {
                            string units = null;
                            XmlAttribute attr = propNode.Attributes["units"];
                            if (attr != null)
                                units = attr.Value;
                            prop_val = new NumericValue(num_val, units);
                        }
                        else
                        {
                            prop_val = new DescribedValue(val_attr.Value);
                        }
                    }
                }
            }

            return prop_val;
        }

        /// <summary>
        /// List of base levels: name/value pairs.
        /// </summary>
        public SpellProperty BaseLevel
        {
            get
            {
                SpellProperty spell_prop = null;
                List<KeyValuePair<string, int>> ret = new List<KeyValuePair<string, int>>();
                foreach (XmlNode lvlNode in _node.SelectNodes("BaseLevels/Level"))
                {
                    // Get name, level
                    string name = lvlNode.Attributes["name"].Value;
                    int level;
                    int.TryParse(lvlNode.Attributes["level"].Value, out level);
                    KeyValuePair<string, int> base_level = new KeyValuePair<string,int>(name, level);
                    ret.Add(base_level);
                }

                if (ret.Count > 0)
                {
                    DescribedValue[] level_schedule = ret.Select(p => new DescribedValue(p.Key)).ToArray();
                    NamedValue n_val = new NamedValue(ret[0].Key, ret[0].Value, level_schedule);
                    spell_prop = new SpellProperty("BaseLevel", n_val, null, null);
                    foreach (KeyValuePair<string, int> name_val in ret)
                    {
                        n_val = new NamedValue(name_val.Key, name_val.Value, level_schedule);
                        SpellModifier mod = new SpellModifier(name_val.Key, 0, n_val, spell_prop);  // no cost <= modifier changes level thru value
                        // SpellModifier mod = new SpellModifier(name_val.Key, name_val.Value, n_val, spell_prop);  // no cost <= modifier changes level thru value
                        spell_prop.ModifierList.Add(mod);
                    }
                }

                return spell_prop;
            }
        }

        /// <summary>
        /// List of possible base levels for morphic spells
        /// </summary>
        public IList<SpellInfo> MorphList
        {
            get
            {
                List<SpellInfo> ret = new List<SpellInfo>();

                foreach (XmlNode spell_node in _node.SelectNodes("MorphList/spell"))
                {
                    // Get base spell-let's
                    SpellInfo morph = new SpellInfo(spell_node);
                    ret.Add(morph);
                }

                return ret;
            }
        }

        public override string ToString()
        {
            return Name;
        }

	}

}
