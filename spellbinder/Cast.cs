﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Linq;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.Cast</name>
    /// <summary>
    /// Represents a spell as actually cast, with all mods.
    /// </summary>
    /// <version>1.0 - Initial Release - 1/7/2013 12:32:43 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class Cast
    {
        private ISpell _spell;  // base spell as written up in the spellbook
        private MorphMinder _morpher = null;   // handle setting of base spell for Morphics

        /// <summary>
        /// Return base spell -- transmuted if necessary
        /// </summary>
        public ISpell Spell 
        { 
            get 
            {
                ISpell ret = _spell;

                return ret; 
            } 
        }

        private Mage _caster;     // mage level
        public Mage Mage { get { return _caster; } }
        private List<IModifier> _modifiers = new List<IModifier>();

        /// <summary>
        /// Retrieve the extra levels for the spellcasts modifiers
        /// </summary>
        private decimal ModifiersLevelCost
        {
            get
            {
                return _modifiers.Sum(c => c.Cost);
            }
        }


        public Cast(ISpell base_spell, Mage mage)
        {
            _spell = base_spell;
            _caster = mage;

            // See about adding base level if Morphic
            if (_spell.IsMorphic)
            {
                //if (_spell.MorphicBaseNamesList.Count > 0)
                //{
                //    string morph_name = _spell.MorphicBaseNamesList[0];
                //    ISpell morphic_base = _spell.GetMorphicBase(morph_name);
                //    _spell = new XMuteSpell(base_spell, morphic_base);
                //}
                _morpher = _spell.MorphMinder;
                if (_morpher.MorphicBaseNamesList.Count > 0)
                {
                    string morph_name = _morpher.MorphicBaseNamesList[0];
                    SetMorphicBase(morph_name);
                }

            }
        }

        /// <summary>
        /// Set base for a morphic spell
        /// </summary>
        /// <param name="name"></param>
        public void SetMorphicBase(string morph_name)
        {
            MorphMinder.MorphicBaseSpell morphic_base = _morpher.GetMorphicBase(morph_name);
            ISpell new_base = new XMuteSpell(_spell.MorphMinder.Base, morphic_base);
            decimal new_level = new_base.Level + ModifiersLevelCost;    // new level of spell (w/modifiers)
            if (new_level > CostCalc.GetMaxLevel(_caster.Level))
            {
                throw new Cast.BadStateException(BadState.MaxLevel, String.Format("Can't set morphic spell '{0}' to base '{1}'; resulting level '{2}' too high for mage of level '{3}'",
                    _spell.Name, morph_name, new_level, _caster.Level));
            }
            _spell = new_base;
            int check_cost; // see if this a legal spell value
            check_cost = this.Cost;
        }

        ///// <summary>
        ///// Set base of morphic spell
        ///// </summary>
        ///// <param name="name"></param>
        //public void SetMorphicBase(string name)
        //{
        //    // Get morphic base
        //    ISpell morph = Spell.GetMorphicBase(name);
        //    XMuteSpell new_base = new XMuteSpell(Spell, morph);
        //    return;
        //}

        /// <summary>
        /// Build new copy of Cast from another cast
        /// </summary>
        /// <param name="cast"></param>
        /// <remarks>Used in building lists</remarks>
        public Cast(Cast cast) : this(cast.Spell, cast._caster)
        {
            // Copy all the modifiers
            foreach (IModifier mod in cast._modifiers)
                _modifiers.Add(mod);
        }

        /// <summary>
        /// Level of spell after modifiers
        /// </summary>
        public decimal Level
        {
            get
            {
                // decimal ret = _spell.Level;   // start w/base spell
                decimal ret = Spell.Level;       // spell level, as adjusted for mutations

                //foreach (IModifier mod in _modifiers)
                //{
                //    ret += mod.Cost;
                //}
                ret += ModifiersLevelCost;

                return ret;
            }
        }

        /// <summary>
        /// Calculate cost in spell points based on mage level and modifiers
        /// </summary>
        public int Cost
        {
            get
            {
                CostCalc calc = new CostCalc(Level);
                return calc.GetCost(_caster.Level);
            }
        }

        /// <summary>
        /// Maximum spell points based for this mage level
        /// </summary>
        public decimal MaxLevel
        {
            get
            {
                return CostCalc.GetMaxLevel(_caster.Level);
            }
        }

        /// <summary>
        /// Value of damage in Dice
        /// </summary>
        public DiceValue Damage
        {
            get
            {
                DiceValue ret = new DiceValue(0, 0);
                if (_spell.Damage != null)
                    ret = _spell.Damage;  // get base damage value

                // Add damage modifiers
                foreach (IModifier modifier in _modifiers.Where(m => m is DamageModifier))
                {
                    ret = (DiceValue)modifier.Bump(ret);
                }

                // Adjust plus on dice
                foreach (IModifier modifier in _modifiers.Where(m => m.Name == "ExtraPlus"))
                {
                    //IPropertyValue bump_num = modifier.IncrementValue;
                    //ret = new DiceValue((int)ret.Value, ret.Units.Sides, (int)(ret.Plus + bump_num.CalculationValue));
                    ret = (DiceValue)modifier.Bump(ret);
                }

                return ret;
            }
        }

        public IPropertyValue ZOE
        {
            get
            {
                IPropertyValue ret = _spell.ZOEProperty.Value;  // get base value

                // loop on ZOE modifiers
                foreach (ZOEModifier mod in _modifiers.Where(m => m is ZOEModifier))
                {
                    ret = mod.Bump(ret);
                }
                return ret;
            }
        }

        /// <summary>
        /// Duration of spell as cast
        /// </summary>
        public string Duration
        {
            get
            {
                IPropertyValue ret = _spell.DurationProperty.Value;   // get base

                // Check for lasting modifier
                if (_modifiers.Any(m => m.Name == "Lasting"))
                {
                    return "Lasting";
                }

                // loop on duration modifiers
                foreach (IModifier mod 
                    in _modifiers.Where(m => m is DurationModifier 
                        || (_spell.DurationProperty.Modifier != null &&
                            m.PropertyName == _spell.DurationProperty.Modifier.PropertyName)
                            )
                         )
                {
                    ret = mod.Bump(ret);
                }

                return ret.DisplayValue;
            }
        }

        private bool IsBumpedToLasting(IPropertyValue inVal)
        {
            bool ret = false;

            if (DurationModifier.LastingValue.Equals(inVal))
            {
                ret = true;
            }
            else if (inVal is NumericValue && inVal.Equals(new NumericValue(16, "days")))
            {
                ret = true;
            }

            return ret;
        }

        /// <summary>
        /// Duration as IPropertyValue
        /// </summary>
        /// <remarks>Used in DurationView</remarks>
        public IPropertyValue DurationPropertyValue
        {
            get
            {
                IPropertyValue ret = _spell.DurationProperty.Value;   // get base
                if (ret is NumericValue)
                {
                    // Check for lasting modifier
                    if (_modifiers.Any(m => m.Name == "Lasting"))
                    {
                        return new DescribedValue("Lasting");
                    }

                    ret = GetDurationNumericValue();
                }

                return ret;
            }
        }

        public IPropertyValue GetDurationNumericValue()
        {
            NumericValue numeric_val = _spell.DurationProperty.Value as NumericValue;
            IPropertyValue ret = new NumericValue(numeric_val.Value, numeric_val.Units);

            // loop on duration modifiers
            foreach (IModifier mod
                in _modifiers.Where(m => m is DurationModifier
                    || (_spell.DurationProperty.Modifier != null &&
                        m.PropertyName == _spell.DurationProperty.Modifier.PropertyName)
                        )
                     )
            {
                ret = mod.Bump(ret);
            }

            return ret;
        }

        /// <summary>
        /// Distance of spell as cast
        /// </summary>
        public string Range
        {
            get
            {
                IPropertyValue ret = Spell.RangeProperty.Value;   // get base

                // Check for At Range modifier
                //if (_modifiers.Any(m => m is SpellModifier && m.Name == "AtRange"))
                //    ret = new NumericValue(60, "feet");

                // loop on duration modifiers
                foreach (RangeModifier mod in _modifiers.Where(m => m is RangeModifier))
                {
                    ret = mod.Bump(ret);
                }

                return ret.DisplayValue;
            }
        }

        /// <summary>
        /// Duration as IPropertyValue
        /// </summary>
        /// <remarks>Used in RangeView</remarks>
        public IPropertyValue RangePropertyValue
        {
            get
            {
                IPropertyValue ret = _spell.RangeProperty.Value;   // get base
                if (ret is NumericValue)
                {
                    NumericValue numeric_val = ret as NumericValue;
                    ret = new NumericValue(numeric_val.Value, numeric_val.Units);

                    // loop on duration modifiers
                    foreach (IModifier mod
                        in _modifiers.Where(m => m is RangeModifier
                            || (_spell.RangeProperty.Modifier != null &&
                                m.PropertyName == _spell.RangeProperty.Modifier.PropertyName)
                                )
                             )
                    {
                        ret = mod.Bump(ret);
                    }
                }

                return ret;
            }
        }

        /// <summary>
        /// Difficulty for Saving Throw
        /// </summary>
        public decimal DifficultyClass
        {
            get
            {
                decimal ret = 0;   // start w/DC = 10
                if (_spell.SavingThrow != "none")
                {

                    ret = 10;   // start w/DC = 10
                    ret += _caster.AbilityBonus;
                    ret += Level;

                    // loop on save modifiers
                    IPropertyValue mod_val = new NumericValue(0, null);
                    foreach (SaveModifier mod in _modifiers.Where(m => m is SaveModifier))
                    {
                        mod_val = mod.Bump(mod_val);
                    }
                    ret += mod_val.CalculationValue;
                }
                return ret;
            }
        }

        public string Category
        {
            get
            {
                return _spell.Category;
            }
        }

        /// <summary>
        /// Get value for given property.
        /// </summary>
        /// <param name="spellProperty"></param>
        /// <returns></returns>
        public IPropertyValue GetPropertyValue(SpellProperty spellProperty)
        {
            IPropertyValue ret = spellProperty.Value;   // start w/base value

            // Adjust for modifiers
            foreach (IModifier prop_mod in _modifiers.Where(m => m == spellProperty.Modifier))
            {
                ret = prop_mod.Bump(ret);
            }

            return ret;
        }

        /// <summary>
        /// Get number of modifiers of a particular type
        /// </summary>
        /// <param name="modifier"></param>
        /// <returns></returns>
        public int GetNumModifiers(IModifier modifier)
        {
            int ret = 0;
            if (modifier == null)
                ret = _modifiers.Count;
            else
            {
                // ret = _modifiers.Count(m => m.GetType() == modifier.GetType());
                ret = _modifiers.Count(m => m.Name == modifier.Name);
            }

            return ret;
        }

        public int GetNumModifiers()
        {
            return GetNumModifiers(null);
        }

        /// <summary>
        /// Add modifiers for calculating costs and effects on spell properties.
        /// </summary>
        /// <param name="modifier"></param>
        public bool AddModifier(IModifier modifier)
        {
            bool ok = false;
            decimal extra_cost = modifier.Cost;
            if (modifier.PropertyName == "BaseLevel")   // see if we're modifying base level for morphic spell
                extra_cost = modifier.IncrementValue.CalculationValue;
            if (Level + extra_cost > MaxLevel)
            {
                throw new BadStateException(BadState.MaxLevel, String.Format("Adding modifier {0} with cost {1} > MaxLevel {2}", modifier.Name, modifier.Cost, MaxLevel));
            }
            else
            {
                // Check against property max
                if (modifier is SpellModifier)
                {
                    SpellModifier spell_mod = modifier as SpellModifier;
                    SpellProperty spell_prop = spell_mod.Property;
                    if (spell_prop != null
                        && GetPropertyValue(spell_prop).Equals(spell_prop.MaxValue))
                    {
                            // return false;
                        throw new BadStateException(BadState.MaxValue, String.Format("Can't add modifier {0} to property {1} at max value {2}", modifier.Name, spell_prop.Name, spell_prop.MaxValue));
                    }
                }
                else if (modifier is DurationModifier)
                {
                    if (_spell.DurationProperty.Value.CompareTo(new NumericValue(1, "day")) > 0)
                    {
                        throw new BadStateException(BadState.Invalid, String.Format("Can't add modifier {0} to property {1} because base value {2} is too long", modifier.Name, "Duration", _spell.DurationProperty.Value));
                    }
                    //if (DurationPropertyValue.Equals(_spell.DurationProperty.MaxValue))
                    if (DurationPropertyValue.CompareTo(new NumericValue(6, "hours"))>=0)
                    {
                        // return false;
                        throw new BadStateException(BadState.MaxValue, String.Format("Can't add modifier {0} to property {1} at max value {2}", modifier.Name, "Duration", _spell.DurationProperty.MaxValue));
                    }
                }

                _modifiers.Add(modifier);
                //if (DurationPropertyValue.CompareTo(_spell.DurationProperty.MaxValue) > 0)
                //{
                //    // return false;
                //    _modifiers.Remove(modifier);
                //    throw new BadStateException(BadState.MaxValue, String.Format("Can't add modifier {0} to property {1} at max value {2}", modifier.Name, "Duration", _spell.DurationProperty.MaxValue));
                //}

                ok = true;
            }

            return ok;
        }

        /// <summary>
        /// Remove last modifier
        /// </summary>
        /// <param name="mod"></param>
        public bool RemoveModifier(IModifier mod)
        {
            bool ok = false;
            // remove last damage modifier
            if (_modifiers.Count > 0)       // NOTE: this probably shouldn't happen, but I've seen it
            {
                IModifier last_mod = _modifiers.Last(m => m.Name == mod.Name);
                ok = _modifiers.Remove(last_mod);
            }

            return ok;
        }

        /// <summary>
        /// Set modifier, replacing existing modifier w/same name
        /// </summary>
        /// <param name="mod"></param>
        /// <returns></returns>
        /// <remarks>Used in Morphic spells</remarks>
        public bool SetModifier(IModifier mod)
        {
            bool ret = false;

            IModifier remove_mod = null;
            if (_modifiers.Any(m => m.PropertyName == mod.PropertyName))
            {
                remove_mod = _modifiers.First(m => m.PropertyName == mod.PropertyName);
                _modifiers.Remove(remove_mod);
            }

            try
            {
                ret = AddModifier(mod);
            }
            catch (Cast.BadStateException excp)
            {
                // see if we need to put back modifier
                if (remove_mod != null)
                    _modifiers.Add(remove_mod);
                throw;
            }
            if (!ret) // see that we added
            {
                // see if we need to put back modifier
                if (remove_mod != null)
                    _modifiers.Add(remove_mod);
            }

            return ret;
        }

        /// <summary>
        /// Display name of spell
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _spell.Name;
        }

        public enum BadState { MaxLevel, MaxValue, Invalid };
        public class BadStateException : Exception
        {
            public BadState State { get; set; }
            public BadStateException(BadState state, string msg) : base(msg) {
                State = state;
            }
        }
    }
}
