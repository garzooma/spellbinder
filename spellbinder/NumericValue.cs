﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <summary>
    /// Return numeric value
    /// </summary>
    public class NumericValue : IPropertyValue, IComparable
    {
        private decimal _value;
        public decimal Value { get { return _value; } }
        private decimal _incrementAmount = 1M;

        public string Units { get { return _units; } }
        private string _units = null;

        public NumericValue(decimal value)
            : this(value, null)
        {
        }

        public NumericValue(decimal value, string units)
            : this(value, units, 1M)
        {
        }

        public NumericValue(decimal value, string units, decimal incrementAmount)
        {
            _value = value;
            if (!String.IsNullOrEmpty(units))   // empty string leaves units as null
                _units = units;
            _incrementAmount = incrementAmount;
        }

        #region IPropertyValue Members
        public virtual string DisplayValue
        {
            get
            {
                string ret = _value.ToString();
                if (!String.IsNullOrEmpty(_units))
                {
                    if (_units == "feet" || _units == "foot")
                        ret += "'";
                    else
                        ret += " " + _units;
                }

                return ret;
            }
        }

        public virtual string TypeValue
        {
            get { throw new NotImplementedException(); }
        }

        public decimal CalculationValue
        {
            get { return _value; }
        }


        public IPropertyValue Increment(IPropertyValue incrAmount)
        {
            NumericValue i_val = incrAmount as NumericValue;
            if (i_val == null)
            {
                throw new ArgumentException("Non-NumericValue in NumericValue.Increment: " + incrAmount.ToString());
            }
            return new NumericValue(_value + i_val._value, _units, _incrementAmount);
        }

        #endregion

        public override bool Equals(object obj)
        {
            bool ret = false;
            if (obj is NumericValue)
            {
                NumericValue num_val = obj as NumericValue;
                if (num_val.Value == _value && num_val.Units == _units)
                    ret = true;
            }
            return ret;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", _value, _units);
        }

        public override int GetHashCode()
        {
            return (((int)this._value * 251) + ((_units != null) ? _units.GetHashCode() : 0));
        }

        #region IComparable Members

        /// <summary>
        /// Compare values depending on type (i.e. duration or range)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            int ret = 1;
            if (obj is DescribedValue)
            {
                DescribedValue lasting = new DescribedValue("Lasting");
                if (lasting.Equals(obj as DescribedValue))  // Numeric value is less than Lasting
                    ret = -1;
            }
            else if (obj is NumericValue)
            {
                NumericValue in_val = obj as NumericValue;
                string unit_compare = _units;
                if (unit_compare.EndsWith("s"))
                    unit_compare = unit_compare.Substring(0, unit_compare.Length - 1);
                string[] units_array = _durationUnits;
                int index = Array.IndexOf(units_array, unit_compare);    // get index of units for this value
                if (index < 0)  // not a duration value
                {
                    units_array = _rangeUnits;  // see if it's a range value
                    index = Array.IndexOf(units_array, unit_compare);
                }
                unit_compare = in_val._units;
                if (unit_compare.EndsWith("s"))
                    unit_compare = unit_compare.Substring(0, unit_compare.Length - 1);
                int in_index = Array.IndexOf(units_array, unit_compare);    // get index of units for input value
                ret = index.CompareTo(in_index);
                if (ret == 0)   // units match
                {
                    ret = _value.CompareTo(in_val._value);
                }
                else if (ret < 0) // input units are bigger
                {
                    // Check for minutes/hour fix
                    if (_units.StartsWith("minute") && in_val._units == "hour")
                    {
                        decimal in_minutes = 60;
                        ret = _value.CompareTo(in_minutes);
                    }
                    else if (_units.StartsWith("hour") && in_val._units.StartsWith("day"))
                    {
                        decimal in_hours = 24 * in_val.Value;
                        ret = _value.CompareTo(in_hours);
                    }
                    else if (_units.StartsWith("feet") && in_val._units.StartsWith("mile"))
                    {
                        decimal in_feet = 5280 * in_val.Value;
                        ret = _value.CompareTo(in_feet);
                    }
                }
                else // (ret > 0 => input units are smaller
                {
                    // Check for minutes/hour fix
                    if (in_val._units.StartsWith("minute") && _units == "hour")
                    {
                        decimal minutes = 60 * _value;
                        ret = minutes.CompareTo(in_val._value);
                    }
                    else if (in_val._units.StartsWith("hour") && _units.StartsWith("day"))
                    {
                        decimal hours = 24 * _value;
                        ret = hours.CompareTo(in_val._value);
                    }
                    else if (in_val._units.StartsWith("feet") && _units.StartsWith("mile"))
                    {
                        decimal feet = 5280 * _value;
                        ret = feet.CompareTo(in_val._value);
                    }
                }
            }

            return ret;
        }

        #endregion

        private string[] _durationUnits = new string[]{
            "second",
            "round",
            "minute",
            "hour",
            "day",
            "week"
        };

        private string[] _rangeUnits = new string[]{
            "feet",
            "mile"
        };
    }
}
