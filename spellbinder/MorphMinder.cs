﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <summary>
    /// Controls the setting of spell base for Morphic spells.
    /// </summary>
    public class MorphMinder
    {
        SpellInfo _spell;  // base spell from book

        public MorphMinder(SpellInfo spellInfo)
        {
            _spell = spellInfo;
        }

        /// <summary>
        /// Get Morphic base spell for given name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public MorphicBaseSpell GetMorphicBase(string name)
        {
            MorphicBaseSpell ret = null;

            SpellInfo spell_info = _spell.MorphList.FirstOrDefault(s => s.Name == name);
            ret = new MorphicBaseSpell(spell_info);

            return ret;
        }

        /// <summary>
        /// Get list of names for base spells
        /// </summary>
        public IList<string> MorphicBaseNamesList
        {
            get
            {
                List<string> ret = new List<string>();
                foreach (SpellInfo morph_info in _spell.MorphList)
                {
                    ret.Add(morph_info.Name);
                }

                return ret;
            }
        }

        /// <summary>
        /// Base of spell as in book
        /// </summary>
        public ISpell Base
        {
            get
            {
                return new BaseSpell(_spell);
            }
        }

        /// <summary>
        /// Morphic base spell.
        /// </summary>
        /// <remarks>Implements subset of ISpell methods.  Identifies properties it provides.</remarks>
        public class MorphicBaseSpell : BaseSpell
        {
            private List<string> _providedProperties = new List<string>();

            public MorphicBaseSpell(SpellInfo spell_info) : base(spell_info)
            {
                // Set properties
                if (spell_info.RangeValue != null)
                {
                    _providedProperties.Add(SpellProperty.StdPropertyNames.Range);
                }
            }


            public bool ProvidesProperty(string propertyName)
            {
                bool ret = _providedProperties.Contains(propertyName);

                return ret;
            }
        }
    }
}
