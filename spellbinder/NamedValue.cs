﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>ArzooSoftware.SpellBinder.spellbinder.NamedValue</name>
    /// <summary>
    /// Value with a descriptive name as well as a numeric value
    /// </summary>
    /// <version>1.0 - Initial Release - 2/3/2013 11:34:55 AM</version>
    /// <author>Greg Arzoomanian</author>    
    class NamedValue : IPropertyValue
    {
        private DescribedValue _value;
        private DescribedValue[] _schedule; // list of possible values
        public NamedValue(string value, int numValue, DescribedValue[] schedule)
        {
            _value = new DescribedValue(value, numValue);
            _schedule = schedule;
        }

        public NamedValue(string value, DescribedValue[] schedule) : this(value, 0, schedule)
        {
        }

        public DescribedValue[] Schedule { get { return _schedule; } }

        /// <summary>
        /// Build schedule from list of Property values
        /// </summary>
        /// <param name="valList"></param>
        /// <returns></returns>
        public static DescribedValue[] CreateSchedule(IList<DescribedValue> valList)
        {
            DescribedValue[] ret = valList.ToArray();

            return ret;
        }

        #region IPropertyValue Members

        public string DisplayValue
        {
            get { return _value.DisplayValue; }
        }

        public string TypeValue
        {
            get { throw new NotImplementedException(); }
        }

        public decimal CalculationValue
        {
            get { return _value.CalculationValue; }
        }

        public IPropertyValue Increment(IPropertyValue incrAmount)
        {
            throw new NotImplementedException();
        }

        #endregion

        public override bool Equals(object obj)
        {
            bool ret = false;
            if (obj is NamedValue)
            {
                NamedValue in_val = obj as NamedValue;
                if (in_val._value.Equals(_value))
                    ret = true;
            }
            return ret;
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            int ret = -1;
            if (obj is NamedValue)
            {
                NamedValue named_val = obj as NamedValue;
                ret = _value.CompareTo(named_val._value);
            }
            else if (obj is DescribedValue)
            {
                DescribedValue descr_val = obj as DescribedValue;
                ret = _value.CompareTo(descr_val);
            }

            return ret;
        }

        #endregion
    }
}
