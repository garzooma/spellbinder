﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.DescribedValue</name>
    /// <summary>
    /// Value described by a single word or phrase
    /// </summary>
    /// <version>1.0 - Initial Release - 1/16/2013 11:53:26 AM</version>
    /// <author>Greg Arzoomanian</author>    
    public class DescribedValue : IPropertyValue
    {
        private string _descriptiveValue;   // desciptive value
        private int _numValue;              // associated numeric value, if any
        public DescribedValue(string value) : this(value, 0)
        {
        }

        public DescribedValue(string value, int numValue)
        {
            _descriptiveValue = value;
            _numValue = numValue;
        }

        #region IPropertyValue Members

        public string DisplayValue
        {
            get { return _descriptiveValue; }
        }

        public string TypeValue
        {
            get { return _descriptiveValue; }
        }

        public decimal CalculationValue
        {
            get { return _numValue; }
        }

        /// <summary>
        /// Replace value with new value
        /// </summary>
        /// <param name="incrAmount"></param>
        /// <returns></returns>
        public IPropertyValue Increment(IPropertyValue incrAmount)
        {
            return incrAmount;
        }

        #endregion

        public override bool Equals(object obj)
        {
            bool ret = false;
            if (obj is DescribedValue)
            {
                DescribedValue in_val = obj as DescribedValue;
                if (in_val.TypeValue == _descriptiveValue)
                    ret = true;
            }
            return ret;
        }

        public override int GetHashCode()
        {
            return _descriptiveValue.GetHashCode();
        }

        public override string ToString()
        {
            return _descriptiveValue;
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            int ret = -1;   // Described values go below numeric value
            if (_descriptiveValue == "Lasting")
                ret = 1;    // Lasting is largest value
            if (obj is DescribedValue)  // input is also described value
            {
                DescribedValue in_val = obj as DescribedValue;
                if (_descriptiveValue == in_val._descriptiveValue)
                    ret = 0;
                else if (_descriptiveValue == "Lasting" && in_val._descriptiveValue != "Lasting")
                    ret = 1;
                else if (_descriptiveValue != "Lasting" && in_val._descriptiveValue == "Lasting")
                    ret = -1;
                else if (_descriptiveValue != "Lasting" && in_val._descriptiveValue != "Lasting")
                    ret = 0;
            }

            return ret;
        }

        #endregion

        public static DescribedValue Lasting { get { return new DescribedValue("Lasting"); } }
    }
}
