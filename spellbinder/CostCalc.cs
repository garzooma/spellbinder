using System;
using System.Collections.Generic;
using System.Linq;

namespace ArzooSoftware.SpellBinder.spellbinder
{
	/// <summary>
	/// Calculates cost of spell based on spell level and mage level
	/// </summary>
	public class CostCalc
	{
        /// <summary>
        /// Spell level
        /// </summary>
		int _level;
		bool _half;
		public CostCalc(int level, bool half)
		{
			_level = level;
			_half = half;
		}

        /// <summary>
        /// Use .5 for halves
        /// </summary>
        /// <param name="level"></param>
        public CostCalc(decimal level)
        {
            _level = (int)level;    // get int part
            if (_level < 1)     // check for morphic base level 0
                _level = 1;     // consider as level 1
            else // non-zero level -- check on half level
               _half = (level - _level) == 0 ? false : true;
        }

		/// <summary>
		/// Cost in spell points for mage
		/// </summary>
		/// <param name="mageLevel"></param>
		/// <returns></returns>
		public int GetCost(int mageLevel)
		{
			//string ret = "";

			// Get index for table
			int[] levelTbl = levelTables[mageLevel];
			int idx = (this._level-1) * 2 + (this._half ? 1 : 0);
			int cost = levelTbl[idx];

            return cost;
		}

        /// <summary>
        /// Maximum spell level for given mage level
        /// </summary>
        /// <param name="mageLevel"></param>
        /// <returns></returns>
        public static decimal GetMaxLevel(int mageLevel)
        {
            decimal ret = 0M;

            // Get index for table
            int[] levelTbl;
            if (mageLevel < 19)
            {
                levelTbl = levelTables[mageLevel];
            }
            else
            {
                levelTbl = getLevelTable(mageLevel);
            }
            int half_levels = levelTbl.Length + 1;  // number of values in column + 1
            ret = half_levels / 2M;

            return ret;
        }

        /// <summary>
        /// Generate level table for given level mage
        /// </summary>
        /// <param name="mageLevel"></param>
        /// <returns></returns>
        private static int[] getLevelTable(int mageLevel)
        {
            List<int> cost_list = new List<int>();

            int num_ones = mageLevel - 6;   // number of cost one spells
            cost_list.AddRange(Enumerable.Repeat(1, num_ones));
            cost_list.AddRange(level6);

            return cost_list.ToArray();
        }

		// Spell cost table
		private static int[] level0 = {8};
		private static int[] level1 = {6,8};
		private static int[] level2 = {5,6,8};
		private static int[] level3 = {4,5,6,8};
		private static int[] level4 = {3,4,5,6,8};
		private static int[] level5 = {2,3,4,5,6,8};
		private static int[] level6 = {2,2,3,4,5,6,8};
		private static int[] level7 = {1,2,2,3,4,5,6,8};
		private static int[] level8 = {1,1,2,2,3,4,5,6,8};
        private static int[] level9 = { 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level10 = { 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level11 = { 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level12 = { 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level13 = { 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level14 = { 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level15 = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level16 = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level17 = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 8 };
        private static int[] level18 = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6 };

		private static int[][] levelTables = {
										  level0,
										  level1,
										  level2,
										  level3,
										  level4,
										  level5,
										  level6,
										  level7,
										  level8,
										  level9,
										  level10,
										  level11,
										  level12,
										  level13,
										  level14,
										  level15,
										  level16,
										  level17,
										  level18
		};
	}	// CostCalc
}
