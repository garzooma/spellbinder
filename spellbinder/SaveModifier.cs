﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArzooSoftware.SpellBinder.spellbinder
{
    /// <copyright>Arzoo Software, 2013</copyright>
    /// <name>spellbinder.SaveModifier</name>
    /// <summary>
    /// Modify a spell's saving throw
    /// </summary>
    /// <version>1.0 - Initial Release - 1/13/2013 3:36:39 PM</version>
    /// <author>Greg Arzoomanian</author>    
    public class SaveModifier : Modifier
    {
        #region IModifier Members

        public SaveModifier() : base("Saving Throw", 0.5M) { }

        /// <summary>
        /// Default amount modifier adds to DC
        /// </summary>
        decimal _modAmount = 1M;

        public override IPropertyValue Bump(IPropertyValue startValue)
        {
            IPropertyValue ret = null;

            decimal cur_num = startValue.CalculationValue;

            decimal new_num = cur_num + _modAmount;


            ret = new NumericValue(new_num, null);

            return ret;
        }

        public override IPropertyValue IncrementValue
        {
            get { return new NumericValue(this._modAmount, null); }
        }

        #endregion
    }
}
